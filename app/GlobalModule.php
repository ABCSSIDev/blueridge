<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class GlobalModule extends Model
{
    //
    protected $modules;
    protected $keyValuePair;
    protected $html;

    public function __construct(Collection $modules)
    {
        $this->modules = $modules;
        foreach ($modules as $module){
            $this->html .= $this->root_module($module,0);
            $this->keyValuePair[$module->key] = $module->value;
        }
    }


    public function has(string $key){ /* check key exists */ }
    public function contains(string $key){ /* check value exists */ }
    public function getModules(){
        return $this->html;
    }
    public function get(string $key){
        return $this->modules[$key];
        /* get by key */
    }

    public function root_module($module, $c = 0){
        $html = '';
        $counter = $c;

//        print groups

        $html .= '<a href="'.(substr($module->route,0,1) != '#' ? url('/') : '').$module->route.'" '.(substr($module->route,0,1) != '#' ? '' : 'data-toggle=collapse').'  aria-expanded="false" class="sidebar-background sidebar-list-group list-group-item-action flex-column align-items-start sidebar-highlight">';
        $html .= '<div class="d-flex w-100 justify-content-start align-items-center text-sidebar">';
        $html .= '<span class="'.$module->icon.' mr-3" style="display: inline;padding-left: 8px;"></span>';
        $html .= ' <span class="menu-collapsed">'.$module->name.'</span>';
        $html .= (substr($module->route,0,1) != '#' ? '' : '<span class="submenu-icon ml-auto"></span>');
        $html .= '</div>';
        $html .= '</a>';


//        get child
        if (count($module->getSubModules) > 0){
            $html .= $this->sub_modules($module->getSubModules,str_replace('#','',$module->route));
        }
        return $html;
    }

    public function sub_modules($sub_modules,$id){
        $html = '';
        $html .= '<div id="'.$id.'" class="collapse sidebar-submenu">';
        foreach ($sub_modules as $sub_module){
            if (count($sub_module->getSubModules) > 0){
                $html .= $this->root_module($sub_module,0);
            }else{
                $html .= '<a href="'.(substr($sub_module->route,0,1) != '#' ? url('/') : '').$sub_module->route.'" class="sidebar-list-group list-group-item-action sidebar-background text-white">';
                $html .= '<span class="menu-collapsed dropdown-sidebar pl-4">'.$sub_module->name.'</span>';
                $html .= '</a>';
            }
        }
        $html .= '</div>';
        return $html;
    }
}
