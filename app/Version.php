<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Version extends Model
{
    protected $table = 'versions';
    protected $primaryKey = 'id';
    protected $fillable = ['version', 'version_title', 'release_notes'];

    public $timestamps = false;
    use SoftDeletes;
}
