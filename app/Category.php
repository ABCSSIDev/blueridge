<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $fillable = ['category_id', 'category_name', 'is_stored'];
    protected $dates = ['deleted_at'];

    public function getCategory(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function getCategorys(){
        return $this->belongsTo('App\Category','category_id');
    }

}
