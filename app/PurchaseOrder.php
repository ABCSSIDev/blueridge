<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [
        'pr_id', 'po_no', 'canvass_id', 'po_date', 'procurement_mode', 'delivery_term', 'place_of_delivery', 'payment_term', 'delivery_date'
    ];
    protected $dates = ['deleted_at'];
    
    public function getPurchaseRequest(){
        return $this->belongsTo('App\PurchaseRequest','pr_id');
    }

    public function getCanvass(){
        return $this->belongsTo('App\Canvass','canvass_id');
    }
    
    public function getCanvassItem(){
        return $this->belongsTo('App\CanvassItem','canvass_id');
    }  
}
