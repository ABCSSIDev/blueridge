<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CanvassItem extends Model
{

    //
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [
        'canvass_id', 'pr_item_id', 'available_quantity', 'unit_price'
    ];
    protected $dates = ['deleted_at'];

    public function getPRItems(){
        return $this->belongsTo('App\PurchaseRequestItem','pr_item_id');
    }

    public function getCanvasses(){
        return $this->belongsTo('App\Canvass','canvass_id');
    }
}
