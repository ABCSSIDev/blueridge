<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    //
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [
        'unit_id', 'category_id', 'inventory_name_id', 'quantity', 'unit_price', 'image','created_at'
    ];
    protected $dates = ['deleted_at'];

    public function getPRequestItems(){
        return $this->hasMany('App\PurchaseRequestItem','id');
    }

    public function getUnits(){
        return $this->belongsTo('App\Unit','unit_id');
    }

    public function getCategory(){
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function getAsset(){
        return $this->hasOne('App\Asset', 'inventory_id');
    }
    public function getInventoryName(){
        return $this->belongsTo('App\InventoryName', 'inventory_name_id');
    }
}
