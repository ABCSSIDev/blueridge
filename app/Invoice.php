<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'invoices';
    protected $fillable = ['payee_id','ledger_id', 'collection_nature', 'or_number', 'invoice_date', 'created_at', 'updated_at', 'deleted_at','due_date'];
    public $timestamps = false;

    use SoftDeletes;

    public function getPayee(){
        return $this->belongsTo('App\GeneralUser','payee_id');
    }

    public function getLedger(){
        return $this->belongsTo('App\AccountLedger','ledger_id');
    }

    public function getInvoice(){
        return $this->hasOne('App\InvoiceItem','id');
    }

    public function getPayment(){
        return $this->morphOne('App\Payment','payable');
    }
}
