<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountLedger extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $fillable = ['group_id', 'ledger_id', 'account_code', 'account_type', 'ledger_name', 'description', 'reference_group', 'reference_ledger', 'budget_percentage', 'attachments'];
    protected $dates = ['deleted_at'];

    public function getLedgers(){
        return $this->hasMany('App\AccountLedger','ledger_id');
    }

    public function getLedger(){
        return $this->belongsTo('App\AccountLedger','ledger_id');
    }

    public function getGroup(){
        return $this->belongsTo('App\AccountGroup','group_id');
    }
}
