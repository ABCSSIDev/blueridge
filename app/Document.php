<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    //
    use SoftDeletes;

    public function getSignatoryTemplates(){
        return $this->hasOne('App\SignatoryTemplate','document_id');
    }
}
