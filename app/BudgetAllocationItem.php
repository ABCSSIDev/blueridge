<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetAllocationItem extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'budget_allocation_items';
    protected $fillable = ['budget_id', 'ledger_id', 'amount','deleted_at','created_at', 'updated_at'];
    public $timestamps = false;
    use SoftDeletes;

    public function getLedger(){
        return $this->belongsTo('App\AccountLedger','ledger_id');
    }
}
