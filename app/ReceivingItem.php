<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivingItem extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'receiving_items';
    protected $fillable = ['receiving_id', 'canvass_item_id', 'quantity_received','remarks','inventory_type', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;

    public function getCanvassItem(){
        return $this->belongsTo('App\CanvassItem','canvass_item_id');
    }
     public function getReceiving(){
        return $this->belongsTo('App\Receiving','receiving_id');
    }
}
