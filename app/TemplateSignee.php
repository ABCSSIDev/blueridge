<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateSignee extends Model
{
    //
    use SoftDeletes;

    public function getSignatory(){
        return $this->belongsTo('App\Signatory','signatory_id');
    }
}
