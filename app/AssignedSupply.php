<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedSupply extends Model
{
    protected $table = 'assigned_supplies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'assigned_user_id','created_at','updated_at'
    ];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    use SoftDeletes;
    public function GeneralUserDetails(){
        return $this->belongsTo('App\GeneralUser','assigned_user_id');

    }
}
