<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryName extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [
        'description',
        'category_id',
        'created_at'
    ];
    protected $dates = ['deleted_at'];

    public function Category(){
        return $this->belongsTo('App\Category','category_id');
    }
    public function getAsset(){
        return $this->hasOne('App\Asset', 'id');
    }
}
