<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralUser extends Model
{
    protected $table = 'general_users';
    protected $primaryKey = 'id';
    protected $fillable = [
        'role_id','name','contact_number','address',
        'created_at','updated_at','first_name',
        'last_name','middle_initial','birth_date',
        'birth_place','citizenship','sex','civil_status','tin','quantifier',
        'cluster','household','religion','health_status','highest_education','occupation'
    ];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    use SoftDeletes;
    public function Role(){
        return $this->belongsTo('App\Role','role_id');
    }
}
