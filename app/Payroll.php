<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{

    

    protected $table = 'payrolls';
    protected $primaryKey = 'id';
    protected $fillable = [
        'barangay', 'treasurer', 'city_municipality', 'province', 'payroll_number', 'ledger_id', 'created_at', 'updated_at', 'created_by', 'date_from', 'date_to', 'due_date',
    ];
    use SoftDeletes;
    public function getPayrollItems(){
        return $this->hasMany('App\PayrollItem','payroll_id');
    }

    public function getPayment(){
        return $this->morphOne('App\Payment','payable');
    }
    
    public function getLedger(){
        return $this->belongsTo('App\AccountLedger','ledger_id');
    }

}
