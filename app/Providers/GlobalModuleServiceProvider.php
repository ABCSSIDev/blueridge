<?php

namespace App\Providers;

use App\GlobalModule;
use App\Module;
use function foo\func;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

class GlobalModuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        if(Schema::hasTable('modules')){
            $this->app->singleton('App\GlobalModule',function ($app){
                return new GlobalModule(Module::where('module_id',null)->get());
            });
        }else{
            return true;
        }
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(GlobalModule $globalModule)
    {
        View::share('globalModules',$globalModule);
    }
}
