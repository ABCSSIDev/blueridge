<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $primaryKey = 'id';
    protected $fillable = ['ledger_id', 'type', 'description', 'amount','payment_id', 'date_received','created_at', 'updated_at', 'deleted_at'];

    public $timestamps = false;
    use SoftDeletes;
}
