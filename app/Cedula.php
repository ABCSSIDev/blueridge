<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cedula extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'cedulas';
    protected $fillable = [
        'ledger_id', 'issued_date','interest','ctc','cedula_id','created_by','item_1','item_2','item_3','item_4','item_5','item_6','item_7','item_8','item_9','item_10','created_at','updated_at','due_date'];
    public $timestamps = false;

    public function getPayment(){
        return $this->morphOne('App\Payment','payable');
    }

    public function getLedger(){
        return $this->belongsTo('App\AccountLedger','ledger_id');
    }
    
    public function userData(){
        return $this->belongsTo('App\GeneralUser','cedula_id');
    }
}
