<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseRequest extends Model
{

    protected $table = 'purchase_requests';
    public $timestamps = false;
    protected $fillable = ['roa_id', 'pr_no','to', 'from', 'delivery_to', 'purpose', 'request_date', 'delivery_period', 'created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';
    use SoftDeletes;

    public function RoaDetails(){
        return $this->belongsTo('App\ROA','roa_id');
    }
    
    public function getCanvasses(){
        return $this->hasMany('App\Canvass','pr_id');
    }

    public function getPurchaseOrder(){
        return $this->hasMany('App\PurchaseOrder','pr_id');
    }

    public function getPurchaseRequestItems(){
        return $this->hasMany('App\PurchaseRequestItem','pr_id');
    }

}
