<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'banks';
    protected $fillable = [
        'name', 'address','contact_number','account_name','account_number','account_type','product_type','initial_deposit','created_by','created_at','updated_at'
    ];
    public $timestamps = false;

    public function getPayments(){
        return $this->hasMany('App\Payment','bank_id');
    }
}
