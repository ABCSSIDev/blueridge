<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Canvass extends Model
{

    //
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [
        'pr_id', 'supplier_id', 'awarded', 'group_id'
    ];
    protected $dates = ['deleted_at'];

    public function getSuppliers(){
        return $this->belongsTo('App\Supplier','supplier_id');
    }

    public function getPurchaseRequest(){
        return $this->belongsTo('App\PurchaseRequest','pr_id');
    }

    public function getPurchaseOrder(){
        return $this->hasMany('App\PurchaseOrder','id');
    }

    public function getCanvasses(){
        return $this->hasMany('App\CanvassItem','canvass_id');
    }
}
