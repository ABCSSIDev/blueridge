<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Signatory extends Model
{
    //
    use SoftDeletes;

    public static function getSignatoryID(){
        $statement = DB::select("show table status like 'signatories'");
        return $statement[0]->Auto_increment;
    }
}
