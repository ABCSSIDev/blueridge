<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    protected $table = 'suppliers';
    protected $primaryKey = 'id';
    protected $fillable = ['supplier_name', 'address', 'contact_person', 'contact_number', 'tin', 'created_at', 'updated_at', 'deleted_at'];

    public $timestamps = false;
    use SoftDeletes;

    public function getCanvass(){
        return $this->hasMany('App\Canvass','id');
    }
}
