<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ORPad extends Model
{
    
    protected $table = 'or_pad';
    protected $primaryKey = 'id';
    protected $fillable = [
        'or_pad_start', 'or_pad_quantity','created_at'
    ];
    public $timestamps = false;

    use SoftDeletes;

}
