<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ROA extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'roas';
    protected $fillable = ['cash_advance_voucher_no', 'cash_advance_date', 'cash_advance_amount', 'amount_refund_or_no', 'amount_refund_date','ledger_id', 'obligation_number', 'resolution_date', 'resolution_number', 'bids_resolution_number', 'bids_mode', 'approval_date', 'signatory', 'total_amount', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;

    public function RoaDetails(){
        return $this->belongsTo('App\PurchaseRequest','id');
    }

    public function getLedger(){
        return $this->belongsTo('App\AccountLedger', 'ledger_id');
    }

    public function getDisbursementVoucher(){
        return $this->belongsTo('App\DisbursementVoucher', 'id');
    }
    
    public function getRoa(){
        return $this->hasOne('App\Roa','id');
    }
}
