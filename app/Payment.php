<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    //
    protected $table = 'payments';
    protected $primaryKey = 'id';
    protected $fillable = ['bank_id', 'payment_id', 'paid_into', 'payable_type', 'payable_id', 'date_received', 'is_cancelled', 'amount_received', 'created_at', 'updated_at', 'deleted_at'];
    public function getBank(){
        return $this->belongsTo('App\Bank','bank_id');
    }

    public function getPaymentMethod(){
        return $this->belongsTo('App\PaymentMethod','payment_id');
    }

    public function payable(){
        return $this->morphTo();
    }
    public $timestamps = false;
    use SoftDeletes;
}
