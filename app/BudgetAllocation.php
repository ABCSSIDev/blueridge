<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetAllocation extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'budget_allocations';
    protected $fillable = ['year', 'deleted_at','created_at', 'updated_at'];
    use SoftDeletes;

    public function getBudgetAllocationItems(){
        return $this->hasMany('App\BudgetAllocationItem','budget_id');
    }
}
