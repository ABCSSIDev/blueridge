<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseRequestItem extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [
        'pr_id', 'unit_id', 'inventory_name_id', 'quantity', 'unit_cost'
    ];
    protected $dates = ['deleted_at'];

    public function getCanvassItems(){
        return $this->hasMany('App\CanvassItem','id');
    }

    public function getUnits(){
        return $this->belongsTo('App\Unit','unit_id');
    }

    public function getPurchaseRequest(){
        return $this->belongsTo('App\PurchaseRequest','pr_id');
    }
    
    public function getInventoryName(){
        return $this->belongsTo('App\InventoryName','inventory_name_id');
    }

}
