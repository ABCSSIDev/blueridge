<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountGroup extends Model
{
    //
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $fillable = ['group_id', 'group_name', 'description'];
    protected $dates = ['deleted_at'];

    public function getGroups(){
        return $this->hasMany('App\AccountGroup','group_id');
    }

    public function getGroup(){
        return $this->belongsTo('App\AccountGroup','group_id');
    }

    public function getLedgers(){
        return $this->hasMany('App\AccountLedger','group_id');
    }


}
