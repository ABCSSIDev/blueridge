<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceItem extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table = 'invoice_items';
    protected $fillable = ['invoice_id','description', 'quantity', 'unit_price', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;
    public function getInvoicess(){
        return $this->belongsTo('App\Invoice','invoice_id');
    }
}
