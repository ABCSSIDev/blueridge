<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DVPad extends Model
{
    
    protected $table = 'dv_pad';
    protected $primaryKey = 'id';
    protected $fillable = [
        'dv_pad_start', 'dv_pad_quantity','created_at'
    ];
    public $timestamps = false;

    use SoftDeletes;

}