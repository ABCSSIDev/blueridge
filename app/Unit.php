<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['unit_name', 'unit_symbol', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    use SoftDeletes;
    public function Unit(){
        return $this->hasMany('App\Inventory','id');
    }

    public function getPRequestItem(){
        return $this->hasMany('App\PurchaseRequestItem','id');
    }
}
