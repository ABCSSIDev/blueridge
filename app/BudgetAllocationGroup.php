<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetAllocationGroup extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'budget_allocation_groups';
    protected $fillable = ['budget_id', 'group_id', 'description','deleted_at','created_at', 'updated_at'];
    public $timestamps = false;
    use SoftDeletes;

    public function getGroup(){
        return $this->hasOne('App\AccountGroup','id');
    }


}
