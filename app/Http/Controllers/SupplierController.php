<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use Yajra\DataTables\DataTables;
use App\Common;
use App\BudgetAllocation;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Setup.Supplier.index',[
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Setup.Supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $supplier_name = $request->get('supplier_name');
            $result = Supplier::where('supplier_name', $supplier_name)->first();

            if(!$result){
                Supplier::create($request->all());
                
                $response = array(
                    'status' => 'success',
                    'desc' => 'Supplier successfully created!'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Supplier name already existing!'
                );
            }
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier_list = Supplier::where('deleted_at',null)->where('id',$id)->first();
        return view('Setup.Supplier.view',['supplier_list' => $supplier_list]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier_list = Supplier::where('deleted_at',null)->where('id',$id)->first();

        return view('Setup.Supplier.edit',['supplier_list' => $supplier_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $supplier_name = $request->get('supp_name');
            $result = Supplier::where('supplier_name', $supplier_name)->where('id','!=',$id)->first();
            if(!$result){
                $Supplier_details = Supplier::where('id', $id)->update([
                    'supplier_name' => $request->input('supp_name'),
                    'address' => $request->input('supp_address'),
                    'contact_person' => $request->input('contact_person'),
                    'contact_number' => $request->input('contact_number'),
                    'tin' => $request->input('tin'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                $response = array(
                    'status' => 'success',
                    'desc' => 'Supplier Successfully Edited!'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Supplier name already existing!'
                );

            }
           
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function RemoveSuppliers($id)
    {
        $supplier_list = Supplier::where('deleted_at',null)->where('id',$id)->first();
        return view('Setup.Supplier.delete',['supplier_list' => $supplier_list]);
    }
    public function FunctionRemoveSupp($id)
    {
        $supplier = Supplier::where('id', $id)->first();
        Supplier::where('id', $id)->delete();
        return redirect(route('supplier.index').'?success=true&url=setup/supplier&module='.$supplier->supplier_name.'');
        
    }
    public function getSuppliers($year){
        if($year == '0000'){
            $supplier_list = Supplier::whereNull('deleted_at')->get();
        }else{
            $supplier_list = Supplier::whereNull('deleted_at')->whereYear('created_at',$year)->get();
        }
        
        return DataTables::of($supplier_list)->addColumn('name',function($model){
            return $model->supplier_name;
        })->addColumn('contact_person',function($model){
            return (empty($model->contact_person)?'N/A':$model->contact_person);
        })->addColumn('contact_number',function($model){
            return (empty($model->contact_number)?'N/A':$model->contact_number);
        })->addColumn('tin',function($model){
            return (empty($model->tin)?'N/A':$model->tin);
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown" style="text-align:center">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("supplier.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                        <a class="dropdown-item" href="'.route("supplier.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item popup_form" data-toggle="modal" data-url="'.route("supplier.removeSuppliers",$model->id).'" title="Delete Supplier Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->make(true);
    }
   
}

