<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Inventory;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Category;
use Intervention\Image\Facades\Image;
use App\Unit;
use App\InventoryName;
use App\Common;
use Illuminate\Support\Facades\Storage;
use App\AssignedSupplyItem;
use App\BudgetAllocation;
use App\CanvassItem;
use App\ReceivingItem;
use App\PurchaseRequestItem;

class ConsumablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $budget_allocation = BudgetAllocation::all();
        return view('Inventory.Consumables.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Inventory.Consumables.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consumable  $consumable
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $asset = Asset::all();
        $assetlist = array();
        $stock = 0;
        foreach($asset as $val){
            $assetlist[] = $val->inventory_id;
        }
        $inventory_consume = Inventory::whereNotIn('id', $assetlist)->orderBy('id','ASC')->get();
        //dd($inventory_consume);
        foreach($inventory_consume as $opex){
            $stock++;
            if($opex->id == $id){
                break;
            }
            
        }
        $inventory = Inventory::where('id',$id)->first();
        return view('Inventory.Consumables.show',[
            'inventory' => $inventory,
            'stock_number' =>date('Y-m',strtotime($inventory->created_at)).'-'.sprintf("%03d",$stock)
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consumable  $consumable
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $inventory = Inventory::where('id', $id)->first();
        $category = Category::all();
        $units = Unit::all();
        return view('Inventory.Consumables.edit',[
            'inventory' => $inventory,
            'categories' => $category,
            'units' => $units
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consumable  $consumable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id) {
        try{
            if($request->has('image')) {
                $base64_str = substr($request->image, strpos($request->image, ",")+1);
                $image = base64_decode($base64_str);
                $imageName = "opex-".time().$id.".png";
                $path = storage_path('/app/public/'). $imageName;
                Image::make(file_get_contents($request->image))->save($path);
            }
            else{
                $imageName = $request->input('old_image');
            }
            $edit_inventory = Inventory::find($id);
            $edit_inventory->unit_id = $request->input('unit');
            $edit_inventory->category_id = $request->input('category');
            $edit_inventory->quantity = $request->input('qty');
            $edit_inventory->unit_price = Common::cleanDecimal($request->input('unit_price'));
            $edit_inventory->minimum_qty = $request->input('min_qty');
            $edit_inventory->image = $imageName;
            $edit_inventory->remarks = $request->input('remarks');
            $edit_inventory->save();

            $edit = InventoryName::where('id', $edit_inventory->inventory_name_id)->update([
                'description' => $request->input('description')
            ]);

            $response = array(
                'status' => 'success',
                'desc'  => 'OPEX Successfully Updated!'
            );
        }
        
        catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consumable  $consumable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $inventory = Inventory::where('id', $id)->first();   
        Inventory::where('id', $id)->delete(); // delete account
        return redirect(route('consumable.index').'?success=true&url=inventory/consumable&module='.$inventory->getInventoryName->description.'');
    }

    public function archived($id) {
        $inventory = Inventory::where('deleted_at',null)->where('id',$id)->first();
        return view('Inventory.Consumables.delete',[
            'inventory' => $inventory
        ]);
    }

    public function getConsumables($year) {
        $asset = Asset::all();
        $assetlist = array();
        foreach($asset as $val){
            $assetlist[] = $val->inventory_id;
        }
        // $inventory = Inventory::whereNotIn('id', $assetlist)->get();
        if($year == '0000'){
            $inventory = Inventory::whereNotIn('id', $assetlist)->get();
        }else{
            $inventory = Inventory::whereNotIn('id', $assetlist)->whereYear('created_at',$year)->get();
        }
        return DataTables::of($inventory)->addColumn('image',function($model){
            $image = ($model->image == null ? asset('images/default.png') : route('consumables.image',$model->id));
            return "<center><div><img src='".$image."' alt='Image'/ height='45' width='45' class='rounded-circle'></div></center>";
        })->addColumn('description',function($model){
            return $model->getInventoryName->description;
        })->addColumn('category_id',function($model){
            return $model->getCategory->category_name;
        })->addColumn('quantity',function($model){
            $AssignedSupplyItem = AssignedSupplyItem::where('inventory_id', $model->id)->get();
            $total = 0;
            foreach($AssignedSupplyItem as $data){
                $total += $data->quantity;
            }
            $Newqty = $model->quantity - $total;
            return number_format($Newqty);
        })->addColumn('action',function($model){
            return "<center>
            <div class='dropdown'>
                <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    <i class='fa fa-bars'></i>
                </button>
                <div class='dropdown-menu dropdown-menu-center' aria-labelledby='dropdownMenuButton'>
                    <a class='dropdown-item' title='View OPEX' href='".route('consumable.show',$model->id)."'><i class='fa fa-eye' aria-hidden='true'></i> View Record</a>
                    <a class='dropdown-item' title='Edit OPEX' href='".route('consumable.edit',$model->id)."'><i class='fa fa-pencil' aria-hidden='true'></i> Edit Record</a>
                    
                </div>
            </div>
        </center>";
        })->rawColumns(['image','action'])->make(true);
    }

    public function getImage($id) {
        try{
            $inventory = Inventory::where('id', $id)->first();
            $file = Storage::disk('public')->get($inventory->image);
            return response($file,200);
        }catch (Exception $exception){
            $response = Common::catchError($exception);
        }
    }

    public function checkQty($inventory_id,$available_qty) {
        // $assigned_qty = AssignedSupplyItem::where('inventory_id', $inventory_id)->get()->sum('quantity');
        // return number_format($available_qty - $assigned_qty,2);
        $total = 0;
        $inventory = Inventory::where('id', $inventory_id)->first();
        $PurchaseRequestItem = PurchaseRequestItem::where('inventory_name_id', $inventory->getInventoryName->id)->first();
        // $CanvassItem = CanvassItem::where('pr_item_id', $PurchaseRequestItem->id)->first();
        // $ReceivingItem = ReceivingItem::where('canvass_item_id', $CanvassItem->id)->get();
        // if($ReceivingItem){
        //     foreach($ReceivingItem as $data){
        //         $total+=$data->quantity_received;
        //     }
        // }
        
        return $inventory->getInventoryName->id;
    }
}
