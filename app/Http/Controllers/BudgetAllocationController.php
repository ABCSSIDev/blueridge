<?php

namespace App\Http\Controllers;


use App\AccountGroup;
use App\AccountLedger;
use App\BudgetAllocation;
use App\BudgetAllocationGroup;
use App\BudgetAllocationItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Database\Eloquent\Builder;
use App\Common;

class BudgetAllocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    private $temp_budget = array();
    private $final_budget = array();    
    private $ledger_temp = array();
    private $ledger_final = array();

    private $edit_array = array();

    private $array_temp = array();
    private $array_group = array();

    private $parent = array();
    private $html = "";
    private $budget_ledgers = [];
    private $budget_groups = [];
    private $class_name = '';

    public function index()
    {
        return view('Accounting.BudgetAllocation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = AccountGroup::where('group_id',null)->get();
        return view('Accounting.BudgetAllocation.create', ['table'=>self::recursiveGroups($groups,'',true, 2)]);
    }
    
    public function getClasses($group_id, $class_name, $name_format = false){
        $class_name = $name_format?$group_id.',':' group-'.$group_id.' ';
        $group = AccountGroup::where('id',$group_id)->first();
        if($group->getGroup){
            $class_name .= self::getClasses($group->getGroup->id,$class_name, $name_format);
        }
        return $class_name;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $budget_year = $request->input('budget_year');
            $ledgers = $request->input('ledgers');
            $groups = $request->input('groups');
            $check_year = BudgetAllocation::where('year', $budget_year)->first();
            if(!$check_year){
                $budget_allocation = BudgetAllocation::create([
                    "year" => $budget_year
                ]);


                if($budget_allocation){
                    foreach ($groups as $group){
                        if(!empty($request->input("group_total-".$group))){
                            $budget_allocation_group = BudgetAllocationGroup::create([
                                "budget_id" => $budget_allocation->id,
                                "group_id" => $group,
                                "description" => $request->input("group-name-".$group)
                            ]);
                        }
                    }

                    foreach ($ledgers as $ledger){
                        $input_ledger = $request->input('ledgers-'.$ledger);
                        if(!empty($input_ledger) && $input_ledger !=="0.00"){
                            $budget_allocation_item = BudgetAllocationItem::create([
                                "budget_id" => $budget_allocation->id,
                                "ledger_id" => $ledger,
                                "amount" => str_replace(",","", $input_ledger)
                            ]);
                        }
                    }
                    $response = array(
                        'status' => 'success',
                        'desc' => 'Budget Successfully Created'
                    );
                }
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Budget for the Year '.$budget_year.' already exits.'
                );
            }


        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'An error occured please contact administrator',
                'exception' => $exception
            );
        }
        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $allocation = BudgetAllocation::findOrFail($id);
        $groups = AccountGroup::all();
        $credit = 0;
        $debit = 0;

        foreach($groups as $group){
            $this->array_group[$group->id]['amount'][] = 0;
            $this->array_group[$group->id]['group_name'] = $group->group_name;
        }

        foreach($allocation->getBudgetAllocationItems as $item){
            if($item->getLedger->account_type == 0){
                $debit += $item->amount;
            }else{
                $credit += $item->amount;
            }
            $this->array_temp[$item->getLedger->group_id][] = (float)$item->amount;
            $this->ledger_temp[$item->ledger_id][] = $item->amount;
        }

        self::getParentGroup();

        self::getSubLedgers();
        $groups = AccountGroup::where('group_id',null)->get();
        
        return view('Accounting.BudgetAllocation.show', [
            'table' => self::recursiveGroups($groups,'',true, null, $id,true),
            'credit' => $credit,
            'debit' => $debit,
            'allocation' => $allocation]);
    }

    public function getParentGroup(){
        $temp_array = array();
        foreach($this->array_temp as $key => $temp){
            $this->array_group[$key]['amount'][] = array_sum($temp);
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                
                $temp_array[$check_if_has_group->getGroup->id][] = array_sum($temp);
            }

        }
        $this->array_temp = $temp_array;
        if(count($this->array_temp) > 0){
            self::getParentGroup();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groups = AccountGroup::where('group_id',null)->get();
        $allocation = BudgetAllocation::findOrFail($id);
        // dd($id);
        $budget_data = self::loadData($allocation);

        self::getParentGroup(); 

        self::getSubLedgers();
        return view(
            'Accounting.BudgetAllocation.edit',
            [
                'table' => self::recursiveGroups($groups,'',true, null, $id),
                'budget_id'=>$id,
                'year' => $allocation->year,
                'debit' => $budget_data['debit'],
                'credit'=> $budget_data['credit'],
                'ending_balance' => $budget_data['ending_balance']
            ]);
    }

    public function loadData($allocation){

        $amounts = array();
        $amounts['debit'] = 0;
        $amounts['credit'] = 0;
        foreach($allocation->getBudgetAllocationItems as $key => $item){
            $this->edit_array[$item->ledger_id] = $item->amount;
            if($item->getLedger->account_type == 0){
                $amounts['debit'] += $item->amount;
            }else{
                $amounts['credit'] += $item->amount;
            }
            $this->array_temp[$item->getLedger->group_id][] = (float)$item->amount;

            // $this->temp_budget[$item->getLedger->group_id][] = $item->amount;
            $this->ledger_temp[$item->ledger_id][] = $item->amount;
        }

        $amounts['ending_balance'] = $amounts['debit'] - $amounts['credit'];
        return $amounts;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $ledgers = $request->input('ledgers');
            $groups = $request->input('groups');
//
            foreach ($groups as $group){
                $check_budget_group = BudgetAllocationGroup::where('budget_id', $id)->where('group_id', $group)->first();
                //if the budget group exists on table then ( update row or delete row if input field is determined empty ) else create a new row
                if(!empty($request->input("group_total-".$group))){
                    if(!$check_budget_group){
                        $budget_allocation_group = BudgetAllocationGroup::create([
                            "budget_id" => $id,
                            "group_id" => $group,
                            "description" => $request->input("group-name-".$group)
                        ]);
                    }

                }else{
                    if($check_budget_group) {
                        $budget_allocation_group = BudgetAllocationGroup::where('budget_id', $id)->where('group_id', $group)->forceDelete();
                    }
                }
            }

            foreach ($ledgers as $ledger){
                $input_ledger = $request->input('ledgers-'.$ledger);
                $budget_ledger = BudgetAllocationItem::where('budget_id', $id)->where('ledger_id', $ledger)->first();
                //if the budget ledger exists on table then ( delete row if input field is determined empty or update row if input field is not ) else create a new row
                if($budget_ledger){
                    // dd(1);
                    if(!empty($input_ledger) && $input_ledger !=="0.00"){
                    // dd($ledger);
                        $budget_allocation_item = BudgetAllocationItem::where('budget_id', $id)
                            ->where('ledger_id', $ledger)
                            ->update([ "amount" => str_replace(",","", $input_ledger) ]);
                    }else{
                        BudgetAllocationItem::where('budget_id', $id)
                            ->where('ledger_id', $ledger)
                            ->forceDelete();
                    }
                }else{
                    if(!empty($input_ledger) && $input_ledger !=="0.00"){
                        $budget_allocation_item = BudgetAllocationItem::create([
                            "budget_id" => $id,
                            "ledger_id" => $ledger,
                            "amount" => str_replace(",","", $input_ledger)
                        ]);
                    }
                }

            }

            $response = array(
                'status' => 'success',
                'desc' => 'Budget Successfully Updated'
            );
//


        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'An error occured please contact administrator',
                'exception' => $exception
            );
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $budget = BudgetAllocation::where('id', $id)
            ->first();
        BudgetAllocation::where('id', $id)
            ->delete();
        return redirect(route('budget-allocation.index').'?success=true&url=/accounting/budget-allocation&module='.$budget->year.'');
    }

    public function getBudgetAllocationList(){
        $budget = BudgetAllocation::whereNull('deleted_at')->get();
        return DataTables::of($budget)->addColumn('year',function($model){
            return $model->year;
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="'.route("budget-allocation.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                        <a class="dropdown-item"  href="'.route("budget-allocation.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                        <a class="dropdown-item popup_form" data-toggle="modal"  style="cursor:pointer" data-url="'.route("remove.budget-allocation",$model->id).'" title="Delete Budget Allocation Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->make(true);
    }
    public function removeBudgetAllocation($id){
        $budget = BudgetAllocation::where('id', $id)
            ->first();

        return view('Accounting.BudgetAllocation.delete', ['id' => $id, 'budget' => $budget]);
    }

    public function getSubGroups(){
        foreach($this->temp_budget as $key => $object){
            
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                $this->temp_budget[$check_if_has_group->getGroup->id][] = array_sum($object);
                // $check_if_has_group
            }
            $this->final_budget[$key][] = array_sum($object);
            unset($this->temp_budget[$key]);
        }
        if(count($this->temp_budget) > 0){
            self::getSubGroups();
        }
    }

    public function getSubLedgers(){
        foreach($this->ledger_temp as $key => $object){

            $check_if_has_ledger = AccountLedger::where('id',$key)->first();
            if($check_if_has_ledger->getLedger){
                $this->ledger_temp[$check_if_has_ledger->getLedger->id][] = array_sum($object);
            }
            $this->ledger_final[$key][] = array_sum($object);
            unset($this->ledger_temp[$key]);
        }
        if(count($this->ledger_temp) > 0){
            self::getSubLedgers();
        }
    }

    public  function recursiveGroups($accounts, $sub_mark = '', $parent = false, $task = null, $budget_id = null,$print = false){
        foreach($accounts as $row){
            $check_data = BudgetAllocationGroup::where('budget_id', $budget_id)->where('group_id', $row->id)->first();
            $group_budget_amount = 0;
            $string_compute = array();
            if(!empty($check_data)){
                $group_budget_amount = (array_key_exists($row->id,$this->array_group) ? number_format(array_sum($this->array_group[$row->id]['amount']),2) : 0);
                $string_compute['text'] = "Remove Total";
                $string_compute['remove'] = 'remove="true"';
            }else{
                $group_budget_amount = "";
                $string_compute['text'] = "Compute Total";
                $string_compute['remove'] = 'remove="false"';
            }
            if(count($this->edit_array) > 0 || !empty($task)){
                    $td = '<input type="hidden" name="group_total-'.$row->id.'" class="group_total-'.$row->id.'" '.$string_compute['remove'].' value="'.$group_budget_amount.'"><div class="group_total-'.$row->id.' sub_total" '.$string_compute['remove'].'><strong>₱&nbsp;'.$group_budget_amount.'</strong></div>';
                    $button = '<input type="hidden" name="groups[]" value="'.$row->id.'" ><input type="hidden" name="group-name-'.$row->id.'" value="Total of '.$row->group_name.'" ><button type="button" style="float: right;" class="btn btn-primary compute-total" data-id="'.$row->id.'" '.$string_compute["remove"].' >'.$string_compute['text'].'</button>';
            }else{
                $button = '';
                $td = '<strong>₱&nbsp;'.$group_budget_amount.'</strong>';
            }

            if($row->description != NULL){
                $this->html .= '<tr style="border-right-style: hidden;border-left-style: hidden;">
                                <td colspan="3" style="padding: 20px 20px 20px 0px;">'.$row->description.'</td>
                                </tr>';
            }
            
             
            if($parent){
                $this->html .= '<tr style="background-color:#0000000d">
                                <td style="vertical-align: middle"><strong>'.$sub_mark.'| '.$row->group_name.'</strong>
                                    '.$button.'
                                </td>
                                <td style="vertical-align: middle"></td>
                                <td class="right-align">
                                    '.($print ? '': $td).'
                                </td>
                            </tr>';
            }else{
                $this->html .= '<tr>
                                <td style="vertical-align: middle">'.$sub_mark.'| <b>'.$row->group_name.'</b>
                                '.$button.'
                                </td>
                                <td style="vertical-align: middle"></td>
                                <td class="right-align">
                                    '.$td.'
                                </td>
                            </tr>';
            }

            
            
            if (count($row->getLedgers) > 0){
                self::iterateLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp', null, false, $task);
            }
            
            if (count($row->getGroups) > 0){
                self::recursiveGroups($row->getGroups, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp',false, $task, $budget_id);
                
            }

            if(!empty($check_data)){
                $this->html .= '<tr style="background-color:#0000000d">
                                    <td style="vertical-align: middle"><strong>'.$sub_mark.$check_data->description.'</strong>
                                        '.$button.'
                                    </td>
                                    <td style="vertical-align: middle"></td>
                                    <td class="right-align">
                                        '.$td.'
                                    </td>
                                </tr>';
            }
        }

        return $this->html;
    }

    public function iterateLedgers($ledgers, $sub_mark = "", $child = false, $parent_group = null, $task = null){
        foreach ($ledgers as $row){
            if(empty($row->ledger_id) || $child){
                if(!empty($row->reference_group)){
                    $referrence_name = "group";
                }else if($row->reference_ledger){
                    $referrence_name = "ledger";
                }else{
                    $referrence_name = "";
                }

                if (count($row->getLedgers) > 0){
                    $parent = "readonly";
                    $parent_label_start = "<strong>";
                    $parent_label_end = "</strong";
                }else{
                    $parent = "";
                    $parent_label_start = "";
                    $parent_label_end = "";
                }

                $budget_readonly = (!empty($row->reference_group) || !empty($row->reference_ledger)?"readonly":"");
                if(array_key_exists($row->id,$this->edit_array) || !empty($task)){
                    $val = (empty($task))?$this->edit_array[$row->id]:0;
                    $class = self::getClasses($row->getGroup->id,'');
                    $data_arr = self::getClasses($row->getGroup->id,'', true);
                    $td  = '<input type="hidden" name="ledgers[]" value="'.$row->id.'">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">₱</span>
                                </div>
                                <input type="text" class="form-control ledger-'.$row->id.' ledger_group_'.$row->ledger_id.' sum_ledgers budget_group-'.$row->reference_group.' 
                                budget_ledger-'.$row->reference_ledger.' '.$class.'" data-ledger="'.$row->ledger_id.'" a-type="'.$row->account_type.'" data-arr="'.$data_arr.'" data-ledger="'.$row->id.'" data-budget="'.$row->budget_percentage.'" 
                                name="ledgers-'.$row->id.'" value="'.$val.'" '.$budget_readonly.' '.$parent.' autocomplete="off">
                            </div>';
                    // <input type="text" class="form-control ledger-'.$row->id.' ledger_group_'.$row->ledger_id.' sum_ledgers budget_group-'.$row->reference_group.' 
                    //             budget_ledger-'.$row->reference_ledger.' '.$class.'" data-ledger="'.$row->ledger_id.'" a-type="'.$row->account_type.'" data-arr="'.$data_arr.'" data-ledger="'.$row->id.'" data-budget="'.$row->budget_percentage.'" 
                    //             name="ledgers-'.$row->id.'" value="'.$val.'" '.$budget_readonly.' '.$parent.' autocomplete="off">';
                }else{
                    $td  = (array_key_exists($row->id,$this->ledger_final) ? number_format(array_sum($this->ledger_final[$row->id]),2) : 0);
                }
                $this->html .= '<tr>
                                    <td class="indent-two">'.$parent_label_start.' '.$sub_mark.'| '.$row->ledger_name.' '.$parent_label_end.'</td>
                                    <td align="center">'.$parent_label_start.''.$row->account_code.''.$parent_label_end.'</td>
                                    <td class="right-align ledger-'.$row->id.'">
                                        ₱&nbsp;'.$parent_label_start.''.$td.''.$parent_label_end.'
                                    </td>
                                </tr>';

                if (count($row->getLedgers) > 0){
                    self::iterateLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp', true, $parent_group, $task);
                }
            }

        }
    }

    public function recursiveAccounts($accounts, $sub_mark = '', $parent = false, $parent_group = null){
        foreach($accounts as $key => $row){
            array_push($this->budget_groups, $row->id);
            if($parent){
                $year = ($key == 0)?'<div class="col-md-4"><label><b>YEAR</b></label><input type="text" class="form-control" name="budget_year" id="yearPicker"></div>':'';
                $parent_group = $row->id;
                $this->html .= '<div class="mySlides">'.$year.'                               
                                <div class="col-md-12">                                 
                                    <table class="table  table-bordered table-hover table-sm" style="margin: 50px 0 0 0">
                                    <thead class="thead-dark">
                                        <tr align="center">
                                        <th width="50%">'.$sub_mark.'| '.$row->group_name.'</th>
                                        <th width="25%">Account Code</th>
                                        <th width="25%">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }else{
                $this->html .= '<tr class="tb-color">
                                    <td class="text-indent">| '.$sub_mark.'<b>'.$row->group_name.'</b></td>
                                    <td></td>
                                    <td></td>
                                </tr>';
            }


            if (count($row->getLedgers) > 0){
                self::recursiveLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp', false, $parent_group);
                $this->budget_groups = array();
                array_push($this->budget_groups, $parent_group);
            }

            if (count($row->getGroups) > 0){
                self::recursiveAccounts($row->getGroups, $sub_mark.'&nbsp&nbsp', false, $parent_group);
            }

            if (count($row->getLedgers) > 0 || count($row->getGroups) > 0) {
                if (!$parent) {
                    $this->html .= '<tr id="' . $row->id . '" hidden>
                                <td style="position: relative;display: flex;"><a class="btn btn-primary btn-color remove-total-' . $row->id . '" >Remove</a><input type="hidden" name="group-total[]" value="' . $row->id . '"><input type="text" id="group-name-' . $row->id . '" name="group-name-' . $row->id . '" class="form-control" style="margin-left: 10px;"></td>
                                <td></td>
                                <td><label id="total-' . $row->id . '"></label></td>
                            </tr>
                            <tr id="create-total-' . $row->id . '">
                                <td><a class="btn btn-primary btn-color create-total" data-id="' . $row->id . '" > Compute Total of ' . $row->group_name . '</a></td>
                                <td></td>
                                <td></td>
                            </tr>';
                }else{
                    $this->html .= '<tr id="' . $row->id . '" >
                                        <td style="position: relative;display: flex;"><input type="hidden" name="group-total[]" value="' . $row->id . '"><input type="text" data-id="'.$row->id.'" id="group-name-' . $row->id . '" name="group-name-' . $row->id . '" class="form-control" value="TOTAL OF '.$row->group_name.'" style="margin-left: 10px;" readonly></td>
                                        <td></td>
                                        <td><label id="total-' . $row->id . '">0</label></td>
                                    </tr>';
                }
            }

            if($parent){
                $button = (count($accounts)-1 == $key)?'<button type="submit" class="btn btn-primary btn-color btn-pad">Submit</button>':'<a class="btn btn-primary btn-color btn-pad" onclick="plusDivs(1)">Next</a>';
                $this->html .= '</tbody>
                                </table>
                                </div>
                                <div class="col-md-12">
                                    <div align="right" style="margin: 50px 0 0 0">
                                        <a class="btn btn-secondary btn-text" onclick="plusDivs(-1)">Previous</a>
                                        '.$button.'
                                    </div>
                                </div>
                            </div>';
            }
        }

    }
    public function recursiveLedgers($ledgers, $sub_mark = "", $child = false, $parent_group = null){

        foreach ($ledgers as $row){

            $class = self::getClasses($row->getGroup->id,'');

            if(empty($row->ledger_id) || $child){
                $id = !empty($parent_id)?$parent_id:$row->group_id;
                $this->html .= '<tr>
                                    <td class="indent-two">'.$sub_mark.'| '.$row->ledger_name.'</td>
                                    <td align="center">'.$row->account_code.'</td>
                                    <td><input type="hidden" autocomplete="false" value="'.$row->id.'" name="ledger[]"><input type="text" value="'.(array_key_exists($row->id,$this->edit_array) ? $this->edit_array[$row->id] : 0).'" class="'.$class.' form-control sum_ledgers price-'.$row->group_id.' parent-price-'.$parent_group.'" data-parent="'.$parent_group.'" data-group="'.$row->group_id.'" name="price-'.$row->id.'" autocomplete="off"></td>
                                </tr>';

                if (count($row->getLedgers) > 0){
                    self::recursiveLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp', true, $parent_group, $row->group_id);
                }
            }

        }
    }
}





