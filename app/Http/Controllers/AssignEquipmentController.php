<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\Asset;
use App\GeneralUser;
use App\InventoryName;
use App\AssignedSupplyItem;
use App\AssignedSupply;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\BudgetAllocation;

class AssignEquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $budget_allocation = BudgetAllocation::all();
        return view('Inventory.AssignEquipment.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Inventory.AssignEquipment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $inventories_list  = Inventory::all();        
        return view('Inventory.AssignEquipment.create',['InventoriesList' => $inventories_list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $GeneralUserData  = GeneralUser::where('name',$request->input('assignto'))->first(); 
            // dd($GeneralUserData->id);  
            if($GeneralUserData){
                $user_id = $GeneralUserData->id;
            }else{
                $user_id = $this->InsertGenwralUser($request);
            }     
            $insert = AssignedSupply::create([
                'assigned_user_id' => $user_id,
                'created_at' => date('Y/m/d H:i:s')
            ]);
            if($request->input('for_savings')){
                foreach($request->input('for_savings') as $val){
                    $inventory = explode("-", $val);
                    $asset_id  = Asset::where('inventory_id',$inventory[0])->first(); 
                    $insert_sub = AssignedSupplyItem::create([
                        'assigned_supply_id' => $insert->id,
                        'inventory_id' => $inventory[0],
                        'asset_id' => ($asset_id?$inventory[1]:null),
                        'quantity' => ($asset_id?1:$request->input('qty'.$val)),
                        'created_at' => date('Y/m/d H:i:s')
                    ]);
                }
            }
            
            
            $response = array(
                'status' => 'success',
                'reload_table' => true,
                'route' => route('assign.reloadtable'),
                'tableid' => 'table_append',
                'desc' => 'Assign Supplies and Equipment successfully created!'
            );
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'Error Message Please Modify'
            );
        }
        return response()->json($response,200);
    }
    public function InsertGenwralUser(Request $request)
    {
        
        $usersert = GeneralUser::create([
            'role_id' => 3,
            'name' => $request->input('assignto'),
            'contact_number' => $request->input('contactno'),
            'address' => $request->input('address'),
            'created_at' => date('Y/m/d H:i:s')
        ]);
        return  $usersert->id;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $GeneralUser  = GeneralUser::where('id',$id)->first();       
        return view('Inventory.AssignEquipment.view',[
            'GeneralUser' => $GeneralUser,
            'AssignedSupplyItemsData' => $this->getAssignId($id)
        ]);
        
    }
    public function getAssignId($id)
    {
        $AssignedSupply  = AssignedSupply::where('assigned_user_id',$id)->get(); 
        $id_list = array();
        foreach($AssignedSupply as $val){
            $id_list[] = $val->id;
        }  
        $AssignedSupplyItems  = AssignedSupplyItem::whereIn('assigned_supply_id',$id_list)->get();  
        return $AssignedSupplyItems;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $GeneralUser  = GeneralUser::where('id',$id)->first();       
        return view('Inventory.AssignEquipment.delete',['data' => $GeneralUser]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public static function inventorieslist($id)
    {
        
        $Inventory = Inventory::where('id',$id)->first();        
        $Asset = Asset::where('inventory_id',$id)->get();
        $result ='';
        $Valcount = 1;
        if(count($Asset)>0){
            foreach($Asset as $value){
                $validation = AssignedSupplyItem::where('inventory_id',$value->inventory_id)->where('asset_id',$value->id)->first();
                if(!$validation){
                    $result .= '<option value="'.$value->inventory_id.'-'.$value->id.'"  >'.$Inventory->getInventoryName->description.'-'.$value->asset_tag.'-'.($value->serial_number == null?'00':$value->serial_number).'</option>';
                }
                
            }
        }else{
            $rem = self::validationInve($id); 
            $rem1 = self::validationInve1($id); 
            $totalrem = $rem - $rem1;
            if($totalrem != 0){
                $result .= '<option value="'.$Inventory->id.'-0"  >'.$Inventory->getInventoryName->description.'</option>';
            }
        }
        return  $result;
    }
    public function autocomplete(Request $request)
    {
        $search = $request->get('term');
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->get();
        return response()->json($result);
    } 
    public function SuppliesandEquipments(Request $request)
    {
        $data = DB::table('assigned_supplies')
        ->leftJoin('assigned_supply_items', 'assigned_supplies.id', '=', 'assigned_supply_items.assigned_supply_id')
        ->where('assigned_user_id',$request->get('id'))
        ->select('assigned_supply_items.inventory_id','assigned_supply_items.quantity')
        ->get();
        $rem1 = 0;
        $rem = 0;
        $totalrem = 0;
        $array = array();
        foreach($data as $val){
            $rem1 +=$val->quantity;
            // dump($rem);
            $rem = self::validationInve($val->inventory_id); 
            $rem1 = self::validationInve1($val->inventory_id); 
            $totalrem =$rem - $rem1;
            if($totalrem == 0){
                $array[] = $val->inventory_id;
            }
        }
        

       
        $Inventory  = Inventory::all(); 
        $tbody = '';
        $tbody .= ' <option value="">Select Supplies and Equipments</option>';
        foreach($Inventory as $data){
            $tbody .= self::inventorieslist($data->id);
        }
        return $tbody;
    } 
    public static function validationInve1($id)
    {
        $rem = 0;
        $Inventory = AssignedSupplyItem::where('inventory_id',$id)->get();
        foreach($Inventory as $data){
            $rem += $data->quantity;
        }
        
        return $rem;
    }
    public static  function validationInve($id)
    {
        $Inventory = Inventory::where('id',$id)->first();
        
        return($Inventory?$Inventory->quantity:0);
    }
    public static  function functionRemoveAssign($id)
    {
        AssignedSupply::where('assigned_user_id',$id)->delete(); 
        return redirect(route('assign-equipment.index'));
    } 
    public static function reloadtable()
    {
        $append = '<tr align="center" style="border-style:hidden;" id="nodatatr">
                    <td colspan="4">No Data Available</td>
                </tr id="assignlist">';
        $append .= '<script>
                    $(document).ready(function() {
                        inventory_ids = [];
                    });</script>';
        return  $append;

    }
    
    public static function suppandequitable(Request $request)
    {
         
        $append = '';
        $list = array();
        $inventory = explode("-", $request->get('id'));
            $Inventory = Inventory::where('id',$inventory[0])->first();        
            $Asset = Asset::where('inventory_id',$inventory[0])->where('id',$inventory[1])->first();
            $data = DB::table('assigned_supplies')
                ->leftJoin('assigned_supply_items', 'assigned_supplies.id', '=', 'assigned_supply_items.assigned_supply_id')
                
                ->where('inventory_id',$inventory[0])
                ->select('assigned_supply_items.inventory_id','assigned_supply_items.quantity')
                ->get();   
                $qty = 0;
                $qty1 = 0;
                foreach($data as $val){
                    $qty1 +=$val->quantity;
                }    
                $qty += $Inventory->quantity - $qty1;
                
                if($Asset){
                    
                    $description =$Inventory->getInventoryName->description.'-'.$Asset->asset_tag.'-'.($Asset->serial_number == null?'00':$Asset->serial_number);
                    $count = '1<input type="hidden" class="col-2 newInput" id="assetsid'.$Asset->id.'" name="assetsid'.$Asset->id.'" value="'.$Asset->id.'">&nbsp;';
                    $qty = 1;
                }else{
                    $description = $Inventory->getInventoryName->description;
                    $count  =   '
                                <input onkeyup="validquantity(this,'.$request->get('id').','.$qty.')" type="text" class="col-2 newInput text-center" id="qty'.$request->get('id').'" name="qty'.$request->get('id').'" >&nbsp;
                                ';
                }
                $append .= '<tr class="appendRow"><input type="hidden" name="for_savings[]" value="'.$request->get('id').'">';
                $append .= '<td><center>'.$description.'</center></td>';
                $append .= '<td><center>'.$qty.'</center></td>';
                $append .= '<td><center>'.$count.'</center></td>';
                $append .= '<td><center><button class="btn btn-primary" type="button" id="deletebtn'.$request->get('id').'">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button></center></td>';
                $append .= '</tr>';
                $append .= '<script>
                            $(".newInput").each( function () {
                                $(this).rules("add",{
                                    "required" : true
                                })
                            });
                            $("#qty'.$request->get('id').'").val("");
                            $( "#plusbtn'.$request->get('id').'" ).click(function(){   
                                var count = $("#qty'.$request->get('id').'").val();
                                count ++;
                                $("#qty'.$request->get('id').'").val(count);
                            });
                            $( "#minusbtn'.$request->get('id').'" ).click(function(){  
                                if($("#qty'.$request->get('id').'").val() > 0){
                                    var count = $("#qty'.$request->get('id').'").val();
                                    count --;
                                    $("#qty'.$request->get('id').'").val(count);
                                } 
                            });
                            $( "#deletebtn'.$request->get('id').'" ).click(function(){
                                $(this).parent().parent().parent().remove();
                                inventory_ids.splice( inventory_ids.indexOf("'.$request->get('id').'"), 1 );
                                if(inventory_ids.length  == 0){
                                    $("#nodatatr").show(); 
                                }
                });</script>';
        return  $append;
    }
    public function getAssign($year){
        if($year == '0000'){
            $asset_list = AssignedSupply::groupBy('assigned_user_id')
            ->select('id','assigned_user_id', DB::raw('count(*) as count'))
            ->get();
        }else{
            $asset_list = AssignedSupply::groupBy('assigned_user_id')
            ->select('id','assigned_user_id', DB::raw('count(*) as count'))
            ->whereYear('created_at',$year)->get();
        }
        
        return DataTables::of($asset_list)->addColumn('assigned_to',function($model){
            return $model->GeneralUserDetails->name;
        })->addColumn('assigned_materials',function($model){
            return $model->count;
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown" style="text-align:center">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route('assign-equipment.show',$model->assigned_user_id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                            <a class="dropdown-item popup_form" data-toggle="modal" data-url="'.route('assign-equipment.edit',$model->assigned_user_id).'" title="Delete Assign Equipment Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->make(true);
    }
}
