<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\Category;
use App\Inventory;
use App\InventoryName;
use Intervention\Image\Facades\Image;
use App\Common;
use App\Asset;
use Illuminate\Support\Facades\DB;

class NewInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::whereNull('deleted_at')->get();
        $categories = Category::whereNull('deleted_at')->get();
        $last_idfinal = DB::table('inventory_names')->orderby('id','desc')->first();
        return view('Inventory.NewInventory.create',[
            'units' => $units,
            'categories' => $categories,
            'last_idfinal' => (!$last_idfinal?'0':$last_idfinal->id)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            if($request->input('for_insert')){
                $count = 1;
                foreach ($request->input('for_insert') as $key => $value){
                    if($request->input('image'.$value)){
                        $base64_str = substr($request->input('image'.$value), strpos($request->input('image'.$value), ",")+1);
                        $image = base64_decode($base64_str);
                        $filename = "inventory-img-".time().$count.".png";
                        $path = storage_path('app/public/').$filename;
                        Image::make(file_get_contents($request->input('image'.$value)))->save($path);
                        $count++;
                    }else{
                        $filename= null;
                    }
                    
                    $name = InventoryName::create([
                        'description' => $request->input('inventory_description'.$value),
                        'category_id' => $request->input('category'.$value)
                    ]);
    
                    $invent = Inventory::create([
                        'unit_id' => $request->input('unit'.$value),
                        'inventory_name_id' => $name->id,
                        'category_id' => $request->input('category'.$value),
                        'description' => $request->input('inventory_description'.$value),
                        'quantity' => str_replace(',', '', $request->input('quantity'.$value)),
                        'unit_price' => str_replace(',', '', $request->input('unit_price'.$value)),
                        'image' => $filename
                    ]);

                    if($request->input('inventory_type'.$value) == 'capex'){
                        $this->addToAsset($request,$value,$invent->id);
                    }
                    
                }

                
                
                $response = array(
                    'status' => 'success',
                    'reload_table' => true,
                    'route' => route('new-inventory.reloadtable',0),
                    'desc'  => 'Inventory successfully created!'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Check inventory items!'
                );
            }
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }

        return response()->json($response,200);
    }

    public function reloadtable($id)
    {
        $append_no_data = '<tr align="center">'.
                        '<td colspan="8">No Data Available</td>'.
                    '</tr>';
        
        return array(
            'append_no_data' => $append_no_data
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addToAsset($request,$value,$id){
        for ($i=0 ; $i < $request->input('quantity'.$value) ; $i++ ) { 
            $asset_create = Asset::create([
                'inventory_id' => $id,
                'inventory_status_id' => 1,
                'asset_tag' => $this->assetTag($request->input('category'.$value)),
                'date_acquired' => date('Y-m-d')
            ]);
        }
    }

    public function assetTag($category_id){
        $get_category = Inventory::where('category_id',$category_id)->get();
        $inventory_id = array();
        foreach($get_category as $inventory){
            $inventory_id[] = $inventory->id;
        }
        $statement = Asset::whereIn('inventory_id',$inventory_id)->orderBy('id', 'desc')->first();

        if($statement){
            $asset_inc = (int)$statement->asset_tag + 1;
        }else{
            $asset_inc = 1;
        }
        return sprintf("%03d", $asset_inc);
    }

}
