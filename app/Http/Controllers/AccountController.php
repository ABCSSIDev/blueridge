<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Mockery\Exception;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use App\Common;
use Auth;
use Illuminate\Support\Facades\DB;
use App\BudgetAllocation;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $budget_allocation = BudgetAllocation::all();
        return view('Setup.Account.index',[
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('Setup.Account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try{
            if($request->has('image')){
                $id = DB::table('users')->orderby('id','desc')->first();
                $base64_str = substr($request->image, strpos($request->image, ",")+1);
                $image = base64_decode($base64_str);
                $filename = "accounts-".time().(!$id?'1':$id->id+1).".png";
                $path = storage_path('app/public/').$filename;
                Image::make(file_get_contents($request->image))->save($path);
            }else{
                $filename= '';
            }
                $new_user = new User([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'address' => $request->input('address'),
                    'position' => $request->input('position'),
                    'password' => Hash::make($request->input('password')),
                    'image' => $filename,
                    'contact_number' => $request->input('contact_number'),
                ]);
        
                $new_user->save();
                $response = array(
                    'status' => 'success',
                    'reload' => true,
                    'desc'  => 'Account successfully created!'
                );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }

        return response()->json($response,200);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::where('id',$id)->where('deleted_at',NULL)->first();
        return view('Setup.Account.show',[
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::where('id',$id)->where('deleted_at',NULL)->first();
        return view('Setup.Account.edit',[
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try{
            if($request->has('image')) {
                $base64_str = substr($request->image, strpos($request->image, ",")+1);
                $image = base64_decode($base64_str);
                $imageName = "accounts-".time().$id.".png";
                $path = storage_path('/app/public/'). $imageName;
                Image::make(file_get_contents($request->image))->save($path);
            }
            else{
                $imageName = $request->input('old_image');
            }

            $edit_user = User::find($id);
            $edit_user->name = $request->input('name');
            $edit_user->email = $request->input('email');
            $edit_user->address = $request->input('address');
            $edit_user->position = $request->input('position');
            $edit_user->image = $imageName;
            $edit_user->contact_number = $request->input('contact_number');
            if($request->input('password') != null)
            {
                $edit_user->password = Hash::make($request->input('password'));
            }
            $edit_user->save();

            $response = array(
                'status' => 'success',
                'desc'  => 'Account successfully updated!'
            );

        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
       //
    }

    public function removeAccount($id) {
        $account_list = User::where('deleted_at',null)->where('id',$id)->first();
        return view('Setup.Account.delete',[
            'account_list' => $account_list
        ]);
    }

    public function functionRemoveAccount($id) {
        $account_details = User::where('id', $id)->first();
        User::where('id', $id)->delete(); // delete account
        return redirect(route('account.index').'?success=true&url=setup/account&module='.$account_details->name.'');
        
    }

    public function getAccount($year){
        if($year == '0000'){
            $account_list = User::whereNull('deleted_at')->get();
        }else{
            $account_list = User::whereNull('deleted_at')->whereYear('created_at',$year)->get();
        }
        // $account_list = User::whereNull('deleted_at')->get();
        return DataTables::of($account_list)->addColumn('image',function($model){
            $image = ($model-> image == null ? asset('images/default.png') : route('account.image',$model->id));
            return '<center><div><img src="'.$image.'" alt="Image"/ height="50" width="50" class="rounded-circle"></div></center>';
        })->addColumn('name',function($model){
            return $model->name;
        })->addColumn('position',function($model){
            return $model->position;
        })->addColumn('contact',function($model){
            return $model->contact_number;
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown" style="text-align:center">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("account.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                        <a class="dropdown-item" href="'.route("account.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                        '.(Auth::User()->id != $model->id ? '<div class="dropdown-divider"></div><a class="dropdown-item popup_form" data-toggle="modal" data-url="'.route("remove.account",$model->id).'" title="Delete Account Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>' : '')
                        .'</div>
                        </div>
                    </center>';
        })->rawColumns(['image','action'])->make(true);
    }

    public function getImage($id){
        try{
            $account = User::findOrFail($id);
            $file = Storage::disk('public')->get($account->image);
            return response($file,200);
        }catch (Exception $exception){
            $response = Common::catchError($exception);
        }
    }

}
