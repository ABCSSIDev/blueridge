<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Signatory;
use App\SignatoryTemplate;
use App\Common;
use App\TemplateSignee;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class TemplateSignatoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Setup.TemplateSignatories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $docs = Document::whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('signatory_templates')
                ->whereRaw('signatory_templates.document_id = documents.id')
                ->whereRaw('signatory_templates.deleted_at is  null');
        })->get();
        $first = Document::all()->first();
        $signs = Signatory::all();
        return view('Setup.TemplateSignatories.create',['docs' => $docs,'signs' => $signs,'first_docs' => $first->url_content]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $signatoryTemplate = new SignatoryTemplate();
            $signatoryTemplate->document_id = $request->input('document_id');

            $signatoryTemplate->save();

            foreach($request->input('signatories') as $key => $value){
                $templateSignee = new TemplateSignee();
                $templateSignee->template_id = $signatoryTemplate->id;
                $templateSignee->signatory_id = $value;

                $templateSignee->save();
            }
            $response = array(
                'status' => 'success',
                'reload_select' => true,
                'route' => route('refresh.select'),
                'desc' => 'Signatory template successfully created!'
            );
        }catch(\Exception $e){
            dd($e);
            $response = Common::catchError($e);
        }

        return response()->json($response,200);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = SignatoryTemplate::findOrFail($id);

        // dd($template->getSignatures[0]->getSignatory->signatory_name);

        $document = view($template->getDocument->url_content)->render();    
        return view('Setup.TemplateSignatories.show',['template' => $template,'document' => $document]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            SignatoryTemplate::findOrFail($id)->delete();
        }catch(\Exception $e){
            $response = Common::catchError($e);
        }

        return redirect(route('template-signatories.index'));
    }

    public function delete($id){
        $template = SignatoryTemplate::findOrFail($id);
        
        return view('Setup.TemplateSignatories.delete',['template' => $template]);
    }

    public function getTemplates(){
        $templates = SignatoryTemplate::all();
        return DataTables::of($templates)->addColumn('template_name',function($model){
            return $model->getDocument->name;
        })->addColumn('action',function($model){
            return "<center>
                        <div class='dropdown'>
                            <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <i class='fa fa-bars'></i>
                            </button>
                            <div class='dropdown-menu dropdown-menu-center' aria-labelledby='dropdownMenuButton'>
                                <a class='dropdown-item' title='View Category' href='".route('template-signatories.show',$model->id)."'><i class='fa fa-eye' aria-hidden='true'></i> View Record</a>
                                <a class='dropdown-item' title='Edit Category' href='".route('template-signatories.edit',$model->id)."'><i class='fa fa-pencil' aria-hidden='true'></i> Edit Record</a>
                                <div class='dropdown-divider'></div>
                                <a class='dropdown-item popup_form' data-toggle='modal' title='Delete Template Record' style='cursor:pointer' data-url='".route('template-signatories.delete',$model->id)."'><i class='fa fa-trash' aria-hidden='true'></i> Delete Record</a>
                            </div>
                        </div>
                    </center>";
        })->make(true);
    }
}
