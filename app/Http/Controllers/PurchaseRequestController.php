<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\InventoryName;
use App\Category;
use App\Unit;
use App\Canvass;
use App\Common as common;
use App\PurchaseRequest;
use App\ROA;
use App\PurchaseRequestItem;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\BudgetAllocation;

class PurchaseRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
    public function index() {
        $budget_allocation = BudgetAllocation::all();
        return view('Procurement.PurchaseRequest.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Procurement.PurchaseRequest.index');
    }
    public function create() {
        $category = Category::get();
        $unit = Unit::get(); 
        $pr = PurchaseRequest::all();
        $exist_id = array();
        foreach($pr as $data){
            $exist_id[] = $data->roa_id;
        }       
        $roa = ROA::whereNotIn('id',$exist_id)->get();
        $purchase_requests = DB::table('purchase_requests')->orderBy('id', 'DESC')->first();

        return view('Procurement.PurchaseRequest.create',[
            'category' => $category,
            'last_id' => common::getPRNumberFormat('purchase_requests'),
            'roa' => $roa,
            'unit' => $unit
        ]);
    }
    public function update(Request $request, $id) {
        try{
            // dd($request->input('exist_id1'));
            $insert = PurchaseRequest::where('id', $id)->update([
                'pr_no' => $request->input('pr_no'),
                'to' => $request->input('pur_to'),
                'from' => $request->input('pur_from'),
                'delivery_to' => $request->input('pur_del_to'),
                'purpose' => $request->input('purpose'),
                'request_date' => date('Y/m/d H:i:s', strtotime($request->input('pur_request_date'))),
                'delivery_period' => date('Y/m/d H:i:s', strtotime($request->input('pur_delivery_date'))),
                'created_at' => date('Y/m/d H:i:s')
            ]);
            if($request->input('for_editing')){
                foreach ($request->input('for_editing') as $value){
                    if($request->input('exist_id'.$value)){
                        $result = Inventory::where('inventory_name_id', $request->input('exist_id'.$value))->first();
                        // dd($request->input('exist_id'.$value));
                        $inv_id = $request->input('exist_id'.$value);
                    }else{
                        $inv_id = $this->insertInventory($request,$value);
                    }
                    // $values = explode(',', $request->input('qtyfrans'.$value));
                    PurchaseRequestItem::where('id', $value)->update([
                        'pr_id' => $id,
                        'unit_id' => ($request->input('pur_unit'.$value)?$request->input('pur_unit'.$value):$this->PurchaseDetails($value,2)->unit_id),
                        'inventory_name_id' => $inv_id,
                        'quantity' =>  $request->input('qtyfrans'.$value),
                        'unit_cost' =>floatval(implode(explode(',',$request->input('cost'.$value)))),
                        'created_at' => date('Y/m/d H:i:s')
                    ]);
                    
                }
            }
            if($request->input('for_savings')){
                foreach($request->input('for_savings') as $value){
                    if($request->input('exist_id'.$value)){
                        $result = Inventory::where('id', $request->input('exist_id'.$value))->first();
                        $inv_id = $result->inventory_name_id;
                    }else{
                        $inv_id = $this->insertInventory($request,$value);
                    }
                    // return $inv_id;
                    // $values = explode(',', $request->input('qtyfrans'.$value));
                    PurchaseRequestItem::create([
                        'pr_id' => $id,
                        'unit_id' => ($request->input('pur_unit'.$value) ? $request->input('pur_unit'.$value) : $result->unit_id),
                        'inventory_name_id' => $inv_id,
                        'quantity' =>  $request->input('qtyfrans'.$value),
                        'unit_cost' =>floatval(implode(explode(',',$request->input('cost'.$value)))),
                        'created_at' => date('Y/m/d H:i:s')
                    ]);
                }
            }
          
                $response = array(
                    'status' => 'success',
                    'reload_table' => false,
                    'route' => route('pur.reloadtableedit',$id),
                    'tableid' => 'table_purchase_edit',
                    'desc' => 'Purchase Request successfully updated!'
                );
            }catch (\Exception $exception){
                $response = array(
                    'status' => 'error',
                    'desc' => 'Error Message Please Modify'
                );
            }
            return response()->json($response,200);
    }
    public function PurchaseDetails($id,$type) {
        if($type == 1){
            $data = PurchaseRequest::where('id', $id)->first();
        }else{
            $data = PurchaseRequestItem::where('id', $id)->first();
        }
        return $data;
    }
    public function reloadtableedit($id) {
        $pr = PurchaseRequestItem::where('pr_id',$id)->get();
        $category = Category::all();
        $roa = ROA::all();
        $unit = Unit::all();
        $pr_items = '';
        foreach($pr as $data => $val){
            $pr_items .= '<tr align="center"><input type="hidden" class="for_editing" name="for_editing[]" value="'.$val->id.'">';
            $pr_items .= '<td></td>
                          <td class="item_no">'.$count = ++$data.'</td>
                          <td>
                            <div class="ui-widget">
                                <input id="search'.$val->id.'" readonly name="search'.$val->id.'" type="text" value="'.$val->getInventoryName->description.'" class="form-control search newInput" placeholder="Search" autocomplete="off"/>
                                <input type="hidden" class="exist_id" name="exist_id'.$val->id.'" id="exist_id'.$val->id.'" value="'.$val->getInventoryName->id.'">
                            </div>
                          </td>
                          <td>
                            <select class="form-control pur_unit newInput" name="pur_unit'.$val->id.'" id="pur_unit'.$val->id.'" disabled>
                                <option value="">Select Unit Type</option>
                                ';
                                foreach($unit as $value){
                                    $pr_items .= '<option value="'.$value->id.'" '.($value->id == $val->getInventoryName->category_id?'selected':'').'>'.$value->unit_symbol.'</option>';
                                }
            $pr_items .= '</select>
                           </td>
                           <td>
                                <select class="form-control category newInput" name="category'.$val->id.'" id="category'.$val->id.'" disabled>
                                    <option value="">Select Category</option>';
                                    foreach($category as $value){
                                        $pr_items .= '<option value="'.$value->id.'" '.($value->id == $val->getInventoryName->category_id?'selected':'').'>'.$value->category_name.'</option>';
                                    }
            $pr_items .= '</select>
                            </td>
                            <td><input type="number" class="form-control subtotal qty newInput text-center" min="1" id="qty'.$val->id.'" name="qty'.$val->id.'" placeholder="Qty" autocomplete="off" data-validation="required" autocomplete="off" value="'.number_format($val->quantity, 0).'" disabled></td>
                            <td>
                                <input type="text" class="form-control subtotal cost newInput" id="cost'.$val->id.'" placeholder="Est. Unit Cost" name="cost'.$val->id.'" autocomplete="off" data-validation="required" autocomplete="off" value="'.number_format($val->unit_cost, 2).'" disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control subtotalval" autocomplete="off" id="subtotal'.$val->id.'" name="subtotal'.$val->id.'" data-validation="required" readonly value="'.number_format($val->quantity * $val->unit_cost , 2).'">
                            </td>
                           ';
            $pr_items .= '</tr>';
        }
        

        return $pr_items;
    }
    public function reloadtable($id) {
        $pr = PurchaseRequest::all();
        $exist_id = array();
        foreach($pr as $data){
            $exist_id[] = $data->roa_id;
        }       
        $roa = ROA::whereNotIn('id',$exist_id)->get();
        $roa_select = '<option value="">Select ROA No.</option>';
        if($roa){
            foreach($roa as $roa_id){
                $roa_select .= '<option value="'.$roa_id->id.'" >'.$roa_id->obligation_number.'</option>';
            }
        }
        return array(
            'new_pr' => common::getPRNumberFormat('purchase_requests'),
            'roa_select' => $roa_select
        );
    }
    public function insertInventory(Request $request,$value) {
        // foreach ($request->input($type) as $key ){
            
        // }
        // dd(1);
        $inventory_names = InventoryName::create([
            'description' => $request->input('search'.$value),
            'category_id' => $request->input('category'.$value)
        ]);
        
        
        return  $inventory_names->id;
    }
    public function store(Request $request) {
        
        try{
            $insert = PurchaseRequest::create([
                'roa_id' => $request->input('pur_roa_no'),
                'pr_no' => $request->input('pur_request_no'),
                'to' => $request->input('pur_to'),
                'from' => $request->input('pur_from'),
                'delivery_to' => $request->input('pur_del_to'),
                'purpose' => $request->input('purpose'),
                'request_date' => date('Y/m/d H:i:s', strtotime($request->input('pur_request_date'))),
                'delivery_period' => date('Y/m/d H:i:s', strtotime($request->input('pur_delivery_date'))),
                'created_at' => date('Y/m/d H:i:s')
            ]);
            foreach ($request->input('for_savings') as $key => $value){
                if($request->input('exist_id'.$value)){
                    $result = Inventory::where('id', $request->input('exist_id'.$value))->first();
                    $inv_id = $result->inventory_name_id;
                    $unit = $result->unit_id;
                }else{
                    $inv_id = $this->insertInventory($request,$value);
                    $unit = $request->input('pur_unit'.$value);
                }
                    // dd($request->input('qtyfran'.$value));
                // $values = explode(',', $request->input('qtyfran'.$value));
                PurchaseRequestItem::create([
                    'pr_id' => $insert->id,
                    'unit_id' => $unit,
                    'inventory_name_id' => $inv_id,
                    'quantity' =>  $request->input('qtyfran'.$value),
                    
                    'unit_cost' => $request->input('cost'.$value),
                    'created_at' => date('Y/m/d H:i:s')
                ]);
            }
            $response = array(
                'status' => 'success',
                'reload_table' => true,
                'route' => route('pur.reloadtable',0),
                'desc' => 'Purchase Request Successfully submitted!'
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }
    public static function computationFrantion($val1,$val2) {
        $f = $val1 / $val2;
        return $f;

    }
    public static function decToFraction($val1,$val2 = null) {
        $f = $val1;
        $l = 0;
        $base = floor($f);
        $out ='';
        if ($base) {
          $out = $base . ' ';
          $l = $f - $base;
          $f = $f - $base;
        }
        if ($f != 0) {
          $d = 1;
          while (fmod($f, 1) != 0.0) {
            $f *= 2;
            $d *= 2;
          }
          $n = sprintf('%.0f', $f);
          $d = sprintf('%.0f', $d);
          $out .= $n . '/' . $d;
        }
        return $out;
    }
    public function show($id) {
        $result = PurchaseRequest::where('id', $id)->first();
        $subPurchaseRequestDetails = PurchaseRequestItem::where('pr_id', $id)->get();
        return view('Procurement.PurchaseRequest.view',[
            'PurchaseRequestDetails' => $result,
            'PurchaseRequestNo' => common::getPRNumberFormat('purchase_requests',$result,1),
            'subPurchaseRequestDetails' => $subPurchaseRequestDetails
        ]);
    }
    public static function validationcassvas(Request $request) {
        $result = Canvass::where('pr_id', $request->get('id'))->count();
        return $result;
    }
    public function autocomplete(Request $request) {
        $search = $request->get('term');
        $result =DB::table('inventories')
        ->select('inventories.id','inventory_names.description','inventories.unit_id','inventories.category_id')
        ->join('inventory_names', 'inventories.inventory_name_id','=','inventory_names.id')
        ->where('description', 'LIKE', '%'. $search. '%')->get();
        return response()->json($result);
    } 
    public function roadetails(Request $request) {
        $search = $request->get('roa');
        $result = ROA::where('id', $search)->first();
        return [
            'total_amount' => $result->total_amount,
            'ledger' => $result->getLedger->ledger_name
        ];
    } 
    public static function PurchaseRequestTotal($id) {
        $result = PurchaseRequestItem::where('pr_id', $id)->get();
        $sub = 0;
        $total = 0;
        foreach($result as $val){
            // $sub = $val->unit_cost * self::computationFrantion($val->quantity,$val->quantity1);
            $sub = $val->unit_cost * $val->quantity;
            $total = $total + $sub;
        }
        return number_format($total, 2);
    } 
    public function RemovePurchaseRequest($id) {
        $pr_no = PurchaseRequest::where('deleted_at',null)->where('id',$id)->first();
        return view('Procurement.PurchaseRequest.delete',
        ['purchase_request' => $pr_no,
         'pr_no' =>   common::getPRNumberFormat('purchase_requests',$pr_no,1)      
        ]);
    }
    public function edit($id) {
        $result = PurchaseRequest::where('id', $id)->first();
        $subPurchaseRequestDetails = PurchaseRequestItem::where('pr_id', $result->id)->get();
        $Canvass = Canvass::where('pr_id', $result->id)->first();
        $category = Category::all();
        $roa = ROA::all();
        $unit = Unit::all();
        $purchase_requests = DB::table('purchase_requests')->orderBy('id', 'DESC')->first();
        return view('Procurement.PurchaseRequest.edit',[
            'PurchaseRequestDetails' => $result,
            'subPurchaseRequestDetails' => $subPurchaseRequestDetails,
            'category' => $category,
            'last_id' => common::getPRNumberFormat('purchase_requests',$result),
            'Canvass' => $Canvass,
            'unit' => $unit,
            'totalamount' => $this->PurchaseRequestTotal($result->id),
            'roa' => $roa
        ]);
    }
    public function FunctionRemovePurchaseRequest($id) {
        $pr = PurchaseRequest::where('id', $id)->first();
        PurchaseRequest::where('id', $id)->delete(); // delete PR
        return redirect(route('purchase-request.index').'?success=true&url=procurement/purchase-request&module= PR ID.'.common::getPRNumberFormat('purchase_requests',$pr,1).'');        
    }
    public function getPurchaseRequest($year) {
        if($year == '0000'){
            $purchaserequest_list = PurchaseRequest::all();
        }else{
            $purchaserequest_list = PurchaseRequest::whereYear('request_date',$year)->get();
        }
        return DataTables::of($purchaserequest_list)->addColumn('prno',function($model){
            return $model->pr_no;
        })->addColumn('to',function($model){
            return $model->to;
        })->addColumn('from',function($model){
            return $model->from;
        })->addColumn('resolutiondate',function($model){
            return common::convertWordDateFormat($model->request_date);
        })->addColumn('action',function($model){
            $result = PurchaseRequest::where('id', $model->id)->first();
            $canvass = Canvass::where('pr_id', $result->id)->first();
            return '<center>
                        <div class="dropdown" style="text-align:center">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="'.route('purchase-request.show',$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                                <a class="dropdown-item" href="'.route('purchase-request.edit',$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item popup_form '.(!$canvass ? "" : "disable-link").'" data-toggle="modal" style="cursor:pointer;" data-url="'.route('pur.RemovePurchaseRequest',$model->id).'" title="Delete PR Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                            </div>
                        </div>
                    </center>';
        })->make(true);
    }
}