<?php

namespace App\Http\Controllers;

use App\AccountGroup;
use App\AccountLedger;
use App\Payroll;
use App\PayrollItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Common as common;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use App\BudgetAllocation;

class PayrollController extends Controller
{
    public function index(){
        $budget_allocation = BudgetAllocation::all();
        return view('Accounting.Payroll.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Accounting.Payroll.index');
    }
    public function create(){
        return view('Accounting.Payroll.create');
    }
    public function PayRollNovalidation(Request $request){
        $payroll = Payroll::where('payroll_number',$request->get('id'))->first();
        $error = "false";
        if($payroll){
            $error = "true";
        }
        return $error;
    }
    public function store(Request $request){
        try{
            if(Payroll::where([
                ['payroll_number', $request->input('payroll_number')]
            ])->exists()){
                $response = array(
                    'status' => 'error',
                    'desc' => 'Payroll Number already exists!'
                );
            }
            else {
                $users = Auth::user();
                $payroll = new Payroll([
                    'barangay' => $request->input('barangay'),
                    'city_municipality' => $request->input('municipality'),
                    'treasurer' => $request->input('treasurer'),
                    'province' => $request->input('province'),
                    'ledger_id' => 8,
                    'created_by' => $users->id,
                    'date_from' => date('Y/m/d H:i:s',strtotime($request->input('date_from'))),
                    'date_to' => date('Y/m/d H:i:s',strtotime($request->input('date_to'))),
                    'due_date' => date('Y/m/d H:i:s',strtotime($request->input('due_date'))),
                    'payroll_number' => $request->input('payroll_number'),
                ]);
                $payroll->save();
                foreach ($request->input('for_payroll') as $key => $value){
                    $payrollItem = PayrollItem::create([
                        'payroll_id' => $payroll->id,
                        'name' => $request->input('name'.$value),
                        'position' => $request->input('position'.$value),
                        'salary' => $request->input('salary'.$value),
                        'honoraria' => $request->input('honoraria'.$value),
                        'other_benefits' => $request->input('other'.$value),
                        'bir' => $request->input('bir'.$value),
                        'created_at' => date('Y/m/d H:i:s')
                    ]);
                }
                $response = array(
                    'status' => 'success',
                    'desc' => 'Payroll successfully created!',
                    'reload_table' => true,

                );
            }
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
       
    }
    
    public function show($id){
        $payroll = Payroll::findOrFail($id);
        return view('Accounting.Payroll.show', [
            'payroll' => $payroll,
            'total_payroll' => $this->getPayrollTotal($payroll->id)
            ]);

    }

    public function edit($id){
        $payroll = Payroll::where('id', $id)
            ->first();
        $payroll_items = PayrollItem::where('payroll_id', $payroll->id)
            ->get();
        return view('Accounting.Payroll.edit', [
            'payroll' => $payroll, 
            'payroll_items' => $payroll_items,
            'total_payroll' => $this->getPayrollTotal($payroll->id)
        ]);
    }

    public function getPayrollTotal($id){
        $payroll_items = PayrollItem::where('payroll_id', $id)->get();
        $total = 0;
        
        foreach($payroll_items as $pay){
            $total += $pay->salary + $pay->honoraria + $pay->other_benefits - $pay->bir;
        }
        
        return number_format($total, 2);
    }

    public function update(Request $request, $id){
        try{
            $payroll = Payroll::where('id', $id)->update([
                'barangay' => $request->input('barangay'),
                'city_municipality' => $request->input('municipality'),
                'treasurer' => $request->input('treasurer'),
                'province' => $request->input('province'),
                'ledger_id' => 8,
                'date_from' => date('Y/m/d H:i:s',strtotime($request->input('date_from'))),
                'date_to' => date('Y/m/d H:i:s',strtotime($request->input('date_to'))),
                'due_date' => date('Y/m/d H:i:s',strtotime($request->input('due_date'))),
                'payroll_number' => $request->input('payroll_number'),
            ]);
            foreach ($request->input('for_editing') as $key => $value){
                $payrollItem = PayrollItem::where('id', $value)->update([
                    'payroll_id' => $id,
                    'name' => $request->input('name'.$value),
                    'position' => $request->input('position'.$value),
                    'salary' => Common::cleanDecimal($request->input('salary'.$value)),
                    'honoraria' => Common::cleanDecimal($request->input('honoraria'.$value)),
                    'other_benefits' => Common::cleanDecimal($request->input('other'.$value)),
                    'bir' => Common::cleanDecimal($request->input('bir'.$value)),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
            }
            if($request->input('for_payroll')){
                foreach ($request->input('for_payroll') as $key => $value){
                    $payrollItem = PayrollItem::create([
                        'payroll_id' => $id,
                        'name' => $request->input('name'.$value),
                        'position' => $request->input('position'.$value),
                        'salary' => $request->input('salary'.$value),
                        'honoraria' => $request->input('honoraria'.$value),
                        'other_benefits' => $request->input('other'.$value),
                        'bir' => $request->input('bir'.$value),
                        'created_at' => date('Y/m/d H:i:s')
                    ]);
                }
            }
            
            $response = array(
                'status' => 'success',
                'desc' => 'Payroll successfully updated!',
                

            );
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'Error Updating!'
            );
        }
        return response()->json($response,200);    
    }
    public function delete($id){
        $payroll = Payroll::where('id', $id)
            ->first();
        return view('Accounting.Payroll.delete', ['payroll' => $payroll, 'id' => $id]);
    }

    public function destroy($id){
        $payroll = Payroll::where('id', $id)
            ->first();
        Payroll::find($id)
            ->delete();
        return redirect(route('payroll.index').'?success=true&url=accounting/payroll&module='.$payroll->payroll_number.'');
    }

    public function getPayrollList($year){
        if($year == '0000'){
            $payroll_list = Payroll::all();
        }else{
            $payroll_list = Payroll::whereYear('date_from',$year)->whereYear('date_to',$year)->get();
        }
        return DataTables::of($payroll_list)->addColumn('date_from',function($model){
            return $model->date_from;
        })->addColumn('date_to',function($model){ 
            return $model->date_to;
        })->addColumn('payroll_no',function($model){
            return $model->payroll_number;
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown" style="text-align:center">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="'.route('payroll.show',$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                                <a class="dropdown-item '.(!is_null($model->getPayment)?'btn-disabled':'').'" href="'.route('payroll.edit',$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item popup_form" data-toggle="modal" data-url="'.route('payroll.delete',$model->id).'" title="Delete Payroll Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                            </div>
                        </div>
                    </center>
                    <style>
                        .btn-disabled,
                        .btn-disabled[disabled] {
                            opacity: .4;
                            cursor: default !important;
                            pointer-events: none;
                        }
                    </style>';
        })->make(true);
    }
}