<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest;
use App\Http\Controllers\PurchaseRequestController;
use App\PurchaseRequestItem;
use App\Canvass;
use App\PurchaseOrder;
use App\Receiving;
use App\Supplier;
use App\CanvassItem;
use App\ROA;
use App\Common as common;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\BudgetAllocation;

class PurchaseOrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $budget_allocation = BudgetAllocation::all();
        return view('Procurement.PurchaseOrder.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Procurement.PurchaseOrder.index');
    }

    public function create() {
        $po = PurchaseOrder::all();
        $exist_id = array();
        foreach($po as $data){
            $canvasses = Canvass::where('awarded','1')->where('pr_id',$data->pr_id)->count();
            $purchase_order = PurchaseOrder::where('pr_id',$data->pr_id)->count();
            if($canvasses <= $purchase_order){
                $exist_id[] = $data->pr_id;
            }
        }
        $PurchaseOrder = PurchaseOrder::whereNotIn('pr_id',$exist_id)->first();
        $pr = PurchaseRequest::whereNotIn('id',$exist_id)->get();
        return view('Procurement.PurchaseOrder.create', [
            'purchase_request' => $pr,
            'po' => $PurchaseOrder,
            'last_id' => common::getPRNumberFormat('purchase_orders')
        ]);
    }
    public static function prvalidation($id) {
        $canvasses = Canvass::where('awarded','1')->where('pr_id',$id)->count();
        $pr = PurchaseRequest::where('id', $id)->first();
        return common::getPRNumberFormat('purchase_requests',$pr,1);
    }
    public function reloadtablePO($id) {
        $po = PurchaseOrder::all();
        $exist_id = array();
        foreach($po as $data){
            $canvasses = Canvass::where('awarded','1')->where('pr_id',$data->pr_id)->count();
            $purchase_order = PurchaseOrder::where('pr_id',$data->pr_id)->count();
            if($canvasses <= $purchase_order){
                $exist_id[] = $data->pr_id;
            }
        }
        $PurchaseOrder = PurchaseOrder::whereNotIn('pr_id',$exist_id)->first();
        $pr = PurchaseRequest::whereNotIn('id',$exist_id)->get();
        $pr_select = '<option value="">Purchase Request No.</option>';
        // dd($po);
        if($pr){
            foreach($pr as $value){
                if(count($value->getCanvasses)>0){
                    $pr_select .= '<option data-id="'.common::getPRNumberFormat('purchase_orders', $PurchaseOrder) . common::getAlphabet($value->id).'" data-url="'.route('purchaseRequestID',$value->id).'" value="'.$value->id.'" >'.self::prvalidation($value->id).'</option>';
                }
            }
        }
        
       
        return array(
            'pr_select' => $pr_select
        );
    }

    public function show($id) {
        $po = PurchaseOrder::where('id', $id)->first();
        $pr_view = PurchaseRequest::where('id', $po->pr_id)->first();
        $canvasses = Canvass::where('awarded','1')
        ->where('id',$po->canvass_id)->first();
        $canvass_item = CanvassItem::where('canvass_id',$canvasses->id)->get();

        $ids = common::getListAlphabet($po->pr_id, $canvasses->id);
        
        return view('Procurement.PurchaseOrder.show', [
            'pr_view' => $pr_view,
            'ids' => $ids,
            'po' => $po,
            'canvass_item' => $canvass_item,
            'pr_no' => common::getPRNumberFormat('purchase_requests',$pr_view,1),
            'canvass' => $canvasses]);
    }

    public function purchaseRequestID($id) {
        $canvasses = Canvass::where('pr_id', $id)
        ->where('awarded','1')->get();
        $obj = "<option>Select Supplier Name</option>";
        foreach($canvasses as $canvass) {
            $check_po = PurchaseOrder::where('pr_id', $id)->where('canvass_id', $canvass->id)->first();
            if(empty($canvass->getSuppliers)) {
                $obj .= "";
            }
            else {
                $condition_check = empty($check_po) ? 0 : $check_po->count();
                if($condition_check <= 0){
                    $obj .= "<option data-id='".$canvass->id."' data-url='".route('purchaseOrder',$canvass->id)."' value='".$canvass->supplier_id."'>".$canvass->getSuppliers->supplier_name."</option>";
                }
            }
        }

        return $obj;
    }

    public function requestDate(Request $request) {
        $pr_id = $request->get('request_date');
        $pr_date = PurchaseRequest::where('id', $pr_id)->select('request_date')->first();
        return date('F d, Y', strtotime($pr_date->request_date));
    }

    public function supplierNameDetails(Request $request) {
        $supplier_id = $request->get('supplier');
        $supplier = Supplier::where('id', $supplier_id)->select('address')->first();
        if(empty($supplier->address)) {
            $supplier_address = "N/A";
        }
        else {
            $supplier_address = $supplier->address;
        }
        return $supplier_address;
    }

    public function purchaseOrder($id) {        
        $canvass = CanvassItem::where('canvass_id',$id)->get();
        $options = "";
        $total = 0;
        foreach ($canvass as $key => $item){
            $est_cost = $item->available_quantity * $item->unit_price;
            $options .= "<tr>
                            <input type='hidden' name='canvass_id' value='".$item->canvass_id."'>
                            <td>".++$key."</td>
                            <td>".$item->getPRItems->getInventoryName->description."</td>
                            <td>".$item->getPRItems->getUnits->unit_name."</td>
                            <td>".PurchaseRequestController::decToFraction($item->available_quantity)."</td>
                            <td>₱ ".number_format($item->unit_price,2)."</td>
                            <td>₱ ".number_format($est_cost,2)."</td>
                        </tr>";

            $total += $item->unit_price * $item->available_quantity;
        }

        $options .= '<tr><td class="align-right" colspan="5">Total:</td><td>₱ '.number_format($total,2).'</td></tr>';

        return $options;
    }

    public function store(Request $request) {
        try {
            $po = PurchaseOrder::create([
                'po_no' => $request->input('po_num'),
                'pr_id' => $request->input('pr_num'),
                'canvass_id' => $request->input('canvass_id'),
                'po_date' => date("Y-m-d", strtotime($request->input('po_date'))),
                'procurement_mode' => $request->input('procurement_mode'),
                'delivery_term' => $request->input('delivery_term'),
                'place_of_delivery' => $request->input('place_of_delivery'),
                'payment_term' => $request->input('payment_term'),
                'delivery_date' => ($request->input('date_of_delivery') != '' ? date("Y-m-d", strtotime($request->input('date_of_delivery'))) : null)
            ]);
            $po->save();
            $response = array(
                'status' => 'success',
                'reload_table' => true,
                'route' => route('po.reloadtable',0),
                'desc' => 'Purchase Order successfully created!'
            );
        }
        catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => "There's something wrong when submitting form"
            );
        }

        return response()->json($response,200);
    }

    public function delete($id) {
        $purchaseOrder = PurchaseOrder::where('id', $id)->first();
        $pr_view = PurchaseRequest::where('id', $purchaseOrder->pr_id)->first();
        $canvasses = Canvass::where('awarded','1')
        ->where('id',$purchaseOrder->canvass_id)->first();
        $ids = common::getListAlphabet($purchaseOrder->pr_id, $canvasses->id);
        // $ids = common::getListAlphabet($po->pr_id, $canvasses->id);
        return view('Procurement.PurchaseOrder.delete', [
            'purchaseOrder' => $purchaseOrder, 
            'id' => $id,
            'pr_view' => $pr_view,
            'ids' => $ids]);
    }

    public function destroy($id) {
        $purchaseOrder = PurchaseOrder::where('id', $id)->delete();
        return redirect(route('purchase-order.index').'?success=true&url=procurement/purchase-order&module='.common::getPRNumberFormat('purchase_orders').'');
    }

    public function getPurchaseOrders($year) {
        if($year == '0000'){
            $po_list = PurchaseOrder::orderBy('created_at', 'asc')->get();
        }else{
            $po_list = PurchaseOrder::orderBy('created_at', 'asc')
            ->whereYear('created_at',$year)->get();
        }
        return DataTables::of($po_list)->addColumn('id',function($model){
            $id = common::getListAlphabet($model->pr_id, $model->canvass_id);
            return $model->po_no;
        })->addColumn('supplier_name',function($model){
            return $model->getCanvass->getSuppliers->supplier_name;
        })->addColumn('pr_id',function($model){
            return $model->getPurchaseRequest->pr_no;
        })->addColumn('action',function($model){ 
            $validation = Receiving::where('po_id',$model->id)->first();
            return "<center>
                        <div class='dropdown'>
                            <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <i class='fa fa-bars'></i>
                            </button>
                            <div class='dropdown-menu dropdown-menu-center' aria-labelledby='dropdownMenuButton'>
                                <a class='dropdown-item' title='View Purchase Order' href='".route('purchase-order.show',$model->id)."'><i class='fa fa-eye' aria-hidden='true'></i> View Record</a>
                                <a class='dropdown-item' title='Edit Purchase Order' href='".route('purchase-order.edit',$model->id)."'><i class='fa fa-pencil' aria-hidden='true'></i> Edit Record</a>
                                <div class='dropdown-divider'></div>
                                <a class='dropdown-item popup_form ".(!$validation ? "" : "disable-link")."' data-toggle='modal' title='Delete Purchase Order Record' style='cursor:pointer;' data-url='".route('purchase.order.delete',$model->id)."'><i class='fa fa-trash' aria-hidden='true'></i> Delete Record</a>
                            </div>
                        </div>
                    </center>";
        })->make(true);
    }

    public function edit($id) {
        $purchaseOrder = PurchaseOrder::where('id', $id)->first();
        $pr_view = PurchaseRequest::where('id', $purchaseOrder->pr_id)->first();
        $canvass = Canvass::where('awarded','1')->where('id',$purchaseOrder->canvass_id)->first();
        $canvass_item = CanvassItem::where('canvass_id',$canvass->id)->get();

        $ids = common::getListAlphabet($purchaseOrder->pr_id, $canvass->id);

        return view('Procurement.PurchaseOrder.edit', [
            'id' => $id,
            'pr_view' => $pr_view,
            'ids' => $ids,
            'purchaseOrder' => $purchaseOrder,
            'pr_no' => common::getPRNumberFormat('purchase_requests',$pr_view,1),
            'canvass_item' => $canvass_item]);
    }

    public function update(Request $request, $id) {
        
        try{
            $purchaseOrder = PurchaseOrder::where('id', $id)->update([
                'po_no' => $request->input('po_num'),
                'po_date' => date('Y/m/d H:i:s', strtotime($request->input('po_date'))),
                'procurement_mode' => $request->input('procurement_mode'),
                'delivery_term' => $request->input('delivery_term'),
                'place_of_delivery' => $request->input('place_of_delivery'),
                'payment_term' => $request->input('payment_term'),
                'delivery_date' => ($request->input('delivery_date') != '' ? date("Y-m-d", strtotime($request->input('delivery_date'))) : null),
            ]);
            $response = array(
                'status' => 'success',
                'desc' => 'Purchase Order successfully updated!'
            );
        }catch (\Exception $exception){
            $response = common::catchError($exception);
        }
        return response()->json($response,200);
    }

}