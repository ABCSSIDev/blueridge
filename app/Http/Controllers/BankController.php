<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
use App\Common;
use Illuminate\Support\Facades\Auth;
use App\BudgetAllocation;
use Yajra\DataTables\DataTables;
class BankController extends Controller
{
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Accounting.Bank.index',[
            'budget_year' => $budget_allocation
        ]);
    }
    public function create()
    {
        return view('Accounting.Bank.create');
    }
    public function store(Request $request)
    {
        try{
            $bank = new Bank();
            $bank->name = $request->input('name');
            $bank->address  = $request->input('address');
            $bank->contact_number = $request->input('contact_number');
            $bank->account_name = $request->input('account_name');
            $bank->account_number = $request->input('account_number');
            $bank->account_type = $request->input('account_type');
            $bank->product_type = $request->input('product_type');
            $bank->initial_deposit = Common::cleanDecimal($request->input('initial_deposit'));
            $bank->created_at = now();
            $bank->save();

            $response = array(
                'status' => 'success',
                'desc' => 'Bank successfully created!'
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }
    public function show($id)
    {
        $bank_data = Bank::where('id',$id)->first();
        return view('Accounting.Bank.show',['bank_data' => $bank_data]);
    }
    public function edit($id)
    {
        $bank_data = Bank::where('id',$id)->first();
        return view('Accounting.Bank.edit',['bank_data' => $bank_data]);
    }
    public function update(Request $request,$id)
    {
        try{
            $edit = Bank::where('id', $id)->update([
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'contact_number' => $request->input('contact_number'),
                'account_name' => $request->input('account_name'),
                'account_number' => $request->input('account_number'),
                'account_type' => $request->input('account_type'),
                'product_type' => $request->input('product_type'),
                'initial_deposit' => Common::cleanDecimal($request->input('initial_deposit')),
                'updated_at' => date('Y/m/d H:i:s')
            ]);

            $response = array(
                'status' => 'success',
                'desc'  => 'Bank Successfully Updated!'
            );
        }
        
        catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);

    }
    public function removeBank($id){
        $bank_data = Bank::where('id',$id)->first();
        return view('Accounting.Bank.delete',['bank_data' => $bank_data]);
    }
    public function destroy($id)
    {
        Bank::where('id', $id)
            ->delete();
        return redirect(route('bank.index'));
    }
    public function getBankList($year){
        if($year == '0000'){
            $bank_list = Bank::all();
        }else{
            $bank_list = Bank::whereYear('created_at',$year)->get();
        }
        return DataTables::of($bank_list)->addColumn('bank_name',function($model){
            return $model->name;
        })->addColumn('account_name',function($model){
            return $model->account_name;
        })->addColumn('account_type',function($model){
            return ($model->account_type == 1?'Savings':'Current');
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("bank.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                            <a class="dropdown-item" href="'.route('bank.edit',$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item popup_form" data-toggle="modal" style="cursor:pointer" data-url="'.route('remove.bank',$model->id).'" title="Delete Bank Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->make(true);
    }
}
