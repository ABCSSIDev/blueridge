<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\GeneralUser;
use App\Role;
use App\Common;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use App\BudgetAllocation;

class ResidentContorller extends Controller
{
    public function index(){
        
        $budget_allocation = BudgetAllocation::all();
        return view('Setup.Residents.index',[
            'budget_year' => $budget_allocation
        ]);
    }
    public function create()
    {
        $role = Role::all();
        return view('Setup.Residents.create',[
            'role' => $role
        ]);
    } 
    public function AutocompleteResidents(Request $request)
    {
        $search = $request->get('term');
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->get();
        return response()->json($result);
    } 
    public function show($id)
    {
        $GeneralUser = GeneralUser::where('id',$id)->first();
        $role = Role::all();
        $name = explode(' ', $GeneralUser->name, 3);
        $address = explode(' ', $GeneralUser->address, 2);
        return view('Setup.Residents.view',[
            'GeneralUser' => $GeneralUser,
            'name' => $name,
            'address' => $address,
            'role' => $role
        ]);
    }
    public function FunctionResidents($id)
    {
        $GeneralUser = GeneralUser::where('id',$id)->first();
        $role = Role::all();
        $name = explode(' ', $GeneralUser->name, 3);
        $address = explode(' ', $GeneralUser->address, 2);
        return view('Setup.Residents.delete',[
            'GeneralUser' => $GeneralUser,
            'name' => $name,
            'address' => $address,
            'role' => $role
        ]);
    }
   
    public function store(Request $request)
    {
        try{
            $result = GeneralUser::where('name', $request->get('first_name') .' '.$request->get('middle_initial').' '.$request->get('last_name'))->first();

            if(!$result){
                $data = $request->all();
                $name = $request->get('first_name').' '.$request->get('middle_initial').' '.$request->get('last_name');
                $address = $request->get('address_no').' '.$request->get('street');
                // dd($data);
                GeneralUser::create([
                    'name' => $name,
                    'role_id' =>$request->input('role_type'),
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'quantifier' =>$request->input('quantifier'),
                    'middle_initial' =>$request->input('middle_initial'),
                    'contact_number' => $request->input('contact_number'),
                    'address' => $address,
                    'citizenship' =>$request->input('citizenship'),
                    'birth_place' => $request->input('birth_place'),
                    'civil_status' => $request->input('civil_status'),
                    'birth_date' => date('Y/m/d H:i:s',strtotime($request->input('birth_date'))),
                    'occupation' => $request->input('occupation'),
                    'sex' => $request->input('sex'),
                    'cluster' =>$request->input('cluster_no'),
                    'household' => $request->input('household_no'),
                    'religion' => $request->input('religion'),
                    'health_status' => $request->input('health_status'),
                    'highest_education' =>$request->input('highest_education'),
                    'created_at' => date('Y/m/d H:i:s')
                ]);
                // GeneralUser::create($request->all());
                
                $response = array(
                    'status' => 'success',
                    'desc' => 'Residents created!'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Residents name already existing!'
                );
            }
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);


    }
    public function edit($id)
    {
        $GeneralUser = GeneralUser::where('id',$id)->first();
        $role = Role::all();
        $name = explode(' ', $GeneralUser->name, 3);
        $address = explode(' ', $GeneralUser->address, 2);
        return view('Setup.Residents.edit',[
            'GeneralUser' => $GeneralUser,
            'name' => $name,
            'address' => $address,
            'role' => $role
        ]);
    }
    public function FunctionRemoveResidents($id)
    {
        $supplier = GeneralUser::where('id', $id)->first();
        GeneralUser::where('id', $id)->delete();
        return redirect(route('residents.index'));
    }
     public function update(Request $request, $id)
    {
        try{
            $result = GeneralUser::where('name', $request->get('first_name') .' '.$request->get('middle_initial').' '.$request->get('last_name'))->where('id','!=',$id)->first();

            if(!$result){
                $data = $request->all();
                $name = $request->get('first_name').' '.$request->get('middle_initial').' '.$request->get('last_name');
                $address = $request->get('address_no').' '.$request->get('street');
                // dd($data);
                GeneralUser::where('id', $id)->update([
                    'name' => $name,
                    'role_id' =>$request->input('role_type'),
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'quantifier' =>$request->input('quantifier'),
                    'middle_initial' =>$request->input('middle_initial'),
                    'contact_number' => $request->input('contact_number'),
                    'address' => $address,
                    'occupation' => $request->input('occupation'),
                    'citizenship' =>$request->input('citizenship'),
                    'birth_place' => $request->input('birth_place'),
                    'civil_status' => $request->input('civil_status'),
                    'birth_date' => date('Y/m/d H:i:s',strtotime($request->input('birth_date'))),
                    'sex' => $request->input('sex'),
                    'cluster' =>$request->input('cluster_no'),
                    'household' => $request->input('household_no'),
                    'religion' => $request->input('religion'),
                    'health_status' => $request->input('health_status'),
                    'highest_education' =>$request->input('highest_education'),
                    'created_at' => date('Y/m/d H:i:s')
                ]);
                // GeneralUser::create($request->all());
                
                $response = array(
                    'status' => 'success',
                    'desc' => 'Residents Update!'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Residents name already existing!'
                );
            }
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }
    public function getResidents($year){
        if($year == '0000'){
            $residents_list = GeneralUser::whereNull('deleted_at')->get();
        }else{
            $residents_list = GeneralUser::whereNull('deleted_at')->whereYear('created_at',$year)->get();
        }
        
        return DataTables::of($residents_list)->addColumn('id',function($model){
            return $model->id;
        })->addColumn('name',function($model){
            return $model->name;
        })->addColumn('contact_number',function($model){
            return (empty($model->contact_number)?'N/A':$model->contact_number);
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown" style="text-align:center">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="'.route("residents.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                            <a class="dropdown-item" href="'.route("residents.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item popup_form" data-toggle="modal" data-url="'.route("residents.FunctionResidents",$model->id).'" title="Delete Residents Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                            </div>
                        </div>
                    </center>';
        })->make(true);
    }
}
