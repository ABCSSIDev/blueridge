<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class DocumentController extends Controller
{

    public function getDocument($id){
        $status = array();
        $document = Document::findOrFail($id);
        $status['view'] = view($document->url_content)->render();
        $status['signatory'] = $document->signatory_count;

        return response()->json($status,200);
    }

    public function refreshSelect(){
        $options = '<option value="">Select Document</option>';
        $docs = Document::whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('signatory_templates')
                ->whereRaw('signatory_templates.document_id = documents.id')
                ->whereRaw('signatory_templates.deleted_at is  null');
        })->get();
        foreach($docs as $document){
            $options .= "<option value=".$document->id." data-url=".route('fetch.document',$document->id).">".$document->name."</option>";
        }
        return $options;
    }
}
