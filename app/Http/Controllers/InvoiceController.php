<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\GeneralUser;
use App\ORPad;
use App\Role;
use App\AccountLedger;
use App\Common;
use App\InvoiceItem;
use App\BudgetAllocation;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Accounting.Invoice.index',[
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::where('name', 'LIKE', '%Payee%')->first();
        $account_ledger = AccountLedger::where('account_type',0)->get();

        $or_number = null;
        $or_settings = ORPad::orderBy('or_pad_start', 'ASC')->first();
        $invoice = Invoice::all();
        if ($or_settings != null){
            $start = $or_settings->first()['or_pad_start'];
            $or_number = $invoice->count() + $start;
        }

        return view('Accounting.Invoice.create',[
            'account_ledger' => $account_ledger,
            'role' => $role,
            'or_number' => $or_number
        ]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{

            $role = Role::where('name', 'LIKE', '%Payee%')->first();
            $results = GeneralUser::where('name', 'like', '' .$request->input('payee').'')->where('role_id',$role->id)->get();
            if(count($results)>0){
                $user_id = $results[0]->id;
            }else{
                $user_id = $this->insertUser($request);
            }
            $Invoicedata = Invoice::where('or_number',$request->input('or_no'))->count();
           
            if($Invoicedata == 0){
                $new_invoice = new Invoice([
                    'payee_id' => $user_id,
                    'ledger_id' => $request->input('ledger'),
                    'collection_nature' => $request->input('nature_of_collection'),
                    'or_number' => $request->input('or_no'),
                    'invoice_date' => date('Y/m/d H:i:s', strtotime($request->input('invoice_date'))),
                    'due_date' => Common::formatDate("Y-m-d",$request->input('due_date')),
                    'created_at' => date('Y/m/d H:i:s')
                ]);

                $new_invoice->save();

                InvoiceItem::create([
                    'invoice_id' => $new_invoice->id,
                    'description' => $request->input('nature_of_collection'),
                    'quantity' => '1.00',
                    'unit_price' => str_replace(',', '', $request->input('unit_price')),
                    'created_at' => date('Y/m/d H:i:s')
                ]);
                // $or_number = null;
                // $or_settings = ORPad::all();
                // $invoice = Invoice::all();
                // if ($or_settings != null){
                //     $start = $or_settings->first()['or_pad_start'];
                //     $or_number = $invoice->count() + $start + 1;
                // }
                $response = array(
                    'status' => 'success',
                    'desc' => 'Invoice successfully created!',
                    'refresh_or' => true,
                    'or_number' => $new_invoice->or_number+1
                );
                
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'This OR Number already Existed!'
                );
            }
                

            
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::where('id', $id)->with('getPayment')->first();

        return view('Accounting.Invoice.show', ['invoice' => $invoice]);
    }
    public function functionORPad()
    {
        $or_settings = ORPad::orderBy('id','DESC')->first();
        $or_pads = ORPad::all();
        $invoice = Invoice::all();
        return view('Accounting.Invoice.or_pad',['or_settings' => $or_settings,'or_pads' => $or_pads,'invoice' => $invoice]);
    }
    public function functionCreateORPad(Request $request)
    {
        // return $request;
        try{
            $new_pad = new ORPad([
                'or_pad_start' => $request->input('pad_start'),
                'or_pad_quantity' =>$request->input('pad_end'),
                'created_at' => date('Y/m/d H:i:s')
            ]);

            $new_pad->save();
            $response = array(
                'status' => 'success',
                'desc' => 'OR Pad successfully created!',
                'refresh_or_pad' => true,
                'or_number' => (int)$new_pad->or_pad_start
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200); 
    }
    public function autocompleteORPad(Request $request)
    {
        $search = $request->get('OR');
        $resultORPad = ORPad::where('or_pad_start',$search)->count();
        if($resultORPad>0){
            $result = 'duplicat';
        }
        $ORPadData = ORPad::orderBy('or_pad_start', 'DESC')->first();
        $LastORNumber =1;
        // $resultInvoice = Invoice::where('or_number',$search)->orderBy('or_number', 'DESC')->first();
        if($LastORNumber == $search){
            $result = 'maxorpad';
        }
        return response()->json($result);
    } 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::where('id',$id)->first();
        $account_ledger = AccountLedger::where('account_type','0')->get();
        $role = Role::where('name', 'LIKE', '%Payee%')->first();
        return view('Accounting.Invoice.edit',[
            'invoice' => $invoice,
            'account_ledger' => $account_ledger,
            'role' => $role
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{
            $role = Role::where('name', 'LIKE', '%Payee%')->first();
            $results = GeneralUser::where('name', 'like', '' .$request->input('payee').'')->where('role_id',$role->id)->get();
            if(count($results)>0){
                $user_id = $results[0]->id;
            }else{
                $user_id = $this->insertUser($request);
            }

            $edit_invoice = Invoice::find($id);
            $edit_invoice->payee_id = $user_id;
            $edit_invoice->ledger_id = $request->input('ledger');
            $edit_invoice->collection_nature = $request->input('nature_of_collection');
            $edit_invoice->or_number = $request->input('or_no');
            $edit_invoice->invoice_date = date('Y/m/d H:i:s', strtotime($request->input('invoice_date')));
            $edit_invoice->due_date = date('Y/m/d H:i:s', strtotime($request->input('due_date')));
            $edit_invoice->updated_at = date('Y/m/d H:i:s');

            $edit_invoice->save();

            $edit_invoice_item = InvoiceItem::where('invoice_id',$request->input('invoice_id'))->first();
            $edit_invoice_item->description = $request->input('nature_of_collection');
            $edit_invoice_item->quantity = str_replace(',', '', $request->input('quantity'));
            $edit_invoice_item->unit_price = str_replace(',', '', $request->input('unit_price'));
            $edit_invoice_item->updated_at = date('Y/m/d H:i:s');

            $edit_invoice_item->save();

            $response = array(
                'status' => 'success',
                'desc' => 'Invoice successfully updated!'
            );

        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::where('id', $id)
            ->first();
        Invoice::where('id', $id)
            ->delete();
        InvoiceItem::where('invoice_id', $invoice->id)
            ->delete();
        return redirect(route('invoice.index').'?success=true&url=/accounting/invoice&module='.$invoice->or_number.'');
    }

   

    public function getInvoiceList($year){
        if($year == '0000'){
            $invoice_list = Invoice::all();
        }else{
            $invoice_list = Invoice::whereYear('invoice_date',$year)->get();
        }
        return DataTables::of($invoice_list)->addColumn('or_number',function($model){
            return $model->or_number.(is_null($model->getPayment) ? '<br/><span class="badge badge-pill badge-primary">Open</span>' : '<br/><span class="badge badge-pill badge-success">Paid</span>');
        })->addColumn('payee',function($model){
            return $model->getPayee->name;
        })->addColumn('nature_of_collection',function($model){
            return $model->collection_nature;
        })->addColumn('due_date',function($model){
            $date = new \DateTime($model->due_date);
            $now = new \DateTime();
            return ($date < $now && is_null($model->getPayment)? "<span class='overdue-date' data-toggle='tooltip' data-placement='right' title='".Common::computeOverdue($model->due_date)."'><b>".Common::formatDate("m/d/Y",$model->due_date)."</b></span>" : Common::formatDate("m/d/Y",$model->due_date));
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("invoice.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                            <a class="dropdown-item '.(!is_null($model->getPayment)?'btn-disabled':'').'" href="'.route('invoice.edit',$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item popup_form '.(!is_null($model->getPayment)?'btn-disabled':'').'" data-toggle="modal" style="cursor:pointer" data-url="'.route("remove.invoice",$model->id).'" title="Delete Invoice Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>
                    <style>
                        .btn-disabled,
                        .btn-disabled[disabled] {
                            opacity: .4;
                            cursor: default !important;
                            pointer-events: none;
                        }
                    </style>';
        })->escapeColumns([])
        ->make(true);
    }

    public function removeInvoice($id){
        $invoice = Invoice::where('id', $id)
        ->first();
    
        return view('Accounting.Invoice.delete', ['id' => $id, 'invoice' => $invoice]);
    }

    public function autocompletePayee(Request $request)
    {
        $search = $request->get('term');
        $role = Role::where('name', 'LIKE', '%Payee%')->first();
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->where('role_id',$role->id)->get();
        return response()->json($result);
    } 
    public function autocompleteOr(Request $request)
    {
        $search = $request->get('OR');
        $ORPadDatas = ORPad::all();
        $optotalcount = 0;
        foreach($ORPadDatas as $data){
            $optotalcount += $data->or_pad_quantity;
        }
        $Invoicecount = Invoice::all();
        $ORPadASC = ORPad::orderBy('id', 'ASC')->first();
        $rangeto = $ORPadASC->or_pad_start + $optotalcount;
        $rangefrom = $ORPadASC->or_pad_start;
        $invtotalcount = 0;
        foreach($Invoicecount as $data){
            if($data->or_number >= $rangefrom && $data->or_number <= $rangeto){
                $invtotalcount += 1;
            }
        }
        $Invoicedata = Invoice::where('or_number',$search)->count();
        $result = '';
        $ORPadDESC = ORPad::orderBy('id', 'DESC')->first();
        $LastORNumber = $ORPadDESC->or_pad_start + $ORPadDESC->or_pad_quantity;
        // dd($total);
        if($Invoicedata>0){
            $result = 'duplicat';
        }
        else if($search > $LastORNumber || $search < $ORPadASC->or_pad_start){
            $result = 'outofrange';
        }
        else if($invtotalcount == $ORPadDESC->or_pad_quantity){
            $result = 'maxorpad';
        }
        else if($search <= 0){
            $result = 'zero';
        }
        
        
        return response()->json($result);
    } 
    public function insertUser(Request $request)
    {
        $insert_user = GeneralUser::create([
            'role_id' => $request->input('role_id'),
            'name' => $request->input('payee'),
            'contact_number' => '',
            'address' => $request->input('address'),
            'created_at' => date('Y/m/d H:i:s')
        ]);
        return  $insert_user->id;
    }
}
