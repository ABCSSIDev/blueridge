<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\Category;
use Yajra\DataTables\DataTables;
use App\Common;
use App\BudgetAllocation;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Setup.Categories.index',[
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('category_id', NULL)
            ->get();

        return view('Setup.Categories.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Category::where('category_name', $request->input('category_name'))->exists()){
            $response = array(
                'status' => 'error',
                'desc'  => 'Category already exists!'
            );
        }else{
            try{
                $newCategory = Category::create([
                    'category_name' => $request->input('category_name'),
                    'is_stored' => ($request->input('inventory') ? 1 : 0)
                ]);
                    
                $response = array(
                    'status' => 'success',
                    'desc'  => 'Category successfully created!'
                );
                }catch (\Exception $exception){
                    $response = Common::catchError($exception);
                }
            }
        return response()->json($response,200);   
    }

    public function storeSubCateg(Request $request){
      
        if(Category::where([
            ['category_id', $request->input('cat_name')],
            ['category_name', $request->input('sub_category_name')]
        ])->exists()){
            $response = array(
                'status' => 'error',
                'desc'  => 'Sub-category already exists!'
            );
        }else{
            try{
            $newSubCateg = Category::create([
                'category_id' => $request->input('cat_name'),
                'category_name' => $request->input('sub_category_name'),
                'is_stored' => ($request->input('inventory') ? 1 : 0)
                
            ]);

            $newSubCateg->save();
            $response = array(
                'status' => 'success',
                'desc'  => 'Sub Category successfully created!'
            );
            }catch (\Exception $exception){
                $response = Common::catchError($exception);
            }
        }
        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::where('id', $id)
            ->first();

        return view('Setup.Categories.show', ['categories' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id',$id)
            ->first();
        
        $categories = Category::where('category_id', NULL)
            ->get();

        return view('Setup.Categories.edit', ['category' => $category, 'categories' => $categories, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            
            $category_id = Category::where('id', $id)->first();
            
            if($category_id->category_id == NULL){ //if Category Only
                if(Category::where('category_name', $request->input('category_name'))->where('category_id',null)->where('id','!=',$category_id->id)->exists()){
                    $response = array(
                        'status' => 'error',
                        'desc'  => 'Category already exists!'
                    );
                }else{
                    $category = Category::where('id', $id)->update([
                        'category_name' => $request->input('category_name')     
                    ]);
                                    
                    $response = array(
                        'status' => 'success',
                        'desc'  => 'Category successfully Edited!'
                    );
                }
            }else{ //if Sub Category Only
                $inventory = $request->input('inventory') ? 1: 0;
                if(Category::where([
                    ['category_id', $request->input('categ_name')],
                    ['category_name', $request->input('category_name')]
                ])->exists()){
                    if($category_id->is_stored != $inventory){
                        $category = Category::where('id', $id)->update([
                            'is_stored' => ($request->input('inventory') ? 1 : 0)  
                        ]);
                        $response = array(
                            'status' => 'success',
                            'desc'  => 'Inventory updated!'
                        );
                    }else{
                        $response = array(
                            'status' => 'error',
                            'desc'  => 'Sub-category already exists!'
                        );
                    }
                }else{
                    $category = Category::where('id', $id)->update([
                        'category_id' => $request->input('categ_name'),
                        'category_name' => $request->input('category_name'),
                        'is_stored' => ($request->input('inventory') ? 1 : 0)  
                    ]);
                           
                    $response = array(
                        'status' => 'success',
                        'desc'  => 'Sub-category successfully Edited!'
                    );
                }
            }

        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where('id', $id)->first();
        Category::find($id)->delete(); // delete category

        return redirect(route('category.index').'?success=true&url=setup/category&module='.$category->category_name.'');
    }

    public function delete($id){
        $category = Category::where('id', $id)
            ->first();
        return view('Setup.Categories.delete', ['category' => $category, 'id' => $id, 'validation' => $this->validationCategory($id)]);
    }

    public function getCategories($year){
        if($year == '0000'){
            $categories = Category::whereNull('deleted_at')->get();
        }else{
            $categories = Category::whereNull('deleted_at')->whereYear('created_at',$year)->get();
        }
        return DataTables::of($categories)->addColumn('category_name',function($model){
            return self::classifyCategory($model)["category_name"];
        })->addColumn('category_id',function($model){
            return self::classifyCategory($model)["sub_category_name"] ? self::classifyCategory($model)["sub_category_name"] : "N/A";
        })->addColumn('is_stored',function($model){
            return ($model->category_id != NULL ? ($model->is_stored ? "Yes" : "No") : 'No');
        })->addColumn('action',function($model){
            return "<center>
                        <div class='dropdown'>
                            <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <i class='fa fa-bars'></i>
                            </button>
                            <div class='dropdown-menu dropdown-menu-center' aria-labelledby='dropdownMenuButton'>
                                <a class='dropdown-item' title='View Category' href='".route('category.show',$model->id)."'><i class='fa fa-eye' aria-hidden='true'></i> View Record</a>
                                <a class='dropdown-item' title='Edit Category' href='".route('category.edit',$model->id)."'><i class='fa fa-pencil' aria-hidden='true'></i> Edit Record</a>
                                <div class='dropdown-divider'></div>
                                <a class='dropdown-item popup_form' data-toggle='modal' title='Delete Category Record' style='cursor:pointer' data-url='".route('category.delete',$model->id)."'><i class='fa fa-trash' aria-hidden='true'></i> Delete Record</a>
                            </div>
                        </div>
                    </center>";
        })->make(true);
    }
    public function validationCategory($category_id){
        $Inventory = Inventory::where("category_id", $category_id)->count();
        return $Inventory;
    }
    public function classifyCategory($category_table){
        $data["category_name"] = "";
        $data["sub_category_name"] = "";
        if($category_table->id == $category_table->category_id){
            $data["category_name"] = $category_table->category_name;

        }else{
            if(empty($category_table->category_id)){
                $data["category_name"] = $category_table->category_name;
            }else{
                $parent_category = Category::where("id", $category_table->category_id)->first();
                $data["category_name"] = $parent_category->category_name;
                $data["sub_category_name"] = $category_table->category_name; 
            }
        }

        return $data;
    }
}
