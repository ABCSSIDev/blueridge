<?php

namespace App\Http\Controllers;

use App\AssignedSupplyItem;
use App\Bank;
use App\CanvassItem;
use App\Cedula;
use App\DisbursementVoucher;
use App\Payment;
use App\Payroll;
use App\PurchaseOrder;
use App\PurchaseRequest;
use App\PurchaseRequestItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AccountGroup;
use App\BudgetAllocation;
use App\BudgetAllocationGroup;
use App\BudgetAllocationItem;
use App\AccountLedger;
use App\Category;
use App\Invoice;
use App\ROA;
use App\Version;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{


    private $group_id = array();
    private $budget = array();
    private $budget_final = array();


    private $array_group = array();
    private $array_temp = array();
    private $array_group_actual = array();
    private $array_temp_actual = array();
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function dateSort($a,$b){
        return strtotime($a) - strtotime($b);
    }

    public function index()
    {
//        dd(self::getBankStatement());
        $status_version = $this->checkVersion();
        $get_v = Version::orderBy('id','DESC')->first();
        if(Auth::check()){
            $array = array();
            $invoices = Invoice::whereNotExists(function ($query){
                $query->select(DB::raw(1))
                    ->from('payments')
                    ->whereRaw('payments.payable_id = invoices.id')
                    ->whereRaw('payments.payable_type like "%Invoice%"')
                    ->whereRaw('payments.deleted_at is  null');
            })->whereDate('due_date','<',date('Y-m-d'))->get();
            $disbursements = DisbursementVoucher::whereNotExists(function ($query){
                $query->select(DB::raw(1))
                    ->from('payments')
                    ->whereRaw('payments.payable_id = disbursement_vouchers.id')
                    ->whereRaw('payments.payable_type like "%DisbursementVoucher%"')
                    ->whereRaw('payments.deleted_at is  null');
            })->whereDate('due_date','<',date('Y-m-d'))->get();
            $payrolls = Payroll::whereNotExists(function ($query){
                $query->select(DB::raw(1))
                    ->from('payments')
                    ->whereRaw('payments.payable_id = payrolls.id')
                    ->whereRaw('payments.payable_type like "%Payroll%"')
                    ->whereRaw('payments.deleted_at is  null');
            })->whereDate('due_date','<',date('Y-m-d'))->get();
            $cedulas = Cedula::whereNotExists(function ($query){
                $query->select(DB::raw(1))
                    ->from('payments')
                    ->whereRaw('payments.payable_id = cedulas.id')
                    ->whereRaw('payments.payable_type like "%Cedula%"')
                    ->whereRaw('payments.deleted_at is  null');
            })->whereDate('due_date','<',date('Y-m-d'))->get();
            $budget_allocation = BudgetAllocation::all();
            foreach ( $invoices as $invoice) {
                $array['due_date'][$invoice->due_date]['invoice'][] = $invoice->id;
            }
            foreach ($disbursements as $disbursement){
                $array['due_date'][$disbursement->due_date]['disbursement'][] = $disbursement->id;
            }
            foreach ( $cedulas as $cedula) {
                $array['due_date'][$cedula->due_date]['cedula'][] = $cedula->id;
            }
            foreach ($payrolls as $payroll){
                $array['due_date'][$payroll->due_date]['payroll'][] = $payroll->id;
            }
            if (!empty($array)){
                ksort($array['due_date']);
            }
            $bank =  Bank::all();
            return view('home',[
                                'budget_year' => $budget_allocation,
                                'overdues' => $array,
                                'show_release' => $status_version,
                                'bank' => $bank,
                                'version' => $get_v]);
        }
        else{
            
            return view('auth.login');
        }
    }

    public function getChart(){
        // self::inventoryChart();

        $response = array(
            'budget_alloc' => self::budgetChart(),
            'inventory' => self::inventoryChart(),
            'procurement' => self::getProcurement(),
            'financial_overview' => self::getFinancialOverview(),
            'bank_statement' => self::getBankStatement(1)
        );
        return response()->json($response,200);
    }
    public function functionBank(Request $request){
        $bank = Bank::where('id',$request->get('id'))->first();
        $response = array(
            'initial_deposit' => $bank->initial_deposit,
            'bank_statement' => self::getBankStatement($request->get('id'))
        );
        return  $response;
    }
    public function budgetYearChart($year){
        $response = array(
            'budget_alloc' => self::budgetChart($year)
        );
        return response()->json($response,200);
    }
    public function getBankStatement($id){
        $status = array();
        $bank = Bank::findOrFail($id);
        $starting_balance = $bank->initial_deposit;
        $date = date("Y-m-01", strtotime("-6 months"));
        $from_date = $date;
        $name = $bank->name;
        for ($i = 0; $i <= 6;$i++){
            $current_date = date("M Y", strtotime( date($date)." +$i months"));
            $status['labels'][] = date("M Y", strtotime( date($date)." +$i months"));
            $to_date = date('Y-m-t',strtotime($current_date));
            $payments = Payment::where('date_received','<=',$to_date)->get();
            $running_balance = 0;
            foreach ($payments as $payment){
                if ($payment->payable_type == 'App\Invoice' || $payment->payable_type == 'App\Cedula'){
                    $running_balance += $payment->amount_received;
                }else{
                    $running_balance -= $payment->amount_received;
                }
            }
            $status['balance'][] = $starting_balance + $running_balance;
        }
        $status['labels'] = ($status['labels']);
        $status['name'] = $name;
        $status['end_balance'] = end($status['balance']);
        return $status;
    }

    public function getFinancialOverview(){
        $or_count = Invoice::all()->count();
        $dv = ROA::all()->count();

        $status = array(
            'receivables' => self::getReceivables(),
            'payables' => self::getPayables(),
            'or' => $or_count,
            'dv' => $dv
        );
        return $status;
    }

    public function getPayables(){
        $payables = 0;
        $roas = ROA::all();
        $payroll = Payroll::all();;
        foreach ($payroll as $pay){
            $payables += ($pay->getPayrollItems->sum('salary') + $pay->getPayrollItems->sum('other_benefits')) - $pay->getPayrollItems->sum('bir');
        }
        foreach($roas as $roa){
            $payables += $roa->total_amount;
        }
        return $payables;
    }

    public function getReceivables(){
        $receivables = 0;
        $invoices = Invoice::all();
        $cedulas = Cedula::all();
        foreach ($cedulas as $cedula){
            $receivables += $cedula->item_1 + $cedula->item_2 + $cedula->item_3 + $cedula->item_4 + $cedula->item_5;
        }
        foreach($invoices as $invoice){
            $unit_price = (int)$invoice->getInvoice->unit_price;
            $receivables += $unit_price; 
        }
        return $receivables;
    }

    public function inventoryChart(){
        return array(
            "inventory_per_category" => self::getInventoryPerCategory(),
            "recently_assigned" => self::getLatestAssignment()
        );
    }

    public function budgetChart($year = null){
        $status = array();
        $groups = AccountGroup::all();
        // $groups = AccountGroup::where('id',2)->get();
        $budget_year = !empty($year)?$year:now();
        $allocation = BudgetAllocation::where('year',$budget_year)->first();
        if($allocation != null){
            self::checkGroups($allocation);
        }
        self::getActualCost();
        foreach($groups as $group){
            $max = rand(1,7966772.09);
            $status['labels'][] = $group->group_name;
            $status['allocated'][] = (array_key_exists($group->id,$this->array_group) ? array_sum($this->array_group[$group->id]['amount']) : 0);
            $status['expended'][] = (array_key_exists($group->id,$this->array_group_actual) ? array_sum($this->array_group_actual[$group->id]['amount']) : 0);
        }
        return $status;
    }

    public function getActualCost(){
        $groups = AccountGroup::all();
        $roas = ROA::all();
        $invoices = Invoice::all();
        $payrolls = Payroll::all();
        $cedulas  = Cedula::all();

        foreach($groups as $group){
            $this->array_group_actual[$group->id]['amount'][] = 0;
            $this->array_group_actual[$group->id]['group_name'] = $group->group_name;
        }      

        foreach($invoices as $invoice){
            $this->array_temp_actual[$invoice->getLedger->group_id][] = (float)$invoice->getInvoice->unit_price;
        }

        foreach($roas as $roa){
            $this->array_temp_actual[$roa->getLedger->group_id][] = (float)$roa->total_amount;
        }

        foreach ($payrolls as $payroll){
            $this->array_temp_actual[$payroll->getLedger->group_id][] = (float)($payroll->getPayrollItems->sum('salary') + $payroll->getPayrollItems->sum('other_benefits')) - $payroll->getPayrollItems->sum('bir');
        }

        // foreach ($cedulas as $cedula){
        //     $this->array_temp_actual[$cedula->getLedger->group_id][] = (float)($cedula->item_1 + $cedula->item_2 + $cedula->item_3 + $cedula->item_4 + $cedula->item_5);
        // }

        self::computeActual();
//        dd(array_key_exists(18,$this->array_group_actual) ? array_sum($this->array_group_actual[18]['amount']) : 0);
    }

    public function checkGroups($allocation){
        $groups = AccountGroup::all();

        foreach($groups as $group){
            $this->array_group[$group->id]['amount'][] = 0;
            $this->array_group[$group->id]['group_name'] = $group->group_name;
        }        

        foreach($allocation->getBudgetAllocationItems as $item){
            $this->array_temp[$item->getLedger->group_id][] = (float)$item->amount;

            // $this->temp_budget[$item->getLedger->group_id][] = $item->amount;
            // $this->ledger_temp[$item->ledger_id][] = $item->amount;
        }

        self::getParentGroup();

    }

    public function computeActual(){
        $temp_array = array();
        foreach($this->array_temp_actual as $key => $temp){
            $this->array_group_actual[$key]['amount'][] = array_sum($temp);
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                
                $temp_array[$check_if_has_group->getGroup->id][] = array_sum($temp);
            }
        }
        $this->array_temp_actual = $temp_array;
        if(count($this->array_temp_actual) > 0){
            self::computeActual();
        }
    }

    public function getParentGroup(){
        $temp_array = array();
        foreach($this->array_temp as $key => $temp){
            $this->array_group[$key]['amount'][] = array_sum($temp);
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                
                $temp_array[$check_if_has_group->getGroup->id][] = array_sum($temp);
            }

        }
        $this->array_temp = $temp_array;
        if(count($this->array_temp) > 0){
            self::getParentGroup();
        }
    }
    public function getSubGroups(){
        foreach($this->budget as $key => $object){
            
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                $this->budget[$check_if_has_group->getGroup->id][] = array_sum($object);
                // $check_if_has_group
            }
            $this->budget_final[$key][] = array_sum($object);
            unset($this->budget[$key]);
        }
        $response = array(
            'budget' => $this->budget,
            'budget_final' => $this->budget_final
        );
        if(count($this->budget) > 0){
            self::getSubGroups();
        }
        // self::getSubGroups();
        
        // dd($response);
    }

    public function getProcurement(){
        return array(
            'purchase_order' => self::getPurchaseOrder(),
            'purchase_request_category' => self::getPurchaseRequestPerCategory()
        );
    }

    public function getPurchaseOrder(){
        $purchase_orders = PurchaseOrder::all();
        $total = 0;
        foreach($purchase_orders as $purchase_order){
            $canvass_items = CanvassItem::where('canvass_id', $purchase_order->canvass_id)->get();
            foreach ($canvass_items as $canvass_item){
                $total += $canvass_item->available_quantity*$canvass_item->unit_price;
            }
        }
        return $total;
    }

    public function getPurchaseRequestPerCategory(){
        $status = array();
        $categories = DB::table('purchase_request_items as pri')
            ->join('inventory_names as in', 'pri.inventory_name_id', '=', 'in.id')
            ->join('categories', 'in.category_id', '=', 'categories.id')
            ->select('categories.category_name', DB::raw('count(in.category_id) as category_count'))
            ->groupBy('in.category_id')
            ->get();

        foreach ($categories as $category){
            $status["label"][] = $category->category_name;
            $status["data"][] = $category->category_count;
        }

        return $status;
    }

    public function getInventoryPerCategory(){
        //retrieves all data of inventory per category received each month
        $categories = array();
        $current_categories = array();
        $inventories = DB::table('inventories as inv')
            ->join('categories', 'inv.category_id', '=', 'categories.id')
            ->select('categories.category_name', DB::raw('count(inv.category_id) as category_count'))
            ->groupBy('inv.category_id')
            ->get();
        $receivings  = DB::table("receiving_items as ri")
            ->join('canvass_items as ci', 'ri.canvass_item_id', '=', 'ci.id')
            ->join('purchase_request_items as pri', 'ci.pr_item_id', '=', 'pri.id')
            ->join('inventory_names as in', 'pri.inventory_name_id', '=', 'in.id')
            ->join('categories', 'in.category_id', '=', 'categories.id')
            ->select('categories.category_name', DB::raw('count(in.category_id) as category_count'), 'categories.id')
            ->groupBy('in.category_id')
            ->get();

        foreach($inventories as $inventory){
            if(!empty($categories[$inventory->category_name])){
                $categories[$inventory->category_name] += $inventory->category_count;
            }else{
                $categories[$inventory->category_name] = $inventory->category_count;
            }
        }

        foreach($receivings as $receiving){
            if(!empty($categories[$receiving->category_name])){
                $categories[$receiving->category_name] += $receiving->category_count;
            }else{
                $categories[$receiving->category_name] = $receiving->category_count;
            }
        }

        $inventories_current_year = DB::table('inventories as inv')
            ->whereRaw('year(`inv`.`created_at`) = ?', array(date('Y')))
            ->join('categories', 'inv.category_id', '=', 'categories.id')
            ->select('categories.category_name', DB::raw('count(inv.category_id) as category_count'))
            ->groupBy('inv.category_id')
            ->get();


        $receivings_current_year = DB::table("receiving_items as ri")
            ->whereRaw('year(`ri`.`created_at`) = ?', array(date('Y')))
            ->join('canvass_items as ci', 'ri.canvass_item_id', '=', 'ci.id')
            ->join('purchase_request_items as pri', 'ci.pr_item_id', '=', 'pri.id')
            ->join('inventory_names as in', 'pri.inventory_name_id', '=', 'in.id')
            ->join('categories', 'in.category_id', '=', 'categories.id')
            ->select('categories.category_name', DB::raw('count(in.category_id) as category_count'), 'categories.id')
            ->groupBy('in.category_id')
            ->get();

        foreach ($inventories_current_year as $inventory_current_year){
            if(!empty($current_categories[$inventory_current_year->category_name])){
                $current_categories[$inventory_current_year->category_name] += $inventory_current_year->category_count;
            }else{
                $current_categories[$inventory_current_year->category_name] = $inventory_current_year->category_count;
            }
        }

        foreach ($receivings_current_year as $receiving_current_year){
            if(!empty($current_categories[$receiving_current_year->category_name])){
                $current_categories[$receiving_current_year->category_name] += $receiving_current_year->category_count;
            }else{
                $current_categories[$receiving_current_year->category_name] = $receiving_current_year->category_count;
            }
        }

        return array(
            "total_category_count" => array_values($categories),
            "category_label" => array_keys($categories),
            "current_categories_count" => array_values($current_categories),

        );
    }

    public function getLatestAssignment(){
        $assigned = AssignedSupplyItem::OrderBy('created_at', 'desc')->limit(10)->get();
        $table ="";

        foreach ($assigned as $assign){
            $table .= '<tr><td>'.$assign->AssignedSupplies->GeneralUserDetails->name.'</td><td>'.$assign->InventoryDetails->getInventoryName->description.'</td></tr>';
        }

        return $table;
    }
    public function checkVersion(){
        $get_v = Version::orderBy('id','DESC')->first();
        $status = true;
        $arr = array();
        if($get_v->is_view != null){
            $arr = json_decode($get_v->is_view);
            if (in_array(Auth::user()->id, $arr)) {
                $status = false;
            }
            else{
                if(!in_array(Auth::user()->id,$arr, true)){
                    array_push($arr,Auth::user()->id);
                }
                $this->editVersion($get_v->id,$arr);
                $status = true;
            }
        }
        else{
            $er = array(Auth::user()->id);
            $rry = json_encode($er);
            $edit_version = Version::find($get_v->id);
            $edit_version->is_view = $rry;
            $edit_version->save();
        }

        return $status;
    }

    public function editVersion($id,$arr){
        $edit_version = Version::find($id);
        $edit_version->is_view = json_encode($arr);
        $edit_version->save();
    }
}
