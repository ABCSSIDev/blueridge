<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  
use App\PurchaseRequest;
use App\PurchaseOrder;
use App\PurchaseRequestItem;
use App\Supplier;
use App\Canvass;
use App\CanvassItem;
use Yajra\DataTables\DataTables;
use App\Common;
use App\BudgetAllocation;

class CanvassingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $budget_allocation = BudgetAllocation::all();
        return view('Procurement.Canvassing.index',[
            'budget_year' => $budget_allocation
        ]);
        return view('Procurement.Canvassing.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        ini_set('max_execution_time', 300); // 5 minutes
        
        $canvassing = Canvass::where('awarded', 1)->get();
        //dd($canvassing);

        $exist_id = array();
        foreach($canvassing as $data){
            $pr_item_qty = $data->getPurchaseRequest->getPurchaseRequestItems->sum('quantity');
            $canvass_item_qty = $data->getCanvasses->sum('available_quantity');
            $remaining = $pr_item_qty - $canvass_item_qty;
            // dump($remaining);
            if ($remaining <= 0){
                $exist_id[] = $data->pr_id;
            }
        }
        
        $purchase_request = PurchaseRequest::whereNotIn('id',$exist_id)->get();
        $suppliers = Supplier::whereNull('deleted_at')->get();
        return view('Procurement.Canvassing.create',[
            'purchase_request' => $purchase_request,
            'suppliers' => $suppliers
        ]);
    }
    // public function validationCan($data)
    // {
    //     $purchase_request_items = PurchaseRequestItem::where('pr_id',$data->pr_id)->sum('quantity');

    //     return $purchase_request_items;
    // }
    // public function validationCan1($data)
    // {
    //     $canvassTotalItem = 
    //     $CanvassItem = CanvassItem::all();
    //     $qty = 0;
    //     foreach ($CanvassItem as $key => $value) {
    //         if($value->getCanvasses && $value->getCanvasses->pr_id == $data->pr_id ){
    //             $qty += $value->available_quantity;
    //         }
    //     }
    //     return $qty;
    // }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $group_id = 1;
        $latest = Canvass::where('pr_id',$request->input('purchase_request'))->latest()->first();
        if ($latest != null){
            $group_id = $latest->group_id + 1;
        }
        try{
            // insert canvass grouped by abstract and pr
            if($request->has('checkChoices')) {
                foreach ($request->input('for_insert') as $key => $item) {
                    $canvass = new Canvass();
                    $canvass->pr_id = $request->input('purchase_request');
                    $canvass->supplier_id = $request->input('supplier' . $item);
                    $canvass->awarded = $request->input('awarded' . $item);
                    $canvass->group_id = $group_id;
                    $canvass->save();
                    $holder = $key + 1;
                    $last_id = $canvass->id;
                    foreach ($request->input('checkChoices') as $checkedItem) {
                        // dd($request->input('quantity_availablez_'.$checkedItem.'_'.$holder));
                        $canvassItem = new CanvassItem();
                        $canvassItem->canvass_id = $last_id;
                        $canvassItem->pr_item_id = $checkedItem;
                        $canvassItem->available_quantity = $request->input('quantity_available_'.$checkedItem.'_'.$holder);
                        $canvassItem->unit_price = Common::cleanDecimal($request->input('unit_price_' . $checkedItem . '_' . $holder));
                        $canvassItem->save();
                        
                    }
                }
                $response = array(
                    'status' => 'success',
                    'reload' => true,
                    'reload_table' => true,
                    'route' => route('canvassing.reloadtable',0),
                    'desc' => 'Canvassing successfully created!'
                );

            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Please add PR items'
                );

            }
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
           
    }
    
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function reloadtable($id)
    {
        $append_no_data = '<tr align="center">'.
                        '<td colspan="9">No data available in table</td>'.
                    '</tr>';
        
        return array(
            'append_no_data' => $append_no_data
        );
    }

    public function show($id,$group_id)
    {
        $canvass = Canvass::where('pr_id', $id)->where('group_id', $group_id)->whereNull('deleted_at')->get();
        $total_per_supplier = $this->totalPerSupplier($canvass);
        //$canvass_items =  Canvass::all();
        $data = array();
        foreach ($canvass as $key => $value) {
            foreach ($value->getCanvasses as $key => $value2) {
                $data[] = $value2;
            }
        }
        $purchase_request_items =  PurchaseRequestItem::whereNull('deleted_at')->get();
        $suppliers = Supplier::whereNull('deleted_at')->get();
        $grouped_canvass_items =  DB::table('canvass_items')
                            ->join('canvasses as cv','canvass_items.canvass_id','cv.id')
                            ->select(DB::raw('MIN(canvass_items.canvass_id) as canvass_id,canvass_items.pr_item_id,MIN(cv.group_id) as group_id'))
                            ->groupBy('canvass_items.pr_item_id')
                            ->where('cv.group_id',$group_id)
                            ->where('cv.pr_id',$id)
                            ->whereNull('cv.deleted_at')
                            ->get();
        $supplier_awarded = Canvass::select('supplier_id')->where('pr_id', $id)->where('group_id', $group_id)->whereNull('deleted_at')->first();
        $supplier = Supplier::where('id',$supplier_awarded->supplier_id)->first();
        return view('Procurement.Canvassing.show',[
            'canvass' => $canvass,
            'canvass_items' => $data,
            'purchase_request_items' => $purchase_request_items,
            'suppliers' => $suppliers,
            'grouped_canvass_items' => $grouped_canvass_items,
            'supplier_awarded' => $supplier_awarded,
            'awarded' => $supplier,
            'group_id' => $group_id,
            'supplier_total' => $total_per_supplier
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$group_id)
    {
        
        $purchase_request_id = Canvass::where('pr_id', $id)->where('group_id', $group_id)->whereNull('deleted_at')->first();
        // dd($purchase_request_id);
        $supplier_awarded = Canvass::select('supplier_id')->where('pr_id', $id)->where('group_id', $group_id)->whereNull('deleted_at')->first();
        $suppliers = Supplier::whereNull('deleted_at')->get();
        $canvass = Canvass::where('pr_id', $id)->where('group_id', $group_id)->whereNull('deleted_at')->get();
        $grouped_canvass_items =  DB::table('canvass_items')
                            ->join('canvasses as cv','canvass_items.canvass_id','cv.id')
                            ->select(DB::raw('MIN(canvass_items.canvass_id) as canvass_id,canvass_items.pr_item_id,MIN(cv.group_id) as group_id'))
                            ->groupBy('canvass_items.pr_item_id')
                            ->where('cv.group_id',$group_id)
                            ->where('cv.pr_id',$id)
                            ->whereNull('cv.deleted_at')
                            ->get();

        $canvass_items =  Canvass::all();
        $data = array();
        foreach ($canvass_items as $key => $value) {
            foreach ($value->getCanvasses as $key => $value2) {
                $data[] = $value2;
            }
        }
        
        $purchase_request_items =  PurchaseRequestItem::whereNull('deleted_at')->get();
        $po = PurchaseOrder::where('canvass_id', $purchase_request_id->id)->first();

        return view('Procurement.Canvassing.edit',[
            'purchase_request_id' => $purchase_request_id,
            'suppliers' => $suppliers,
            'supplier_awarded' => $supplier_awarded,
            'canvass' => $canvass,
            'canvass_items' => $data,
            'grouped_canvass_items' => $grouped_canvass_items,
            'purchase_request_items' => $purchase_request_items,
            'id' => $id,
            'po' => $po,
            'group_id' => $group_id,
            'validation' => self::getValdationQuantity($id,$group_id,0,0)
        ]);
    }

   
    public static function QtyValidationCanvass($canvass_id,$pr_id)
    {
        $canvassitims = CanvassItem::where('pr_item_id',$pr_id)->get();
        $total = 0;
        $PR = 0;
        foreach($canvassitims as $data){
            if($data->getCanvasses->awarded == 1){
                $total +=$data->available_quantity;
            }
            $PR = $data->getPRItems->quantity;
        }  
        // return $total;
        if($total == $PR){
                return 1;
            }else{
                return 0;

            }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{
            $count_x = 1;
            foreach ($request->input('for_insert_canvass') as $key => $value){
                $count_x ++;
                $canvassitims = CanvassItem::where('id',$request->input('canvass_id'.$value))->update([
                    'canvass_id' => $request->input('canvass_item_id'.$value),
                    'pr_item_id' => $request->input('pr_item_id'.$value),
                    'available_quantity' => $request->input('quantity_availablez_'.$value),
                    'unit_price' => str_replace(',', '', $request->input('unit_price'.$value)),
                    'created_at' => date('Y/m/d H:i:s')
                ]);
            }
            $response = array(
                'status' => 'success',
                'desc' => 'Canvassing successfully updated!'
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete_canvass = Canvass::where('id', $id)->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
    }

    public function delete($id,$group_id){
        
        $canvass = Canvass::where('pr_id', $id)
            ->where('group_id',$group_id)
            ->first();

        return view('Procurement.Canvassing.delete',[
            'id' => $id,
            'group_id'=> $group_id,
            'canvass' => $canvass
        ]);
    }

    public function functionRemoveCanvassing($id,$group_id){
        $delete_canvass = Canvass::where('pr_id',$id)->where('group_id',$group_id)->first();
        Canvass::where('pr_id',$id)->where('group_id',$group_id)->delete();
        $canvass = Canvass::where('pr_id',$id)->where('group_id',$group_id)->onlyTrashed()->get();
        foreach($canvass as $value){
            CanvassItem::where('canvass_id',$value->id)->delete();
        }
   
        return redirect(route('canvassing.index').'?success=true&url=procurement/canvassing&module='.Common::getPRNumberFormat("purchase_requests", $delete_canvass->getPurchaseRequest).'');
    }

    public function getPurchaseRequest($id){
        //
        $purchase_request = PurchaseRequest::findOrFail($id);
        $purchase_request_items = $purchase_request->getPurchaseRequestItems;
        $last_idfinal = DB::table('canvasses')->orderby('id','desc')->first();
        $group_id = DB::table('canvasses')->orderby('id','desc')->where('pr_id',$id)->whereNull('deleted_at')->first();
        $append = '';
        $count_x = 1;
        $count_y = 1;
        
        foreach ($purchase_request_items as $key => $value) {
            $canvass_items = CanvassItem::whereHas('getCanvasses',function (Builder $query){
                $query->where('awarded','=',1);
            })->where('pr_item_id',$value->id)->get()->sum('available_quantity');
            $remaining = $value->quantity - $canvass_items;
            $max_quantity = $remaining;
            $remaining = number_format($remaining,3,'.', '');

            if ($remaining > 0){
                $append .= '<tr align="center" class="appendRow">'.
                    '<td class="v-align">'.
                    '<input type="checkbox" class="check_canvass'.$value->id.'" name="checkChoices[]" data-id="'.$value->id.'" onClick="checkBox('.$value->id.','.strval($count_y).')" value="'.$value->id.'">'.
                    '<input type="hidden" name="group_id" value="'.(!$group_id?'1':$group_id->group_id+1).'">'.
                    '</td>'.
                    '<td class="v-align">'.number_format($value->unit_cost,2).'</td>'.
                    '<td class="v-align">'.PurchaseRequestController::decToFraction($value->quantity).'</td>'.
                    '<td class="v-align">'.PurchaseRequestController::decToFraction($remaining).'</td>'.
                    '<td class="v-align">'.$value->getInventoryName->description.'</td>'.
                    '<td class="v-align">'.$value->getUnits->unit_name.'</td>'.
                    '<td class="v-align">'.
                    '<div class="row mt-2 canvass">'.
                    '<input type="hidden" name="for_insert_canvass[]" value="_'.$value->id.'_1">'.
                    '<input type="hidden" name="pr_item_id_'.$value->id.'_1" value="'.$value->id.'">'.
                    '<div class="col" hidden><input type="text" class="form-control qtycans'.$value->id.'_1 newInput format_number available'.$key.'2 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x) . strval($count_y).'" name="quantity_availables_'.$value->id.'_1" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col"><input type="text" class="form-control newInput format_number price unit_price price_unit'.$key.'1 select_unit'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x) . strval($count_y).'" name="unit_price_'.$value->id.'_1"  autocomplete="off" placeholder="0.00" title="Unit Price" disabled></div>'.
                    '<div class="col"><input type="text" class="form-control newInput format_number price quantity_available available'.$key.'2 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x) . strval($count_y).'" onkeyup=" return validquantity(this,'.$value->id.','.$max_quantity.',event,'.$value->id.',1)" name="quantity_available_'.$value->id.'_1" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col" hidden ><input type="text" class="form-control qtycanss'.$value->id.'_1 newInput format_number available'.$key.'2 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x) . strval($count_y).'" name="quantity_availablez_'.$value->id.'_1" id="quantity_availablez_'.$value->id.'_1" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col" id="total'.$key.'3" style="top: 7px;"><input type="hidden" class="total_price_class'.strval($count_x).'" data-selector="1"><label class="total_price" value=""></label></div>'.
                    '</div>'.
                    '</td>'.
                    '<td class="v-align">'.
                    '<div class="row mt-2 canvass">'.
                    '<input type="hidden" name="for_insert_canvass[]" value="_'.$value->id.'_2">'.
                    '<input type="hidden" name="pr_item_id_'.$value->id.'_2" value="'.$value->id.'">'.
                    '<div class="col" hidden><input type="text" class="form-control qtycans'.$value->id.'_2 newInput format_number available'.$key.'5 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+1) . strval($count_y).'" name="quantity_available_'.$value->id.'_2" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col"><input type="text" class="form-control newInput format_number price unit_price price_unit'.$key.'4 select_unit'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+1) . strval($count_y).'" name="unit_price_'.$value->id.'_2"  autocomplete="off" placeholder="0.00" title="Unit Price" disabled></div>'.
                    '<div class="col"><input type="text" class="form-control newInput format_number price quantity_available available'.$key.'2 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+1) . strval($count_y).'" onkeyup=" return validquantity(this,'.$value->id.','.$max_quantity.',event,'.$value->id.',2)" name="quantity_available_'.$value->id.'_2" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col" hidden ><input type="text" class="form-control qtycanss'.$value->id.'_2 newInput format_number   available'.$key.'5 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+1) . strval($count_y).'" id="quantity_availablez_'.$value->id.'_2" name="quantity_availablez_'.$value->id.'_2" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col" id="total'.$key.'3" style="top: 7px;"><input type="hidden" class="total_price_class'.strval($count_x+1).'" data-selector="2"><label class="total_price"></label></div>'.
                    '</div>'.
                    '</td>'.
                    '<td class="v-align">'.
                    '<div class="row mt-2 canvass">'.
                    '<input type="hidden" name="for_insert_canvass[]" value="_'.$value->id.'_3">'.
                    '<input type="hidden" name="pr_item_id_'.$value->id.'_3" value="'.$value->id.'">'.
                    '<div class="col" hidden><input type="text" class="form-control qtycans'.$value->id.'_3 newInput format_number available'.$key.'8 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+2) . strval($count_y).'" name="quantity_available_'.$value->id.'_3" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col"><input type="text" class="form-control newInput format_number price unit_price price_unit'.$key.'7 select_unit'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+2) . strval($count_y).'" name="unit_price_'.$value->id.'_3"  autocomplete="off" placeholder="0.00" title="Unit Price" disabled></div>'.
                    '<div class="col"><input type="text" class="form-control newInput format_number price quantity_available available'.$key.'3 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+2) . strval($count_y).'" onkeyup=" return validquantity(this,'.$value->id.','.$max_quantity.',event,'.$value->id.',3)" name="quantity_available_'.$value->id.'_3" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col" hidden><input type="text" class="form-control qtycanss'.$value->id.'_3 newInput format_number   available'.$key.'8 available_quantity'.$value->id.'" style="text-align:center" data-selector="'.strval($count_x+2) . strval($count_y).'" id="quantity_availablez_'.$value->id.'_3" name="quantity_availablez_'.$value->id.'_3" autocomplete="off" placeholder="0" title="Quantity Available" disabled></div>'.
                    '<div class="col" id="total'.$key.'3" style="top: 7px;"><input type="hidden" class="total_price_class'.strval($count_x+2).'" data-selector="3"><label class="total_price" value=""></label></div>'.
                    '</div>'.
                    '</td>'.
                    '</tr>
            
            <script>
                $(".priceval").number(true,2);
                function checkBox(ids){
                    $(".check_canvass"+ids+"").each(function(){
                        if($(this).is(":checked")){
                            
                            $(".select_unit"+ids+"").prop("disabled", false);
                            $(".available_quantity"+ids+"").prop("disabled", false);

                            rules["unit_price'.strval($count_x) . strval($count_y).'"] = { required : true };
                            rules["unit_price'.strval($count_x+1) . strval($count_y).'"] = { required : true };
                            rules["unit_price'.strval($count_x+2) . strval($count_y).'"] = { required : true };
                            
                            $("#table_canvassing").on("keyup", ".price", function () {
                                var selector = this.getAttribute("data-selector");
                                var unit_price = 0;   
                                var quantity_available =0;
                                var total =0;   

                                
                                if($(this).hasClass("unit_price")){
                                    
                                    if($(this).parent().siblings().children(".quantity_available").val() == ""){
                                        quantity_available = 0;
                                    }else{
                                        quantity_available = Number(fractionTodecimal($(this).parent().siblings().children(".quantity_available").val()));
                                    }
                                        unit_price = Number($(this).val()); 
                                }
                                
                                if( $(this).hasClass("quantity_available") ){
                                    quantity_available = Number(fractionTodecimal($(this).val()));
                                    unit_price = Number($(this).parent().siblings().children(".unit_price").val() ); 
                                }

                                var total_amount = unit_price * quantity_available;
                                $(this).parent().siblings().children(".total_price").html("₱ " + total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                                $(this).parent().siblings().children(".total_price_class"+selector.charAt(0)+"").val(unit_price * quantity_available);
                                $(".total_price_class"+selector.charAt(0)+"").each(function(){
                                    if($(this).parent().parent().parent().siblings().children().is(":checked")){
                                        total += Number($(this).val());
                                    }
                                });

                                $(".total_amount"+selector.charAt(0)+"").html("₱ " + total.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                            });
                            function fractionTodecimal(fraction){
                                var fraction_eval = 0;
                                var string = fraction + "";
                                var fractionParts = string.split("-");
                                if (fractionParts.length === 1) {
                                    /* try space as divider */
                                    fractionParts = string.split(" ");
                                }
                                if (fractionParts.length > 1 && fraction.indexOf("/") !== -1) {
                                    var integer = parseInt(fractionParts[0]);
                                    var decimalParts = fractionParts[1].split("/");
                                    var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
                                    fraction_eval = integer + decimal;
                                }
                                else if (fraction.indexOf("/") !== -1) {
                                    var decimalParts = fraction.split("/");
                                    var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
                                    fraction_eval = decimal;
                                }
                                else {
                                    fraction_eval = parseInt(fraction);
                                }
                                return fraction_eval;
                            }
                            total();
                        }
                        else{
                            total();
                            $(".select_unit"+ids+"").prop("disabled", true);
                            $(".available_quantity"+ids+"").prop("disabled", true);
                        }
                    });
                }
                
               
                function total(){
                    for(var i = 1; i <= 3; i++){
                        var total =0;
                        $(".total_price_class"+i+"").each(function(){
                            if($(this).parent().parent().parent().siblings().children().is(":checked")){
                                total += Number($(this).val());
                            }
                            
                        });
        
                        $(".total_amount"+i+"").html("₱ "+total.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                    }
                }

                $(".newInput").each( function () {
                    $(this).rules("add",{
                        "required" : true
                    })
                });
            </script>';

                $count_y++;
            }
        }
        $total = 1;
        $append .='<tr class="appendRow">'.
            '<td class="v-align" colspan="6"></td>'.
            '<td class="v-align">'.
                '<div class="row mt-2">'.
                    '<div class="col">Total</div>'.
                    '<div class="col"><label class="total_amount'.$total.'"></label></div>'.
                '</div>'.
            '</td>'.
            '<td class="v-align">'.
                '<div class="row mt-2">'.
                    '<div class="col">Total</div>'.
                    '<div class="col"><label class="total_amount'.($total+1).'"></label></div>'.
                '</div>'.
            '</td>'.
            '<td class="v-align">'.
                '<div class="row mt-2">'.
                    '<div class="col">Total</div>'.
                    '<div class="col"><label class="total_amount'.($total+2).'"></label></div>'.
                '</div>'.
            '</td>'.
        '</tr>';
       
        return $append;

    }

    public function getCanvassing($year){
        // $canvass = Canvass::where('group_id','!=',null)->groupBy('group_id');
        // $canvass = DB::table('canvasses')
        //             ->select(DB::raw('MIN(pr_id) as id,group_id, pr_id'))
        //             ->groupBy('group_id','pr_id')
        //             ->whereNull('deleted_at')
        //             ->get();
        if($year == '0000'){
            $canvass = DB::table('canvasses')
            ->join('purchase_requests as pr','canvasses.pr_id','pr.id')
            ->select(DB::raw('MIN(canvasses.pr_id) as id,pr.pr_no as pr_no, canvasses.group_id, canvasses.pr_id,canvasses.id as cas_id'))
            ->groupBy('canvasses.group_id','canvasses.pr_id')
            ->whereNull('canvasses.deleted_at')
            ->get();
        }else{
            $canvass = DB::table('canvasses')
                    ->join('purchase_requests as pr','canvasses.pr_id','pr.id')
                    ->select(DB::raw('MIN(pr_id) as id,canvasses.group_id, canvasses.pr_id, pr.request_date, canvasses.id as cas_id, pr.pr_no as pr_no'))
                    ->groupBy('canvasses.group_id','canvasses.pr_id')
                    ->whereNull('canvasses.deleted_at')
                    ->whereYear('pr.request_date',$year)
                    ->get();
        }
        return DataTables::of($canvass)->addColumn('purchase_no',function($model){
            return $model->pr_no;
        })->addColumn('abstract_no',function($model){
            return  $model->group_id;
        })->addColumn('action',function($model){
            $PurchaseOrder = DB::table('purchase_orders')->where('canvass_id',$model->cas_id)->where('deleted_at',null)->first();
            // return $model->cas_id;
            return '<center>
                        <div class="dropdown" style="text-align:center">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="'.route("canvassing.show",[$model->pr_id,$model->group_id]).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                                <a class="dropdown-item" href="'.route("canvassing.edit",[$model->pr_id,$model->group_id]).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item popup_form '.($PurchaseOrder?'btn-disabled':'').'" data-toggle="modal" style="cursor:pointer;" data-url="'.route("canvassing.delete",[$model->pr_id,$model->group_id]).'" title="Delete Canvassing Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                            </div>
                        </div>
                    </center>
                    <style>
                        .btn-disabled,
                        .btn-disabled[disabled] {
                            opacity: .4;
                            cursor: default !important;
                            pointer-events: none;
                        }
                    </style>';
        })->make(true);
    }

    public function totalPerSupplier($canvass_data){
        $arr = array();
        $total = 0;
        foreach ($canvass_data as $key => $value){
           $total = 0;
           $canvass_item = CanvassItem::where('canvass_id', $value->id)->get();
           foreach($canvass_item as $item)
           {
               $total = $total + ($item->available_quantity * $item->unit_price);
               
           } 
           $arr[$value->supplier_id] = $total;
        }
        return $arr;

    }
    
    public function getPR($canvass){
        $pr = PurchaseRequest::withTrashed()->where('id', $canvass->pr_id)->first();
        return $pr;
    }

    public static function getMaxQuantity($id,$group_id,$pr_item_id){

        $purchase_request = PurchaseRequest::findOrFail($id);
        $purchase_request_items = $purchase_request->getPurchaseRequestItems;
        
        $grouped_canvass_items =  DB::table('canvass_items')
                            ->join('canvasses as cv','canvass_items.canvass_id','cv.id')
                            ->select(DB::raw('MIN(canvass_items.canvass_id) as canvass_id,canvass_items.pr_item_id,MIN(cv.group_id) as group_id'))
                            ->groupBy('canvass_items.pr_item_id')
                            ->where('canvass_items.pr_item_id',$pr_item_id)

                            ->where('cv.group_id',$group_id)
                            ->where('cv.pr_id',$id)
                            ->whereNull('cv.deleted_at')
                            ->get();
        
        foreach($grouped_canvass_items as $key =>  $grouped_item){
            if($group_id == $grouped_item->group_id){
                foreach ($purchase_request_items as $key => $value) {
                    if($value->id == $grouped_item->pr_item_id){
                        $remaining = $value->quantity;
                    } 
                }
                return $remaining;
            }
        }
    }
    public static function getValdationQuantity($id,$group_id,$value,$pr_item_id){
        $overalltotalqty = self::getMaxQuantity($id,$group_id,$pr_item_id);
        // $grouped_canvass_items =  CanvassItem::all();
        $grouped_canvass_items =  DB::table('canvass_items')
        ->join('canvasses as cv','canvass_items.canvass_id','cv.id')
        ->groupBy('canvass_items.pr_item_id')
        ->where('cv.awarded',1)
        ->where('canvass_items.pr_item_id',$pr_item_id)
        ->where('cv.pr_id',$id)
        ->whereNull('cv.deleted_at')
        ->get();
        $canvassqty = 0;
        $count = 0;
        foreach($grouped_canvass_items as $data){
                $count += 1;
                $canvassqty += $data->available_quantity;
            
        }
        
        if($count == 0){
            $result = $overalltotalqty;
        }else{
            $result = ($overalltotalqty - $canvassqty)+$value;
        }
        // dd($value);
        return $result;
    }
    public static function getValdationQuantitys($id,$group_id,$value,$pr_item_id){
        $overalltotalqty = self::getMaxQuantity($id,$group_id,$pr_item_id);
        $grouped_canvass_items =  CanvassItem::where('pr_item_id',$pr_item_id)->get();
        $canvassqty = 0;
        $count = 0;
        foreach($grouped_canvass_items as $data){
            if($data->getCanvasses->awarded == 1 && $data->getCanvasses->pr_id == $id){
                $count += 1;
                $canvassqty += $data->available_quantity;
            }
        }
        
        if($count == 0){
            $result = $overalltotalqty;
        }else{
            $result = ($overalltotalqty - $canvassqty)+$value;
        }
        // dd($value);
        return $result;
    }
}



 