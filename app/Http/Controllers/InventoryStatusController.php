<?php

namespace App\Http\Controllers;

use App\InventoryStatus;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Common;
use App\BudgetAllocation;

class InventoryStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Setup.InventoryStatus.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Setup.InventoryStatus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Setup.InventoryStatus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(InventoryStatus::where(
            'status_name',$request->input('status_name'))->where(
            'status_type',$request->input('status_type'))->exists()){
                $response = array(
                    'status'  => 'error',
                    'desc'    => 'Inventory Status already exists!'
                );
        }else{
            try{
                
                $inventory_status = InventoryStatus::create ([
                    'status_name' => $request->input('status_name'),
                    'status_type' => $request->input('status_type'),
                    'notes'       => $request->input('note'),
                    'created_at'  => date('y-m-d H:i:s')
                ]);
    
                $inventory_status->save();
                $response = array(
                    'status' => 'success',
                    'desc'  => 'Inventory Status successfully created!'
                );
            }catch (\Exception $exception){
                $response = Common::catchError($exception);
            }
        }
       

        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getInfo = InventoryStatus::where('deleted_at',null)->where('id',$id)->first();
        if($getInfo->status_type == '1'){
            $getInfo->status_type = "Deployed";
        }else if($getInfo->status_type == '2'){
            $getInfo->status_type = "Ready to Deployed";
        }else if($getInfo->status_type == '3'){
            $getInfo->status_type = "Undeployable";
        }else if($getInfo->status_type == '4'){
            $getInfo->status_type = "Pending";
        }else{
            $getInfo->status_type = "Archived";
        }
        return view('Setup.InventoryStatus.show',['InventoryStatus' => $getInfo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editInfo = InventoryStatus::where('deleted_at', null)->where('id',$id)->first();
        return view('Setup.InventoryStatus.edit', ['InventoryEdit' => $editInfo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(InventoryStatus::where('status_name',$request->input('status_name'))
        ->where('status_type',$request->input('status_type'))->exists()){
            $response = array(
                'status' => 'error',
                'desc'   => 'Inventory Status already exists!'
            );
        }else{
            try{
                $updateInfo = InventoryStatus::where('id', $id)->update([
                    'status_name' => $request->input('status_name'),
                    'status_type' => $request->input('status_type'),
                    'notes'       => $request->input('note'),
                    'updated_at'  => date('y-m-d H:i:s')
                ]);
    
                $response = array(
                    'status' => 'success',
                    'desc'  => 'Inventory Status successfully Edited!'
                );
            }catch (\Exception $exception){
                $response = Common::catchError($exception);
            }
        }
       

        return response()->json($response,200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }

    public function removeInventory($id)
    {
        $inventory_status = InventoryStatus::where('id',$id)->first();
        return view('Setup.InventoryStatus.delete',['id' => $id,'inventory_status' => $inventory_status]);
    }

    public function getInventoryStatus($year){
        if($year == '0000'){
            $inventory = InventoryStatus::whereNull('deleted_at')->get();
        }else{
            $inventory = InventoryStatus::whereNull('deleted_at')->whereYear('created_at',$year)->get();
        }
        
        return DataTables::of($inventory)->addColumn('status_name',function($model){
            return $model->status_name;
        })->addColumn('status_type',function($model){
            if($model->status_type == '1'){
                $model->status_type = "Deployed";
            }else if($model->status_type == '2'){
                $model->status_type = "Ready to Deployed";
            }else if($model->status_type == '3'){
                $model->status_type = "Undeployable";
            }else if($model->status_type == '4'){
                $model->status_type = "Pending";
            }else{
                $model->status_type = "Archived";
            }
            return $model->status_type;

        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("inventory-status.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                        <a class="dropdown-item" href="'.route("inventory-status.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item popup_form" data-toggle="modal" data-url="'.route("remove.inventory",$model->id).'" title="Delete Inventory Status Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->make(true);
    }

    public function deleteInventory($id)
    {
        $inventory_status = InventoryStatus::where('id',$id)->first();
        InventoryStatus::where('id',$id)->delete();
        return redirect(route('inventory-status.index').'?success=true&url=setup/inventory-status&module='.$inventory_status->status_name.'');
    }
}

