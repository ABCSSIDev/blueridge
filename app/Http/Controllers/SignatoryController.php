<?php

namespace App\Http\Controllers;
use App\Signatory;
use Illuminate\Http\Request;
use App\Common;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use App\BudgetAllocation;

class SignatoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Setup.Signatory.index',[
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Setup.Signatory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Signatory $sign)
    {
        //
        try{
            $filename = "";
            if($request->has('signature')){
                $imageName = 'Signatory/'.$sign->getSignatoryID().'.jpg';
                $saveImage = Common::saveImage($request->file('signature'),$imageName);
                if($saveImage){
                    $filename = $imageName;
                }
            }
            $signatory = new Signatory();
            $signatory->signatory_name = $request->input('signatory_name');
            $signatory->email = $request->input('email');
            $signatory->position = $request->input('position');
            $signatory->sign_image = $filename;

            $signatory->save();
            $response = array(
                'status' => 'success',
                'desc' => 'Signatory successfully created!'
            );
        }catch(\Exception $e){
            $response = Common::catchError($e);
        }

        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $signatory = Signatory::findOrFail($id);
        
        return view('Setup.Signatory.show', ['signatory' => $signatory]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $signatory = Signatory::findOrFail($id);

        return view('Setup.Signatory.edit', ['signatory' => $signatory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{
            $filename = "";
            $signatory = Signatory::findOrFail($id);
            $signatory->signatory_name = $request->input('signatory_name');
            $signatory->position = $request->input('position');
            $signatory->email = $request->input('email');
            if($request->has('signature')){
                $imageName = 'Signatory/'.$signatory->id.'.jpg';
                $saveImage = Common::saveImage($request->file('signature'),$imageName);
                if($saveImage){
                    $signatory->sign_image = $imageName;
                }
            }
            $signatory->save();
            $response = array(
                'status' => 'success',
                'desc' => 'Signatory successfully created!'
            );
        }catch(\Exception $e){
            $response = Common::catchError($e);
        }

        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $signatory = Signatory::findOrFail($id)->delete();

        return redirect(route('signatory.index'));
    }

    public function delete($id){
        $signatory = Signatory::findOrFail($id);

        return view('Setup.Signatory.delete',['signatory' => $signatory]);
    }

    public function getSignatories($year){
        if($year == '0000'){
            $signatories = Signatory::all();
        }else{
            $signatories = Signatory::whereYear('created_at',$year)->get(); 
        }
        
        return DataTables::of($signatories)->addColumn('signatory_name',function($model){
            return $model->signatory_name;
        })->addColumn('position',function($model){
            return $model->position;
        })->addColumn('email',function($model){
            return $model->email;
        })->addColumn('action',function($model){
            return "<center>
                        <div class='dropdown'>
                            <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <i class='fa fa-bars'></i>
                            </button>
                            <div class='dropdown-menu dropdown-menu-center' aria-labelledby='dropdownMenuButton'>
                                <a class='dropdown-item' title='View Category' href='".route('signatory.show',$model->id)."'><i class='fa fa-eye' aria-hidden='true'></i> View Record</a>
                                <a class='dropdown-item' title='Edit Category' href='".route('signatory.edit',$model->id)."'><i class='fa fa-pencil' aria-hidden='true'></i> Edit Record</a>
                                <div class='dropdown-divider'></div>
                                <a class='dropdown-item popup_form' data-toggle='modal' title='Delete Signatory Record' style='cursor:pointer' data-url='".route('signatory.delete',$model->id)."'><i class='fa fa-trash' aria-hidden='true'></i> Delete Record</a>
                            </div>
                        </div>
                    </center>";
        })->make(true);
    }

    public function getImage($id){
        try{
            $signatory = Signatory::findOrFail($id);
            $file = Storage::disk('public')->get($signatory->sign_image);
            return response($file,200);
        }catch (Exception $exception){
            $response = Common::catchError($exception);
        }
    }
}
