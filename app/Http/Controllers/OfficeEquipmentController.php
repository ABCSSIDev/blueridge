<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Common;
use App\Asset;
use App\PurchaseOrder;
use App\Inventory;
use App\CanvassItem;
use App\InventoryStatus;
use App\Category;
use App\InventoryName;
use App\Unit;
use App\BudgetAllocation;

class OfficeEquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        $equipment_list = Asset::whereNull('deleted_at')->get();
        return view('Inventory.OfficeEquipment.index', [ 
            'Equipmentlist'=> $equipment_list,
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = Asset::find($id);
        return view('Inventory.OfficeEquipment.show', [ 
            'asset'=> $asset,
            'asset_tag' => $this->assetTagging($asset,$asset->inventory_id) 
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $inventory_status = InventoryStatus::whereNull('deleted_at')->get();
        $category = Category::whereNull('deleted_at')->get();
        $asset = Asset::where('id',$id)->whereNull('deleted_at')->first();
        $category_id = Inventory::where('id',$asset->inventory_id)->whereNull('deleted_at')->first();
        $unit = Unit::all();
       
        return view('Inventory.OfficeEquipment.edit',[
            'asset' => $asset,
            'asset_tag' => $this->assetTagging($asset,$asset->inventory_id),
            'inventory_status' => $inventory_status,
            'units' => $unit,
            'category' => $category,
            'category_id' => $category_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function canvcost($id)
    {
        $CanvassItem = CanvassItem::all();
        $total = 0;
        foreach($CanvassItem as $data){
            if($data->getPRItems->inventory_name_id == $id->getInventories->inventory_name_id && $data->getCanvasses->awarded == 1){
                $total = $data->unit_price;
            }
        }
        return $total;
    }
    public function update(Request $request, $id)
    {
       
        try{
            if($request->has('image')) {
                $base64_str = substr($request->image, strpos($request->image, ",")+1);
                $image = base64_decode($base64_str);
                $imageName = $request->input('asset_no').".png";
                $path = storage_path('/app/public/'). $imageName;
                Image::make(file_get_contents($request->image))->save($path);
            }
            else{
                $imageName = $request->input('old_image');
            }

            $edit_asset = Asset::find($id);
            $edit_asset->inventory_status_id = $request->input('inventory_status');
            $edit_asset->serial_number = $request->input('serial');
            $edit_asset->date_acquired = date('Y/m/d', strtotime($request->input('date_acquired')));
            $edit_asset->location = $request->input('location');
            $edit_asset->custodian =  $request->input('custodian');
            $edit_asset->other_details =  $request->input('other_details');
            $edit_asset->remark =  $request->input('remark');
            $edit_asset->save();

            $edit_inventory = Inventory::find($request->input('inventory_id'));
            $edit_inventory->image = $imageName;
            $edit_inventory->category_id = $request->input('categ_name');
            $edit_inventory->unit_price = Common::cleanDecimal($request->input('unit_cost'));
            $edit_inventory->unit_id = $request->input('unit');

            $edit_inventory->save();

            $edit_inventory_name = InventoryName::where('id',$request->input('inventory_name_id'))->first();
            $edit_inventory_name->description = $request->input('description');
            $edit_inventory_name->category_id = $request->input('categ_name');
            
            $edit_inventory_name->save();
            
            $response = array(
                'status' => 'success',
                'desc'  => 'Capital Expenditures successfully edited!'
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function functionRemoveEquipment($id)
    {
        $equipment_delete = Asset::where('id',$id)->first();
        Asset::where('id',$id)->delete();
        return redirect(route('office-equipment.index').'?success=true&url=inventory/office-equipment&module='.$equipment_delete->asset_tag.'');
    }
    
   

    public function removeEquipment($id)
    {
        $equipment_list = Asset::where('deleted_at',null)->where('id',$id)->first();
        return view('Inventory.OfficeEquipment.delete',[
            'equipment_list' => $equipment_list,
            'asset_tag' => $this->assetTagging($equipment_list,$equipment_list->inventory_id)
        ]);
    }

    public function getEquipments($year){
        if($year == '0000'){
            $equipment_list = Asset::whereNull('deleted_at')->get();
        }else{
            $equipment_list = Asset::whereNull('deleted_at')->whereYear('created_at',$year)->get();
            // $bank_list = Bank::whereYear('created_at',$year)->get();
        }
        
        return DataTables::of($equipment_list)->addColumn('image',function($model){
            // dd($model->getInventories);
            $image = ($model->getInventories->image == null ? asset('images/default.png') : route('inventory.image',$model->inventory_id));

            return '<center><div><img src="'.$image.'" alt="Image"/ height="50" width="50" class="rounded-circle"></div></center>';
        })->addColumn('asset_no',function($model){
            return $this->assetTagging($model,$model->inventory_id);
        })->addColumn('description',function($model){
            return $model->getInventories->getInventoryName->description;
        })->addColumn('status',function($model){
            return $model->getInvetoryStatus->status_name;
        })->addColumn('category',function($model){
            return $model->getInventories->getCategory->category_name;
        })->addColumn('action',function($model){
            $validation = Inventory::where('id',$model->inventory_id)->first();
            return '<center>
                        <div class="dropdown" style="text-align:center">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("office-equipment.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                        <a class="dropdown-item" href="'.route("office-equipment.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                        </div>
                        </div>
                    </center>';
        })->rawColumns(['image','action'])->make(true);
    }

    public function getImage($id){
        try{
            $inventory = Inventory::findOrFail($id);
            $file = Storage::disk('public')->get($inventory->image);
            return response($file,200);
        }catch (Exception $exception){
            $response = Common::catchError($exception);
        }
    }

    public function assetTagging($asset,$id){
        return Common::AssetTagCode($asset,$id);

    }
}
