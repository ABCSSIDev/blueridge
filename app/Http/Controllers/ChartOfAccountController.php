<?php

namespace App\Http\Controllers;

use App\AccountGroup;
use App\AccountLedger;
use App\BudgetAllocationGroup;
use App\BudgetAllocationItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ChartOfAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $html = "";
    public function index()
    {
        $groups = AccountGroup::where('group_id',null)->get();
        self::recursiveAccounts($groups, '', true);
//        dd($this->html);
        return view('Accounting.ChartofAccounts.index', ['table'=>$this->html]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_account_groups = AccountGroup::whereNull('deleted_at')->get();
        $account_groups = AccountGroup::whereNull('deleted_at')->whereNotNull('group_id')->get();
        $all_account_ledgers = AccountLedger::whereNull('deleted_at')->get();
        return view('Accounting.ChartofAccounts.create',[
            'all_account_groups' => $all_account_groups,
            'account_groups' => $account_groups,
            'account_ledgers' => $all_account_ledgers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function editAccountGroup($id)
    {
        $all_account_groups = AccountGroup::whereNull('deleted_at')->get();
        $group_data = AccountGroup::where('id', $id)->first();
        return view('Accounting.ChartofAccounts.edit_account_group', [
            "all_account_groups" => $all_account_groups,
            "group_data" => $group_data
        ]);
    }

    public function editAccountLedger($id)
    {
        $all_account_groups = AccountGroup::whereNull('deleted_at')->get();
        $ledger_data = AccountLedger::where('id', $id)->first();
        $all_account_ledgers = AccountLedger::whereNull('deleted_at')->get();
        $account_groups = AccountGroup::whereNull('deleted_at')->whereNotNull('group_id')->get();
        return view('Accounting.ChartofAccounts.edit_account_ledger', [
            "all_account_groups" => $all_account_groups,
            "account_ledgers" => $all_account_ledgers,
            "ledger_data" => $ledger_data,
            'account_groups' => $account_groups

        ]);
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeAccountGroup(Request $request){
        try{
            $parent_group = $request->input('parent_group');
            $description = $request->input('description');
            $group_name = $request->input('group_name');

            $insert_group = AccountGroup::create([
                'group_id' => $parent_group,
                'group_name' => $group_name,
                'description' => $description
            ]);

            if($insert_group){
                $response = array(
                    'status' => 'success',
                    'desc' => 'Account Group '.$group_name.' successfully created'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Failed to insert to database, Please Contact the administrator'
                );
            }
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'Something went wrong please contact the administrator'
            );
        }

        return response()->json($response,200);
    }

    public function storeAccountLedger(Request $request){
        try{
            //main inputs
            $group_name = $request->input('group_name');
            $ledger_name = $request->input('ledger_name');
            $account_code = $request->input('account_code');
            $parent_ledger = $request->input('parent_ledger');
            $account_type = $request->input('account_type');
            $description = $request->input('description');

            //generate budget
            $generate_budget = $request->input('assign_budget');
            $budget_account = !empty($generate_budget)?$request->input('assign_budget_account'):null;
            $budget_percentage = !empty($generate_budget)?$request->input('assign_percentage'):null;
            $attachment = !empty($generate_budget)?$request->file('attachments'):null;
            $attachment_name = !empty($generate_budget)?$request->input('attachment_name').".".$attachment->getClientOriginalExtension():null;
            $reference_group = null;
            $reference_ledger = null;
            $url = null;
            $fileName = "";
            if(!empty($budget_account)){
                $reference = explode("-", $budget_account);
                ($reference[0] == 0)?$reference_group = $reference[1]:$reference_ledger = $reference[1];
            }



            $insert_ledger = AccountLedger::create([
                'group_id' => $group_name,
                'ledger_id' => $parent_ledger,
                'account_code' => $account_code,
                'account_type' => $account_type,
                'ledger_name' => $ledger_name,
                'description' => $description,

            ]);

            if(!empty($attachment)){
                $fileName = strtotime($insert_ledger->created_at) . $insert_ledger->id . '.' . $attachment->getClientOriginalExtension();
                $path = $attachment->storeAs('public/Budget_attachments', $fileName);
//                dd(storage_path('app/public/Budget_attachments/'.  strtotime(now()) . sprintf('%03d', $statement[0]->Auto_increment) . '.' . $attachment->getClientOriginalExtension()));
            }

            $add_budget = AccountLedger::where('id', $insert_ledger->id)->update([
                'reference_group' => $reference_group,
                'reference_ledger' => $reference_ledger,
                'budget_percentage' => $budget_percentage,
                'attachments_name' => $attachment_name,
                'attachments' => 'Budget_attachments/'.$fileName
            ]);

            if($insert_ledger || $add_budget){

                $response = array(
                    'status' => 'success',
                    'desc' => 'Account Ledger '.$ledger_name.' Successfully created'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Oops, Something went wrong please contact the administrator'
                );
            }
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'Oops, Something went wrong please contact the administrator',
                'exception' => $exception
            );
        }

        return response()->json($response,200);
    }

    public function recursiveAccounts($accounts, $sub_mark = '', $parent = false){
        foreach($accounts as $row){
            $check_allocated_budget = self::checkGroupChild($row, false, true)?"":"popup_form";
            if($parent){
                $this->html .= '<tr style="background-color:#0000000d">
                                <td style="vertical-align: middle"><strong>'.$sub_mark.'| '.$row->group_name.'</strong></td>
                                <td style="vertical-align: middle"></td>
                                <td><div class="dropdown" align="center">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                        <i class="fa fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="'.route("edit.chart-account-group", $row->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                                        <a class="dropdown-item '.$check_allocated_budget.'" data-toggle="modal" title="Archive Account Group" data-url="'.route("delete.chart-account-group", $row->id).'"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                                    </div>
                                </div></td>
                            </tr>';
            }else{

                $this->html .= '<tr>
                                <td style="vertical-align: middle">'.$sub_mark.'<b>| '.$row->group_name.'</b></td>
                                <td style="vertical-align: middle"></td>
                                <td><div class="dropdown" align="center">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                        <i class="fa fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="'.route("edit.chart-account-group", $row->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                                        <a class="dropdown-item '.$check_allocated_budget.'" data-toggle="modal" title="Archive Account Group" data-url="'.route("delete.chart-account-group", $row->id).'"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                                    </div>
                                </div></td>
                            </tr>';
            }


            if (count($row->getLedgers) > 0){
                self::recursiveLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp');
            }
            if (count($row->getGroups) > 0){
                self::recursiveAccounts($row->getGroups, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp');
            }

        }
    }

    public function recursiveLedgers($ledgers, $sub_mark = "", $child = false){
        foreach ($ledgers as $row){
            if(empty($row->ledger_id) || $child){
                $check_allocated_budget = self::checkLedgerChild($row, false, true)?"":"popup_form";
                $this->html .= '<tr>
                                <td style="vertical-align: middle">'.$sub_mark.'| '.$row->ledger_name.'</td>
                                <td style="vertical-align: middle">'.$row->account_code.'</td>
                                <td><div class="dropdown" align="center">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                        <i class="fa fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="'.route("edit.chart-account-ledger", $row->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                                        <a class="dropdown-item '.$check_allocated_budget.'" data-toggle="modal" title="Archive Account Ledger" data-url="'.route("delete.chart-account-ledger", $row->id).'"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                                    </div>
                                </div></td>
                            </tr>';

                if (count($row->getLedgers) > 0){
                    self::recursiveLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp', true);
                }
            }

        }
    }

    public function updateAccountGroup(Request $request, $id){

        try{
            $parent_group = $request->input('parent_group');
            $description = $request->input('description');
            $group_name = $request->input('group_name');

            $update_group = AccountGroup::where('id', $id)->update([
                "group_id" => $parent_group,
                "description" => $description,
                "group_name" => $group_name
            ]);

            if($update_group){
                $response = array(
                    'status' => 'success',
                    'desc' => 'Account Group Successfully Updated'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Oops, Something went wrong please contact administrator',
                );
            }

        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'An error occured please contact administrator',
                'exception' => $exception
            );
        }

        return response()->json($response,200);

    }

    public function updateAccountLedger(Request $request, $id){
        try{
            //main inputs
            $group_name = $request->input('group_name');
            $ledger_name = $request->input('ledger_name');
            $account_code = $request->input('account_code');
            $parent_ledger = $request->input('parent_ledger');
            $account_type = $request->input('account_type');
            $description = $request->input('description');

            //generate budget
            $generate_budget = $request->input('assign_budget');
            $budget_account = !empty($generate_budget)?$request->input('assign_budget_account'):null;
            $budget_percentage = !empty($generate_budget)?$request->input('assign_percentage'):null;
            $attachment = !empty($generate_budget)?$request->file('attachments'):null;
            $attachment_name = !empty($generate_budget)?$request->input('attachment_name').".".$attachment->getClientOriginalExtension():null;
            $reference_group = null;
            $reference_ledger = null;
            $url = null;
            $data_file_name = null;

            if(!empty($budget_account)){
                $reference = explode("-", $budget_account);
                ($reference[0] == 0)?$reference_group = $reference[1]:$reference_ledger = $reference[1];
            }

            $update_ledger = AccountLedger::where('id', $id)->update([
                'group_id' => $group_name,
                'ledger_id' => $parent_ledger,
                'account_code' => $account_code,
                'account_type' => $account_type,
                'ledger_name' => $ledger_name,
                'description' => $description,

            ]);


            if(!empty($attachment)){
                $ledger_data = AccountLedger::where('id', $id)->first();
                $data_file_name = strtotime($ledger_data->created_at) . $ledger_data->id;
                $current_file_name = pathinfo($attachment->getClientOriginalName(), PATHINFO_FILENAME);;
                if($current_file_name != $data_file_name){
                    $delete_data_file = File::delete(storage_path("app/public/".$ledger_data->attachments));
                    if($delete_data_file){
                        $fileName = strtotime($ledger_data->created_at) . $ledger_data->id . '.' . $attachment->getClientOriginalExtension();
                        $path = $attachment->storeAs('public/Budget_attachments', $fileName);
                    }
                }
            }
            $update_budgets = AccountLedger::where('id', $id)->update([
                'reference_group' => $reference_group,
                'reference_ledger' => $reference_ledger,
                'budget_percentage' => $budget_percentage,
                'attachments_name' => $attachment_name,
                'attachments' => $data_file_name
            ]);


            if($update_ledger || $update_budgets){

                $response = array(
                    'status' => 'success',
                    'desc' => 'Account Ledger '.$ledger_name.' Successfully Updated'
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'desc' => 'Oops, Something went wrong please contact the administrator'
                );
            }
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc' => 'Oops, Something went wrong please contact the administrator',
                'exception' => $exception
            );
        }

        return response()->json($response,200);

    }

    public function deleteAccountGroup($id){
        $group_data = AccountGroup::where('id', $id)->first();
        return view('Accounting.ChartofAccounts.delete_group', ['group_data' => $group_data]);
    }

    public function deleteAccountLedger($id){
        $ledger_data = AccountLedger::where('id', $id)->first();
        return view('Accounting.ChartofAccounts.delete_ledger', ['ledger_data' => $ledger_data]);
    }

    public function archiveAccountGroup($id){
        $group_data = AccountGroup::where('id', $id)->first();
        $delete_group = AccountGroup::where('id', $id)->delete();
        return redirect(route('chart-account.index').'?success=true&url=/accounting/chart-account&module='.$group_data->group_name.'');

    }

    public function archiveAccountLedger($id){
        $ledger_data= AccountLedger::where('id', $id)->first();
        $delete_group = AccountLedger::where('id', $id)->delete();
        return redirect(route('chart-account.index').'?success=true&url=/accounting/chart-account&module='.$ledger_data->ledger_name.'');
    }

    public function checkGroupChild($groups, $foo = false, $parent = false){
        if($parent){

            $bar = BudgetAllocationGroup::where('group_id', $groups->id)->count();
            $foo = ($bar>0);

            if (count($groups->getLedgers) > 0){
                $foo = self::checkLedgerChild($groups->getLedgers, $foo);
            }

            if (count($groups->getGroups) > 0){
                $foo = self::checkGroupChild($groups->getGroups, $foo);
            }

        }else {

            foreach($groups as $key => $group){
                $bar = BudgetAllocationGroup::where('group_id', $group->id)->count();
                $foo = ($bar>0);

                if (count($group->getLedgers) > 0){
                    $foo = self::checkLedgerChild($group->getLedgers, $foo);
                }

                if (count($group->getGroups) > 0){
                    $foo = self::checkGroupChild($group->getGroups, $foo);
                }
            }

        }
        return $foo;

    }

    public function checkLedgerChild($ledgers, $foo = false, $parent = false){

        if($parent){
            $bar = BudgetAllocationItem::where('ledger_id', $ledgers->id)->count();
            $foo = ($bar>0);

            if (count($ledgers->getLedgers) > 0){
                $foo = self::checkLedgerChild($ledgers->getLedgers, $foo);
            }
        }else{
            foreach ($ledgers as $ledger){
                $bar = BudgetAllocationItem::where('ledger_id', $ledger->id)->count();
                $foo = ($bar>0);

                if (count($ledger->getLedgers) > 0){
                    $foo = self::checkLedgerChild($ledger->getLedgers, $foo);
                }
            }
        }


        return $foo;
    }

}
