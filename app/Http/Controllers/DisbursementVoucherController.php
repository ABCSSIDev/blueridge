<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GeneralUser;
use App\AccountLedger;
use App\ROA;
use App\Invoice;
use App\Role;
use App\DisbursementVoucher;
use Illuminate\Support\Facades\DB;
use App\Common as common;
use Yajra\DataTables\DataTables;
use App\BudgetAllocation;
use App\BudgetAllocationItem;
use App\Canvass;
use App\DVPad;
use App\PurchaseOrder;
use App\PurchaseRequest;
class DisbursementVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_allocation = BudgetAllocation::all();
        return view('Accounting.ROADV.index',[
            'budget_year' => $budget_allocation
        ]);
        // return view('Accounting.ROADV.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $check_number = null;
        $or_settings = DVPad::orderBy('dv_pad_start', 'ASC')->first();
        $invoice = DisbursementVoucher::all();
        if ($or_settings != null){
            $start = $or_settings->first()['dv_pad_start'];
            $check_number = $invoice->count() + $start;
        }
        $roas = ROA::orderBy('id', 'DESC')->first();
        $role = Role::where('name', 'LIKE', '%Claimant%')->first();
        $id = array(7,11,13,49,44,42,38);
        $account_ledger = AccountLedger::where('account_type',1)->whereNotIn('id',$id)->get();
        return view('Accounting.ROADV.create',[
            'account_ledger' => $account_ledger,
            'role' => $role,
            'check_number' => $check_number,
            'last_id' => common::getPRNumberFormat('roas'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if(ROA::where([
                    ['obligation_number', $request->input('obligation_no')],
                    ['resolution_number', $request->input('resolution_number')]
                ])->exists()){
                    $response = array(
                        'status' => 'error',
                        'desc' => 'ROA/Disbursement Voucher already exists!'
                    );
            }else{  
                $new_roa = new ROA([
                    'ledger_id' => $request->input('ledger_id'),
                    'obligation_number' => $request->input('obligation_number'),
                    'resolution_date' =>date('Y/m/d H:i:s', strtotime($request->input('resolution_date'))),
                    'resolution_number' =>$request->input('resolution_number'),
                    'bids_resolution_number' =>$request->input('bids_resolution_number'),
                    'bids_mode' => $request->input('bids_mode'),
                    'approval_date' => date('Y/m/d H:i:s', strtotime($request->input('approval_date'))),
                    'signatory' => $request->input('signatory'),
                    'total_amount' => str_replace(',', '', $request->input('total_amount')),
                    
                    'cash_advance_voucher_no' => $request->input('cash_advance_voucher_no'),
                    'cash_advance_date' =>(empty($request->input('cash_advance_date'))?null:date('Y/m/d H:i:s', strtotime($request->input('cash_advance_date')))),
                    'cash_advance_amount' =>str_replace(',', '', $request->input('cash_advance_amount')),
                    'amount_refund_date' => (empty($request->input('amount_refund_date'))?null:date('Y/m/d H:i:s', strtotime($request->input('amount_refund_date')))),
                    'amount_refund_or_no' => $request->input('amount_refund_or_no'),
                    'created_at' => date('Y/m/d H:i:s')
                ]);

                $new_roa->save();

                $role = Role::where('name', 'LIKE', '%Claimant%')->first();
                $results = GeneralUser::where('name', 'like', '' .$request->input('claimant').'')->where('role_id',$role->id)->get();
                if(count($results)>0){
                    $user_id = $results[0]->id;
                }else{
                    $user_id = $this->insertUser($request);
                }
                if(DisbursementVoucher::where([
                            ['check_number', $request->input('check_no')],
                            ['invoice_no', $request->input('invoice_no')]
                        ])->exists()){
                    $response = array(
                        'status' => 'error',
                        'desc' => 'ROA/Disbursement Voucher already exists!'
                    );
                }else{
                    $DVdata = DisbursementVoucher::create([
                        'roa_id' => $new_roa->id,
                        'claimant_id' => $user_id,
                        'voucher_no' => $request->input('voucher_no'),
                        'check_number' => $request->input('check_no'),
                        'claimant_address' => $request->input('address'),
                        'cheque_date' => date('Y/m/d', strtotime($request->input('cheque_date'))),
                        'due_date' => Common::formatDate("Y-m-d",$request->input('due_date')),
                        'mode' => $request->input('mode_type'),
                        'tin' => $request->input('tin'),
                        'description' => $request->input('description'),
                        'invoice_no' => $request->input('invoice_no'),
                        'rer' => $request->input('rer'),
                        'created_at' => date('Y/m/d H:i:s')
                    ]);

                    $response = array(
                        'status' => 'success',
                        'desc' => 'ROA/Disbursement Voucher successfully created!',
                        'refresh_dv' => true,
                        'check_number' => $DVdata->check_number+1
                    );
                }
            }
        }catch (\Exception $exception){
            $response = common::catchError($exception);
        }
        return response()->json($response,200);
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roas = ROA::where('id',$id)->first();
        $dv = DisbursementVoucher::findOrFail($id);
        $pr = PurchaseRequest::where('roa_id', $dv->roa_id)->first();
        $po = null;
        if(!empty($pr)){
            // $canvass = Canvass::where('id', $pr->id)->first();
            $po = PurchaseOrder::where('pr_id', $pr->id)->first(); // trying to get property id
        }
        $yr = date('Y',strtotime($dv->getROA->resolution_date));
        $get_fund_yr = BudgetAllocation::where('year',$yr)->first();
        $get_fund_ledger = NULL;
        if($get_fund_yr){
            $get_fund_ledger = BudgetAllocationItem::where('budget_id',$get_fund_yr->id)
                                ->where('ledger_id',$dv->getRoa->ledger_id)->first();
        }
        return view('Accounting.ROADV.show', [
            'roas' => $dv,
            'po_mode' => $po, 
            'year' => $yr,
            'amount_in_words' => common::NumberToWords($dv->getROA->total_amount),
            'fund' => $get_fund_ledger
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roas = DisbursementVoucher::findORFail($id);
        $ids = array(7,11,13,49,44,42,38);
        $account_ledger = AccountLedger::where('account_type',1)->whereNotIn('id',$ids)->get();

        $role = Role::where('name', 'LIKE', '%Claimant%')->first();
        $Invoice = Invoice::all();
        return view('Accounting.ROADV.edit',[
            'roas' => $roas,
            'account_ledger' => $account_ledger,
            'role' => $role,
            'Invoice' => $Invoice
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = DisbursementVoucher::find($id);
        //
        try {
            if(ROA::where([
                    ['obligation_number', $request->input('obligation_no')],
                    ['resolution_number', $request->input('resolution_number')]
                ])->exists() && $validation->check_no != $request->input('check_no') && $validation->invoice_no != $request->input('invoice_no')
                ){
                    $response = array(
                        'status' => 'error',
                        'desc' => 'ROA/Disbursement Voucher already exists!'
                    );
            }
           else{
                $edit_roa = ROA::find($validation->getROA->ledger_id);
                $edit_roa->ledger_id = $request->input('ledger_id');
                $edit_roa->obligation_number = $request->input('obligation_number');
                $edit_roa->resolution_date = date('Y/m/d H:i:s', strtotime($request->input('resolution_date')));
                $edit_roa->resolution_number = $request->input('resolution_number');
                $edit_roa->bids_resolution_number = $request->input('bids_resolution_number');
                $edit_roa->bids_mode = $request->input('bids_mode');
                $edit_roa->approval_date = date('Y/m/d H:i:s', strtotime($request->input('approval_date')));
                $edit_roa->signatory = $request->input('signatory');
                $edit_roa->total_amount = str_replace(',', '', $request->input('total_amount'));
                $edit_roa->updated_at = date('Y/m/d H:i:s');

                $edit_roa->cash_advance_voucher_no =  $request->input('cash_advance_voucher_no');
                $edit_roa->cash_advance_date = (empty($request->input('cash_advance_date'))?null:date('Y/m/d H:i:s', strtotime($request->input('cash_advance_date'))));
                $edit_roa->cash_advance_amount = str_replace(',', '', $request->input('cash_advance_amount'));
                $edit_roa->amount_refund_date = (empty($request->input('amount_refund_date'))?null:date('Y/m/d H:i:s', strtotime($request->input('amount_refund_date'))));
                $edit_roa->amount_refund_or_no = $request->input('amount_refund_or_no');
                $edit_roa->save();

                
                $role = Role::where('name', 'LIKE', '%Claimant%')->first();
                $results = GeneralUser::where('name', 'like', '' .$request->input('claimant').'')->where('role_id',$role->id)->get();
                if(count($results)>0){
                    $user_id = $results[0]->id;
                }else{
                    $user_id = $this->insertUser($request);
                }
                $edit_disbursement = DisbursementVoucher::where('roa_id',$request->input('roa_id'))->first();
                $edit_disbursement->claimant_id = $user_id;
                $edit_disbursement->voucher_no = $request->input('voucher_no');
                $edit_disbursement->check_number = $request->input('check_no');
                $edit_disbursement->claimant_address = $request->input('address');
                $edit_disbursement->cheque_date = date('Y/m/d', strtotime($request->input('cheque_date')));
                $edit_disbursement->mode = $request->input('mode_type');
                $edit_disbursement->tin = $request->input('tin'); 
                $edit_disbursement->description = $request->input('description');
                $edit_disbursement->invoice_no = $request->input('invoice_no');
                $edit_disbursement->rer = $request->input('rer');
                $edit_disbursement->or_id = $request->input('or_id');
                $edit_disbursement->updated_at = date('Y/m/d H:i:s');

                $edit_disbursement->save();

                $response = array(
                    'status' => 'success',
                    'desc'  => 'ROA/Disbursement Voucher successfully updated!'
                );
                }
            }catch (\Exception $exception){
                $response = Common::catchError($exception);
            }
    
        return response()->json($response,200);
    }
    public function functionORPad()
    {
        
        $or_settings = DVPad::orderBy('id','DESC')->first();
        $or_pads = DVPad::all();
        $DisbursementVoucher = DisbursementVoucher::all();
        return view('Accounting.ROADV.or_pad',['or_settings' => $or_settings,'or_pads' => $or_pads,'DisbursementVoucher' => $DisbursementVoucher]);
    }
    public function autocompletePayee(Request $request)
    {
        $search = $request->get('term');
        $role = Role::where('name', 'LIKE', '%Payee%')->first();
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->where('role_id',$role->id)->get();
        return response()->json($result);
    } 
    public function autocompleteOr(Request $request)
    {
        $search = $request->get('OR');
        $ORPadDatas = DVPad::all();
        $optotalcount = 0;
        foreach($ORPadDatas as $data){
            $optotalcount += $data->dv_pad_quantity;
        }
        $DVcount = DisbursementVoucher::all();
        $ORPadASC = DVPad::orderBy('id', 'ASC')->first();
        $rangeto = $ORPadASC->dv_pad_start + $optotalcount;
        $rangefrom = $ORPadASC->dv_pad_start;
        
        $invtotalcount = 0;
        foreach($DVcount as $data){
            if($data->check_number >= $rangefrom && $data->check_number <= $rangeto){
                $invtotalcount += 1;
            }
        }
        $DVdata = DisbursementVoucher::where('check_number',$search)->count();
        $result = '';
        $ORPadDESC = DVPad::orderBy('id', 'DESC')->first();
        $LastORNumber = $ORPadDESC->dv_pad_start + $ORPadDESC->dv_pad_quantity;
        // dd($LastORNumber);
        if($DVdata>0){
            $result = 'duplicat';
        }
        else if($search > $LastORNumber || $search < $ORPadASC->dv_pad_start){
            $result = 'outofrange';
        }
        else if($invtotalcount == $ORPadDESC->dv_pad_quantity){
            $result = 'maxorpad';
        }
        else if($search <= 0){
            $result = 'zero';
        }
        // dd($LastORNumber);
        
        return response()->json($result);
    } 
    public function functionCreateORPad(Request $request)
    {
        // return $request;
        try{
            $new_pad = new DVPad([
                'dv_pad_start' => $request->input('pad_start'),
                'dv_pad_quantity' =>$request->input('pad_end'),
                'created_at' => date('Y/m/d H:i:s')
            ]);

            $new_pad->save();
            $response = array(
                'status' => 'success',
                'desc' => 'OR Pad successfully created!',
                'refresh_dv_pad' => true,
                'dv_number' => (int)$new_pad->dv_pad_start
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voucher = DisbursementVoucher::where('id', $id)
            ->first();
        DisbursementVoucher::where('id', $id)
            ->delete();
        ROA::where('id', $voucher->roa_id)
            ->delete();
        return redirect(route('disbursement-voucher.index').'?success=true&url=accounting/roa-dv&module='.common::getPRNumberFormat('disbursement_vouchers',$voucher).'');
    }

    public function delete($id){
        $voucher = DisbursementVoucher::where('id', $id)
            ->first();
        
        return view('Accounting.ROADV.delete', ['id' => $id, 'voucher' => $voucher]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        $disbursementVoucher = DisbursementVoucher::findOrFail($id);
//        dd(common::NumberToWords($disbursementVoucher->getROA->total_amount,true));
        return view('Accounting.ROADV.print',['dv' => $disbursementVoucher]);
    }

    public function autocompleteClaimant(Request $request)
    {
        $search = $request->get('term');
        $role = Role::where('name', 'LIKE', '%Claimant%')->first();
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->where('role_id',$role->id)->get();
        return response()->json($result);
    } 

    public function insertUser(Request $request)
    {
        $insert_user = GeneralUser::create([
            'role_id' => $request->input('role_id'),
            'name' => $request->input('claimant'),
            'contact_number' => '',
            'address' => $request->input('address'),
            'created_at' => date('Y/m/d H:i:s')
        ]);
        return  $insert_user->id;
    }

    public function getVoucher($year){
        if($year == '0000'){
            $voucher = DisbursementVoucher::whereNull('deleted_at')->get();
        }else{
            $id=array();
            $ROA = ROA::whereNull('deleted_at')->whereYear('resolution_date',$year)->whereYear('approval_date',$year)->get();
            foreach($ROA as $data){
                $id[] = $data->id;
            }
            
            $voucher = DisbursementVoucher::whereNull('deleted_at')->whereIn('roa_id',$id)->get();
        }

        return DataTables::of($voucher)->addColumn('obligation_number',function($model){
            return $model->getROA->obligation_number.(is_null($model->getPayment) ? '<br/><span class="badge badge-pill badge-primary">Open</span>' : ($model->getPayment->is_cancelled == 1 ? '<br/><span class="badge badge-pill badge-danger">Cancelled</span>' : '<br/><span class="badge badge-pill badge-success">Paid</span>'));
        })->addColumn('voucher_number',function($model){
            return $model->voucher_no;
        })->addColumn('check_number',function($model){
            return $model->check_number;
        })->addColumn('claimant_name',function($model){
            return $model->getClaimant->name;
        })->addColumn('due_date',function ($model){
            $date = new \DateTime($model->due_date);
            $now = new \DateTime();
            return ($date < $now && is_null($model->getPayment)? "<span class='overdue-date' data-toggle='tooltip' data-placement='right' title='".Common::computeOverdue($model->due_date)."'><b>".Common::formatDate("m/d/Y",$model->due_date)."</b></span>" : Common::formatDate("m/d/Y",$model->due_date));
        })->addColumn('action',function($model){
            $validation = PurchaseRequest::where('roa_id', $model->getROA->id)->first();
            return '<center>
                        <div class="dropdown" style="text-align:center">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:50px">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="'.route('disbursement-voucher.show',$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                        <a class="dropdown-item '.(!is_null($model->getPayment)?'btn-disabled':'').'" href="'.route('disbursement-voucher.edit',$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                        <a class="dropdown-item" href="'.route('roa-dv.print',$model->id).'"><i class="fa fa-print" aria-hidden="true"></i> Print Cheque</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item popup_form '.($validation?'btn-disabled':'').'" data-toggle="modal" style="cursor:pointer" data-url="'.route('disbursement-voucher.delete',$model->id).'" title="Delete Disbursement Voucher Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>
                    <style>
                        .btn-disabled,
                        .btn-disabled[disabled] {
                            opacity: .4;
                            cursor: default !important;
                            pointer-events: none;
                        }
                    </style>';
        })->escapeColumns([])
            ->make(true);
    }
}