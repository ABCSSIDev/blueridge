<?php

namespace App\Http\Controllers;

use App\Receiving;
use App\ReceivingItem;
use App\CanvassItem;
use App\Canvass;
use App\Inventory;
use App\Asset;
use App\Common;
use App\Supplier;
use App\PurchaseOrder;
use App\AssignedSupplyItem;
use App\PurchaseRequest;
use App\DisbursementVoucher;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BudgetAllocation;
use App\Http\Controllers\PurchaseRequestController;
use NumberToWords\NumberToWords;

class ReceivingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $budget_allocation = BudgetAllocation::all();
        return view('Inventory.Receiving.index',[
            'budget_year' => $budget_allocation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $get_purchase = PurchaseOrder::where('deleted_at',null)->get();
        // dd($get_purchase);
        $supplier = Canvass::where('awarded',1)->first();
        return view('Inventory.Receiving.create',[
            'PurchaseList' => $get_purchase,
            'Supplier' => $supplier
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $po = explode("-", $request->input('purchase_no'));
            $canvass_id = $po[0];
            
            $data_validation = Receiving::where('po_id',$canvass_id)->first();
            $can_val = self::ReceiValidation($canvass_id,$request);
            // dd($can_val);   
            if($can_val != 0){
                
                foreach($request->input('for_insert_receiving') as $key => $value){
                    if($request->input('quantity_received'.$value) != null && $request->input('remarks'.$value) != null){
                        $validation = ReceivingItem::where('canvass_item_id',$request->input('canvass_item_id'.$value))->first();
                        // dd($validation->quantity_received + $request->input('qtyRecei'.$value));
                        if($validation){
                            $newqty =  $validation->quantity_received + $request->input('quantity_received'.$value);
                            $receiving_item = ReceivingItem::where('canvass_item_id', $request->input('canvass_item_id'.$value))->update([
                                'quantity_received' => $newqty,
                                'updated_at'        => date('y-m-d H:i:s')
                            ]);
                        }else{
                            // dd($request->input('qtyRecei'.$value));
                            $receiving_item = ReceivingItem::create([
                                'receiving_id'      => $data_validation->id,
                                'canvass_item_id'   => $request->input('canvass_item_id'.$value),
                                'quantity_received' => $request->input('quantity_received'.$value),
                                'remarks'           => $request->input('remarks'.$value),
                                'inventory_type'    => $request->input('inventory_type'.$value),
                                'created_at'        => date('y-m-d H:i:s')
                            ]); 
                         
                        }
                         
                    }
                }  
            }else{
                $inventory_receiving = Receiving::create([
                    'po_id'             => $canvass_id,
                    'date_received'     => date('Y/m/d H:i:s', strtotime($request->input('date'))),
                    'receipt_number'    => $request->input('receipt_no'),
                    'rer_number'        => $request->input('rer_no'),
                    'inspector'         => $request->input('inspector'),
                    'created_at'        => date('y-m-d H:i:s')
                ]);
    
                foreach($request->input('for_insert_receiving') as $key => $value){
                    if($request->input('quantity_received'.$value) != null ){
                         $receiving_item = ReceivingItem::create([
                            'receiving_id'      => $inventory_receiving->id,
                            'canvass_item_id'   => $request->input('canvass_item_id'.$value),
                            'quantity_received' => $request->input('quantity_received'.$value),
                            'remarks'           => $request->input('remarks'.$value),
                            'inventory_type'    => $request->input('inventory_type'.$value),
                            'created_at'        => date('y-m-d H:i:s')
                        ]);
                    }
                }
            }
           
            $this->storeToInventory($request);

            $response = array(
                'status' => 'success',
                'reload' => true,
                'route' => route('getReloadtable'),
                'tableid' => 'receiving_details',
                'desc' => 'Inventory Receiving successfully created!'
            );
        } catch (\Exception $e){
            $response = array(
                'status' => 'error',
                'desc' => $e->getMessage()
            );
        }
        return response()->json($response,200);
    }
    public function ReceiValidation($can_id,$request) {
        $data_validation = Receiving::where('po_id',$can_id)->first();
        $data_validation1 = Canvass::where('id',$can_id)->first();
        $total1 = 0;
        $total = 0;
        $finaltotal = 0;
       
        if($data_validation){
            foreach($data_validation->getReceivingItems as $data){
                $total += $data->quantity_received;
            }
            foreach($data_validation1->getCanvasses as $data1){
                $total1 += $data1->available_quantity;
            }
            $finaltotal = $total1 - $total;
        }
        return $finaltotal;
    }
    public function getReloadtable() {
        $append = '';
        $append .= '<tr align="center">';
        $append .= '<td colspan="7">No data available in table</td>';
        $append .= '</tr>';
        return $append;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $inventory_receiving = Receiving::where('id',$id)->first();
        $po = PurchaseOrder::where('id', $inventory_receiving->po_id)->first();
        $pr_view = PurchaseRequest::where('id', $po->pr_id)->first();
        $getDisbursement = DisbursementVoucher::where('roa_id', $po->getPurchaseRequest->roa_id)->first();
        $receive_items = ReceivingItem::where('receiving_id',$id)->get();
        $totalrec_receive_items = ReceivingItem::all();
        $inventories = Receiving::where('po_id', $inventory_receiving->po_id)->get();
        // dd($receive_items);
        $get_total = 0;
        $totalrec = 0;
        $totalcount = 0;
        $total_items = 0;

        $canvasses = Canvass::where('awarded','1')->where('id',$po->canvass_id)->first();
        $canvass_item = CanvassItem::where('canvass_id',$canvasses->id)->get();
        $ids = common::getListAlphabet($po->pr_id, $canvasses->id);

        foreach($receive_items as $receiving) {
            $get_total += $receiving->getCanvassItem->unit_price * $receiving->quantity_received;
        }

        foreach($inventories as $inventory) {
            foreach($inventory->getReceivingItems as $items){
                $totalrec += $items->quantity_received;
            }
            if($inventory->id == $id){
                break;
            }
        }
        
        foreach($po->getCanvass->getCanvasses as $val) {
            $totalcount +=$val->available_quantity;
        }
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');
        return view('Inventory.Receiving.show',[
            'pr_view' => $pr_view,
            'receive_items' => $receive_items,
            'canvass_item' => $canvass_item,
            'pr_no' => common::getPRNumberFormat('purchase_requests', $pr_view),
            'ids' => $ids,
            'id' => $id,
            'po' => $po,
            'disbursement' => $getDisbursement,
            'inventory_receiving' => $inventory_receiving,
            'total_accept' => $get_total,
            'totalcount' => $totalcount,
            'total_recs' => $totalrec,
            'total_rec_in_words' => $numberTransformer->toWords($totalrec),
            'inventories' => $receive_items->count() > 0 ? $receive_items : false
        ]);
    }

    public function purchaseRequestID($id) {
        $canvasses = Canvass::where('pr_id', $id)
        ->where('awarded','1')->get();
        $obj = "<option>Select Supplier Name</option>";
        foreach($canvasses as $canvass) {
            $check_po = PurchaseOrder::where('pr_id', $id)->where('canvass_id', $canvass->id)->first();
            if(empty($canvass->getSuppliers)) {
                $obj .= "";
            }
            else {
                $condition_check = empty($check_po) ? 0 : $check_po->count();
                if($condition_check <= 0){
                    $obj .= "<option data-id='".$canvass->id."' data-url='".route('purchaseOrder',$canvass->id)."' value='".$canvass->supplier_id."'>".$canvass->getSuppliers->supplier_name."</option>";
                }
            }
        }
        return $obj;
    }
    public static function POtotal($id) {
        $receive_items = ReceivingItem::where('receiving_id',$id)->get();
        $total = 0;
        foreach($receive_items as $data){
            $canvass_item = CanvassItem::where('id',$data->canvass_item_id)->first();
            $total += $data->quantity_received * $canvass_item->unit_price;
        }
        return $total;
    }
    public function requestDate(Request $request) {
        $pr_id = $request->get('request_date');
        $pr_date = PurchaseRequest::where('id', $pr_id)->select('request_date')->first();
        return date('F d, Y', strtotime($pr_date->request_date));
    }

    public function supplierNameDetails(Request $request) {
        $supplier_id = $request->get('supplier');
        $supplier = Supplier::where('id', $supplier_id)->select('address')->first();
        if(empty($supplier->address)) {
            $supplier_address = "N/A";
        }
        else {
            $supplier_address = $supplier->address;
        }
        return $supplier_address;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $inventory_receiving = Receiving::where('id',$id)->first();
        $get_purchase = PurchaseOrder::all();
        $purchaseOrder = PurchaseOrder::where('id', $inventory_receiving->po_id)->first();
        $pr_view = PurchaseRequest::where('id', $purchaseOrder->pr_id)->first();
        $supplier = Canvass::where('awarded','1')->where('id',$purchaseOrder->canvass_id)->first();
        $ids = common::getListAlphabet($purchaseOrder->pr_id, $supplier->id);

        return view('Inventory.Receiving.edit',[
            'pr_view' => $pr_view,
            'ids' => $ids,
            'inventory_receiving' => $inventory_receiving,
            'PurchaseList' => $get_purchase,
            'Supplier'=>$supplier
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function functionRemoveInventoryReceiving($id) {
        Receiving::where('id',$id)->delete(); 
        return redirect(route('receiving.index'));
    }

    public function update(Request $request, $id) {
        try{
            $inventory_receiving = Receiving::where('id', $id)->update([
                'po_id'             => $request->input('purchase_order_id'),
                'date_received'     => date('y-m-d H:i:s',strtotime($request->input('date'))),
                // 'receipt_number'    => $request->input('receipt_no'),
                'updated_at'        => date('y-m-d H:i:s')
            ]);
            $this->updateToInventory($request);
            foreach($request->input('for_insert_receiving') as $key => $value){
                $receiving_item = ReceivingItem::where('id', $value)->update([
                    'quantity_received' => $request->input('qtyRecei'.$value),
                    'remarks'           => $request->input('remarks'.$value),
                    'inventory_type'    => $request->input('inventory_type'.$value),
                    'updated_at'        => date('y-m-d H:i:s')
                ]);
            }
            $response = array(
                'status' => 'success',
                'desc' => 'Inventory Receiving successfully Edited!'
            );
        }catch (\Exception $e){
            $response = array(
                'status' => 'error',
                'desc' => $e->getMessage()
            );
        }
        return response()->json($response,200);
    }
    public function updateToInventory($request) {
        foreach($request->input('for_insert_receiving') as $key => $value){
            if($request->input('quantity_received'.$value) != null){
                $ReceivingItem = ReceivingItem::where('id',$value)->first();
                $canvass = CanvassItem::where('id',$ReceivingItem->getCanvassItem->id)->first();
                $inventory = Inventory::where('inventory_name_id', $ReceivingItem->getCanvassItem->getPRItems->inventory_name_id)->first();
                $update_qty = Inventory::where('id', $inventory->id)->update([
                    'quantity' => $request->input('quantity_received'.$value),
                    'unit_price'        => $canvass->unit_price
                ]);
                error_reporting(0);
                if($request->input('inventory_type'.$value) == 'capex'){
                   
                    if($request->input('quantity_received'.$value) != $ReceivingItem->quantity_received){
                        $NewData =$request->input('quantity_received'.$value);
                        if($NewData > $ReceivingItem->quantity_received){
                            for ($i=$ReceivingItem->quantity_received ; $i < $request->input('quantity_received'.$value); $i++ ) { 
                                $asset_create = Asset::create([
                                    'inventory_id' => $inventory->id,
                                    'inventory_status_id' => 1,
                                    'asset_tag' => $this->assetTag($canvass->getPRItems->getInventoryName->category_id),
                                    'date_acquired' => date('Y-m-d')
                                ]);
                            }
                        }else{
                            $Assetlest = Asset::where('inventory_id',$inventory->id)->take($request->input('quantity_received'.$value))
                            ->get();
                            $id = array();
                            foreach($Assetlest as $data){
                                $id[] = $data->id; 
                            }
                            Asset::where('inventory_id',$inventory->id)->whereNotIn('id',$id)->delete(); 
                        }
                    }
                }
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function removeReceiving($id) {
        $inventory_receiving = Receiving::where('id',$id)->first();
        $get_purchase = PurchaseOrder::all();
        $purchaseOrder = PurchaseOrder::where('id', $inventory_receiving->po_id)->first();
        
        $pr_view = PurchaseRequest::where('id', $purchaseOrder->pr_id)->first();
        $supplier = Canvass::where('awarded','1')->where('id',$purchaseOrder->canvass_id)->first();
        $ids = common::getListAlphabet($purchaseOrder->pr_id, $supplier->id);

        $receiving = Receiving::where('id', $id)->first();
        return view('Inventory.Receiving.delete',['receiving' => $receiving,
        'ids' => $ids
        ]);
    }

    public function getPurchaseReceiving($id) {
        self::checkAllPOQuantity(1);
        $canvass = CanvassItem::where('canvass_id',$id)->get();

        $options = "";
        $item_ctr = 0;
        foreach ($canvass as $item){
            $remaining_quantity = self::getRemainingCanvassQuantity($item->id);
            if($remaining_quantity > 0){
                $ReceItem = ReceivingItem::where('canvass_item_id',$item->id)->first();
                $item_ctr++;
                $options .= "
                        <tr align='center'>
                            <input type='hidden' name='for_insert_receiving[]' value=".$item->id.">
                            <td><input type='hidden' name='canvass_item_id".$item->id."' value=".$item->id.">".$item_ctr."</td>
                            <td>".$item->getPRItems->getInventoryName->description."</td>
                            <td>".PurchaseRequestController::decToFraction($remaining_quantity)."</td>
                            <td>".$item->getPRItems->getUnits->unit_symbol."</td>
                            
                            <td><input type='hidden' name='qtyRecei".$item->id."' id='qtyRecei".$item->id."' class='qtyRecei".$item->id."'><input type='text'autocomplete='off' class='form-control text-center validation qty' placeholder='Qty. Received'   id='qty_input".$item->id."' onkeyup=' return validquantity(this,".$item->id.",".$remaining_quantity.",event)'   name='quantity_received".$item->id."'></td>
                            <td><input type='text' class='form-control text-center validation remarks' placeholder='Remarks' data-error='.error-container' value='".($ReceItem?$ReceItem->remarks:'')."' id='remarks".$item->id."' name='remarks".$item->id."' ></td>
                            <td><select ".($ReceItem?'readonly':'')." class='form-control'  name='inventory_type".$item->id."' data-error='.error-container'><option value='capex' ".($ReceItem && $ReceItem->inventory_type == 'capex'?'selected':'').">Capital Expenditures</option><option value='opex' ".($ReceItem && $ReceItem->inventory_type == 'opex'?'selected':'').">Operational Expenditures</option></select></td>
                        </tr>";
            }

        }

        return $options;
    }

    public function getReceivingList($year) {
        if($year == '0000'){
            $receiving_list = Receiving::whereNull('deleted_at')->get();
        }else{
            $receiving_list = Receiving::whereNull('deleted_at')->whereYear('date_received',$year)->get();
        } 
        // $receiving_list = Receiving::whereNull('deleted_at')->get();
        return DataTables::of($receiving_list)->addColumn('purchase_no',function($model){
            $id = common::getListAlphabet($model->purchaseOrder->pr_id, $model->PurchaseOrder->canvass_id);
            return $model->purchaseOrder->po_no;
            // return common::getPRNumberFormat("purchase_orders", $model->purchaseOrder->getPurchaseRequest,1).common::getAlphabet($model->purchaseOrder->pr_id, true, $id);
        })->addColumn('supplier_name',function($model){
            return $model->purchaseOrder->getCanvass->getSuppliers->supplier_name;
        })->addColumn('date_received',function($model){
            return Common::convertWordDateFormat($model->date_received);
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("receiving.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                      
                        </div>
                    </div>
                    </center>';
                    // <a class="dropdown-item '.(!$validation ? "" : "disable-link").'" href="'.route("receiving.edit",$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                    // <a class="dropdown-item popup_form '.(!$validation ? "" : "disable-link").'" data-toggle="modal" data-url="'.route("remove.receiving",$model->id).'" title="Delete Inventory Receiving Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
        })->make(true);
    }

    public function getRemainingCanvassQuantity($canvass_item_id) {
        $receiving_item_count = ReceivingItem::where("canvass_item_id", $canvass_item_id)->sum('quantity_received');
        $canvass_item = CanvassItem::where("id", $canvass_item_id)->first();
        return $canvass_item->available_quantity - $receiving_item_count;
    }

    public static function UnitDetails($id) {
        $data = DB::table('canvass_items')->where('canvass_items.id', $id)
        ->join('purchase_request_items as pritems','canvass_items.pr_item_id','pritems.id')
        ->join('units as un','pritems.unit_id','un.id')
        ->join('inventories as inv','pritems.inventory_id','inv.id')
        ->select('canvass_items.available_quantity','un.unit_name','inv.description')
        ->first();
        $data = CanvassItem::where('id',$id)->first();
        // dd($data);
        return $data;
    }

    public static function SupplierDetails($id) {
        // return $id;
        $data = DB::table('receivings')->where('receivings.id', $id)
        ->join('purchase_orders as pro','receivings.po_id','pro.id')
        ->join('canvasses as can','pro.canvass_id','can.id')
        ->join('suppliers as supp','can.supplier_id','supp.id')
        ->select('supp.supplier_name')
        ->first();
        return $data;
    }

    public static function checkAllPOQuantity($po_id) {
        $receivings = Receiving::where("po_id", $po_id)->get();

        $count = 0;
        foreach($receivings as $receiving){
            $receiving_items = ReceivingItem::where('receiving_id',$receiving->id)->groupBy("canvass_item_id")->get();
            foreach ($receiving_items as $receiving_item){
                $receiving_item_count = ReceivingItem::where("canvass_item_id", $receiving_item->canvass_item_id)->sum('quantity_received');
                $canvass_item = CanvassItem::where("id", $receiving_item->canvass_item_id)->first();
                // return $canvass_item;
                $quantity = $canvass_item->available_quantity - $receiving_item_count;
                if($quantity > 0){
                    $count++;
                }
            }
        }
        
        if($count > 0 || count($receivings) == 0){
            return true;
        }else{
            return false;
        }
    }

    public function storeToInventory($request) {
        foreach($request->input('for_insert_receiving') as $key => $value){
            if($request->input('quantity_received'.$value) != null && $request->input('remarks'.$value) != null){
                $canvass = CanvassItem::where('id',$request->input('canvass_item_id'.$value))->first();
                $inventory = Inventory::where('inventory_name_id', $canvass->getPRItems->inventory_name_id)->first();
                if($inventory){
                    // dd($inventory->quantity + $request->input('qtyRecei'.$value));
                    $update_qty = Inventory::where('id', $inventory->id)->update([
                        'quantity' => ($inventory->quantity + $request->input('quantity_received'.$value)),
                        'unit_price'        => $canvass->unit_price
                    ]);
                    error_reporting(0);
                    $id_data = $inventory->id;
                    $inventory_data = $inventory;
                }
                else{
                    $receiving_item = Inventory::create([
                        'inventory_name_id' => $canvass->getPRItems->inventory_name_id,
                        'unit_id'           => $canvass->getPRItems->unit_id,
                        'category_id'       => $canvass->getPRItems->getInventoryName->category_id,
                        'quantity'          => $request->input('quantity_received'.$value),
                        'unit_price'        => $canvass->unit_price
        
                    ]);
                    $inventory_data = $receiving_item;
                    $id_data = $receiving_item->id;
                }
                
                if($request->input('inventory_type'.$value) == 'capex'){
                    for ($i=0 ; $i < $request->input('quantity_received'.$value); $i++ ) { 
                        $asset_create = Asset::create([
                            'inventory_id' => $id_data,
                            'inventory_status_id' => 1,
                            'asset_tag' => $this->assetTag($canvass->getPRItems->getInventoryName->category_id),
                            'date_acquired' => date('Y-m-d')
                        ]);
                    }
                }
                // else{
                //     for ($i=0 ; $i < $request->input('quantity_received'.$value) ; $i++ ) { 
                        
                //         $asset_create = Inventory::create([
                //             'unit_id' => $inventory_data->unit_id,
                //             'category_id' => $inventory_data->category_id,
                //             'inventory_name_id' => $inventory_data->inventory_name_id,
                //             'quantity' => $inventory_data->quantity,
                //             'unit_price' => $inventory_data->unit_price,
                //             'image' => $inventory_data->image,
                //             'created_at' => date('Y-m-d')
                //         ]);
                //     }
                // }
            }
        }
    }

    public function assetTag($category_id) {
        $get_category = Inventory::where('category_id',$category_id)->get();
        $inventory_id = array();
        foreach($get_category as $inventory){
            $inventory_id[] = $inventory->id;
        }
        $statement = Asset::whereIn('inventory_id',$inventory_id)->orderBy('id', 'desc')->first();
        if($statement){
            $asset_inc = (int)$statement->asset_tag + 1;
        }else{
            $asset_inc = 1;
        }
        return sprintf("%03d", $asset_inc);

    }

    public function supplierName(Request $request) {
        $canvass = explode("-", $request->get('canvass_id'));
        $canvass_id = $canvass[1];
    
        $canvass = Canvass::where('id', $canvass_id)->first();
        $supplier = Supplier::where('id', $canvass->supplier_id)->select('supplier_name')->first();
        return $supplier->supplier_name;       
    }

    public function registration_obligation_search(Request $request){
        $append = '';
        $append .= '<tr align="center">';
        $append .= '<td colspan="7">No data available in table</td>';
        $append .= '</tr>';
        return $append;
    }
}
