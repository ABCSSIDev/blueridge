<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cedula;
use App\Common;
use App\Role;
use App\GeneralUser;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use App\BudgetAllocation;
use App\CedulaPad;

class CedulaController extends Controller
{
    public function index()
    {
        
        $budget_allocation = BudgetAllocation::all();
        return view('Accounting.Invoice.Cedula.index',[
            'budget_year' => $budget_allocation
        ]);
        
    }
    public function create()
    {
        $check_number = null;
        $or_settings = CedulaPad::orderBy('cd_pad_start', 'ASC')->first();
        $Cedula = Cedula::all();
        if ($or_settings != null){
            $start = $or_settings->first()['cd_pad_start'];
            $check_number = $Cedula->count() + $start;
        }
        return view('Accounting.Invoice.Cedula.create',[
            'ctc_number' => $check_number,
            'Cedula' => $Cedula,
        ]);
    }
    public function store(Request $request)
    {
        $user = Auth::user();
        try{
            $role = Role::where('name', 'LIKE', '%Cedula%')->first();
            $results = GeneralUser::where('first_name', 'like', '' .$request->input('first_name').'')->where('last_name', 'like', '' .$request->input('last_name').'')->where('middle_initial', 'like', '' .$request->input('middle_initial').'')->where('role_id',$role->id)->get();
            if(count($results)>0){
                $user_id = $results[0]->id;
            }else{
                $user_id = $this->insertUser($request);
            }

            $new_cedula = new Cedula([
                'ledger_id' => 3,
                'issued_date' => date('Y/m/d H:i:s',strtotime($request->input('issued_date'))),
                'cedula_id' => $user_id,
                'item_1' => $request->input('item_1'),
                'item_2' => $request->input('item_2'),
                'item_3' => $request->input('item_3'),
                'item_4' => $request->input('item_4'),
                'item_5' => $request->input('item_5'),
                'item_6' => $request->input('item_6'),
                'item_7' => $request->input('item_7'),
                'item_8' => $request->input('item_8'),
                'item_9' => $request->input('item_9'),
                'item_10' => $request->input('item_10'),
                'interest' => $request->input('interest'),
                'due_date' => Common::formatDate('Y-m-d',$request->input('due_date')),
                'ctc' => $request->input('ctc_number'),
                'created_by' => $user->id,
                'created_at' => date('Y/m/d H:i:s')
            ]);

            $new_cedula->save();

            $response = array(
                'status' => 'success',
                'desc' => 'Cedula successfully created!',
                'refresh_cd' => true,
                'ctc_number' => $new_cedula->ctc+1
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }
    public function functionORPad()
    {
        
        $or_settings = CedulaPad::orderBy('id','DESC')->first();
        $or_pads = CedulaPad::all();
        $Cedula = Cedula::all();
        return view('Accounting.Invoice.Cedula.cd_pad',['or_settings' => $or_settings,'or_pads' => $or_pads,'Cedula' => $Cedula]);
    }
    public function autocompleteCtc(Request $request)
    {
        $search = $request->get('Ctc');
        $ORPadDatas = CedulaPad::all();
        $optotalcount = 0;
        foreach($ORPadDatas as $data){
            $optotalcount += $data->cd_pad_quantity;
        }
        $Invoicecount = Cedula::all();
        $ORPadASC = CedulaPad::orderBy('id', 'ASC')->first();
        $rangeto = $ORPadASC->cd_pad_start + $optotalcount;
        $rangefrom = $ORPadASC->cd_pad_start;
        $invtotalcount = 0;
        foreach($Invoicecount as $data){
            if($data->ctc >= $rangefrom && $data->ctc <= $rangeto){
                $invtotalcount += 1;
            }
        }
        $Invoicedata = Cedula::where('ctc',$search)->count();
        $result = '';
        $ORPadDESC = CedulaPad::orderBy('id', 'DESC')->first();
        $LastORNumber = $ORPadDESC->cd_pad_start + $ORPadDESC->cd_pad_quantity;
        // dd($total);
        if($Invoicedata>0){
            $result = 'duplicat';
        }
        else if($search > $LastORNumber || $search < $ORPadASC->cd_pad_start){
            $result = 'outofrange';
        }
        else if($invtotalcount == $ORPadDESC->cd_pad_quantity){
            $result = 'maxorpad';
        }
        else if($search <= 0){
            $result = 'zero';
        }
        
        
        return response()->json($result);
    }     
    public function functionCreateCTCPad(Request $request)
    {
        // return $request;
        try{
            $new_pad = new CedulaPad([
                'cd_pad_start' => $request->input('pad_start'),
                'cd_pad_quantity' =>$request->input('pad_end'),
                'created_at' => date('Y/m/d H:i:s')
            ]);

            $new_pad->save();
            $response = array(
                'status' => 'success',
                'desc' => 'CTC Pad successfully created!',
                'refresh_cd_pad' => true,
                'ctc_number' => (int)$new_pad->cd_pad_start
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200); 
    }
    public function show($id)
    {
        $cedula_data = Cedula::where('id',$id)->first();
        $int = ($cedula_data->item_1 + $cedula_data->item_2 + $cedula_data->item_3 + $cedula_data->item_4 + $cedula_data->item_5 + $cedula_data->item_6 + $cedula_data->item_7 + $cedula_data->item_8 + $cedula_data->item_9 + $cedula_data->item_10)*$cedula_data->interest;
        $totalamount = $int + $cedula_data->item_1 + $cedula_data->item_2 + $cedula_data->item_3 + $cedula_data->item_4 + $cedula_data->item_5 + $cedula_data->item_6 + $cedula_data->item_7 + $cedula_data->item_8 + $cedula_data->item_9 + $cedula_data->item_10;
        return view('Accounting.Invoice.Cedula.show',['cedula_data' => $cedula_data,'totalamount' => $totalamount]);
    }
    public function edit($id)
    {
        $cedula_data = Cedula::where('id',$id)->first();
        $int = ($cedula_data->item_1 + $cedula_data->item_2 + $cedula_data->item_3 + $cedula_data->item_4 +$cedula_data->item_5 + $cedula_data->item_6 + $cedula_data->item_7 + $cedula_data->item_8 + $cedula_data->item_9 + $cedula_data->item_10)*$cedula_data->interest;
        $totalamount = $int + $cedula_data->item_1 + $cedula_data->item_2 + $cedula_data->item_3 + $cedula_data->item_4 + $cedula_data->item_5 + $cedula_data->item_6 + $cedula_data->item_7 + $cedula_data->item_8 + $cedula_data->item_9 + $cedula_data->item_10;
        return view('Accounting.Invoice.Cedula.edit',['cedula_data' => $cedula_data,'totalamount' => $totalamount]);
    }
    public function update(Request $request,$id)
    {
        try{
            $edit = Cedula::where('id', $id)->update([
                'ledger_id' => 3,
                'issued_date' => date('Y/m/d H:i:s',strtotime($request->input('issued_date'))),
                'item_1' => $request->input('item_1'),
                'item_2' => $request->input('item_2'),
                'item_3' => $request->input('item_3'),
                'item_4' => $request->input('item_4'),
                'item_5' => $request->input('item_5'),
                'item_6' => $request->input('item_6'),
                'item_7' => $request->input('item_7'),
                'item_8' => $request->input('item_8'),
                'item_9' => $request->input('item_9'),
                'item_10' => $request->input('item_10'),
                'interest' => $request->input('interest'),
                'ctc' => $request->input('ctc'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $get_user = Cedula::where('id',$id)->first();
            $edit_user = GeneralUser::where('id',$get_user->cedula_id)->update([
                'name' => $request->input('first_name').' '.$request->input('last_name'),
                'first_name' => $request->input('first_name'),
                'middle_initial' => $request->input('middle_initial'),
                'last_name' => $request->input('last_name'),
                'address' => $request->input('address'),
                'citizenship' => $request->input('citizenship'),
                'birth_place' => $request->input('birth_place'),
                'civil_status' => $request->input('civil_status'),
                'birth_date' => date('Y/m/d H:i:s',strtotime($request->input('birth_date'))),
                'sex' => $request->input('sex'),
                'tin' => $request->input('tin'),
            ]);

            $response = array(
                'status' => 'success',
                'desc'  => 'Cedula Successfully Updated!'
            );
        }
        
        catch (\Exception $exception){
            
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);

    }
    public function removeCedula($id){
        $Cedula = Cedula::where('id', $id)
        ->first();
        return view('Accounting.Invoice.Cedula.delete', ['id' => $id, 'Cedula' => $Cedula]);
    }
    public function destroy($id)
    {
        Cedula::where('id', $id)
            ->delete();
        return redirect(route('cedula.index'));
    }
    public function getCedulaList($year){
        if($year == '0000'){
            $cedula_list = Cedula::all();
        }else{
            $cedula_list = Cedula::whereYear('issued_date',$year)->get();
        }
        return DataTables::of($cedula_list)->addColumn('first_name',function($model){
            return $model->userData->first_name;
        })->addColumn('last_name',function($model){
            return $model->userData->last_name;
        })->addColumn('tin',function($model){
            return $model->userData->tin ? $model->userData->tin : "N/A"; 
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route("cedula.show",$model->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                            <a class="dropdown-item" href="'.route('cedula.edit',$model->id).'"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item popup_form" data-toggle="modal" style="cursor:pointer" data-url="'.route('remove.cedula',$model->id).'" title="Delete Cedula Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->escapeColumns([])
            ->make(true);
    }
    public function autocompleteResidents(Request $request)
    {
        $search = $request->get('term');
        // $role = Role::where('name', 'LIKE', '%Cedula%')->first();
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->get();
        // dd($result);
        return response()->json($result);
    } 
    public function autocompleteCedula(Request $request)
    {
        $search = $request->get('term');
        $role = Role::where('name', 'LIKE', '%Cedula%')->first();
        $result = GeneralUser::where('name', 'LIKE', '%'. $search. '%')->where('role_id',$role->id)->get();
        return response()->json($result);
    } 

    public function insertUser(Request $request)
    {
        $role = Role::where('name', 'LIKE', '%Cedula%')->first();
        $insert_user = GeneralUser::create([
            'role_id' => $role->id,
            'name' => $request->input('first_name').' '.$request->input('last_name'),
            'first_name' => $request->input('first_name'),
            'middle_initial' => $request->input('middle_initial'),
            'last_name' => $request->input('last_name'),
            'address' => $request->input('address'),
            'citizenship' => $request->input('citizenship'),
            'birth_place' => $request->input('birth_place'),
            'civil_status' => $request->input('civil_status'),
            'birth_date' => date('Y/m/d H:i:s',strtotime($request->input('birth_date'))),
            'sex' => $request->input('sex'),
            'tin' => $request->input('tin'),
            'created_at' => date('Y/m/d H:i:s')
        ]);
        return  $insert_user->id;
    }
}
