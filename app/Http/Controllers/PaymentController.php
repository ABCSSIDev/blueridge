<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Cedula;
use App\Common;
use App\DisbursementVoucher;
use App\Invoice;
use App\Payment;
use App\PaymentMethod;
use App\Payroll;
use App\ROA;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type,$id)
    {
        $banks = Bank::all();
        $paymentMethods = PaymentMethod::all();
        $total  = 0;
        switch ($type){
            case 'invoice':
                $reference = Invoice::findOrFail($id);
                $label = 'Receive payment for OR#: '.$reference->or_number;
                $model = 'App\Invoice';
                $total = $reference->getInvoice->unit_price;
                break;
            case 'disbursement-voucher':
                $reference = DisbursementVoucher::findOrFail($id);
                $model = 'App\DisbursementVoucher';
                $label = 'Make payment for ROA with Obligation#: '.$reference->getRoa->obligation_number;
                $total = $reference->getRoa->total_amount;
                break;
            case 'cedula':
                $reference = Cedula::findOrFail($id);
                $model = 'App\Cedula';
                $label = 'Receive payment for Cedula#'.$reference->id;
                $total = $reference->item_1 + $reference->item_2 + $reference->item_3 + $reference->item_4 + $reference->item_5;
                break;
            case 'payroll':
                $reference = Payroll::findOrFail($id);
                $model = 'App\Payroll';
                $label = 'Make payment for Payroll#: '.$reference->payroll_number;
                $total = ($reference->getPayrollItems->sum('salary') + $reference->getPayrollItems->sum('honoraria') + $reference->getPayrollItems->sum('other_benefits')) - $reference->getPayrollItems->sum('bir');
                break;
            default:
                $label = '';
                $model = '';
                $total = 0;
                break;

        }
        $message = '<div class="alert alert-primary" role="alert">
                      '.$label.'
                    </div>';
        return view('Accounting.Payment.create',['banks' => $banks , 'payment_methods' => $paymentMethods,'total' => $total,'type' => $type,'reference_id' => $id,'message' => $message,'model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $payment = new Payment();
            $payment->bank_id = $request->input('bank_id');
            $payment->paid_into = $request->input('paid_into');
            $payment->payment_id  = $request->input('payment_id');
            $payment->payable_type = $request->input('model');
            $payment->payable_id = $request->input('reference_id');
            $payment->date_received = Common::formatDate("Y-m-d",$request->input('received_date'));
            $payment->amount_received = floatval(implode(explode(',',$request->input('amount'))));
            $payment->is_cancelled = ($request->input('cancelled') ? 1 : 0);
            $payment->created_at = now();
            $payment->save();

            $response = array(
                'status' => 'success',
                'href' => true,
                'route' => route($request->input('type').'.show',$request->input('reference_id')),
                'desc' => 'Payment successfully created!'
            );
        }catch (\Exception $exception){
            $response = Common::catchError($exception);
        }
        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
