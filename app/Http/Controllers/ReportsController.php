<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Transaction;
use App\Common;
use App\PurchaseRequest;
use App\Http\Controllers\BudgetAllocationController;
use App\Payment;
use App\ORPad;
use App\DVPad;
use App\AccountLedger;
use App\BudgetAllocationItem;
use App\Invoice;
use App\BudgetAllocation;
use App\DisbursementVoucher;
use App\InvoiceItem;
use Illuminate\Http\Request;
use App\Document;
use App\Cedula;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Inventory;
use App\AccountGroup;
use App\BudgetAllocationGroup;
use App\InventoryName;
use App\PaymentMethod;
use App\ROA;
use App\GeneralUser;
use App\Payroll;
use Carbon\Carbon;
use App\AssignedSupplyItem;
class ReportsController extends Controller{
    private $array_group_actual = array();
    private $array_temp_actual = array();
    private $edit_array = array();
    private $html = "";

    public function index(){
        return view('Reports.index');
    }
    public function estimatedActual(){
        return view('Reports.Accounting.estimated_actual');
    }
    public function rbiform(){
        $tbody = '';
        $GeneralUser = GeneralUser::orderBy('cluster', 'asc')->get();
        foreach ($GeneralUser as $data){
            $name = explode(' ', $data->name, 3);
            $address = explode(' ', $data->address, 2);
            $tbody .= '<tr>';
            $tbody .= '<td>'.$data->cluster.'</td>';
            $tbody .= '<td>'.$data->household.'</td>';
            $tbody .= '<td>'.(!empty($address[0]) ? $address[0] : '').'</td>';
            $tbody .= '<td>'.(!empty($address[1]) ? $address[1] : '').'</td>';
            $tbody .= '<td>'.(!empty($name[1]) ? $name[1] : '').'</td>';
            $tbody .= '<td>'.(!empty($name[0]) ? $name[0] : '').'</td>';
            $tbody .= '<td>'.(!empty($name[2]) ? $name[2] : '').'</td>';
            $tbody .= '<td>'.$data->quantifier.'</td>';
            $tbody .= '<td>'.date("M",strtotime($data->birth_date)).'</td>';
            $tbody .= '<td>'.date("d",strtotime($data->birth_date)).'</td>';
            $tbody .= '<td>'.date("Y",strtotime($data->birth_date)).'</td>';
            $tbody .= '<td>'.$data->birth_place.'</td>';
            $tbody .= '<td>'.($data->sex == 1 ? "Male" : "Female").'</td>';
            $tbody .= '<td>'.($data->civil_status == 1?'Single':($data->civil_status == 2?'Married':($data->civil_status == 3?'Widowed':'Divorced'))).'</td>';
            $tbody .= '<td>'.$data->occupation.'</td>';
            $tbody .= '<td>'.$data->citizenship.'</td>';
            $tbody .= '<td>'.$data->religion.'</td>';
            $tbody .= '<td>'.$data->health_status.'</td>';
            $tbody .= '<td>'.$data->highest_education.'</td>';
            $tbody .= '<td>'.$data->contact_number.'</td>';
            $tbody .= '</tr>';
        }
        return view('Reports.Accounting.rbi',['tbody' => $tbody]);
    }
    private $array_group = array();

    //Views for reports
    public function accountStatement($id){
        // $bank = Bank::with(['getPayments' => function($query){
        //     $query->orderBy('date_received','asc');
        // }])->where('id',$id)->first();
        $bank = '';
        $BankOption = Bank::all();
        $tbody = '';
        $balance = '0.00';
        $validation = 'show';
        // dd($bank);
        // if($bank || $bank != null){
        //     $balance = $bank->initial_deposit;
        //     $validation = 'hide';

        //     foreach ($bank->getPayments as $payment){
        //         $balance += Common::getPaymentType($payment->id)['balance'];
        //         $tbody .= '<tr>';
        //         $tbody .= '<td>'.Common::getPaymentType($payment->id)['payment_type'].'</td>';
        //         $tbody .= '<td>'.Common::formatDate('F d, Y',$payment->date_received).'</td>';
        //         $tbody .= '<td>'.Common::getPaymentType($payment->id)['description'].'</td>';
        //         $tbody .= '<td>'.Common::getPaymentType($payment->id)['spent'].'</td>';
        //         $tbody .= '<td>'.Common::getPaymentType($payment->id)['received'].'</td>';
        //         $tbody .= '<td>'.($balance > 0 ? number_format($balance,2) : "(".number_format(abs($balance),2).")").'</td>';
        //         $tbody .= '</tr>';
        //     }
            
        // }else{
        $tbody .= '<tr align="center">
            <td colspan="15">No Data Available</td>
        </tr>';
        // }
        
        $to = '';
        $from = '';
        return view('Reports.Accounting.account_statement',['bank' => $bank,'BankOption' => $BankOption,'tbody' => $tbody,'from' => $from,'to' => $to]);
    }
    public function functionaccountStatement(Request $request){
        // dd(1);
        $bank = Bank::with(['getPayments' => function($query){
            $query->orderBy('date_received','asc');
        }])->where('id',$request->input('bank'))->first();
        $BankOption = Bank::all();
        $tbody = '';
        $to = $request->input('to');
        $from = $request->input('from');
        $balance = $bank->initial_deposit;
        $count =0;
        foreach ($bank->getPayments as $payment){
            if(date('d/m/Y',strtotime($payment->created_at)) > date('d/m/Y',strtotime($request->input('from'))) && date('d/m/Y',strtotime($payment->created_at)) < date('d/m/Y',strtotime($request->input('to')))){
                $balance += Common::getPaymentType($payment->id)['balance'];
                $tbody .= '<tr>';
                $tbody .= '<td>'.Common::getPaymentType($payment->id)['payment_type'].'</td>';
                $tbody .= '<td>'.Common::formatDate('F d, Y',$payment->date_received).'</td>';
                $tbody .= '<td>'.Common::getPaymentType($payment->id)['description'].'</td>';
                $tbody .= '<td>'.Common::getPaymentType($payment->id)['spent'].'</td>';
                $tbody .= '<td>'.Common::getPaymentType($payment->id)['received'].'</td>';
                $tbody .= '<td>'.($balance > 0 ? '₱&nbsp;'.number_format($balance,2) : "(₱&nbsp;".number_format(abs($balance),2).")").'</td>';
                $tbody .= '</tr>';
                $count ++;
            }
           
        }
        if($count == 0){
            $tbody .= '<tr align="center" style="border-style:hidden;">
                <td colspan="15">No data available in table</td>
            </tr>';
        }
        return view('Reports.Accounting.account_statement',['bank' => $bank,'BankOption' => $BankOption,'tbody' => $tbody,'from' => $from,'to' => $to]);
    }
    public function manualTransactions(){
        $paymentMethods = PaymentMethod::all();
        return view('Reports.Accounting.addmanualtransactions',['payment_methods' => $paymentMethods]);
    }
    public function functionmanualTransactions(Request $request){
        $Transaction = Transaction::create([
            'ledger_id' => $request->input('ledgers'),
            'type' => $request->input('type'),
            'description' => (empty($request->input('description'))?'':$request->input('description')),
            'amount' => $request->input('amount'),
            'payment_id' => $request->input('payment_methods'),
            'date_received' => date('Y/m/d H:i:s'),
            'created_at' => date('Y/m/d H:i:s')
        ]);
        $Payment = Payment::create([
            'bank_id' => 1,
            'payment_id' => $request->input('payment_methods'),
            'payable_type' => 'App\Transaction',
            'payable_id' => $Transaction->id,
            'date_received' => date('Y/m/d H:i:s'),
            'amount_received' => $request->input('amount'),
            'created_at' => date('Y/m/d H:i:s')
        ]);
        $response = array(
            'status' => 'success',
            'desc' => 'Manual Transaction Successfully Created!'
        );
        return response()->json($response,200);
    }
    public function ledgersmanualTransactions(Request $request){
        $type = ($request->input('type') == 1?0:1);
        $Transaction = AccountLedger::where('account_type',$type)->get();
        $tbody = '';
        $tbody .= ' <option value="">Select Ledgers</option>';
        foreach($Transaction as $data){
            $tbody .= ' <option value="'.$data->id.'">'.$data->ledger_name.'</option>';
        }
        return $tbody;
    }
    public function appropriation(){
        return view('Reports.Accounting.appropriation');
    }
    public function searchappropriation(Request $request){
     
        $data = BudgetAllocation::where('year',$request->input('year'))->first();
        $tbody = '';
        $tbody .= '<tr>
                    <td colspan="2" rowspan="6" style="border-bottom-color: white;">
                        <div class="align-left" style="margin-top: 8px;">
                            <span style="margin-left: 1rem;">I.</span> <span style="margin-left: 1rem;">Current Year Appropriations</span>
                            <div class="soa-p per-s">
                                <p>Personnel Services</p>
                            </div>
                            <div class="soa-p mooe">
                                <p>Maintenance and Other Operating Expenses</p>
                            </div>
                            <div class="soa-p spa-p" style="width: 90%;">
                                <p>Special Purpose Appropriation (Sangguniang Kabataan)</p>
                            </div>
                            <div class="soa-p per-s">
                                <p>Total Basic and Facilities Program</p>
                            </div>
                            <div class="soa-p total-appro">
                                <p class="fw-700">TOTAL</p>
                            </div>
                        </div>
                    </td>
                    <td colspan="5" style="text-align: left; border-bottom-color: black; border-left: hidden;padding-top: 0.5rem;padding-bottom: 0.5rem;">₱'.self::appropriationtotal($request->input('year')).'</td>
                   </tr>';
        if($data){
            for ($x = 0; $x <= 4; $x++) {
                $array = array();
                if($x == 0){
                    $array = array(4);
                }if($x == 1){
                    $array = array(5,6,7,8,9,10,11,12,13,14,15);
                }if($x == 2){
                    $array = array(17);
                }if($x == 3){
                    $array = array(18);
                }if($x == 4){
                    $array = array(4,5,6,7,8,9,10,11,12,13,14,15,17,18);
                }
                $total_amnt = [];
                // dd($request->input('year'));
                self::appropriationobli($request->input('year'), $array,2);
               
                foreach($array as $arr){
                    array_push($total_amnt,(array_key_exists($arr,$this->array_group_actual) ? array_sum($this->array_group_actual[$arr]['amount']) : 0));
                    $table_arr = [
                        '₱&nbsp;'.number_format(self::appropriationfinal($request->input('year'),$array), 2),'&nbsp;','₱&nbsp;'.number_format(self::appropriationfinal($request->input('year'),$array), 2), '₱&nbsp;'.number_format(array_sum($total_amnt), 2),'₱&nbsp;'.number_format(self::appropriationfinal($request->input('year'),$array)-array_sum($total_amnt), 2)
                    ];
                }
                // dd($table_arr);
                $tbody .= self::createappropriationTable($table_arr,'tr','td class="bbc-soa" style="padding-top: 0.5rem;padding-bottom: 0.5rem;"');
                // dump($tbody);
                $this->array_group_actual = array();
            }
            return $tbody;
        }
       
    }
    public function appropriationfinal($year,$id){
        $groups = BudgetAllocation::where('year',$year)->first();
        $total = 0;
        foreach($groups->getBudgetAllocationItems as $val){
            if(in_array($val->getLedger->group_id, $id)){
                $total += $val->amount;
            }
        }
        return $total;
    }

    public function appropriationobli($year,$id,$type){
        $groups = AccountGroup::all();
        $roas = ROA::whereYear('approval_date',$year)->get();
        $invoices = Invoice::whereYear('invoice_date',$year)->get();
        $payrolls = Payroll::whereYear('updated_at',$year)->get();
        // $disbursements = DisbursementVoucher::whereYear('updated_at',$year)->get();

        foreach($groups as $group){
            $this->array_group_actual[$group->id]['amount'][] = 0;
            $this->array_group_actual[$group->id]['group_name'] = $group->group_name;
        }      

        foreach($invoices as $invoice){
            if(!is_null($invoice->getPayment)){
                $this->array_temp_actual[$invoice->getLedger->group_id][] = (float)$invoice->getInvoice->unit_price;
            }
           
        }
        
        foreach($roas as $roa){
            $voucher = DisbursementVoucher::whereNull('deleted_at')->where('roa_id',$roa->id)->first();
            if(!is_null($voucher->getPayment)){
                $this->array_temp_actual[$roa->getLedger->group_id][] = (float)$roa->total_amount;
            }
        }

        foreach ($payrolls as $payroll){
            // if(!is_null($payroll->getPayment)){
                $this->array_temp_actual[$payroll->getLedger->group_id][] = (float)($payroll->getPayrollItems->sum('salary') + $payroll->getPayrollItems->sum('honoraria') + $payroll->getPayrollItems->sum('other_benefits')) - $payroll->getPayrollItems->sum('bir');
            // }
        }
        self::computeActual();
        
        $total = 0 ;
        if($type == 2){
            foreach($id as $val){
                $total += (array_key_exists($val,$this->array_group_actual) ? array_sum($this->array_group_actual[$val]['amount']) : 0);
            }
        }
        // return 1;
        return $total;
    }
    public function computeActual(){
        $temp_array = array();
        foreach($this->array_temp_actual as $key => $temp){
            $this->array_group_actual[$key]['amount'][] = array_sum($temp);
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                $temp_array[$check_if_has_group->getGroup->id][] = array_sum($temp);
            }
        }
        $this->array_temp_actual = $temp_array;
        if(count($this->array_temp_actual) > 0){
            self::computeActual();
        }
    }
    public function appropriationtotal($year){
        $BudgetAllocation = BudgetAllocation::where('year',$year)->first();
        $credit = 0;
        $debit = 0;
        if($BudgetAllocation){
            foreach($BudgetAllocation->getBudgetAllocationItems as $val){
                if($val->getLedger->account_type == 0){
                    $debit += $val->amount;
                }else{
                    $credit += $val->amount;
                }
                
            }
        }
     
        return number_format($credit , 2);
    }
    public function createappropriationTable($table_data,$head,$body){
        $table_string = "<".$head.">";
        foreach($table_data as $table_datum){
            $table_string .= "<".$body.">".$table_datum."</td>";
        }
        $table_string .= "</".$head.">";
        return $table_string;
    }
    public function transmittal(){
        $collection_nature = Invoice::orderBy('collection_nature', 'desc')
        ->groupBy('collection_nature')
        ->get();
        return view('Reports.Accounting.transmittal_letter', [
            'year' => '',
            'month' => '',
            'RCDSuppDoc' => null,
            'OtherReports' => null,
            'collection_nature' => $collection_nature,
            'collectionval' => 'none']);
    }

    public function searchTransmittal(Request $request){
        
        $total = 0;
        $pay = 0;
    
            $collection_nature = Invoice::orderBy('collection_nature', 'desc')
            ->groupBy('collection_nature')
            ->get();
            $year = $request->input('year');
            $month = $request->input('month');
            $collectionval = $request->input('collection_nature');
           
            if($collectionval == ""){
                $invoices = Invoice::whereYear('invoice_date', $request->input('year'))
                    ->whereMonth('invoice_date', $request->input('month'))->get();
            }else{
                 $invoices = Invoice::whereYear('invoice_date', $request->input('year'))
                ->whereMonth('invoice_date', $request->input('month'))
                ->where('collection_nature',$collectionval)
                ->get();
            }
            $payments = Payment::whereYear('date_received', $request->input('year'))
                ->whereMonth('date_received', $request->input('month'))->get();
            foreach($invoices as $invoice){
                
                $total += $invoice->getInvoice->quantity * $invoice->getInvoice->unit_price;
            }
            $DVPadData = $this->BeginningPadData('dv_pad','dv_pad_start','dv_pad_quantity');
            $ORPadData = $this->BeginningPadData('or_pad','or_pad_start','or_pad_quantity'); 
            $ORPadsData = $this->IssuedPadData('invoices','or_number','invoice_date',$request->input('year'),$collectionval,$request->input('month'));
            $DVPadsData = $this->IssuedPadData('disbursement_vouchers','check_number','cheque_date',$request->input('year'),$collectionval,$request->input('month'));
           
            foreach($payments as $paid){
                $pay += $paid->amount_received;
            }
            
            $DisbursementVoucher = DisbursementVoucher::whereYear('cheque_date', $request->input('year'))
            ->whereMonth('cheque_date', $request->input('month'))->get();
            $total = $pay;
            // dd($this->RCDSuppDoc($month,$year,$invoices,$collectionval));
            return view('Reports.Accounting.transmittal_letter', [
            'OtherReports' => 1,
            'DisbursementVoucher' => $DisbursementVoucher, 
            'RCDSuppDoc' => $this->RCDSuppDoc($month,$year,$invoices,$collectionval),
            'collectionval' => $collectionval,
            'collection_nature' => $collection_nature,
            'ORPadsData' => $ORPadsData,
            'DVPadsData' => $DVPadsData,
            'ORPad' => $ORPadData,
            'DVPad' => $DVPadData,
            'invoices' => $invoices,
             'payments' => $payments,
              'year' => $year, 'month' => $month, 'total' => $total]);
        
       
    }
    public function functionAccountability(Request $request){
        $year = $request->input('year');
        $month = $request->input('month');
        $DVPadData = $this->BeginningPadData('dv_pad','dv_pad_start','dv_pad_quantity');
        $ORPadData = $this->BeginningPadData('or_pad','or_pad_start','or_pad_quantity');
        $DVPadsData = $this->IssuedPadDataAcc('disbursement_vouchers','check_number','cheque_date',$request->input('year'),$request->input('month'));
        $ORPadsData = $this->IssuedPadDataAcc('invoices','or_number','invoice_date',$request->input('year'),$request->input('month'));
        return view ('Reports.Accounting.accountability_form',[
            'ORPadsData' => $ORPadsData,
            'DVPadsData' => $DVPadsData,
            'ORPadData' => $ORPadData,
            'DVPad' => $DVPadData,
            'year' => $year,
            'month' => $month,
            'ORPad' => $ORPadData,
            'DVPadData' => $DVPadData]);
    }
    public function IssuedPadDataAcc($table,$selected1,$selected2,$year,$month){
        $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->first();
        $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->get();
        $DVPadData = array();
        $quantitydvs = 0;
        $lastdvs = 0;
        $firstdata = 0;
        foreach($DVPads as $data){
            $quantitydvs += 1;
            $lastdvs = $data->$selected1;
            $firstdata = $DVPadfirst->$selected1;     
        }
        $DVPadData['quantity'] = $quantitydvs;
        $DVPadData['lastdvs'] = $lastdvs;
        $DVPadData['first'] = $firstdata;
        return $DVPadData;
    }
    public function RCDSuppDoc($Month,$Year,$Invoices,$collectionval){
        $RCDSuppDocData = array();
        $dt = Carbon::createFromDate($Year, $Month);
        // dd($Month);
        $PadDataOR = $this->TableIssuedPadData('invoices','or_number','invoice_date',$Year,$collectionval,$Month);
        $amount = 0;
        foreach($Invoices as $data){
            // if(!is_null($data->getPayment)){ 
                 $amount += $data->getInvoice->unit_price;
            // }
        }
        // dd($amount);
        $RCDSuppDocData['Date'] =date('M',strtotime($Year.'-'.$Month.'-01')).' 1 - '.$dt->daysInMonth.' , '.$Year;
        $RCDSuppDocData['rangepad'] =$PadDataOR['first'] .' - '.$PadDataOR['lastdvs'];
        $RCDSuppDocData['total'] =$amount;
        return $RCDSuppDocData;
    }
    public function TableIssuedPadData($table,$selected1,$selected2,$year,$collectionval,$month){
        $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->first();
        $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->get();
        if($table == 'invoices'){
            $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->get();
            $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->first();
            if($collectionval != null){
                $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->where('collection_nature',$collectionval)->get();
                $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->where('collection_nature',$collectionval)->first();
            }
        }
        // return $table;
        $DVPadData = array();
        $quantitydvs = 0;
        $lastdvs = 0;
        $firstdata = 0;
        foreach($DVPads as $data){
            if($table == 'invoices'){
                $validation =Invoice::where('id',$data->id)->first();
                if(!is_null($validation->getPayment)){ 
                    $lastdvs = $data->$selected1;
                    $firstdata = $DVPadfirst->$selected1;    
                    $quantitydvs ++; 
                }
            }
            if($table == 'disbursement_vouchers'){
                $validation =DisbursementVoucher::where('id',$data->id)->first();
                // if(!is_null($validation->getPayment)){ 
                    $lastdvs = $data->$selected1;
                    $firstdata = $DVPadfirst->$selected1;    
                    $quantitydvs ++; 
                // }
            }
           
        }
        // dd($quantitydvs);
        $DVPadData['quantity'] = $quantitydvs;
        $DVPadData['lastdvs'] = $lastdvs;
        $DVPadData['first'] = $firstdata;
        return $DVPadData;
    }
    public function BeginningPadData($table,$selected1,$selected2){
        $Pad= DB::table($table)->orderBy($selected1, 'desc')->get();
        $PadData = array();
        $quantity = 0;
        $start = 0;
        foreach($Pad as $data){
            $quantity += $data->$selected2;
            $start = $data->$selected1;
        }
        $PadData['quantity'] = $quantity;
        $PadData['start'] = $start;
        return $PadData;
    }
    public function IssuedPadData($table,$selected1,$selected2,$year,$collectionval,$month){
        $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->first();
        $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->get();
        if($table == 'invoices'){
            $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->get();
            $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->first();
            if($collectionval != null){
                $DVPads = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->where('collection_nature',$collectionval)->get();
                $DVPadfirst = DB::table($table)->orderBy($selected1, 'asc')->whereYear($selected2, $year)->whereMonth($selected2, $month)->where('deleted_at', null)->where('collection_nature',$collectionval)->first();
            }
        }
        // return $table;
        $DVPadData = array();
        $quantitydvs = 0;
        $lastdvs = 0;
        $firstdata = 0;
        foreach($DVPads as $data){
            $lastdvs = $data->$selected1;
            $firstdata = $DVPadfirst->$selected1;    
            $quantitydvs += 1; 
        }
        $DVPadData['quantity'] = $quantitydvs;
        $DVPadData['lastdvs'] = $lastdvs;
        $DVPadData['first'] = $firstdata;
        return $DVPadData;
    }
    public function accountability(){
        return view('Reports.Accounting.accountability_form');
    }
   
    public function suppliesMaterials(){
        return view ('Reports.Inventory.supplies_materials');
    }
    public function Searchcashbook(Request $request){
        $listquarterly = array();
        $report_items = '';
        // if($request->get('period') == 1){
            foreach($request->get('quarterly') as $data){
                $listquarterly[] = $data.'-'.$request->get('year');
                $bank = Bank::where('id',$request->get('bank'))->first();
                $disbursementVoucher = DisbursementVoucher::whereYear('created_at',$request->get('year'))->whereMonth('created_at',$data)->get();
                $Transaction = Transaction::whereYear('created_at',$request->get('year'))->whereMonth('created_at',$data)->get();
                $Invoice = Invoice::whereYear('created_at',$request->get('year'))->whereMonth('created_at',$data)->first();
                $totaltrans = 0;
                $totalamount = 0;
                $totalTran = 0;
                $totalfinal = 0;
                if($bank){
                    $table_arr = [
                        '',
                        'Beginning Balance','','','','','','',
                        '₱&nbsp;'.number_format($bank->initial_deposit , 2),'','','','','',''
                    ];
                    $report_items .= self::createCashBookTable($table_arr);
                    $totalfinal = $bank->initial_deposit;
                    if($Invoice){
                        $table_arr_invoice = [
                            date('d',strtotime($Invoice->created_at)),
                            'Various Collection',
                            'RCD No.',
                            $this->variouscollection($data.' '.$request->get('year'),$request->get('bank')),'',
                            $this->variouscollection($data.' '.$request->get('year'),$request->get('bank')),'','',
                            '₱&nbsp;'.number_format($bank->initial_deposit , 2),
                            '','','','','',''
                        ];
                        $report_items .= self::createCashBookTable($table_arr_invoice);
                        $totalfinal = $bank->initial_deposit;
                    }
                    
                    // dd($report_items);
                    
                    $payment = Payment::whereYear('created_at',$request->get('year'))->whereMonth('created_at',$data)->where('payable_type','!=','App\Transaction')->first();
                    if($payment){
                        $table_arr_payment = [
                            date('d',strtotime($payment->date_received)),
                            'Deposit of Collection',
                            'VDS',
                            '',
                            '₱&nbsp;'.number_format($this->paymentcollection($data.' '.$request->get('year'),$request->get('bank')), 2),
                            '',
                            '₱&nbsp;'.number_format($this->paymentcollection($data.' '.$request->get('year'),$request->get('bank')), 2),'',
                            '₱&nbsp;'.number_format($bank->initial_deposit + $this->paymentcollection($data.' '.$request->get('year'),$request->get('bank')) , 2),
                            '',
                            '','','','',''
                        ];
                        $totalfinal = $bank->initial_deposit+$this->paymentcollection($data.' '.$request->get('year'),$request->get('bank'));
                        $report_items .= self::createCashBookTable($table_arr_payment);
                    }
                   
                    $totaldis = 0;
                    $subtotalamount = $bank->initial_deposit + ($payment?$this->paymentcollection($data.' '.$request->get('year'),$request->get('bank')):0);
                    foreach($disbursementVoucher as $datadis){
                        if($datadis->getPayment){
                            if($datadis->getPayment->bank_id == $request->get('bank')){
                                $subtotalamount = $subtotalamount - $datadis->getROA->total_amount;
                                $table_arr_dis = [
                                    date('d',strtotime($datadis->created_at)),
                                    'Payroll '.$datadis->getClaimant->name,
                                    'Check No.'.$datadis->check_number,
                                    '','','-','',
                                    '₱&nbsp;'.number_format($datadis->getROA->total_amount , 2),
                                    '₱&nbsp;'.number_format($subtotalamount , 2),
                                    '','','','','',''
                                ];
                                $report_items .= self::createCashBookTable($table_arr_dis);
                                $totaldis += $datadis->getROA->total_amount;
                                $totalfinal = $subtotalamount;
                            }
                        }
                //   dump($subtotalamount);
                    }
                    
                    foreach($Transaction as $datamanual){
                        $totalamount = $subtotalamount + $datamanual->amount;
                        $totalTran += $datamanual->amount;
                        $table_arr_manual = [
                            date('d',strtotime($datamanual->created_at)),
                            'Time Deposit (Transfer)',
                            '','','',
                            '-',number_format($datamanual->amount , 2),'',
                            '₱&nbsp;'.number_format($subtotalamount + $totalTran , 2),
                            '','','','','',''
                        ];
                        $report_items .= self::createCashBookTable($table_arr_manual);
                        $totaltrans += $datamanual->amount;
                        $totalfinal = $subtotalamount + $totalTran;
                    }
                    $table_arr_total = [
                        date('F',strtotime($request->get('year').'-'.$data.'-1')),
                        'Totals','',
                        $this->variouscollection($data.' '.$request->get('year'),$request->get('bank')),
                        ($payment ?'₱&nbsp;'.number_format($this->paymentcollection($data.' '.$request->get('year'),$request->get('bank')), 2):"-"),'-',
                        ($payment ?'₱&nbsp;'.number_format($totaltrans + $this->paymentcollection($data.' '.$request->get('year'),$request->get('bank')) , 2):"-"),
                        '₱&nbsp;'.number_format($totaldis, 2),
                        '₱&nbsp;'.number_format($totalfinal, 2),'','','','','',''
                    ];
                    $report_items .= self::createCashBookTabletotal($table_arr_total);
                    $table_arr_baltotal = [
                        date('F',strtotime($request->get('year').'-'.$data.'-1')),
                        'Bal. Ending','','','','','','',
                        '₱&nbsp;'.number_format($totalfinal, 2),'','','','','',''
                    ];
                    $report_items .= self::createCashBookTabletotal($table_arr_baltotal);
                }
                
            }
        
        // }
     return $report_items;
    }
    public static function paymentcollection($created_at,$Bank){
        $Payment = Payment::where('payable_type','!=','App\Transaction')->where('bank_id',$Bank)->get();
        $total = 0;
        foreach($Payment as $data){
            // return $created_at;
            if($created_at && $data->payable_type == "App\Invoice"){
                $Invoice = Invoice::where('id',$data->payable_id)->first();
                if(date('m Y',strtotime($Invoice->invoice_date)) == $created_at){
                    $total += $data->amount_received;
                }
            }
            
        }
        return $total;
    }
    public static function variouscollection($created_at,$bank){
        // return $created_at;
        $invoice = InvoiceItem::get();
        $total = 0;
        foreach($invoice as $data){
            // return $created_at;
            if(!is_null($data->getInvoicess->getPayment) && date('m Y',strtotime($data->getInvoicess->invoice_date)) == $created_at && $data->getInvoicess->getPayment->bank_id == $bank){
            
                $total += ($data->quantity * $data->unit_price);
            }
            
        }
        return number_format($total , 2);
    }        
    public function SearchsuppliesMaterials(Request $request){
        $stock = 0;
        // $date_created = $request->get('month').'-'.$request->get('year');
        // if($request->get('month')){
        //     $date_

        // }
        // $Inventory = Inventory::all();
        // 
        // $list = array();
        // foreach($Inventory as $data){
        //     if(date('m-Y',strtotime($data->created_at)) == $date_created){
                
        //         $list[] = $data->id;
        //     } 
        // }
        if(!$request->get('year') && !$request->get('month')){
            $Inventory_list = Inventory::all();
        }
        else{
            // $SQL = "SELECT * FROM `inventories` where deleted_at = NULL";
            // if($request->get('year')){
            //     $SQL .= "AND YEAR(`created_at`) = ".$request->get('year')."";
            // }
            // if($request->get('month')){
            //     $SQL .= "AND MONTH(`created_at`) = ".$request->get('month')."";
            // }
            $query = Inventory::query();
            if($request->get('year')){
                $query  = $query->whereYear("created_at","=",$request->get("year"));
            }
            if($request->get('month')){
                $query = $query->whereMONTH("created_at","=",$request->get("month"));
            }
            $Inventory_list = $query->get();
        }
        $pr_items = '';
        foreach($Inventory_list as $data){
            $stock++;
            $pr_items .= '<tr>';
            $pr_items .= '<td>-</td>';
            $pr_items .= '<td>'.$data->getInventoryName->description.'</td>';
            $pr_items .= '<td>'.date('Y-m',strtotime($data->created_at)).'-'.sprintf("%03d",$stock).'</td>';
            $pr_items .= '<td>'.$data->getUnits->unit_name.'</td>';
            $pr_items .= '<td>₱&nbsp;'.$data->unit_price.'</td>';
            $pr_items .= '<td>'.$this->onhandper($data->id,1,$data->quantity).'</td>';
            $pr_items .= '<td>'.$this->onhandper($data->id,2,null).'</td>';
            $pr_items .= '<td>0</td>';
            $pr_items .= '<td>0</td>';
            $pr_items .= '<td>'.$data->remarks.'</td>';
            // $pr_items .= '<td>'.($data->getAsset?$data->getAsset:'Operating').'</td>';
            $pr_items .= '</tr>';
            // dump($data->getAsset);
        }
       
        return $pr_items;
    }
    public function comparison_statement_search(Request $request){
        $dateneeded = $request->get('month').'-'.$request->get('year');
        $allocation = BudgetAllocation::where('year',$request->get('year'))->first();

        $groups = AccountGroup::all();
        $credit = 0;
        $debit = 0;

        foreach($groups as $group){
            $this->array_group[$group->id]['amount'][] = 0;
            $this->array_group[$group->id]['group_name'] = $group->group_name;
        }

        foreach($allocation->getBudgetAllocationItems as $item){
            if($item->getLedger->account_type == 0){
                $debit += $item->amount;
            }else{
                $credit += $item->amount;
            }
            $this->array_temp[$item->getLedger->group_id][] = (float)$item->amount;
            $this->ledger_temp[$item->ledger_id][] = $item->amount;
        }

        self::getParentGroup();

        
        self::getSubLedgers();
        $groups = AccountGroup::where('group_id',null)->get();
        return self::recursiveGroups($groups,'',true, null, 1,true,$request->get('year'),$request->get('month'));
    }
    public function getSubLedgers(){
        foreach($this->ledger_temp as $key => $object){

            $check_if_has_ledger = AccountLedger::where('id',$key)->first();
            if($check_if_has_ledger->getLedger){
                $this->ledger_temp[$check_if_has_ledger->getLedger->id][] = array_sum($object);
            }
            $this->ledger_final[$key][] = array_sum($object);
            unset($this->ledger_temp[$key]);
        }
        if(count($this->ledger_temp) > 0){
            self::getSubLedgers();
        }
    }
    public function getParentGroup(){
        $temp_array = array();
        foreach($this->array_temp as $key => $temp){
            $this->array_group[$key]['amount'][] = array_sum($temp);
            $check_if_has_group = AccountGroup::where('id',$key)->first();
            if($check_if_has_group->getGroup){
                
                $temp_array[$check_if_has_group->getGroup->id][] = array_sum($temp);
            }

        }
        $this->array_temp = $temp_array;
        if(count($this->array_temp) > 0){
            self::getParentGroup();
        }
    }
    public function iterateLedgers($ledgers, $sub_mark = "", $child = false, $parent_group = null, $task = null,$year,$month){
        $OverAllOriginal = 0;
        $OverAllActual = 0;
        foreach ($ledgers as $row){
            if($row->group_id == 1 && $row->id != 3){
                if(!empty($row->reference_group)){
                    $referrence_name = "group";
                }else if($row->reference_ledger){
                    $referrence_name = "ledger";
                }else{
                    $referrence_name = "";
                }
                
                $total_amnt = self::appropriationobliviaid($year, $row->id,$month);
                $this->array_group_actual = array();
                $budget_readonly = (!empty($row->reference_group) || !empty($row->reference_ledger)?"readonly":"");
                if(array_key_exists($row->id,$this->edit_array) || !empty($task)){
                    $val = (empty($task))?$this->edit_array[$row->id]:0;
                    $class = self::getClasses($row->getGroup->id,'');
                    $data_arr = self::getClasses($row->getGroup->id,'', true);
                    $td  = '<input type="hidden" name="ledgers[]" value="'.$row->id.'"><input type="text" class="form-control sum_ledgers budget_group-'.$row->reference_group.' 
                                budget_ledger-'.$row->reference_ledger.' '.$class.'" a-type="'.$row->account_type.'" data-arr="'.$data_arr.'" data-ledger="'.$row->id.'" data-budget="'.$row->budget_percentage.'" 
                                name="ledgers-'.$row->id.'" value="'.$val.'" '.$budget_readonly.' autocomplete="off">';
                }else{
                    $td  = (array_key_exists($row->id,$this->ledger_final) ? (array_sum($this->ledger_final[$row->id])) : 0);
                }
                $this->html .= '<tr>
                    <td align="left">'.$row->ledger_name.'</td>
                    <td class="right-align">
                        ₱&nbsp;'.number_format($td, 2).'
                    </td>
                    <td class="right-align">
                        ₱&nbsp;'.number_format($td, 2).'
                    </td>
                    <td class="right-align">
                        
                    </td>
                    <td class="right-align">
                        ₱&nbsp;'.number_format($total_amnt, 2).'
                    </td>
                    <td class="right-align">
                        ₱&nbsp;'.($td - $total_amnt > 0 ? number_format($td - $total_amnt,2) : "(".number_format(abs($td - $total_amnt),2).")").'
                    </td>
                </tr>';
                $OverAllOriginal += $td;
                $OverAllActual += $total_amnt;
                // if (count($row->getLedgers) > 0){
                //     self::iterateLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp', true, $parent_group, $task,$year,$month);
                // }
                
            }

        }
        $this->html .= '<tr class="tb-color" style="border-style:hidden;">
                <td align="left"><strong>TOTAL</strong></td>
                <td align="right">₱&nbsp;'.number_format($OverAllOriginal, 2).'</td>
                <td align="right">₱&nbsp;'.number_format($OverAllOriginal, 2).'</td>
                <td align="right"></td>
                <td align="right">₱&nbsp;'.number_format($OverAllActual, 2).'</td>
                <td align="right">₱&nbsp;'.number_format($OverAllOriginal- $OverAllActual, 2).'</td>

                </tr>';
    }
     public function appropriationobliviaid($year,$id,$month){
        $totalAmount = 0;
        $roas = Roa::whereYear('approval_date',$year)->whereMonth('approval_date',$month)->where('ledger_id',$id)->get();
        $invoices = Invoice::whereYear('invoice_date',$year)->whereMonth('invoice_date',$month)->where('ledger_id',$id)->first();
        $payroll = Payroll::whereYear('created_at',$year)->whereMonth('created_at',$month)->where('ledger_id',$id)->first();
        if($invoices){
            if(!is_null($invoices->getPayment)){
                $totalAmount += (float)$invoices->getInvoice->unit_price;
            }
                
        }

        if($roas){
            foreach($roas as $val){
                $totalAmount += (float)$val->total_amount;
            }
        }
        if($payroll){
                $totalAmount += (float)($payroll->getPayrollItems->sum('salary') + $payroll->getPayrollItems->sum('honoraria') + $payroll->getPayrollItems->sum('other_benefits')) - $payroll->getPayrollItems->sum('bir');
        }
        return $totalAmount;
    }
    public  function recursiveGroups($accounts, $sub_mark = '', $parent = false, $task = null, $budget_id = null,$print = false,$year,$month){
        $this->html .= '<tr class="tb-color" style="border-style:hidden;">
                        <td align="left" colspan="15"><b>Revenue</b></td>
                    </tr>';
        foreach($accounts as $row){
            if (count($row->getLedgers) > 0 ){
                self::iterateLedgers($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp', null, false, $task,$year,$month);
            }
        }
        $this->html .= '<tr class="tb-color" style="border-style:hidden;">
                <td align="left" colspan="15"><b>Expenses</b></td>
            </tr>';
        $this->html .= '<tr class="tb-color" style="border-style:hidden;">
            <td align="left" colspan="15"><b>Current Year Appropriations</b></td>
        </tr>';
        $groups = AccountGroup::where('group_id',3)->get();
        $BudgetAllocation = BudgetAllocation::whereYear('created_at',$year)->first();
        $OverAllOriginal = 0;
        $OverAllActual = 0;
        foreach($groups as $row){
            $amount = 0;
            $array = array();
            if($row->id == 4){
                $array = array(4);
            }else if($row->id == 5){
                $array = array(5,6,7,8,9,10,11,12,13,14,15);
            }else if($row->id == 15){
                $array = array(16);
            }else if($row->id == 17){
                $array = array(37);
            }
            $total_amnt = [];
            if($row->id != 17){
                
                $group_budget_amount = (array_key_exists($row->id,$this->array_group) ? array_sum($this->array_group[$row->id]['amount']) : 0);
                $original = $group_budget_amount;
                $actual = self::appropriationobli($year, $array,2);
                $table_arr_data = [$row->group_name,'₱&nbsp;'.number_format($original, 2),'₱&nbsp;'.number_format($original, 2),'','₱&nbsp;'.number_format($actual,2),'₱&nbsp;'.number_format($original-$actual,2)];
                $this->html .= self::expensestab($table_arr_data);
                $OverAllOriginal += $original;
                $OverAllActual += $actual;
            }
            if($row->id == 17){
                $BudgetAllocation = BudgetAllocation::whereYear('created_at',$year)->first();
                if($BudgetAllocation->getBudgetAllocationItems){
                    foreach($BudgetAllocation->getBudgetAllocationItems as $data){
                        if($data->ledger_id == 37){
                            $original = $data->amount; 
                            $actual = self::appropriationobli($year, $array,2);
                            $table_arr_data = ['20% Development Fund','₱&nbsp;'.number_format($original,2),'₱&nbsp;'.number_format($original,2),'','₱&nbsp;'.number_format($actual,2),'₱&nbsp;'.number_format($original-$actual,2)];
                            $this->html .= self::expensestab($table_arr_data);
                            $OverAllOriginal += $original; 
                            $OverAllActual += $actual;
                        }
                    }
                }
                
            }
            
        }
        $this->html .= '<tr class="tb-color" style="border-style:hidden;">
            <td align="left"><strong>TOTAL</strong></td>
            <td align="left">₱&nbsp;'.number_format($OverAllOriginal,2).'</td>
            <td align="left">₱&nbsp;'.number_format($OverAllOriginal,2).'</td>
            <td align="left"></td>
            <td align="left">₱&nbsp;'.number_format($OverAllActual,2).'</td>
            <td align="left">₱&nbsp;'.number_format($OverAllOriginal - $OverAllActual,2).'</td>
            </tr>';
        return $this->html;
    }
    public static function expensestab($table_data){
        $table_string = "<tr align='left'>";
        foreach($table_data as $table_datum){
            $table_string .= "<td>".$table_datum."</td>";
        }
        $table_string .= "</tr>";
        return $table_string;
    }
    public static function onhandper($id,$type,$quantity){
        $AssignedSupplyItem = AssignedSupplyItem::where('inventory_id',$id)->get();
        $total = 0;
        foreach($AssignedSupplyItem as $data){
            $total += $data->quantity;
        }
        if($type == 1){
            $total = $quantity - $total;
        }
        return $total;
    }
    public function cashbook(){
        $Bank = Bank::all();
        return view ('Reports.Accounting.cashbook',[
            'Bank' => $Bank
        ]);
    }

    public function comparison_statement(){
        return view('Reports.Accounting.comparison_statement');
    }

    public function registration_obligation(){
        return view('Reports.Accounting.registration_obligation');
    }

    public function estimatedActualIncome(){
        return view('Reports.Accounting.estimated_actual_income');
    }

    public function createCashBookTable($table_data){
        $table_string = "<tr>";
        foreach($table_data as $table_datum){
            $table_string .= "<td>".$table_datum."</td>";
        }
        $table_string .= "</tr>";
        return $table_string;
    }
    public function createCashBookTabletotal($table_data){
        $table_string = "<tr style='border: 2px solid #000'>";
        foreach($table_data as $table_datum){
            $table_string .= "<td><strong>".$table_datum."</strong></td>";
        }
        $table_string .= "</tr>";
        return $table_string;
    }
    public function createTableheader($table_data){
        $table_string = "<thead class='thead-dark'><tr>";
        foreach($table_data as $table_datum){
            $table_string .= "<th class='v-align'>".$table_datum."</th>";
        }
        $table_string .= "</tr><thead>";
        return $table_string;
    }
    public function registration_obligation_search(Request $request){
        $report_items = '<p style="margin: 0; text-decoration: underline;"><i><b>Personal Services:<b></i></p>';
        $stock=0;
        $personal = array(4,1);
        $disbursementVoucher = DisbursementVoucher::whereYear('created_at',$request->get('year'))->whereMonth('created_at',$request->get('month'))->get();
        $report_items .= '<table class="table table-striped table-bordered table-hover text-center table-mediascreen" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%; margin-bottom: 1rem;">';
        $tablehead = [
            'Date','DOC No.','ROA No.','PAYEE','PARTICULARS','APPROPRIATION','OBLIGATION','BALANCE'
        ];
        $report_items .= self::createTableheader($tablehead);
        foreach($disbursementVoucher as $data){
            if(in_array($data->getROA->getLedger->group_id,$personal)){
                $stock++;
                $pr_view = PurchaseRequest::where('id', $data->pr_id)->first();
                $appropriation = self::appropriationtotalviaid($request->get('year'),$data->getROA->getLedger->id);
                $balance = ($appropriation - $data->getROA->total_amount > 0 ? '₱&nbsp;'.number_format($appropriation - $data->getROA->total_amount,2) : "(₱&nbsp;".number_format(abs($appropriation - $data->getROA->total_amount),2).")");
                $table = [
                    date('Y-m-d',strtotime($data->created_at)),
                    $stock,Common::getPRNumberFormat('purchase_requests', $pr_view),$data->getClaimant->name,$data->getROA->getLedger->ledger_name,'₱&nbsp;'.number_format($appropriation, 2),'₱&nbsp;'.number_format($data->getROA->total_amount, 2),$balance
                ];
                $report_items .= self::createCashBookTable($table);
            }
        }
        if($stock < 1){
            $report_items .= '  <tr align="center" style="border-style:hidden;">
            <td colspan="15">No data available in table</td>
        </tr>';
        }
    
        $report_items .= '<table class="table table-striped table-bordered table-hover text-center table-mediascreen" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%; margin-bottom: 1rem;">';
        $report_items .= '<p style="margin: 0; text-decoration: underline;"><i><b>MOOE:<b></i></p>';
        $tablehead = [
            'Date','DOC No.','ROA No.','PAYEE','PARTICULARS','APPROPRIATION','OBLIGATION','BALANCE'
        ];
        $report_items .= self::createTableheader($tablehead);
        foreach($disbursementVoucher as $data){
            if(!in_array($data->getROA->getLedger->group_id,$personal)){
                $stock++;
                $pr_view = PurchaseRequest::where('id', $data->pr_id)->first();
                $appropriation = self::appropriationtotalviaid($request->get('year'),$data->getROA->getLedger->id);
                $balance = ($appropriation - $data->getROA->total_amount > 0 ? '₱&nbsp;'.number_format($appropriation - $data->getROA->total_amount,2) : "(₱&nbsp;".number_format(abs($appropriation - $data->getROA->total_amount),2).")");
                $table = [
                    date('Y-m-d',strtotime($data->created_at)),
                    $stock,Common::getPRNumberFormat('purchase_requests', $pr_view),$data->getClaimant->name,$data->getROA->getLedger->ledger_name,'₱&nbsp;'.number_format($appropriation, 2),'₱&nbsp;'.number_format($data->getROA->total_amount, 2),$balance
                ];
                $report_items .= self::createCashBookTable($table);
            }
        }
        if($disbursementVoucher){
            $report_items .= '  <tr align="center" style="border-style:hidden;">
            <!-- <td colspan="15">No data available in table</td> -->
        </tr>';
        }
        $report_items .= '</table>';
       
        return $report_items;
    }
    public function appropriationtotalviaid($year,$id){
        $BudgetAllocation = BudgetAllocation::where('year',$year)->first();
        $credit = 0;
        $debit = 0;
        if($BudgetAllocation){
            foreach($BudgetAllocation->getBudgetAllocationItems as $val){
                if($val->getLedger->account_type != 0 && $val->ledger_id == $id){
                    $credit += $val->amount;
                }
                
            }
        }
     
        return $credit;
    }

    public function cedula() {
        return view('Reports.Accounting.cedula',[
            'CTCVPadData' => '',
            'year' => '',
            'totalitem' => '',
            'month' => '',
            'CTCPadsData' =>''
            ]);
    }
    public function searchCedula(Request $request) {
        $CTCVPadData = $this->BeginningPadData('cedula_pad','cd_pad_start','cd_pad_quantity');
        $CTCPadsData = $this->IssuedPadData('cedulas','ctc','created_at',$request->input('year'),1,$request->input('month'));
        $Cedula = Cedula::whereYear('created_at',$request->input('year'))->whereMonth('created_at',$request->input('month'))->get();
        $totalitem = 0;
        foreach($Cedula as $data){
            $int = ($data->item_1 + $data->item_2 + $data->item_3 + $data->item_4 + $data->item_5) * $data->interest;
            $totalitem += ($data->item_1 + $data->item_2 + $data->item_3 + $data->item_4 + $data->item_5) + $int;
            // dd($data->item_1);
        }
        return view('Reports.Accounting.cedula',[
            'CTCVPadData' => $CTCVPadData,
            'year' => $request->input('year'),
            'month' => $request->input('month'),
            'totalitem' => number_format($totalitem,2),
            'CTCPadsData' =>$CTCPadsData
        ]);
    }
    public function official_receipt() {
        return view('Reports.Accounting.official_receipt',['year' => '',
        'month' => ''
        ]);
    }
    public function searchIncome(Request $request) {
        $dateneeded = $request->get('month').'-'.$request->get('year');
        $allocation = BudgetAllocation::where('year',$request->get('year'))->first();

        $groups = AccountGroup::all();
        $credit = 0;
        $debit = 0;

        foreach($groups as $group){
            $this->array_group[$group->id]['amount'][] = 0;
            $this->array_group[$group->id]['group_name'] = $group->group_name;
        }

        foreach($allocation->getBudgetAllocationItems as $item){
            if($item->getLedger->account_type == 0){
                $debit += $item->amount;
            }else{
                $credit += $item->amount;
            }
            $this->array_temp[$item->getLedger->group_id][] = (float)$item->amount;
            $this->ledger_temp[$item->ledger_id][] = $item->amount;
        }

        self::getParentGroup();

        
        self::getSubLedgers();
        $groups = AccountGroup::where('group_id',null)->get();
        return self::recursiveGroupss($groups,'',true, null, 1,true,$request->get('year'),$request->get('month'));
    }
    public  function recursiveGroupss($accounts, $sub_mark = '', $parent = false, $task = null, $budget_id = null,$print = false,$year,$month){

        foreach($accounts as $row){
            if (count($row->getLedgers) > 0 ){
                self::iterateLedgerss($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp', null, false, $task,$year,$month,1);
            }
        }
        foreach($accounts as $row){
            if (count($row->getLedgers) > 0 ){
                self::iterateLedgerss($row->getLedgers, $sub_mark.'&nbsp&nbsp&nbsp&nbsp&nbsp', null, false, $task,$year,$month,2);
            }
        }
        return $this->html;
    }
      public function iterateLedgerss($ledgers, $sub_mark = "", $child = false, $parent_group = null, $task = null,$year,$month, $type = null){
        $OverAllOriginal = 0;
        $OverAllActual = 0;
        if($type == 1){
            $this->html .= '<tr>
                                <td colspan="10"><b>A. Income Estimates</b></td>
                            </tr>';
            $this->html .= '<tr> <td class="right-align"></td><td class="right-center"><b>Annual Budget</b></td>';
            $this->html .= '<td class="right-center"><b>Ord No. 01-S-'.$year.'</b></td>';
        }else{
            $this->html .= '<tr></tr>';
            $this->html .= '<tr> <td class="right-align"></td><td class="right-center"><b>Total = </b></td>';
            $this->html .= '<td class="right-center"></td>';
           
        }
       
        foreach ($ledgers as $row){
            if($row->group_id == 1){
                if(!empty($row->reference_group)){
                    $referrence_name = "group";
                }else if($row->reference_ledger){
                    $referrence_name = "ledger";
                }else{
                    $referrence_name = "";
                }
                
                // $total_amnt = self::appropriationobliviaid($year, $row->id,$month);
                $this->array_group_actual = array();
                $budget_readonly = (!empty($row->reference_group) || !empty($row->reference_ledger)?"readonly":"");
                if(array_key_exists($row->id,$this->edit_array) || !empty($task)){
                    $val = (empty($task))?$this->edit_array[$row->id]:0;
                    $class = self::getClasses($row->getGroup->id,'');
                    $data_arr = self::getClasses($row->getGroup->id,'', true);
                    $td  = '<input type="hidden" name="ledgers[]" value="'.$row->id.'"><input type="text" class="form-control sum_ledgers budget_group-'.$row->reference_group.' 
                                budget_ledger-'.$row->reference_ledger.' '.$class.'" a-type="'.$row->account_type.'" data-arr="'.$data_arr.'" data-ledger="'.$row->id.'" data-budget="'.$row->budget_percentage.'" 
                                name="ledgers-'.$row->id.'" value="'.$val.'" '.$budget_readonly.' autocomplete="off">';
                }else{
                    $td  = (array_key_exists($row->id,$this->ledger_final) ? (array_sum($this->ledger_final[$row->id])) : 0);
                }
                $this->html .= '
                    <td class="right-align">
                        ₱&nbsp;'.number_format($td, 2).'
                    </td>';
                $OverAllOriginal += $td;
                
            }

        } 
         $this->html .= '<td class="right-center">₱&nbsp;'.number_format($OverAllOriginal, 2).'</td></tr>';
        
         if($type != 1){
            $this->html .= '<tr>
                <td colspan="10"><b>B. Actual Collections</b></td>
            </tr>';
            for ($x = 01; $x <= 12; $x++) {
                
                    $this->html .= '<tr>
                        <td class="right-align">
                            '.date("M Y",strtotime($year.'-'.$x.'-01')).'
                        </td>
                        <td class="right-align">Various Collection</td>
                        <td class="right-align"></td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(1)).'</td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(2)).'</td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(3)).'</td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(4)).'</td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(5)).'</td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(6)).'</td>
                        <td class="right-align">₱&nbsp;'.$this->searchIncomeInvoice($year,$x,array(1,2,3,4,5,6)).'</td>
                        </tr>';
            }
            $this->html .= '<tr>';
            for ($x = 01; $x <= 10; $x++) {
                if($x == 1){
                    $label= date("M Y",strtotime($year.'-12-01'));
                }else  if($x == 2){
                    $label= 'Total to Date';
                }else if($x == 3){
                    $label= '';
                }else{
                    if($x == 4){
                        $array= array(1);
                    }
                    else if($x == 5){
                        $array= array(2);
                    }
                    else if($x == 6){
                        $array= array(3);
                    }
                    else if($x == 7){
                        $array= array(4);
                    }
                    else if($x == 8){
                        $array= array(5);
                    }
                    else if($x == 9){
                        $array= array(6);
                    }else{
                        $array= array(1,2,3,4,5,6);
                    }
                    $label= $this->searchIncomeTotal($array) == null ? "₱ 0.00" : $this->searchIncomeTotal($array);
                }
                $this->html .= '<td class="right-align"><b>'.$label.'</b></td>';
            }
            $this->html .= '</tr>';

        }
         
    }
    public function searchIncomeInvoice($year,$month,$ledger_id) {
        $Invoice = Invoice::whereYear('invoice_date',$year)
        ->whereIn('ledger_id',$ledger_id)
        ->whereMonth('invoice_date',$month)
        ->get();
        $total = 0;
        foreach($Invoice as $data){
            if(!is_null($data->getPayment)){
                $total += $data->getInvoice->unit_price;
            }
            
        }
        if($total < 1){
            $total = '0.00';
        }else{
            $total = number_format($total,2);
        }
        return $total;
    }
    public function searchIncomeTotal($ledger_id) {
        $Invoice = Invoice::whereIn('ledger_id',$ledger_id)
        ->get();
        $total = 0;
        foreach($Invoice as $data){
            if(!is_null($data->getPayment)){
                $total += $data->getInvoice->unit_price;
            }
        }
        if($total < 1){
            $total = '₱&nbsp;0.00';
        }else{
            $total = number_format($total,2);
        }
        return $total;
    }
    public function searchReceipt(Request $request) {
           
        $total = 0;
        $pay = 0;
    
            $collection_nature = Invoice::orderBy('collection_nature', 'desc')
            ->groupBy('collection_nature')
            ->get();
            $year = $request->input('year');
            $month = $request->input('month');
                $invoices = Invoice::whereYear('invoice_date', $request->input('year'))
                    ->whereMonth('invoice_date', $request->input('month'))->get();
            $payments = Payment::whereYear('date_received', $request->input('year'))
                ->whereMonth('date_received', $request->input('month'))->get();
            foreach($invoices as $invoice){
                
                $total += $invoice->getInvoice->quantity * $invoice->getInvoice->unit_price;
            }
            $DVPadData = $this->BeginningPadData('dv_pad','dv_pad_start','dv_pad_quantity');
            $ORPadData = $this->BeginningPadData('or_pad','or_pad_start','or_pad_quantity'); 
            $ORPadsData = $this->IssuedPadData('invoices','or_number','invoice_date',$request->input('year'),0,$request->input('month'));
            $DVPadsData = $this->IssuedPadData('disbursement_vouchers','check_number','cheque_date',$request->input('year'),0,$request->input('month'));
           
            foreach($payments as $paid){
                $pay += $paid->amount_received;
            }
            $DisbursementVoucher = DisbursementVoucher::whereYear('created_at', $request->input('year'))
            ->whereMonth('created_at', $request->input('month'))->get();
            $total = $pay;
            // dd($this->RCDSuppDoc($month,$year,$invoices,0));
            return view('Reports.Accounting.official_receipt', ['DisbursementVoucher' => $DisbursementVoucher, 'RCDSuppDoc' => $this->RCDSuppDoc($month,$year,$invoices,0),'collection_nature' => $collection_nature,'ORPadsData' => $ORPadsData,'DVPadsData' => $DVPadsData,'ORPad' => $ORPadData,'DVPad' => $DVPadData,'invoices' => $invoices, 'payments' => $payments, 'year' => $year, 'month' => $month, 'total' => $total]);
    }
}