<?php

namespace App\Http\Middleware;

use App\Cedula;
use App\DisbursementVoucher;
use App\Invoice;
use App\Payroll;
use App\ROA;
use Closure;

class CheckPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $type = $request->route('type');
        $id = $request->route('id');
        switch ($type){
            case 'invoice':
                $model = Invoice::findOrFail($id);
                break;
            case 'disbursement-voucher':
                $model = DisbursementVoucher::findOrFail($id);
                break;
            case 'cedula':
                $model = Cedula::findOrFail($id);
                break;
            case 'payroll':
                $model = Payroll::findOrFail($id);
                break;
            default:
                $model = null;
                break;
        }
        if (!is_null($model) && is_null($model->getPayment)){
            return $next($request);
        }else{
            $notification = array(
                'message' => 'Payment for this transaction has already been made.',
                'alert-type' => 'warning'
            );
            return redirect()->route($type.'.show',$id)->with($notification);
        }
    }
}
