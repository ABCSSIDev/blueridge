<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SignatoryTemplate extends Model
{
    //
    use SoftDeletes;

    public function getDocument(){
        return $this->belongsTo('App\Document','document_id');
    }
    
    public function getSignatures(){
        return $this->hasMany('App\TemplateSignee','template_id');
    }
}
