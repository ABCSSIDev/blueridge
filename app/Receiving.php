<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receiving extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'receivings';
    protected $fillable = ['po_id', 'date_received', 'receipt_number', 'rer_number', 'inspector', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;


    public function purchaseOrder()
    {
        return $this->belongsTo('App\PurchaseOrder','po_id');
    }
    public function getSupplier()
    {
        return $this->belongsTo('App\Supplier','po_id');
    }

    public function getReceivingItems(){
        return $this->hasMany('App\ReceivingItem','receiving_id');
    }

    
}
