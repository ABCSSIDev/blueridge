<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CedulaPad extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'cedula_pad';
    protected $fillable = [
        'cd_pad_start','cd_pad_quantity','created_at','updated_at','due_date'];
    public $timestamps = false;
}
