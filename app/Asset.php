<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    //
    protected $primaryKey = 'id';
    protected $fillable = [
        'inventory_id', 'asset_tag','remark', 'inventory_status_id','date_acquired','custodian','location','other_details'
    ];
    protected $dates = ['deleted_at'];
    use SoftDeletes;


    public function getInventories(){
        return $this->belongsTo('App\Inventory','inventory_id');
    }

    public function getInvetoryStatus(){
        return $this->belongsTo('App\InventoryStatus','inventory_status_id');
    }
   
}
