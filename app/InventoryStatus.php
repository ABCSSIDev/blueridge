<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryStatus extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'inventory_statuses';
    protected $fillable = ['status_name', 'status_type', 'notes', 'created_at', 'updated_at', 'deleted_at'];
    public $timestamps = false;

    use SoftDeletes;
}
