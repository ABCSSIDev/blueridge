<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisbursementVoucher extends Model
{
    //
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'disbursement_vouchers';
    protected $fillable = [
        'roa_id','or_id', 'claimant_id','voucher_no','rer','check_number','cheque_date','tin','invoice_no','description','claimant_address','created_at','updated_at','due_date', 'mode'
    ];
    public $timestamps = false;


    public function getClaimant(){
        return $this->belongsTo('App\GeneralUser','claimant_id');
    }
    
    public function getROA(){
        return $this->belongsTo('App\ROA', 'roa_id', 'id');
    }
    public function getInvoice(){
        return $this->belongsTo('App\Invoice', 'or_id');
    }
    public function getPayment(){
        return $this->morphOne('App\Payment','payable');
    }
}
