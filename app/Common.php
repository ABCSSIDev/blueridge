<?php
namespace  App;
use App\Canvass;
use App\Category;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use NumberToWords\NumberToWords;
use App\Inventory;
use App\PurchaseOrder;
use Image;

class Common{

    public static function  convertDateFormat($db_date){
        return date("d-M-Y", strtotime($db_date));
    }

    public static function formatDate($format,$date){
        return date($format, strtotime($date));
    }

    public static function computeOverdue($stringDate){
        $date = $stringDate;
        $date = strtotime(date("Y-m-d", strtotime($date)));
        $date = date("Y-m-d", $date); // next maintenance day

        $date1 = date("Y-m-d");
        $date2 = $date;

        $diff = abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        return ( $days == 0 ? 'Due date today' : 'Overdue by '.$days.' days');
    }

    public static function convertWordDateFormat($db_date){
        return date("F d, Y", strtotime($db_date));
    }
    public static function convertLastYearFormat($db_date){
        return date("y", strtotime($db_date));
    }
    public static function convertToYear($db_date){
        return date("Y", strtotime($db_date));
    }

    public static function successMessage( $msg ){
        $html = "";
        $html .= '<div class="alert alert-dismissable alert-success" id="success">';
        $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        $html .= '<span aria-hidden="true">&times;</span>';
        $html .= '</button>';
        $html .= '<strong>';
        $html .= $msg;
        $html .= '</strong>';
        $html .= '</div>';
        return $html;
    }

    public static function errorMessage( $msg ){
        $html = "";
        $html .= '<div class="alert alert-dismissable alert-danger" id="success">';
        $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        $html .= '<span aria-hidden="true">&times;</span>';
        $html .= '</button>';
        $html .= '<strong>';
        $html .= $msg;
        $html .= '</strong>';
        $html .= '</div>';
        return $html;
    }

    public static function getPRNumberFormat($pr_table,$model = null,$series = null){
        // return $series;
        //where $pr_table = to the table name if you want to get the next ID format to be created and $id when you want to show format of an existing ID
        $statement = DB::select('show table status like "'.$pr_table.'"');
        $pr_id = $statement[0]->Auto_increment;
        
        $date = now();
        if ($model != null){
            $pr_id = $model->id;
            $date = $model->created_at;
        }
        if($series == null){
            $formatted_number = sprintf('%03d', $pr_id);
        }else{
            $formatted_number = sprintf('%03d', $pr_id);
        }
        
        return $formatted_number;
    }
    public static function  seriesYears(){
        $series = date("Y", strtotime(date('Y/m/d H:i:s'))) - 2019;
        return $series+1;
    }
    public static function catchError($msg){
        $response = array(
            'status' => 'error',
            'desc' => $msg
        );
        return $response;
    }

    public static function NumberToWords($amount,$ucwords = false){
        $numberToWords = new NumberToWords();

        $numberTransformer = $numberToWords->getNumberTransformer('en');
        return  ($ucwords ? ucwords($numberTransformer->toWords($amount).' pesos only') : $numberTransformer->toWords($amount).' pesos only');
    }

    public static function saveImage($image, $imagename){
        try{
            //create an image instance of the parameter $image
            $img = Image::make($image->getRealPath());
            $make_directory = true;
            //if the directory given does not exist create new directory
            $path = explode('/', $imagename);
            if (!file_exists(storage_path('app/public/'.$path[0]))) {
               $make_directory = mkdir(storage_path('app/public/'.$path[0]), 0777, true);
            }
            //save to the storage_path given the given directory and change its quality to 60
            //$imagename example 'PreOrder/'.$preorder->pre_order_id.'['.$key.'].jpg' file extension should always be jpg
            if($make_directory){
                $img->save(storage_path('app/public/'.$imagename) , 60);
            }
            return true;
        }catch(Exception $e){
            return false;
        }
    }
    //function for generating asset-tag
    public static function AssetTagCode($asset,$inventory_id){
        $inventory = Inventory::where('id', $inventory_id)->first();
        $category = $inventory->getCategory;
        if($category->category_id != NULL){
            $asset_tag = self::abbreviate($category->getCategory->category_name).'-'.
                         self::abbreviate($category->category_name).'-'.
                         date('Y', strtotime($asset->date_acquired)).'-'.
                         $asset->asset_tag;
        }else{
            $asset_tag = self::abbreviate($category->category_name).'-'.$asset->asset_tag;
        }

        return $asset_tag;
    }

    public static function abbreviate($string){
        $abbreviation = "";
        $string = strtoupper($string);
        $words = explode(" ", "$string");
          
          if(count($words) == 1){
            $abbreviation = substr($string, 0, 2); 
          }
          else{
            foreach($words as $word){
                $abbreviation .= $word[0];
            }
          }
       return $abbreviation; 
    }

    public static function cleanDecimal($string){
        return str_replace(',','',$string);
    }

    public static function getAlphabet($id, $list = false, $count = 0){
        $alphabet_arr = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        $char_po = "";
        $po_count = 0;
        if($list){
            if($count !== "NULL"){
                if ($count > 25) {
                    $added = $count - 25;
                    $char_po = $alphabet_arr[25] . $added;
                } else {
                    $char_po =  $alphabet_arr[$count];
                }
            }
        }else {
        
            $canvasses = Canvass::where('pr_id',$id)->where('awarded',1)->get();
        
            // return $pr->count();
            if ($canvasses->count() > 0) {
                foreach($canvasses as $canvass){
                    $po = PurchaseOrder::where('canvass_id', $canvass->id)->first();
                    if($po){
                        $po_count = $po_count + 1;
                    }
                }

                if ($po_count > 25) {
                    $added = $po_count - 25;
                    $char_po = $alphabet_arr[25] . $added;
                } else {
                    $char_po = $alphabet_arr[$po_count];
                }
            } else {
                $char_po = "";
            }
        }
       return $char_po;
    }
    public static function getListAlphabet($pr_id, $canvass_id){
        $count = 0;
        $canvasses = Canvass::where('pr_id', $pr_id)
            ->where('awarded', 1)
            ->orderBy('created_at', 'asc')->get();
        
        if(count($canvasses) > 1){
            foreach($canvasses as $canvass){
                if($canvass->id !== $canvass_id){
                    $count++;
                }
                else{
                    break;
                }
            }
        }else{
            $count = "NULL";
        }
        
        return $count;
    }

    public static function getPaymentType($id){
        $payment = Payment::findOrFail($id);
        $reference = $payment->payable;
        $spent = number_format(0,2);
        $received = number_format(0,2);
        $balance = 0;
        $status = array();
        $description = '';
        switch ($payment->payable_type){
            case 'App\Invoice':
                $payment_type = "OR Payment";
                $description = 'Received from '.$reference->getPayee->name.' (OR#: '.$reference->or_number.')';
                $received = number_format($payment->amount_received,2);
                $balance = 1 * $payment->amount_received;
                break;
            case 'App\Cedula':
                $payment_type = "Cedula Payment";
                $received = number_format($payment->amount_received,2);
                $description = 'Received from '.$reference->first_name.' '.$reference->last_name.' (Cedula#: '.$reference->id.')';
                $balance = 1 * $payment->amount_received;
                break;
            case 'App\DisbursementVoucher':
                $payment_type = "Disbursement Payment";
                $spent = number_format($payment->amount_received,2);
                $description = 'Made to '.$reference->getClaimant->name.' (Disbursement#: '.$reference->id.')';
                $balance = -1 * $payment->amount_received;
                break;
            case 'App\Payroll':
                $payment_type = "Payroll Payment";
                $description = 'Made to  (Payroll#: '.$reference->payroll_number.')';
                $spent = number_format($payment->amount_received,2);
                $balance = -1 * $payment->amount_received;
                break;
            default:
                $payment_type = '';
                break;
        }
        $status['payment_type'] = $payment_type;
        $status['description'] = $description;
        $status['spent'] = $spent;
        $status['received'] = $received;
        $status['balance'] = $balance;
        return $status;
    }
}