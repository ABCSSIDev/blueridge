<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedSupplyItem extends Model
{
    protected $table = 'assigned_supply_items';
    protected $primaryKey = 'id';
    protected $fillable = [
        'assigned_supply_id','inventory_id','asset_id','quantity','created_at','updated_at'
    ];
    public $timestamps = false;
    // use SoftDeletes;
    public function AssestTagDetails(){
        return $this->belongsTo('App\Asset','asset_id');
    }

    public function InventoryDetails(){
        return $this->belongsTo('App\Inventory','inventory_id');
    }

    public function AssignedSupplies(){
        return $this->belongsTo('App\AssignedSupply','assigned_supply_id');

    }
}
