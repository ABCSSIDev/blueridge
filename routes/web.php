<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/bank', 'HomeController@functionBank')->name('home.functionBank');
Route::middleware(['auth'])->group(function() {
    //

    

    Route::get('/assign-equipment/image/{file}','OfficeEquipmentController@getImage')->name('inventory.image');
    Route::group(['prefix' => 'dashboard'],function(){
        Route::get('/chart','HomeController@getChart')->name('budget.chart');
        Route::get('/chart/{year}','HomeController@budgetYearChart')->name('budget.year');

        // Route::get('/chart/releaseNotes', 'HomeController@releaseNotes')->name('charts.releaseNotes');
    });
    Route::group(['prefix'=>'inventory'], function(){
        Route::resource('/consumable','ConsumablesController');
        Route::get('/assign-equipment/suppandequitable', 'AssignEquipmentController@suppandequitable')->name('assign.suppandequitable');
        Route::get('/assign-equipment/autocomplete', 'AssignEquipmentController@autocomplete')->name('assign.autocomplete');
        Route::get('/assign-equipment/suppandequi', 'AssignEquipmentController@SuppliesandEquipments')->name('assign.suppandequi');
        Route::get('/assign-equipment/generaldata', 'AssignEquipmentController@generaldata')->name('assign.generaldata');
        Route::get('/assign-equipment/reloadtable', 'AssignEquipmentController@reloadtable')->name('assign.reloadtable');
        Route::get('/assign-equipment/getAssign/{year}', 'AssignEquipmentController@getAssign')->name('assign.getAssign');
        Route::put('/assign-equipment/functionRemoveAssign/{id}', 'AssignEquipmentController@functionRemoveAssign')->name('functionRemoveAssign');
        Route::get('/consumables/getConsumables/{year}', 'ConsumablesController@getConsumables')->name('consumables.getConsumables');
        Route::resource('/assign-equipment','AssignEquipmentController');
        Route::get('/remove-equipment/{id}', 'OfficeEquipmentController@removeEquipment')->name('removeEquipment');
        Route::put('/equipment/functionRemoveEquipment/{id}', 'OfficeEquipmentController@functionRemoveEquipment')->name('equipment.functionRemoveEquipment');
        Route::resource('/assign-equipment','AssignEquipmentController');
        Route::get('/office-equipment/getEquipments/{year}', 'OfficeEquipmentController@getEquipments')->name('equipment.getEquipments');
        Route::resource('/office-equipment', 'OfficeEquipmentController');
        Route::resource('/receiving', 'ReceivingController');
        Route::get('/consumables/image/{file}','ConsumablesController@getImage')->name('consumables.image');
        Route::get('/consumables/archived/{id}','ConsumablesController@archived')->name('consumable.archived');
        Route::get('/get-receiving/supplier', 'ReceivingController@supplierName')->name('get.supplier.name');
        Route::group(['prefix' => 'receivings'],function(){
            Route::get('/remove-receiving/{id}', 'ReceivingController@removeReceiving')->name('remove.receiving');
            Route::get('/get-receiving/{id}', 'ReceivingController@getPurchaseReceiving')->name('get.receiving');
            Route::get('/get-receivings/getReloadtable', 'ReceivingController@getReloadtable')->name('getReloadtable');
            Route::get('/get-receiving-list/{year}', 'ReceivingController@getReceivingList')->name('get.receiving.list');
            Route::put('/get-receiving/functionRemoveInventoryReceiving/{id}', 'ReceivingController@functionRemoveInventoryReceiving')->name('functionRemoveInventoryReceiving');

        });
        Route::get('/new-inventory/reloadtable/{id}', 'NewInventoryController@reloadtable')->name('new-inventory.reloadtable');
        Route::resource('/new-inventory', 'NewInventoryController');
    });
    Route::get('/disbursement-voucher/autocompleteClaimant', 'DisbursementVoucherController@autocompleteClaimant')->name('dv.autocompleteClaimant');
    Route::group(['prefix'=>'accounting'], function(){
        Route::resource('/roa-dv', 'DisbursementVoucherController');
        Route::get('/roa-dv/{id}/delete', 'DisbursementVoucherController@delete')->name('roa-dv.delete');
        Route::get('/roa-dv/{id}/print', 'DisbursementVoucherController@print')->name('roa-dv.print');
        Route::get('/roa-dvs/autocompleteOr', 'DisbursementVoucherController@autocompleteOr')->name('roa-dv.autocompleteOr');
        Route::get('/roa-dvs/ORPad', 'DisbursementVoucherController@functionORPad')->name('roa-dv.functionORPad');
        Route::post('/roa-dv/CreateORPad', 'DisbursementVoucherController@functionCreateORPad')->name('roa-dv.functionCreateORPad');
        Route::resource('payroll', 'PayrollController');
        Route::get('/payroll/delete/{id}', 'PayrollController@delete')->name('payroll.delete');
        Route::get('/payroll-validations', 'PayrollController@PayRollNovalidation')->name('payroll.validation');
        Route::get('/payroll-list/{year}', 'PayrollController@getPayrollList')->name('get.payrollList');
        Route::get('/invoice/autocompletePayee', 'InvoiceController@autocompletePayee')->name('invoice.autocompletePayee');
        Route::get('/invoice/autocompleteOr', 'InvoiceController@autocompleteOr')->name('invoice.autocompleteOr');
        Route::get('/invoice/autocompleteORPad', 'InvoiceController@autocompleteORPad')->name('invoice.autocompleteORPad');
        Route::get('/invoice/ORPad', 'InvoiceController@functionORPad')->name('invoice.functionORPad');
        Route::post('/invoice/CreateORPad', 'InvoiceController@functionCreateORPad')->name('invoice.functionCreateORPad');
        Route::resource('/invoice', 'InvoiceController');
        Route::group(['prefix' => 'invoices'],function(){
            Route::get('/get-invoice-list/{year}', 'InvoiceController@getInvoiceList')->name('get.invoice.list');
            Route::get('/remove-invoice/{id}', 'InvoiceController@removeInvoice')->name('remove.invoice');
        });
        Route::resource('/bank', 'BankController');
        Route::get('/get-bank-list/{year}', 'BankController@getBankList')->name('get.bank.list');
        Route::get('/remove-bank/{id}', 'BankController@removeBank')->name('remove.bank');
        Route::get('/cedula/autocompleteCedula', 'CedulaController@autocompleteCedula')->name('autocompleteCedula');
        Route::resource('/cedula', 'CedulaController');
        Route::post('/cedulas/CreateCTCPad', 'CedulaController@functionCreateCTCPad')->name('cedulas.functionCreateCTCPad');
        Route::get('/cedulas/ORPad', 'CedulaController@functionORPad')->name('cedula.functionORPad');
        Route::get('/cedulas/autocompleteCtc', 'CedulaController@autocompleteCtc')->name('cedula.autocompleteCtc');
        Route::get('/get-cedula-list/{year}', 'CedulaController@getCedulaList')->name('get.cedula.list');
        Route::get('/remove-cedula/{id}', 'CedulaController@removeCedula')->name('remove.cedula');
        Route::resource('/chart-account', 'ChartOfAccountController');
        Route::post('/account-group/store', 'ChartOfAccountController@storeAccountGroup')->name('store.accountGroup');
        Route::post('/account-ledger/store', 'ChartOfAccountController@storeAccountLedger')->name('store.accountLedger');
        Route::resource('payment','PaymentController')->except(['create']);
        Route::get('payment/create/{type}/{id}','PaymentController@create')->name('payment.create')->middleware('check.payment');
        Route::put('/account-ledger/update/{id}', 'ChartOfAccountController@updateAccountLedger')->name('update.chart-account-ledger');
        Route::put('/account-group/update/{id}', 'ChartOfAccountController@updateAccountGroup')->name('update.chart-account-group');
        Route::get('/account-group/edit/{id}', 'ChartOfAccountController@editAccountGroup')->name('edit.chart-account-group');
        Route::get('/account-ledger/edit/{id}', 'ChartOfAccountController@editAccountLedger')->name('edit.chart-account-ledger');
        Route::get('/account-ledger/delete/{id}', 'ChartOfAccountController@deleteAccountLedger')->name('delete.chart-account-ledger');
        Route::get('/account-group/delete/{id}', 'ChartOfAccountController@deleteAccountGroup')->name('delete.chart-account-group');
        Route::delete('/account-group/archive/{id}', 'ChartOfAccountController@archiveAccountGroup')->name('archive.accountGroup');
        Route::delete('/account-ledger/archive/{id}', 'ChartOfAccountController@archiveAccountLedger')->name('archive.accountLedger');
    });

    Route::get('/account/getAccount', 'AccountController@getAccount')->name('getAccount');
    Route::get('/account/image/{file}','AccountController@getImage')->name('account.image');
    Route::group(['prefix' => 'setup'], function(){
        Route::get('/account/getAccount/{year}', 'AccountController@getAccount')->name('account.getAccount');
        Route::put('/account/functionRemoveAccount/{user_id}', 'AccountController@functionRemoveAccount')->name('account.functionRemoveAccount');
        Route::get('/supplier/getSuppliers/{year}', 'SupplierController@getSuppliers')->name('supplier.getSuppliers');
        Route::get('/supplier/RemoveSuppliers/{supp_id}', 'SupplierController@RemoveSuppliers')->name('supplier.removeSuppliers');
        Route::put('/supplier/FunctionRemoveSupp/{supp_id}', 'SupplierController@FunctionRemoveSupp')->name('supplier.functionRemoveSupp');
        Route::get('/category/getCategories/{year}', 'CategoryController@getCategories')->name('category.getCategories');
        
        Route::resource('/account', 'AccountController');
        Route::group(['prefix' => 'accounts'],function(){
            Route::get('/remove-account/{id}', 'AccountController@removeAccount')->name('remove.account');
        });
        Route::resource('/inventory-status', 'InventoryStatusController');
        Route::group(['prefix' => 'inventory-statuses'],function(){
            Route::get('/remove-inventory/{id}', 'InventoryStatusController@removeInventory')->name('remove.inventory');
            Route::get('/get-inventory-status/{year}', 'InventoryStatusController@getInventoryStatus')->name('get.inventory.list');
            Route::put('/delete-inventory/{id}', 'InventoryStatusController@deleteInventory')->name('delete.inventory');
        });
        Route::resource('/supplier', 'SupplierController');
        Route::resource('/category', 'CategoryController');
        Route::get('/residents/autocompleteResidents', 'ResidentContorller@autocompleteResidents')->name('autocompleteResidents');
        Route::get('/residents/FunctionResidents/{res_id}', 'ResidentContorller@FunctionResidents')->name('residents.FunctionResidents');
        Route::put('/residents/FunctionRemoveResidents/{res_id}', 'ResidentContorller@FunctionRemoveResidents')->name('residents.FunctionRemoveResidents');
        Route::resource('/residents', 'ResidentContorller');
        Route::get('/residents/getResidents/{year}', 'ResidentContorller@getResidents')->name('residents.getResidents');
        Route::POST('/subcategory', 'CategoryController@storeSubCateg')->name('store.subcateg');
        Route::get('/category-delete/{id}/delete', 'CategoryController@delete')->name('category.delete');
        
        Route::get('/signatory/{id}/delete', 'SignatoryController@delete')->name('signatory.delete');
        Route::get('/signatories/getSignatories/{year}', 'SignatoryController@getSignatories')->name('signatories.list');
        Route::get('/signatory/image/{file}','SignatoryController@getImage')->name('signatory.image');
        Route::resource('/signatory', 'SignatoryController');
        Route::get('/template-signatories/{id}/delete','TemplateSignatoriesController@delete')->name('template-signatories.delete');
        Route::get('/template-signatories/getTemplates', 'TemplateSignatoriesController@getTemplates')->name('templates.list');
        Route::resource('/template-signatories', 'TemplateSignatoriesController');
    });

    Route::get('/purchase-order/list/{year}', 'PurchaseOrderController@getPurchaseOrders')->name('purchase-order.getPurchaseOrders');


    Route::group(['prefix'=>'accounting'], function(){
       
        Route::resource('/disbursement-voucher', 'DisbursementVoucherController');
        Route::get('/disbursement-vouchers/getVoucher/{year}', 'DisbursementVoucherController@getVoucher')->name('disbursement-voucher.list');
        Route::get('/disbursement-vouchers/{id}/delete', 'DisbursementVoucherController@delete')->name('disbursement-voucher.delete');
        Route::resource('/chart-account', 'ChartOfAccountController');
        Route::resource('/budget-allocation', 'BudgetAllocationController');
        Route::group(['prefix' => 'budget-allocations'],function(){
            Route::get('/get-budget-allocation', 'BudgetAllocationController@getBudgetAllocationList')->name('get.budgetallocation.list');
            Route::get('/remove-budget-allocation/{id}/delete','BudgetAllocationController@removeBudgetAllocation')->name('remove.budget-allocation');
        });
    });
 

    Route::group(['prefix' => 'procurement'], function(){
        Route::get('/canvassing/reloadtable/{id}', 'CanvassingController@reloadtable')->name('canvassing.reloadtable');
        Route::get('/canvassing-delete/{id}/delete', 'CanvassingController@delete')->name('canvassing.delete');
        Route::put('/canvassing/functionRemoveCanvassing/{id}/group/{group_id}', 'CanvassingController@functionRemoveCanvassing')->name('functionRemoveCanvassing');
        Route::get('/canvassing/getCanvassing/{year}', 'CanvassingController@getCanvassing')->name('canvassing.getCanvassing');
        Route::resource('/canvassing', 'CanvassingController');
        Route::get('/canvassing-show/{id}/show/{group_id}', 'CanvassingController@show')->name('canvassing.show');
        Route::get('/canvassing-edit/{id}/edit/{group_id}', 'CanvassingController@edit')->name('canvassing.edit');
        Route::get('/canvassing-delete/{id}/remove/{group_id}', 'CanvassingController@delete')->name('canvassing.delete');
        Route::get('/purchase-request/getPurchaseRequest/{year}', 'PurchaseRequestController@getPurchaseRequest')->name('pur.getPurchaseRequest');
        Route::get('/purchase-request/reloadtable/{pur_id}', 'PurchaseRequestController@reloadtable')->name('pur.reloadtable');
        Route::get('/purchase-request/reloadtableedit/{pur_id}', 'PurchaseRequestController@reloadtableedit')->name('pur.reloadtableedit');
        Route::get('/purchase-request/RemovePurchaseRequest/{perchase_id}', 'PurchaseRequestController@RemovePurchaseRequest')->name('pur.RemovePurchaseRequest');
        Route::get('/purchase-request/autocomplete', 'PurchaseRequestController@autocomplete')->name('pur.autocomplete');
        Route::get('/purchase-request/canvass', 'PurchaseRequestController@validationcassvas')->name('pur.canvass');
        Route::put('/purchase-request/FunctionRemovePurchaseRequest/{purchase_id}', 'PurchaseRequestController@FunctionRemovePurchaseRequest')->name('pur.FunctionRemovePurchaseRequest');
        Route::get('/purchase-request/roadetails', 'PurchaseRequestController@roadetails')->name('pur.roadetails');
        Route::get('/canvassing-delete/{id}/delete', 'CanvassingController@delete')->name('canvassing.delete');
        Route::get('/canvassing/getPurchaseRequest/{id}', 'CanvassingController@getPurchaseRequest')->name('getPurchaseRequest');
        Route::get('/purchase-order/reloadtable/{id}', 'PurchaseOrderController@reloadtablePO')->name('po.reloadtable');
        Route::get('/purchase-order/fetch/{id}', 'PurchaseOrderController@purchaseOrder')->name('purchaseOrder');
        Route::get('/purchase-order/fetch-pr/{id}', 'PurchaseOrderController@purchaseRequestID')->name('purchaseRequestID');
        Route::get('/purchase-order/supplier', 'PurchaseOrderController@supplierNameDetails')->name('supplierNameDetails');
        Route::get('/purchase-order/request-date', 'PurchaseOrderController@requestDate')->name('requestDate');
        Route::resource('/purchase-order', 'PurchaseOrderController');
        Route::get('/purchase-order-delete/{id}/delete', 'PurchaseOrderController@delete')->name('purchase.order.delete');
        Route::resource('/purchase-request', 'PurchaseRequestController');
        Route::group(['prefix' => 'purchase-requests'],function(){
            Route::resource('/related-docs', 'RelatedDocsController');
        });
    });
    Route::group(['prefix' => 'documents'], function(){
        Route::get('/get-document/{document_id}','DocumentController@getDocument')->name('fetch.document');
        Route::get('/refresh-select','DocumentController@refreshSelect')->name('refresh.select');
    });
    Route::resource('/reports', 'ReportsController');
    Route::group(['prefix' => 'report'],function (){
        Route::group(['prefix' => 'accounting'],function (){
            Route::get('/account-statement/{bank_id}','ReportsController@accountStatement')->name('account_statement.report');
            Route::get('/manual-transactions','ReportsController@manualTransactions')->name('manual_transactions.report');
            Route::get('/ledgers-manual-transactions','ReportsController@ledgersmanualTransactions')->name('ledgersmanual_transactions');
            Route::post('/function-manual-transactions','ReportsController@functionmanualTransactions')->name('manual_transactions.create');
            Route::get('/statement-of-appropriations','ReportsController@appropriation')->name('obligations_balances.report');
            Route::get('/statement-of-appropriations/search','ReportsController@searchappropriation')->name('obligations_balances.search');
            Route::get('/transmittal-letter','ReportsController@transmittal')->name('transmital_letter.report');
            Route::POST('/transmittal-letter/search', 'ReportsController@searchTransmittal')->name('search.transmittal');
            Route::POST('/cedula/search', 'ReportsController@searchCedula')->name('search.cedula');
            Route::POST('/officialreceipt/search', 'ReportsController@searchReceipt')->name('search.receipt');
            Route::get('/estimated-actual-income/search', 'ReportsController@searchIncome')->name('search.income');
            Route::POST('/accountability/search', 'ReportsController@functionAccountability')->name('search.functionAccountability');
            Route::POST('/account_statement/search', 'ReportsController@functionaccountStatement')->name('search.account_statement');
            Route::get('/accountable-form','ReportsController@accountable')->name('accountable_form.report');
            Route::get('/accountability-form','ReportsController@accountability')->name('accountability_form.report');
            Route::get('/rbi_form-form','ReportsController@rbiform')->name('rbi_form.report');
            Route::get('/cashbook', 'ReportsController@cashbook')->name('cashbook.report');
            Route::get('/cashbook/search', 'ReportsController@Searchcashbook')->name('cashbook.search');
            Route::get('/statement-of-comparison','ReportsController@comparison_statement')->name('comparison_statement.report');
            Route::get('/statement-of-comparison/search','ReportsController@comparison_statement_search')->name('comparison_statement.search');
            Route::get('/registration-of-obligation','ReportsController@registration_obligation')->name('registration_obligation.report');
            Route::get('/registration-of-obligation/search','ReportsController@registration_obligation_search')->name('registration_obligation.search');
            Route::get('/estimated-actual-income', 'ReportsController@estimatedActualIncome')->name('estimated_actual_income.report');
            Route::get('/cedula', 'ReportsController@cedula')->name('cedula.report');
            Route::get('/reports-of-collections-and-deposits', 'ReportsController@official_receipt')->name('official_receipt.report');
        });
        Route::group(['prefix' => 'inventory'],function (){
            Route::get('/supplies-and-materials', 'ReportsController@suppliesMaterials')->name('supplies_materials.report');
            Route::get('/supplies-and-materials/search', 'ReportsController@SearchsuppliesMaterials')->name('supplies_materials.search');

        });
        Route::group(['prefix' => 'procurement'],function (){

        });
    });
});
