
(function($) {
    //general function for validation and submit of forms
    $.fn.registerFields = function (rules, resets=true) {
        let $form_id = $(this);
        $($form_id).validate({
            rules: rules,
            errorPlacement: function(error,element){//custom function for special cases of the error messages
                let placement = $(element).data('error');
                console.log(error[0])
                let html = '<label class="error">Highlighted fields are required.</label>'
                if(placement){
                    $(placement).html(html)
                }else{
                    error.insertAfter(element);
                }
            },
            highlight: function (element) {
                $(element).parent().addClass('validate-has-error');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('validate-has-error');
            },
            submitHandler: function (event) {
                let $formData = new FormData($form_id[0]);
                $.ajax({
                    type: 'POST',
                    url: $form_id.attr('action'),
                    data: $formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        //add pre load animation
                    },
                    error: function (data) {
                        //error
                    },
                    success: function (data) {
                        if (data.status === "success"){
                            if (data.reload == true) {
                                window.location.reload();
                            }
                            if (data.href == true){
                                window.location.href = data.route;
                            }
                            if(resets){
                                $form_id.each(function(){
                                    this.reset(); //resets the form values
                                    // empties elements not included as form fields
                                    var revert_selector = "[revert]";
                                    $(revert_selector).each(function(){$(this).text("").val("")})
                                    if($form_id.attr('id') === "budget_form"){
                                        $('.compute-total').each(function(){ $(this).attr('remove', false).text("Compute Total"); });
                                        $(revert_selector).each(function(){$(this).attr('remove', false)});
                                        $('.sub_total').each(function(){$(this).text('')});
                                        $('.general_total').each(function(){$(this).text('₱0')});
                                    }
                                    $("#supplier_id").prop('disabled', true);
                                    $(".label-info").remove('');
                                    if($form_id.attr('id') === "roa_form"){
                                        document.getElementById("total_amount_spent").innerHTML = '	₱ 0.00';
                                        document.getElementById("total_refund_or_no").innerHTML = '	₱ 0.00';
                                    }
                                });
                            }
                            if(data.refresh_or){
                                $('#or_no').val(data.or_number);
                            }
                            if(data.refresh_dv){
                                $('#check_no').val(data.check_number);
                            }
                            if(data.refresh_cd){
                                $('#ctc_number').val(data.ctc_number);
                            }
                            if(data.refresh_or_pad){
                                // alert(data.or_number)
                                $('#myModal').modal('hide');
                                $('#or_no').val(data.or_number);
                            }
                            if(data.refresh_cd_pad){
                                // alert(data.or_number)
                                $('#myModal').modal('hide');
                                $('#ctc_number').val(data.ctc_number);
                            }
                            if(data.refresh_dv_pad){
                                // alert(data.or_number)
                                $('#myModal').modal('hide');
                                $('#check_no').val(data.dv_number);
                            }
                            if(data.reload_table){
                                $.ajax({
                                    url: data.route,
                                    method: "GET",
                                    success: function(datas){
                                        $("#"+data.tableid+"").empty(); 
                                        $("#"+data.tableid+"").html(datas);

                                        //delete only append data
                                        $(".appendRow").remove();
                                        //this function uses for reloading data needed
                                        reloadData(datas);
                                    },
                                    
                                });
                            }
                            if(data.reload_select){
                                $('#max-signatory').val('0');
                                $.ajax({
                                    url: data.route,
                                    method: "GET",
                                    success: function(data){
                                        signees = []
                                        sign_names = {}
                                        sign_positions = {}
                                        $('#form_id').empty().html(data);
                                        $("#keep-order").multiSelect("destroy").multiSelect({ 
                                            keepOrder: true,
                                            afterSelect: function(value,text){
                                                if ($("#keep-order option:selected").length > $('#max-signatory').val()) {
                                                    toastMessage('Oopps!','Maximum numbers of signatures reached.','error');
                                                    $('#keep-order').multiSelect('deselect',([value[0]]))
                                                }
                                                sign_positions[value[0]] = $('#sign' + value[0]).attr('data-position');
                                                console.log(sign_positions)
                                                sign_names[value[0]] = $('#sign' + value[0]).text()
                                                signees.push(value[0]);
                                                changeSignees();
                                            },
                                            afterDeselect: function(value,text){
                                                signees = $.grep(signees, function(v) {
                                                    return v != value[0];
                                                });
                                                delete sign_names[value[0]];
                                                delete sign_positions[value[0]];
                                                changeSignees();
                                            }
                                        });
                                    }
                                })
                            }
                            toastMessage('Success',data.desc,data.status);
                            $('#purchase_order_list').empty();
                            $("#card_docs").empty();
                            $('#purchase_order_list').append('<tr><td colspan="6">No data available in table</td></tr>');
                            $('#po_num').val('Purchase Order No.');
                        } else{
                            toastMessage('Ooops!',data.desc,data.status);
                        }
                    },
                    complete: function () {
                        
                    }
                });
                return false;
            },
            invalidHandler: function(event, validator){
                toastMessage('Oopps!','It seems like you missed some required fields',"error");
            }
        });
    };

}(jQuery));

(function($) {
    //general function for validation and submit of forms
    $.fn.registerFieldReport = function (rules, resets=true) {
        let $form_id = $(this);
        $($form_id).validate({
            rules: rules,
            errorPlacement: function(error,element){//custom function for special cases of the error messages
                let placement = $(element).data('error');
                console.log(error[0])
                let html = '<label class="error">Highlighted fields are required.</label>'
                if(placement){
                    $(placement).html(html)
                }else{
                    error.insertAfter(element);
                }
            },
            highlight: function (element) {
                $(element).parent().addClass('validate-has-error');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('validate-has-error');
            },
            submitHandler: function (event) {
                let $formData = new FormData($form_id[0]);
                $.ajax({
                    type: 'POST',
                    url: $form_id.attr('action'),
                    data: $formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                });
                return true;
            },
            invalidHandler: function(event, validator){
                toastMessage('Oopps!','It seems like you missed some required fields',"error");
            }
        });
    };

}(jQuery));
function toastMessage(title,message,toastType){
    toastr.remove();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr[toastType](message, title);
}


$(document).ready(function() {
    $(".manage-accounts").popover({ trigger: "click" , html: true, animation:false})
        .on("mouseenter", function () {
            var _this = this;
            $(this).popover("show");
            $(".popover").on("mouseleave", function () {
                $(_this).popover('hide');
            });
        }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });
    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });
    //Toast alert after deleting
    var url_string = window.location.href
    var url = new URL(url_string);
    var param = url.searchParams.get("success");
    var param2 = url.searchParams.get("module");
    
    console.log(param);
    if(param == 'true'){
        toastMessage('Success',param2+' moved to Archived!',"success");
        window.history.pushState({}, document.title, "/" + url.searchParams.get("url")); //remove url param
    }
  });
$(document).on('click','.print-form',function (e) {
    e.preventDefault();
    // let form_id = $(this).closest("form").attr('id');
    // let div = $('.container');
    let form_id = $(this).attr('data-form');
    $('#'+form_id).removeAttr('hidden');
    printJS({
        printable: form_id,
        type: 'html',
        showModal: true,
        targetStyles: ['*']
    })
    // printJS(form_id, 'html');
    $('#'+form_id).attr('hidden', true);
    return false;
});
// Mark\
$(document).on('click','.print-form-css',function (e) {
    e.preventDefault();
    // let form_id = $(this).closest("form").attr('id');
    // let div = $('.container');
    let form_id = $(this).attr('data-form');
    $('#'+form_id).removeAttr('hidden');
    printJS({
        printable: form_id,
        type: 'html',
        showModal: true,
        // targetStyles: ['*']
        scanStyles: false,
        css: 'http://staging.abcsystems.com.ph/blueridge/public/css/print.css'
    })
    // printJS(form_id, 'html');
    $('#'+form_id).attr('hidden', true);
    return false;
});

$(document).on('click','.print-form-report',function (e) {
    e.preventDefault();
    let form_id = $(this).attr('data-form');
    $('#'+form_id).removeAttr('hidden');
    printJS({
        printable: form_id,
        type: 'html',
        showModal: true,
        scanStyles: false,
        css: 'http://staging.abcsystems.com.ph/blueridge/public/css/print-report.css'
    })
    $('#'+form_id).attr('hidden', true);
    return false;
});
function listRandomColors(color_count){
    //create a random array of colors using rgb format
    let colors = [];

    for (i = 0; i < color_count; i++) {
        var r = Math.floor(Math.random() * 200);
        var g = Math.floor(Math.random() * 200);
        var b = Math.floor(Math.random() * 200);
        var v = Math.floor(Math.random() * 500);
        colors.push('rgb(' + r + ', ' + g + ', ' + b + ')');
    }

    return colors;
}

$(document).ready(function(){
    $( ".datetimepicker" ).datepicker({
        dateFormat: 'MM d, yy',
    }).on('change', function() {
        $(this).valid();  // triggers the validation test
    });
});
