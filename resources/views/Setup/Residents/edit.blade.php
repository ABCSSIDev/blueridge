@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Residents</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Residents</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('residents.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST" id="edit_residents_form" class="form-horizontal" action="{{route('residents.update',$GeneralUser->id)}}" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    
                    <div class="errors"></div>
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Full Name</label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control" value="{{ ($GeneralUser->first_name != null && $GeneralUser->last_name != null?$GeneralUser->first_name :(!empty($name[0]) ? $name[0] : '')) }}" name="first_name" id="first_name"  placeholder="First Name" autocomplete="off">
                        </div>
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control" value="{{ ($GeneralUser->first_name != null && $GeneralUser->last_name != null?$GeneralUser->last_name :(!empty($name[1]) ? $name[1] : '')) }}" name="last_name" id="last_name"  placeholder="Last Name" autocomplete="off">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" value="{{ ($GeneralUser->first_name != null  && $GeneralUser->last_name != null?$GeneralUser->middle_initial :(!empty($name[2]) ? $name[2] : '')) }}" name="middle_initial" id="middle_initial"  placeholder="Middle Name" autocomplete="off">
                        </div>
                        <div class="form-group col-md-1">
                            <input type="text" class="form-control" value="{{ ($GeneralUser->first_name != null  && $GeneralUser->last_name != null?$GeneralUser->quantifier :(!empty($name[3]) ? $name[3] : '')) }}" name="quantifier" id="quantifier"  placeholder="Quantifier" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Address</label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>No.</label>
                            <input type="text" class="form-control" value="{{ !empty($address[0]) ? $address[0] : '' }}" onkeypress="return isNumberKeySlash(event);" name="address_no" id="address_no"  placeholder="No" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Street</label>
                            <input type="text" class="form-control" value="{{ !empty($address[1]) ? $address[1] : '' }}" name="street" id="street"  placeholder="Street" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Highest Education Attainment</label>
                            <input type="text" class="form-control" id="highest_education" value="{{ $GeneralUser->highest_education }}" name="highest_education" placeholder="Highest Education Attainment" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Role Type</label>
                            <select class="form-control" name="role_type" id="role_type">
                                <option value="">Select Role Type</option>
                                @foreach($role as $val)
                                    <option value="{{ $val->id }}" {{ $val->id == $GeneralUser->role_id ? 'selected' : '' }}>{{ $val->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Cluster No.</label>
                            <input type="text" class="form-control" onkeypress="return isNumberKeySlash(event);" value="{{ $GeneralUser->cluster }}"  name="cluster_no" id="cluster_no"  placeholder="Cluster No" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Household No.</label>
                            <input type="text" class="form-control" onkeypress="return isNumberKeySlash(event);" value="{{ $GeneralUser->household }}"   name="household_no" id="household_no"  placeholder="Household No" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Date of Birth</label>
                            <input type="" class="form-control datetimepicker" name="birth_date" id="birth_date" value="{{ $GeneralUser->birth_date }}" placeholder="Date of Birth" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Place of Birth</label>
                            <input type="text" class="form-control" id="birth_place" name="birth_place" value="{{ $GeneralUser->birth_place }}" placeholder="Place of Birth" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Citizenship</label>
                            <input type="text" class="form-control" id="citizenship" name="citizenship" value="{{ $GeneralUser->citizenship }}" placeholder="Citizenship" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Sex</label>
                            <select class="form-control" name="sex" id="sex">
                                <option value="">Select Gender</option>
                                <option value="0" {{ 0 == $GeneralUser->sex ? 'selected' : '' }}>Female</option>
                                <option value="1" {{ 1 == $GeneralUser->sex ? 'selected' : '' }}>Male</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Civil Status</label>
                            <select class="form-control" name="civil_status" id="civil_status">
                                <option value="">Select Civil Status</option>
                                <option value="1" {{ 1 == $GeneralUser->civil_status ? 'selected' : '' }}>Single</option>
                                <option value="2" {{ 2 == $GeneralUser->civil_status ? 'selected' : '' }}>Married</option>
                                <option value="3" {{ 3 == $GeneralUser->civil_status ? 'selected' : '' }}>Widowed</option>
                                <option value="4" {{ 4 == $GeneralUser->civil_status ? 'selected' : '' }}>Divorced</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Religion.</label>
                            <input type="text" class="form-control" value="{{ $GeneralUser->religion }}" id="religion" name="religion" placeholder="Religion" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Health Status</label>
                            <input type="text" class="form-control" value="{{ $GeneralUser->health_status }}" id="health_status" name="health_status" placeholder="Health Status" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact No.</label>
                            <input type="number" class="form-control price" value="{{ $GeneralUser->contact_number }}" id="contact_number" name="contact_number" placeholder="Contact No." autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Occupation</label>
                            <input type="text" class="form-control" id="occupation" value="{{ $GeneralUser->occupation }}" name="occupation" placeholder="Occupation" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        // $(".price").number(true,2);
        let rules = {
            first_name : {
                required: true,
            },
            last_name : {
                required: true,
            },
            middle_name : {
                required: true,
            },
            street : {
                required: true,
            },
            address_no : {
                required: true,
            },
            cluster_no : {
                required: true,
            },
            household_no : {
                required: true,
            },
            birth_date : {
                required: true,
            },
            birth_place : {
                required: true,
            },
            civil_status : {
                required: true,
            },
            religion : {
                required: true,
            },
            health_status : {
                required: true,
            },
            contact_no : {
                required: true,
            },
            highest_education : {
                required: true,
            }, 
            citizenship : {
                required: true,
            },
            role_type : {
                required: true,
            },
            sex : {
                required: true,
            },
        };
        $('#edit_residents_form').registerFields(rules,false);

        // allow letters and whitespaces only.
        $(".text-only").keydown(function(event){
            var inputValue = event.which;
            if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
                event.preventDefault(); 
            }
        });
        function isNumberKeySlash(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode != 47 && charCode > 32
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        // $( "#first_name" ).autocomplete({
        //     source: function(request, response) {
        //             $.ajax({
        //             url: "{{route('autocompleteResidents')}}",
        //             data: {
        //                     term : request.term
        //             },
                    
        //             dataType: "json",
        //             success: function(data){
                      
        //             var resp = $.map(data,function(obj){
        //                 return obj.name;
        //             }); 
        //             response(resp);
        //             auto_data = data;
                    
        //             }
        //         });
                
        //     },
        //     select: function(event,ui){
        //         event.preventDefault();
        //         event.stopImmediatePropagation()
        //         $.map(auto_data,function(obj){
        //             if(ui.item.label == obj.name){
        //                 var name = obj.name;
        //                 var data = name.split(" ");
        //                 var address = obj.address;
        //                 var values = address.split(" ");
        //                 $('#first_name').val(data[0]);
        //                 $('#last_name').val(data[2]);
        //                 $('#address_no').val(values[0]);
        //                 $('#street').val(values[1]);
        //                 $('#middle_initial').val(data[1]);
        //                 $('#birth_date').val(obj.birth_date);
        //                 $('#birth_place').val(obj.birth_place);
        //                 $('#contact_no').val(obj.contact_number);
        //                 $('#sex').val(obj.sex);
        //                 $('#citizenship').val(obj.citizenship);
        //                 $('#civil_status').val(obj.civil_status);
        //                 $('#tin').val(obj.tin);
        //             }
        //         }); 
        //     },
        // });
        // $('.ui-autocomplete-clear').click(function(){
        //         $('#first_name').val('');
        //         $('#last_name').val('');
        //         $('#address').val('');
        //         $('#middle_initial').val('');
        //         $('#birth_date').val('');
        //         $('#birth_place').val('');
        //         $('#sex').val('');
        //         $('#address_no').val();
        //         $('#street').val();
        //         $('#civil_status').val('');
        //         $('#citizenship').val('');
        //         $('#contact_no').val(''); 
        //         $('#tin').val(''); 
        // });
        $('#contact_person').keydown(function (e) {
            if (e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
                }
            }
            });
    </script>
@endpush

@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
