@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Suppliers</li>
        </ol>
    </nav>
    <h4 class="form-head">Residents List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"> <a href="{{ route('residents.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table table-striped table-bordered table-hover table-mediascreen" id="supplier_residents">
                <thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">ID</th>
                        <th scope="col">Residents Name</th>
                        <th scope="col">Contact No.</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody >
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
<style>
</style>
@push('scripts')
<script>
         var oTable = $("#supplier_residents").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('residents.getResidents',0000) !!}",
                columns:[
                    {data: "id","name":"id","width":"30%", className:"v-align text-center"},
                    {data: "name","name":"name","width":"30%", className:"v-align text-center"},
                    {data: "contact_number","name":"contact_number","width":"20%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("residents.getResidents", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush
