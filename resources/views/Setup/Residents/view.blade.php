@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Residents</li>
        </ol>
    </nav>
    <h4 class="form-head">View Residents</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('residents.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Full Name</label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <p>&nbsp;{{ $name[0] }} {{ !empty($name[1]) ? $name[1] : '' }} {{ !empty($name[2]) ? $name[2] : '' }}{{ !empty($name[3]) ? $name[3] : '' }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Address.</label>
                             <p>&nbsp;{{ !empty($address[0]) ? $address[0] : '' }} {{ !empty($address[1]) ? $address[1] : '' }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Highest Education Attainment</label>
                            <p>&nbsp;{{ ($GeneralUser->highest_education != Null?$GeneralUser->highest_education:'N/A') }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Role Type</label>
                            <p>&nbsp;{{ $GeneralUser->Role->name }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Cluster No.</label>
                            <p>&nbsp;{{ ($GeneralUser->cluster != Null?$GeneralUser->cluster:'N/A') }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Household No.</label>
                            <p>&nbsp;{{ ($GeneralUser->household != Null?$GeneralUser->household:'N/A') }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Date of Birth</label>
                            <p>&nbsp;{{ ($GeneralUser->birth_date != Null && $GeneralUser->birth_date != '0000-00-00'?\App\Common::formatDate("F d, Y",$GeneralUser->birth_date):'N/A') }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Place of Birth</label>
                            <p>&nbsp;{{ ($GeneralUser->birth_place != Null?$GeneralUser->birth_place:'N/A') }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Citizenship</label>
                            <p>&nbsp;{{ ($GeneralUser->citizenship != Null?$GeneralUser->citizenship:'N/A') }}</p>
                           
                        </div>
                        <div class="form-group col-md-6">
                            <label>Sex</label>
                            <p>&nbsp;{{ $GeneralUser->sex == 1?'Male':'Female' }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Civil Status</label>
                            <!-- <p>&nbsp;{{ $GeneralUser->civil_status }}</p> -->
                            <p>&nbsp;{{ ($GeneralUser->civil_status == 1?'Single':($GeneralUser->civil_status == 2?'Married':($GeneralUser->civil_status == 3?'Widowed':'Divorce')))}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Religion.</label>
                            <p>&nbsp;{{ ($GeneralUser->religion != Null?$GeneralUser->religion:'N/A') }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Health Status</label>
                            <p>&nbsp;{{ ($GeneralUser->health_status != Null?$GeneralUser->health_status:'N/A') }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact No.</label>
                            <p>&nbsp;{{ ($GeneralUser->contact_number != Null?$GeneralUser->contact_number:'N/A') }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Occupation</label>
                            <p>&nbsp;{{ ($GeneralUser->occupation != Null?$GeneralUser->occupation:'N/A') }}</p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>



@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
