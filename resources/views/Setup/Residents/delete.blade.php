<form method="POST" id="remove_res_form"  class="form-horizontal" action="{{ route('residents.FunctionRemoveResidents',$GeneralUser->id)}}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    <h6> Are you sure you want to delete {{ $name[0] }} {{ !empty($name[1]) ? $name[1] : '' }} {{ !empty($name[2]) ? $name[2] : '' }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{route('residents.index')}}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>