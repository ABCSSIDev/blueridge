@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Inventory Status</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Inventory Status</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('inventory-status.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST" id="inventory_status_form"  class="form-horizontal" action="{{route('inventory-status.store')}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="status_name">Status Name</label>
                                <input type="text" class="form-control" name="status_name" id="status_name" placeholder="Status Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="status_type">Status Type</label>
                                <select class="form-control" name="status_type" id="status_type">
                                    <option value="">Select Status Type</option>
                                    <option value="1">Deployed</option>
                                    <option value="2">Ready to Deployed</option>
                                    <option value="3">Undeployable</option>
                                    <option value="4">Pending</option>
                                    <option value="5">Archived</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Note (Optional)</label>
                                <textarea class="form-control" name="note" placeholder="Note"></textarea>
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
</style>

@push('scripts')
<script>
    let rules = {
        status_name : {
            required: true
        },
        status_type: {
            required: true
        }
    };
    $("#inventory_status_form").registerFields(rules);
</script>
@endpush
