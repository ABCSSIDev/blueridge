@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Setup</li>
        <li class="breadcrumb-item active" aria-current="page">Inventory Status</li>
    </ol>
    </nav>
    <h4 class="form-head">Edit Inventory Status</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('inventory-status.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST"  class="form-horizontal" id="edit_inventory_form"  enctype="multipart/form-data" action="{{route('inventory-status.update',$InventoryEdit->id)}}">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Status Name</label>
                                <input type="text" class="form-control" name="status_name" id="status_name" value="{{$InventoryEdit->status_name}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Status Type</label>
                                <select class="form-control" name="status_type" id="status_type">
                                    <option value="1" {{$InventoryEdit->status_type == 1 ? "selected" : ""}}>Deployed</option>
                                    <option value="2" {{$InventoryEdit->status_type == 2 ? "selected" : ""}}>Ready to Deployed</option>
                                    <option value="3" {{$InventoryEdit->status_type == 3 ? "selected" : ""}}>Undeployable</option>
                                    <option value="4" {{$InventoryEdit->status_type == 4 ? "selected" : ""}}>Pending</option>
                                    <option value="5" {{$InventoryEdit->status_type == 5 ? "selected" : ""}}>Archived</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Note</label>
                                <textarea class="form-control" name="note" >{{$InventoryEdit->notes}}</textarea>
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
</style>

@push('scripts')

<script>
    let rules = {
        status_name : {
            required: true
        },
        status_type: {
            required: true
        }
    };
    $('#edit_inventory_form').registerFields(rules);
</script>

@endpush
