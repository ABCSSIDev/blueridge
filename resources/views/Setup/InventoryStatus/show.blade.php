@extends('layouts.app')


@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Setup</li>
        <li class="breadcrumb-item active" aria-current="page">Inventory Status</li>
    </ol>
    </nav>
    <h4 class="form-head">View Inventory Status</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('inventory-status.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>Status Name:</b></label>
                            <p>{{ $InventoryStatus->status_name}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Status Type:</b></label>
                            <p>{{ $InventoryStatus->status_type}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label><b>Note:</b></label>
                            <p>{{ $InventoryStatus->notes}}</p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>


@endsection

<style>
    .margin-top{
        margin-top:50px;
    }
</style>
