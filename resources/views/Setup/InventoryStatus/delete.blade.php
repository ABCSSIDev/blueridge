<form method="POST" class="form-horizontal" enctype="multipart/form-data" action="{{route('delete.inventory',$id)}}" >
<input type="hidden" name="_method" value="PUT">
@csrf
    <h6> Are you sure you want to delete {{ $inventory_status->status_name }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>