@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Setup</li>
            <li class="breadcrumb-item">Signatories</li>
            <li class="breadcrumb-item active" aria-current="page">Signatory Templates</li>
        </ol>
    </nav>
    <h4 class="form-head">View Signatory Templates</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('template-signatories.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST" id="template_form" class="form-horizontal" action="{{ route('template-signatories.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="card" id="card_docs" style="margin-top: 30px; padding: 15px">
                                {!! $document !!}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        let signees = [], sign_names = [],sign_positions = [];
        @foreach($template->getSignatures as $signatory)
            signees.push({{ $signatory->getSignatory->id }});
            sign_names.push('{{ $signatory->getSignatory->signatory_name }}');
            sign_positions.push('{{ $signatory->getSignatory->position }}');
        @endforeach
        // console.log(sign_names);
        // $.each(sign_names,function(index,value){
        //     console.log(value)
        // })
        $('.sign-name').each(function (index){
            let sign_name = 'Signee';
            let position = 'Position';

            let name_container = $(this);
            let position_container = $(this).attr('ref')
            if(sign_names[index] != null){
                sign_name = sign_names[index];
                position = sign_positions[index];
            }
            $(document).find(name_container).html(sign_name);
            $(document).find(position_container).html(position);
        });
    </script>
@endpush