@extends('layouts.app')


@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Setup</li>
            <li class="breadcrumb-item">Signatories</li>
            <li class="breadcrumb-item active" aria-current="page">Signatory Templates</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Signatory Templates</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('template-signatories.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST" id="template_form" class="form-horizontal" action="{{ route('template-signatories.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label>Form Name</label>
                                <select class="form-control" id="form_id" name="document_id">
                                    <option value="">Select Document</option>
                                    @foreach($docs as $doc)
                                        <option value="{{ $doc->id }}" data-url="{{ route('fetch.document',$doc->id) }}">{{ $doc->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Signatories</label><input type="hidden" id="max-signatory" value="0">
                                <div class="signatory_errors"></div>
                                <div>
                                <select id='keep-order' name="signatories[]" class="sign-check" multiple='multiple'>
                                    @foreach($signs as $sign)   
                                        <option id="sign{{ $sign->id }}" data-position="{{$sign->position}}" value="{{ $sign->id }}">{{ $sign->signatory_name }}</option>
                                    @endforeach
                                </select>
                                    <!-- @foreach($signs as $sign)
                                        <input id="signatory{{ $sign->id }}" class="sign-check" type="checkbox" data-error=".signatory_errors" name="signatories[]" value="{{ $sign->id }}"> {{ $sign->signatory_name }}<br>
                                        <label for="signatory{{ $sign->id }}" class="margin-left">({{ $sign->position }})</label><br><br>
                                    @endforeach -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card" id="card_docs" style="margin-top: 30px; padding: 15px">

                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary mt-3"><span class="fa fa-floppy-o"></span> SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let signees = [], sign_names = {},sign_positions = {};
    // $( document ).ready(function() {
    //     $.ajax({
    //         url: "{{ $first_docs }}",
    //         success: function(html) {
    //             $("#card_docs").empty().append(html);
    //         },
    //         dataType: 'html'
    //     });
    // });

    $('#keep-order').multiSelect({ 
        keepOrder: true,
        afterSelect: function(value,text){
            if ($("#keep-order option:selected").length > $('#max-signatory').val()) {
                toastMessage('Oopps!','Maximum numbers of signatures reached.','error');
                $('#keep-order').multiSelect('deselect',([value[0]]))
            }
            sign_positions[value[0]] = $('#sign' + value[0]).attr('data-position');
            sign_names[value[0]] = $('#sign' + value[0]).text()
            signees.push(value[0]);
            changeSignees();
        },
        afterDeselect: function(value,text){
            signees = $.grep(signees, function(v) {
                return v != value[0];
            });
            delete sign_names[value[0]];
            delete sign_positions[value[0]];
            changeSignees();
        }
    });
    $('#keep-order').change(function(){
        
    })
    $('#form_id').change(function(){
        var url = $('option:selected', this).attr('data-url');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                $("#card_docs").empty().html(data.view);
                $('#max-signatory').val(data.signatory);
                changeSignees();
            }
        });
    });
    function changeSignees(){
        $('#template_form .sign-name').each(function(index){
            let pos = $(this).attr('ref');
            console.log($(this))
            let sign_person = signees[index];
            let name = 'Signee';
            let position = 'Position';
            let item = $('#template_form .sign-name')[index];

            if(sign_person != null){
                name = sign_names[sign_person]
                position = sign_positions[sign_person];
            }         
            $('#template_form').find(item).html(name);
            $('#template_form').find(pos).html(position)

        })
    }
  
    let rules = {
        document_id : {
            required: true
        },
        'signatories[]': {
            required: true
        }
    };
    $('#template_form').registerFields(rules);
</script>
@endsection


<style>
    /* .margin-top{
        margin-top:50px;
    }
    .margin-left{
        font-size: 12px;
        margin-left: 15px;
    }
    .underline{
        text-decoration: underline;
    }
    .signa-margin{
        margin: 30px 0px 30px 0px;
    } */
</style>

