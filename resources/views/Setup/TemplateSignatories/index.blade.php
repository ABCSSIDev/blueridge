@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Setup</li>
            <li class="breadcrumb-item">Signatories</li>
            <li class="breadcrumb-item active" aria-current="page">Signatory Templates</li>
        </ol>
    </nav>
    <h4 class="form-head">Signatory Templates List</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('template-signatories.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table table-striped table-bordered table-hover table-mediascreen text-center" id="template-list">
                <thead class="thead-dark">
                    <tr>
                        <th>Form Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>
         $("#template-list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('templates.list') !!}",
                columns:[
                    {data: "template_name","template_name":"template_name","width":"30%"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
    </script>
@endpush
<style>
    .margin-top{
        margin-top:50px;
    }
</style>

