@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Accounts</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Account</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{route('account.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" action="{{route('account.store')}}" enctype="multipart/form-data" id="account_form">
                    {{ csrf_field() }}
                    <div class="errors"></div>
                    <div class="center">
                        <div class="default" style="cursor: pointer;">
                            <img onclick="onImage()" src="{{asset('images/default.png')}}" height="100" width="100" id="im" name="im" class="rounded-circle">
                            <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                        </div>
                        <span class="icon-camera" onclick="onImage()" style="cursor: pointer;"> <i class="fa fa-camera"></i></span>
                    </div>
                    <h5 >Personal Information</h5><hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input type="text" id="txtNumeric" name="name" class="form-control" autocomplete="off" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="position">Position</label>
                            <input type="text" id="position" name="position" class="form-control" autocomplete="off" placeholder="Position">
                        </div>
                    </div>
                    <h5>Contact Details</h5><hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email"  class="form-control" autocomplete="off" placeholder="Ex. blueridge@yahoo.com">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_number">Contact Number</label>
                            <input type="number" name="contact_number" maxlength="11" id="contact_number" class="form-control" autocomplete="off" placeholder="Contact Number">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="address">Address</label>
                            <textarea class="form-control" name="address"  id="address" autocomplete="off" placeholder="Address"></textarea>
                        </div>
                    </div>
                    <h5>User Credentials</h5><hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="password">Password</label>
                            <input type="password"  id="password" name="password"  class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">Confirm Password</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        function onImage() {
            $('#image').click();
        }

        function onFileSelected() {
            var selectedFile = event.target.files[0];
            var files = event.target.files;
            var reader = new FileReader();
            if(selectedFile && selectedFile.size < 2097152)
            {
                var imgtag = document.getElementById("im");
                imgtag.title = selectedFile.name;
                reader.onload = function(event) {
                    imgtag.src = event.target.result;
                };
                reader.readAsDataURL(selectedFile);
            }
        }

        let rules = {
            name : {
                required: true
            },
            position : {
                required: true
            },
            email : {
                required: true
            },
            contact_number : {
                required: true,
                maxlength: 11
            },
            address : {
                required: true
            },
            password : {
                required: true
            },
            password_confirmation : {
                required: true,
                equalTo : "#password"
            },
        };
        $('#account_form').registerFields(rules, true);

        $(function() {

        $('#txtNumeric').keydown(function (e) {

        if (e.altKey) {
        
            e.preventDefault();
            
        } else {
        
            var key = e.keyCode;
            
            if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            
            e.preventDefault();
            
            }

        }
        
        });

        });
    </script>
@endpush
@endsection
<style>
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .default{
        position: relative;
    }
    .icon-camera{
        height: 20px;
        width: 20px;
        position: absolute;
        margin-top: -16px;
        margin-left: 10px;
        font-size: 10px;
        border-radius: 50%;
        color: #fff;
        background-color: #3490dc;
    }
    .fa-camera{
        margin-top: 6px;
    }
</style>

