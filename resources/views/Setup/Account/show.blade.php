@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Setup</li>
        <li class="breadcrumb-item active" aria-current="page">Accounts</li>
    </ol>
    </nav>
    <h4 class="form-head">View Account</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{route('account.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <form method="POST"  class="form-horizontal" action="" enctype="multipart/form-data">
                    <div class="center">
                        <img src="{{ $user->image == null ? asset('images/default.png') : route('account.image',$user->id)}}" class="rounded-circle" height="100" width="100"> 
                    </div>
                    <h5 >Personal Information</h5><hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Name:</label>
                            <p>{{ $user->name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Position:</label>
                            <p>{{ $user->position }}</p>
                        </div>
                    </div>
                    <h5 class="top">Contact Details</h5><hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Email:</label>
                            <p>{{ $user->email }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact No.</label>
                            <p>{{ $user->contact_number }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Address:</label>
                            <p>{{ $user->address }}</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
</style>
