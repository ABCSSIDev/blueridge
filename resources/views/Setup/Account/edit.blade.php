@extends('layouts.app')


@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Accounts</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Account</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{route('account.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="col-12"> 
                <div class="tab-content">
                    <form method="POST"  id="account_edit_form" class="form-horizontal" action="{{ route('account.update', $user->id) }}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                        <div class="errors"></div>
                        <div class="center">
                            <div class="default">
                                <img onclick="onImage()" src="{{ $user->image == null ? asset('images/default.png') : route('account.image',$user->id)}}" height="100" width="100" class="rounded-circle" id="im" name="im">
                                <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                                <input name="old_image" id="old_image" value="{{(!$user?'':($user->image==null?'':$user->image))}}" hidden>
                            </div>
                            <span class="icon-camera"> <i class="fa fa-camera"></i></span>
                        </div>
                        <h5 >Personal Information</h5><hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="position">Position</label>
                                <input type="text" class="form-control" id="position" name="position" value="{{ $user->position }}" autocomplete="off">
                            </div>
                        </div>
                        <h5>Contact Details</h5><hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number">Contact No.</label>
                                <input type="text" class="form-control" name="contact_number" value="{{ $user->contact_number }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="address">Address</label>
                                <textarea class="form-control" name="address"  id="address" autocomplete="off">{{ $user->address }}</textarea>
                            </div>
                        </div>
                        <h5>User Credentials <button id="edit_btn" title="Edit password" type="button" class="btn btn-dark"><i class="fa fa-edit"></i></button></h5><hr>
                        <div class="form-row credentials">
                            <div class="form-group col-md-6">
                                <label for="password">Password</label>
                                <input type="password"  id="password" name="password"  class="form-control password" >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="password_confirmation">Confirm Password</label>
                                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control confirm">
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        let rules = {
            name : {
                required: true
            },
            position : {
                required: true
            },
            email : {
                required: true
            },
            contact_number : {
                required: true,
                minlength: 11
            },
            address : {
                required: true
            },
        };
        $(".credentials").toggle();
        $("#edit_btn").click(function(){
            $(".credentials").toggle();
            var wasVisible = $(".credentials").is(":visible");
            if(wasVisible){
                $('.password').rules('add',{'required' : true})
                $('.confirm').rules('add',{'required' : true, 'equalTo' : "#password"})
            }
            else{
                $('.password').rules('add',{'required' : false})
                $('.confirm').rules('add',{'required' : false})
            }
        });
        function onImage() {
            $('#image').click();
        }

        function onFileSelected() {
            var selectedFile = event.target.files[0];
            var files = event.target.files;
            var reader = new FileReader();
            if(selectedFile && selectedFile.size < 2097152)
            {
                var imgtag = document.getElementById("im");
                imgtag.title = selectedFile.name;
                reader.onload = function(event) {
                    imgtag.src = event.target.result;
                };
                reader.readAsDataURL(selectedFile);
            }
        }
        $('#account_edit_form').registerFields(rules);
    </script>
@endpush
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .default{
        position: relative;
    }
    .icon-camera{
        height: 20px;
        width: 20px;
        position: absolute;
        margin-top: -16px;
        margin-left: 10px;
        font-size: 10px;
        border-radius: 50%;
        color: #fff;
        background-color: #3490dc;
    }
    .fa-camera{
        margin-top: 6px;
    }
</style>