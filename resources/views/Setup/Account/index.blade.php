@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Setup</li>
        <li class="breadcrumb-item active" aria-current="page">Accounts</li>
    </ol>
    </nav>
    <h4 class="form-head">Accounts List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('account.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table table-striped table-bordered table-hover table-mediascreen" id="account_list">
                <thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">Image</th>
                        <th scope="col">Name</th>
                        <th scope="col">Position</th>
                        <th scope="col">Contact No.</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody >

                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')
    <script>
    // $(document).ready(function(){
        var oTable = $('#account_list').DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('account.getAccount',0000) !!}",
                columns:[
                    {data: "image","name":"image",orderable:false,searchable:false,"width":"10%", className:"v-align text-center"},
                    {data: "name","name":"name","width":"25%", className:"v-align text-center"},
                    {data: "position","name":"position","width":"25%", className:"v-align text-center"},
                    {data: "contact","name":"contact","width":"25%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"15%", className:"v-align text-center"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
    // });
    $('#budget_year').change(function(){
        var year = $(this).val();
        var url = '{{ route("account.getAccount", ':year') }}';
        url = url.replace(':year', $(this).val());
        oTable.ajax.url(url).load();
    });
    

    
    </script>
@endpush
@endsection




