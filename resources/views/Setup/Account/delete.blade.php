<form method="POST" id="delete_account_form" class="form-horizontal" enctype="multipart/form-data" action="{{route('account.functionRemoveAccount',$account_list->id)}}">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    <div class="errors"></div>
    <h6> Are you sure you want to delete {{ $account_list->name }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>
@push('scripts')
<script>
let rules = {
  };
$('#delete_account_form').registerFields(rules);
</script>
@endpush