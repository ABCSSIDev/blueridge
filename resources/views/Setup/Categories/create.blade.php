@extends('layouts.app')

@section('content')
<div class="container">
  <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Category</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">       
        <div align="right" style=""><a href="{{route('category.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
          <div class="tab-content">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="about-nav-tab">
                              <ul class="nav nav-tabs nav-tab-category" role="tablist">
                                  <li class="nav-items"><a href="#category_section" class="nav-link active" data-toggle="tab">Category</a></li>
                                  <li class="nav-items"><a href="#sub_category_section" class="nav-link" data-toggle="tab">Sub Category</a></li>
                              </ul>
                              <div class="tab-content-about">
                                  <div class="tab-pane fade active show" id="category_section">
                                      <form method="POST" id="category_form"  class="form-horizontal" action="{{ route('category.store') }}" enctype="multipart/form-data">
                                              {{ csrf_field() }}
                                          <div class="errors"></div>
                                          <div class="form-row">
                                              <div class="form-group col-md-6">
                                                  <label>Category Name</label>
                                                  <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Category Name">
                                              </div>
                                          </div>
                                          <div class="form-group" align="right" >
                                              <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                                          </div>
                                      </form>
                                  </div>
                                  <div class="tab-pane fade" id="sub_category_section">
                                      <form method="POST" id="sub_category_form"  class="form-horizontal" action="{{ route('store.subcateg') }}" enctype="multipart/form-data">
                                          {{ csrf_field() }}
                                          <div class="form-row">
                                              <div class="form-group col-md-6">
                                                  <label>Category</label>
                                                  <select class="form-control" name="cat_name" id="cat_name">
                                                  <option>Select Category</option>
                                                  @foreach($categories as $category)
                                                      <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                                  @endforeach
                                                  </select>
                                              </div>
                                              <div class="form-group col-md-6">
                                                  <label>Sub-category Name</label><br>
                                                  <input type="text" class="form-control" name="sub_category_name" id="sub_category_name" placeholder="Sub-Category Name">
                                              </div>
                                          </div>
                                          <div class="form-group sw-top">
                                            <label>Stored In Inventory?</label>
                                            <div class="col-sm-3 col-md-3 col-lg-3">
                                                <label class="switch">
                                                <input type="checkbox" id = "inventory" name= "inventory">
                                                <div class="slider"></div>
                                              </label>
                                            </div>
                                          </div>
                                          <div class="form-group" align="right" >
                                              <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>


@endsection

<style>
/* Custom checkbox */
.stored-label {
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 16px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.stored-label input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}
.checkmark {
  position: absolute;
  top: 40px;
  left: 6rem;
  height: 25px;
  width: 25px;
  background-color: #eee;
}
.stored-label:hover input ~ .checkmark {
  background-color: #ccc;
}
.stored-label input:checked ~ .checkmark {
  background-color: #2196F3;
}
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}
.stored-label input:checked ~ .checkmark:after {
  display: block;
}
.stored-label .checkmark:after {
    left: 9px;
    top: 5px;
    width: 6px;
    height: 12px;
    border: solid white;
    border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.margin-top{
    margin-top:50px;
}

/* Tabs for Categories */
.about-nav-tab .nav-tabs {
  margin-bottom: 40px;
}
.about-nav-tab .nav-tabs .nav-link.active {
  border-color: #132644 !important;
}
.about-nav-tab .nav-tabs .nav-link.active {
    color: white;
    background-color: #132644;
}
.about-nav-tab .nav-tabs .nav-link {
  color: black;
  border: none;
}
.about-nav-tab .nav-tabs .nav-items .nav-link {
  font-weight: 600;
  padding: 18px 30px;
  line-height: 1;
  border-color: #132644;
  font-size: 20px;
}
.nav-tab-category {
  border-bottom: 3px solid #132644 !important
}
.tab-content-about > .tab-pane.active {
  display: block;
}
.about-nav-tab .tab-content-about .tab-pane p {
  margin-bottom: 25px;
  font-size: 18px;
  color: aliceblue;
}
.about-nav-tab .tab-content-about .tab-pane ul {
  margin-bottom: 43px;
}
.tab-content-about>.tab-pane {
  display: none;
}
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}
.switch input {display:none;}
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #8593ae;
  -webkit-transition: .4s;
  transition: .4s;
}
.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}
input:checked + .slider {
  background-color: rgba(10, 122, 211, 0.82);
}
input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}
input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}
.slider.round {
  border-radius: 34px;
}
.slider.round:before {
  border-radius: 50%;
}
.sw-top{
  margin-top: 20px;
}
</style>
@push('scripts')
<script>
  let rules1 = {
    category_name : {
        required: true
    }
  };
  let rules2 = {
    cat_name : {
        required: true
    },
    sub_category_name : {
        required: true
    }
  };
  $('#category_form').registerFields(rules1);
  $('#sub_category_form').registerFields(rules2);
</script>
@endpush