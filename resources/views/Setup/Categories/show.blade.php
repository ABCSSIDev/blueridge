@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>
    <h4 class="form-head">View Category</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{ route('category.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <div class="tab-content">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Category:</label>
                            <p>{{ $categories->category_id ? $categories->getCategory->category_name : $categories->category_name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Sub Category:</label>
                            <p>{{ $categories->category_id ? $categories->category_name : "N/A" }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Stored In Inventory</label>
                            <p>{{ $categories->is_stored ? "Yes" : "No" }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
</style>
