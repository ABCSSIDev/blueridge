@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>
    <h4 class="form-head">Category List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12 margin-top">
            <div align="right" style="margin: 0px 0px 20px 0px"> <a href="{{ route('category.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table cell-border hover row-border stripe text-center table-mediascreen" id="category_list">
                <thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">Category</th>
                        <th scope="col">Sub Category</th>
                        <th scope="col">Stored in Inventory</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
<style>
</style>
@push('scripts')
    <script>
         var oTable =  $("#category_list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('category.getCategories',0000) !!}",
                columns:[
                    {data: "category_name","category_name":"category_name","width":"30%", className:"v-align text-center"},
                    {data: "category_id","category_id":"category_id","width":"30%", className:"v-align text-center"},
                    {data: "is_stored","is_stored":"is_stored","width":"20%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("category.getCategories", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush