@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Suppliers</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Supplier</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('supplier.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST" id="supp_form" class="form-horizontal" action="{{route('supplier.store')}}" enctype="multipart/form-data">
                    <div class="errors"></div>
                {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Supplier Name</label>
                            <input type="text" class="form-control" name="supplier_name" id="supp_name"  placeholder="Supplier Name" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Person</label>
                            <input type="text" class="form-control text-only" name="contact_person" id="contact_person"  placeholder="Contact Person" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Supplier Address</label>
                            <input type="text" class="form-control" name="address" id="supp_address"  placeholder="Supplier Address" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Number</label>
                            <input type="number" class="form-control price"  name="contact_number" id="contact_number"  placeholder="Contact Number" maxlength="11" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>TIN Number</label>
                            <input type="text" class="form-control"  name="tin" id="tin"  placeholder="Tin Number" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        // $(".price").number(true,2);
        let rules = {
            supplier_name : {
                required: true,
            },
        };
        $('#supp_form').registerFields(rules);

        // allow letters and whitespaces only.
        // $(".text-only").keydown(function(event){
        //     var inputValue = event.which;
        //     if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
        //         event.preventDefault(); 
        //     }
        // });

        $('.text-only').keydown(function (e) {
            if (e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
                }
            }
            });
    </script>
@endpush

@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
