<form method="POST" id="remove_supp_form"  class="form-horizontal" action="{{route('supplier.functionRemoveSupp',$supplier_list->id)}}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    <h6> Are you sure you want to delete {{$supplier_list->supplier_name}}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{route('supplier.index')}}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>