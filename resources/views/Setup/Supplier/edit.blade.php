@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Suppliers</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Supplier</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0p"><a href="{{route('supplier.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST" id="edit_supp_form"  class="form-horizontal" action="{{route('supplier.update',$supplier_list->id)}}" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Supplier Name</label>
                            <input type="text" class="form-control text-only" name="supp_name" id="supp_name" value="{{$supplier_list->supplier_name}}" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Person</label>
                            <input type="text" class="form-control text-only" name="contact_person" id="contact_person" value="{{$supplier_list->contact_person == null ? 'N/A' : $supplier_list->contact_person}}" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Supplier Address</label>
                            <input type="text" class="form-control" name="supp_address" id="supp_address" value="{{$supplier_list->address == null ? 'N/A' : $supplier_list->address }}" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Number</label>
                            <input type="text" class="form-control" name="contact_number" id="contact_number" value="{{$supplier_list->contact_number == null ? 'N/A' : $supplier_list->contact_number}}" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>TIN Number</label>
                            <input type="text" class="form-control" name="tin" id="tin" value="{{$supplier_list->tin == null ? 'N/A' : $supplier_list->tin}}" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
@push('scripts')
    <script>
        let rules = {
            supp_name : {
                required: true
            },
            
        };
        $('.text-only').keydown(function (e) {
            if (e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
                }
            }
        });
        $('#edit_supp_form').registerFields(rules, false);
    </script>
@endpush