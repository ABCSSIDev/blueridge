@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Suppliers</li>
        </ol>
    </nav>
    <h4 class="form-head">View Supplier</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('supplier.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Supplier Name</b></label><br>
                        <p>&nbsp;{{$supplier_list->supplier_name}}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Contact Person</b></label><br>
                        <p>&nbsp;{{$supplier_list->contact_person == null ? "N/A" : $supplier_list->contact_person}}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Supplier Address</b></label><br>
                        <p>&nbsp;{{$supplier_list->address == null ? "N/A" : $supplier_list->address}}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Contact Number</b></label><br>
                        <p>&nbsp;{{$supplier_list->contact_number == null ? "N/A" : $supplier_list->contact_number}}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>TIN Number</b></label><br>
                        <p>&nbsp;{{$supplier_list->tin == null ? "N/A" : $supplier_list->tin}}</p>
                    </div>
                </div>
                <!-- <div class="form-group" align="right" >
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span>PRINT</button>
                </div> -->
            </div>
        </div>
    </div>
</div>


@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
