@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Setup</li>
            <li class="breadcrumb-item">Signatories</li>
            <li class="breadcrumb-item active" aria-current="page">New Signatory</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Signatory</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('signatory.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="col-12"> 
                    <div class="tab-content">
                        <form method="POST" id="signatory-form"  class="form-horizontal" action="{{ route('signatory.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="signatory_name">Signatory Name</label>
                                    <input type="text" name="signatory_name" id="signatory_name" class="form-control" placeholder="Signatory Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email Address</label>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="blueridge@yahoo.com">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="position">Position</label>
                                    <input type="text" name="position" id="position" class="form-control" placeholder="Position">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Upload Signature</label><br>
                                    <input style="border: 0px;" type="file" name="signature" id="signature" class="form-control upload-signature">
                                </div>
                            </div>
                            <div class="form-group" align="right" >
                                <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span> SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
@push('scripts')
    <script>
        let rules1 = {
            signatory_name : {
                required: true
            },
            email: {
                required: true
            },
            position: {
                required: true
            },
            // signature: {
            //     required: true
            // }
        };
        $('#signatory-form').registerFields(rules1);
    </script>
@endpush
<style>
    .margin-top{
        margin-top:50px;
    }
</style>