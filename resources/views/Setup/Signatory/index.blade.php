@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item">Signatories</li>
            <li class="breadcrumb-item active" aria-current="page">New Signatory</li>
        </ol>
    </nav>
    <h4 class="form-head">Signatory List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 50px 0px 10px 0px"> <a href="{{ route('signatory.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;CREATE</button></a></div>
        </div>
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover table-mediascreen text-center" id="signatory_list">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Signatory Name</th>
                        <th scope="col">Position</th>
                        <th scope="col">Email Address</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                
            </table>
        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>
        var oTable =  $("#signatory_list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('signatories.list',0000) !!}",
                columns:[
                    {data: "signatory_name","signatory_name":"signatory_name","width":"30%"},
                    {data: "position","position":"position","width":"30%"},
                    {data: "email","email":"email","width":"25%"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"15%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("signatories.list", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
 