@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item">Signatories</li>
            <li class="breadcrumb-item active" aria-current="page">New Signatory</li>
        </ol>
    </nav>
    <h4 class="form-head">View Signatory</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{ route('signatory.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <div class="tab-content">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Signatory:</label>
                            <p>{{ $signatory->signatory_name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Position:</label>
                            <p>{{ $signatory->position }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Email:</label>
                            <p>{{ $signatory->email }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Signature Image:</label>
                            @if($signatory->sign_image != '')
                            <img style="width: 100px;height: 150px;" src="{{ route('signatory.image',$signatory->id) }}" alt="">
                            @else
                            <p>No Signature image available.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
</style>
