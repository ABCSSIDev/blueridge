@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">New Inventory</li>
        </ol>
    </nav>
    <h4 class="form-head" style="margin: 0px 0px 50px 0px">Create New Inventory</h4>
    <div class="row justify-content-center" >
        <div class="col-md-12">
            <div class="tab-content">
                    <form method="POST" id="add_inventory_form"  class="form-horizontal" action="" enctype="multipart/form-data">
                        <div class="validation-sample" id="validation-sample">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="status_name">Image (Optional)</label><br>
                                    <img onclick="onImage()" src="{{asset('images/default.png')}}" height="100" width="100" id="im" name="im" class="rounded-circle" style="display: none;" >
                                    <input type="file" onchange="onFileSelected(event)" name="" id="image" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="status_name">Inventory Description</label>
                                    <input type="text" class="form-control input-valid" name="inventory_description" id="inventory_description" placeholder="Inventory Description" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Units</label>
                                    <select class="form-control select-valid" name="unit" id="unit" data-validate="required">
                                        <option value="">Select Units</option>
                                        @foreach($units as $unit)  
                                        <option value="{{$unit->id}}">{{$unit->unit_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Category</label>
                                    <select class="form-control select-valid" name="category" id="category">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)  
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="status_name">Quantity</label>
                                    <input type="text" class="form-control price input-valid" name="quantity" id="quantity" placeholder="Quantity" autocomplete="off">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="status_name">Unit Price</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">₱</span>
                                        </div>
                                        <input type="text" class="form-control price input-valid" name="unit_price" id="unit_price" placeholder="Unit Price" autocomplete="off">
                                    </div>
                                    <!-- <input type="text" class="form-control price input-valid" name="unit_price" id="unit_price" placeholder="Unit Price" autocomplete="off"> -->
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Inventory Type</label>
                                    <select class="form-control select-valid" name="inventory_type" id="inventory_type">
                                        <option value="">Select Inventory Type</option>
                                        <option value="capex">Capital Expenditures</option>
                                        <option value="opex">Operating Expenses</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="unit_date">Date</label>
                                    <input type="text" class="form-control datetimepicker" name="unit_date" id="unit_date" placeholder="Date" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group" align="right" >
                                <button type="button" class="btn btn-primary" id="add_inventory"><i class="fa fa-plus" aria-hidden="true"></i> ADD</button>
                            </div>
                        </div>
                    </form>
                    <form method="POST" id="inventory_form"  class="form-horizontal" action="{{route('new-inventory.store')}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead class="thead-dark">
                                        <tr align="center">
                                        <th>Image</th>
                                        <th>Inventory Description</th>
                                        <th>Units</th>
                                        <th>Category</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Inventory Type</th>
                                        <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_inventory">
                                        
                                    </tbody>
                                    <tbody id="no_data">
                                        <tr align="center">
                                            <td colspan="8">No Data Available</td>
                                        </tr>
                                    </tbody>

                                    <tbody id="no_datas">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
    .table td{
        vertical-align: middle !important;
    }
</style>

@push('scripts')
    <script>
        var picture = '';
        var validation = false;
        var ctr = 1;
        $("#no_data").show();
        $(".price").number(true,2);

        function onImage() {
            $('#image').click();
        }

        function onFileSelected() {
            var selectedFile = event.target.files[0];
            var files = event.target.files;
            var reader = new FileReader();
            if(selectedFile && selectedFile.size < 2097152)
            {
                var imgtag = document.getElementById("im");
                imgtag.title = selectedFile.name;
                reader.onload =  function(event) {
                    imageIsLoaded(event);
                    imgtag.src = event.target.result;
                    
                };
                reader.readAsDataURL(selectedFile);
                validation = true;
            }
        }

        function imageIsLoaded(e) {
            picture = e.target.result;
        }

        $('#add_inventory_form').validate({
            rules : {
                unit : {
                    required: true
                },
                inventory_description : {
                    required: true
                },
                quantity : {
                    required: true
                },
                inventory_type : {
                    required: true
                },
                unit_price : {
                    required: true
                },
                category : {
                    required: true
                },
            }
        });

       
        $("#add_inventory").click(function(e){
            $("#no_data").hide();
            $('#no_datas').empty().html();
            
            var inventory_description = $("#inventory_description").val();
            var unit = $("#unit option:selected").text();
            var unit_id = $("#unit").val();
            var category = $("#category option:selected").text()
            var category_id = $("#category").val()
            var unit_price = $("#unit_price").val();
            var quantity = $("#quantity").val()
            var inventory_type = $("#inventory_type option:selected").text();
            var inventory_type_value = $("#inventory_type").val();
         
            if(validation == false){
                img_ = "{{ asset('images/default.png')}}";
                picture = "";
            }
            else{
                img_ = picture;
            }
            if($('#add_inventory_form').valid()){
                
                $('#add_inventory_form').each(function(){
                    this.reset(); //resets the form values
                });
                $('.valid').each(function(){
                    $(this).removeClass('validate-has-error');
                });
               
                $('#table_inventory').append('<tr class="appendRow" align="center">'+
                    '<td><input type="hidden" name="for_insert[]" value="'+ctr+'"><img src='+img_+' height="50" width="50" class="rounded-circle"><input type="" name="image'+ctr+'" value="'+picture+'" style="display: none;" ></td>'+
                    '<td><input type="hidden" name="inventory_description'+ctr+'" value="'+inventory_description+'">'+inventory_description+'</td>'+
                    '<td><input type="hidden" name="unit'+ctr+'" value="'+unit_id+'">'+unit+'</td>'+
                    '<td><input type="hidden" name="category'+ctr+'" value="'+category_id+'">'+category+'</td>'+
                    '<td><input type="hidden" name="quantity'+ctr+'" value="'+quantity+'">'+quantity+'</td>'+
                    '<td><input type="hidden" name="unit_price'+ctr+'" value="'+unit_price+'">'+unit_price+'</td>'+
                    '<td><input type="hidden" class="inventory_name" name="inventory_type'+ctr+'" value="'+inventory_type_value+'">'+inventory_type+'</td>'+
                    '<td><button class="btn btn-primary remove_inventory"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'+
                '</tr>');

                $('#table_inventory').on('click','.remove_inventory',function(e){
                    e.preventDefault();
                    $(this).parent().parent().remove();
                    if($('#table_inventory').children("tr").length == 0){
                        $("#no_data").show();
                    }
                });
                
                ctr++;
                validation = false;
            }else{
                $("#no_data").show();
                $('.error').each(function(){
                    $(this).addClass('validate-has-error');
                });
                $('.select-valid').click(function(){
                    ValidError(this);
                });
                $('.input-valid').keyup(function(){
                    ValidError(this);
                });
                validation = false;
                $('#image').empty();

            }
        });
        let rules = '';
        $('#inventory_form').registerFields(rules);

        function reloadData(data){
            //this function uses for reloading data needed
            $("#no_datas").empty().append(data.append_no_data);
        }

        function ValidError(element){
            if($(element).val()){
                $(element).removeClass('validate-has-error');
            }
            else{
                $(element).addClass('validate-has-error');
            }
        }


    </script>
@endpush

