@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Operating Expense (OPEX)</li>
        </ol>
    </nav>
    <h4 class="form-head" >Operating Expense List</h4>
    <div class="row" >
        <div class="col-md-2" style="margin: 0px 0px 40px 0px">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12" >
            <table  class="table table-striped table-bordered table-hover table-mediascreen" id="consumable_list" >
                <thead class="thead-dark">
                    <tr align="center">
                        <th scope="col">Image</th>
                        <th scope="col">Description</th>
                        <th scope="col">Category</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
    .table > tbody > tr > td  {
        vertical-align: middle;
    }
</style>
@push('scripts')
    <script>

    var oTable = $("#consumable_list").DataTable(
        {
            pagingType: "full_numbers",
            processing: true,
            //serverSide: true,
            type: "GET",
            ajax:"{!! route('consumables.getConsumables',0000) !!}",
            columns:[
                    {data: "image","image":"image",orderable:false,searchable:false,"width":"10%", className:"text-center"},
                    {data: "description","description":"description","width":"25%", className:"v-align text-center"},
                    {data: "category_id","category_id":"category_id","width":"25%", className:"v-align text-center"},
                    {data: "quantity","quantity":"quantity","width":"25%", className:"v-align text-center"},
                    {data: "action","action":"action","width":"15%", className:"v-align text-center"},
            ],
            select: {
                style: "multi"
            }
        }
    );
    $('#budget_year').change(function(){
        var year = $(this).val();
        var url = '{{ route("consumables.getConsumables", ':year') }}';
        url = url.replace(':year', $(this).val());
        oTable.ajax.url(url).load();
    });
    </script>
@endpush