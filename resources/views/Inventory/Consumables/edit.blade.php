@extends('layouts.app')


@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Operating Expense (OPEX)</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Operating Expense</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{route('consumable.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="col-12"> 
                <div class="tab-content">
                    <form method="POST"  id="opex_edit_form" class="form-horizontal" action="{{ route('consumable.update', $inventory->id) }}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                        <div class="errors"></div>
                        <div class="center">
                            <div class="default">
                                <img onclick="onImage()" src="{{ $inventory->image == null ? asset('images/default.png') : route('consumables.image',$inventory->id)}}" height="100" width="100" class="rounded-circle" id="im" name="im">
                                <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                                <input name="old_image" id="old_image" value="{{(!$inventory?'':($inventory->image==null?'':$inventory->image))}}" hidden>
                            </div>
                            <span class="icon-camera"> <i class="fa fa-camera"></i></span>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Description</label>
                                    <input type="text" class="form-control" id="description" name="description" value="{{ $inventory->getInventoryName->description }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="position">Category</label>
                                <select class="form-control" name="category" id="category">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id}}" {{ ($category->id == $inventory->category_id ? 'selected' : '' ) }}>{{$category->category_name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="address">Unit</label>
                                    <select class="form-control" name="unit" id="unit">
                                    @foreach($units as $unit)
                                        <option value="{{ $unit->id}}" {{ ($unit->id == $inventory->unit_id ? 'selected' : '' ) }}>{{$unit->unit_name}}</option>
                                    @endforeach
                                    </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Quantity</label>
                                    <input type="text" class="form-control" id="qty" name="qty" value="{{ number_format($inventory->quantity) }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="contact_number">Unit Price</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control price" name="unit_price" id="unit_price" value="{{ $inventory->unit_price }}" autocomplete="off">
                                </div>
                                <!-- <input type="text" class="form-control price" name="unit_price" id="unit_price" value="{{ $inventory->unit_price }}" autocomplete="off"> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="address">Minimum Quantity</label>
                                <input type="text" class="form-control" name="min_qty" id="min_qty" value="{{ $inventory->minimum_qty }}" autocomplete="off" placeholder="Minimum Quantity">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="remarks">Remarks</label>
                                <input type="text" class="form-control" name="remarks" id="remarks" value="{{ $inventory->remarks }}" autocomplete="off" placeholder="Remarks">
                            </div>
                        </div>
                        
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(".price").number(true,2);
        let rules = {
            description : {
                required: true
            },
            category : {
                required: true
            },
            unit : {
                required: true,
            },
            unit_price : {
                required: true,
            },
            qty : {
                required: true,
            }

        };
        function onImage() {
            $('#image').click();
        }

        function onFileSelected() {
            var selectedFile = event.target.files[0];
            var files = event.target.files;
            var reader = new FileReader();
            if(selectedFile && selectedFile.size < 2097152)
            {
                var imgtag = document.getElementById("im");
                imgtag.title = selectedFile.name;
                reader.onload = function(event) {
                    imgtag.src = event.target.result;
                };
                reader.readAsDataURL(selectedFile);
            }
        }
        $('#opex_edit_form').registerFields(rules, false);
    </script>
@endpush
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .default{
        position: relative;
    }
    .icon-camera{
        height: 20px;
        width: 20px;
        position: absolute;
        margin-top: -16px;
        margin-left: 10px;
        font-size: 10px;
        border-radius: 50%;
        color: #fff;
        background-color: #3490dc;
    }
    .fa-camera{
        margin-top: 6px;
    }
</style>