@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Operating Expense (OPEX)</li>
        </ol>
    </nav>
    <h4 class="form-head">View Operating Expense</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{ route('consumable.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <div class="tab-content">
                    <div class="center">
                        <img src="{{ $inventory->image == null ? asset('images/default.png') : route('consumables.image',$inventory->id)}}" class="rounded-circle" height="100" width="100"> 
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Description:</label>
                            <p>{{$inventory->getInventoryName->description}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Category:</label>
                            <p>{{$inventory->getCategory->category_name}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Unit:</label>
                            <p>{{$inventory->getUnits->unit_name}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Quantity:</label>
                            <p>{{ number_format($inventory->quantity) }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Unit Price:</label>
                            <p>₱{{ number_format($inventory->unit_price,2) }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Minimum Quantity:</label>
                            <p>{{($inventory->minimum_qty == NULL? 'None' : $inventory->minimum_qty)}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Stock Number:</label>
                            <p>{{$stock_number}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Remarks</label>
                            <p>{{($inventory->remarks == NULL? 'None' : $inventory->remarks)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
</style>
