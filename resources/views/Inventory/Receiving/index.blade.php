@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Inventory Receiving</li>
        </ol>
    </nav>
    <h4 class="form-head">Inventory Receiving List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('receiving.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
        <table class="table table-striped table-bordered table-hover table-mediascreen" id="inventory_receiving_list">
            <thead class="thead-dark">
                <tr align="center">
                    <th>PO ID</th>
                    <th>Supplier</th>
                    <th>Date Received</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
<style>
</style>

@push('scripts')
<script>
     var oTable = $("#inventory_receiving_list").DataTable(
        {
            pagingType: "full_numbers",
            processing: true,
            //serverSide: true,
            type: "GET",
            ajax:"{!! route('get.receiving.list',0000) !!}",
            columns:[
                {data: "purchase_no","name":"purchase_no","width":"20%", className:"v-align text-center"},
                {data: "supplier_name","name":"supplier_name","width":"20%", className:"v-align text-center"},
                {data: "date_received","name":"date_received","width":"20%", className:"v-align text-center"},
                // {data: "receipt_no","name":"receipt_no","width":"20%", className:"v-align text-center"},
                {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
            ],
            select: {
                style: "multi"
            }
        }
    );
    $('#budget_year').change(function(){
        var year = $(this).val();
        var url = '{{ route("get.receiving.list", ':year') }}';
        url = url.replace(':year', $(this).val());
        oTable.ajax.url(url).load();
    });

</script>

@endpush
