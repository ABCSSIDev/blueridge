<form method="POST" class="form-horizontal" enctype="multipart/form-data" action="{{ route('functionRemoveInventoryReceiving',$receiving->id)}}" >
<input type="hidden" name="_method" value="PUT">
@csrf
    <h6> Are you sure you want to delete " P.O ID {{ App\Common::getPRNumberFormat("purchase_orders", $receiving->purchaseOrder->getPurchaseRequest,1).App\Common::getAlphabet($receiving->purchaseOrder->getPurchaseRequest, true, $ids) }}"?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>