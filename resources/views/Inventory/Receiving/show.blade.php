@extends('layouts.app')

@section('content')


<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Inventory Receiving</li>
        </ol>
    </nav>
    <h4 class="form-head">View Inventory Receiving Details</h4>
    <div class="row justify-content-center">
        <div class="col-md-12 margin-bottom">
            <div align="right" style="margin-bottom:20px"><a href="{{ route('receiving.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Purchase Order No.</b></label>
                        <p>{{ $po->po_no }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Date Received</b></label>
                        <p>{{ App\Common::convertWordDateFormat($inventory_receiving->date_received) }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Supplier Name</b></label>
                        <p>{{$inventory_receiving->purchaseOrder->getCanvass->getSuppliers->supplier_name}}</p>
                    </div>
                    <div class="form-group col-md-3">
                        <label><b>Receipt Number</b></label>
                        <p>{{$inventory_receiving->receipt_number == null ? 'N/A' : $inventory_receiving->receipt_number}}</p>
                    </div>
                    <div class="form-group col-md-3">
                        <label><b>RER Number</b></label>
                        <p>{{$inventory_receiving->rer_number == null ? 'N/A' : $inventory_receiving->rer_number}}</p>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr align="center">
                        <th>Item No.</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Qty. Received</th>
                        <th>Remarks</th>
                        <th>Inventory Type</th>
                        </tr>
                    </thead>
                    <tbody >
                        @php $total_rec = 0; $total_qty = 0; @endphp
                        @foreach($inventory_receiving->getReceivingItems as $data => $val)
                            @php 
                                 $totalcount;
                                 $total_rec+= $val->quantity_received; 
                                $total_qty += $val->getCanvassItem->available_quantity; 
                            @endphp
                            <tr align="center">
                                <input type='hidden' name='for_insert_receiving[]' value="{{ $val->id }}">
                                <td class="v-align">{{ ++$data }}</td>
                                <td class="v-align">{{ $val->getCanvassItem->getPRItems->getInventoryName->description }}</td>
                                <td class="v-align">{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->getCanvassItem->available_quantity) }}</td>
                                <td class="v-align">{{ $val->getCanvassItem->getPRItems->getUnits->unit_name }}</td>
                                <td class="v-align">{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity_received) }}</td>
                                <td class="v-align">{{ $val->remarks }}</td>
                                <td class="v-align">{{ ($val->inventory_type == 'capex'?'Capital Expenditures':'Operating Expenses') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="form-group" align="right" >
                    <button class="btn btn-primary print-form-css" data-form="print_receiving"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    @php $total = 0; $item_no = 0; @endphp  
    @foreach($canvass_item as $canvass_value => $item)
        @php
            $item_no++;
            $est_cost = $item->available_quantity * $item->unit_price;
            $total += $item->unit_price * $item->available_quantity;
        @endphp
    @endforeach
   
</div>
<div id="print_receiving" hidden>
    <!-- @include('layouts.header') -->
    @include('Docs.acceptance-inspection')
    <div class="break-page"></div>
    <!-- @include('layouts.header') -->
    @include('Docs.delivery-acceptance')
    <!-- <div class="break-page"></div>
        @include('layouts.header')
        @include('Docs.inspection-report') -->
</div>



@endsection


<style>
    .footer {
        page-break-after: always;
    }
    .margin-top{
        margin-top:50px;
    }
</style>
