@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Inventory Receiving</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Inventory Receiving</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('receiving.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <div class="tab-content">
                    <form method="POST"  id="receiving_form" class="form-horizontal" enctype="multipart/form-data" action="{{route('receiving.store')}}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Purchase Order ID</label>
                                <select class="form-control"  id="po_list_receiving" name="purchase_no">
                                    <option value="">Select Purchase Order ID</option>
                                    @foreach($PurchaseList as $purchase)
                                        @if(\App\Http\Controllers\ReceivingController::checkAllPOQuantity($purchase->id))
                                            {{ $id = App\Common::getListAlphabet($purchase->pr_id, $purchase->canvass_id) }}
                                            <option value="{{$purchase->id.'-'.$purchase->canvass_id}}" data-url="{{route('get.receiving',$purchase->canvass_id)}}">{{ $purchase->po_no }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Date Received:</label>
                                <input type="text" autocomplete='off' placeholder="Date Received" class="form-control datetimepicker" name="date" id="date">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Supplier Name</label>
                                <input type="text" class="form-control" placeholder="Supplier Name" id="supplier_name" disabled>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Receipt Number</label>
                                <input type="text" autocomplete='off' class="form-control" placeholder="Receipt Number" id="receipt_no" name="receipt_no">
                            </div>
                            <div class="form-group col-md-3">
                                <label>RER Number</label>
                                <input type="text" autocomplete='off' class="form-control" placeholder="RER Number" id="rer_no" name="rer_no">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Authorized Inspector</label>
                                <select class="form-control"  id="inspector" name="inspector">
                                    <option value="Kgd. Thomas J.T.F. de Castro">Kgd. Thomas J.T.F. de Castro</option>
                                    <option value="Kgd. Ayjell Acejas- de Castro">Kgd. Ayjell Acejas- de Castro</option>
                                </select>
                            </div>
                        </div>
                        <div class="error-container"></div>
                        <table class="table table-striped table-bordered table-hover"  >
                            <thead class="thead-dark" > 
                                <tr align="center">
                                <th width="5%">Item No.</th>
                                <th width="20%">Description</th>
                                <th width="10%">Quantity</th>
                                <th width="10%">Unit</th>
                                <th width="10%">Qty. Received</th>
                                <th width="20%">Remarks</th>
                                <th width="20%">Inventory Type</th>
                                </tr>
                            </thead>
                            <tbody id="receiving_details">
                                <tr align="center" id="nodatatr">
                                    <td colspan="7">No data available in table</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span> SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

<style>
    .table{
        margin-top: 20px;
    }
</style>

@push('scripts')

<script>
    $(document).on('keyup','.validation',function(e){
        let val = $(this).val();
        if(val == null || val == "" ){// remove input class for required
            if($(this).hasClass('remarks')){ 
                $(this).rules('add',{
                    'required' : false
                })
                $(this).parent().siblings('td').children('.qty').rules('add',{
                    'required' : false
                })
                
            }else{
                $(this).rules('add',{
                    'required' : false
                })
                $(this).parent().siblings('td').children('.remarks').rules('add',{
                    'required' : false
                })
            }
            $(this).parent().next().removeClass('validate-has-error')
        }else{//add required input 0
            if($(this).hasClass('remarks')){ 
                $(this).rules('add',{
                    'required' : true
                })
                $(this).parent().siblings('td').children('.qty').rules('add',{
                    'required' : true
                })
            }else{
                $(this).rules('add',{
                    'required' : true
                })
                $(this).parent().siblings('td').children('.remarks').rules('add',{
                    'required' : true
                })
            }
        }
    
    });

    $('#po_list_receiving').change(function(){
        $('#nodatatr').hide();
        var url = $('option:selected', this).attr('data-url');
        if(url == null){
            $('#receiving_details').empty();
            $('#receiving_details').append('<tr align="center"><td colspan="7">No data available in table</td></tr>');
            // $('#supplier_name').val('');
        }
        else{
            $.ajax({
                type: "GET",
                url: url,
                success: function(html){
                    $('#receiving_details').empty().html(html);
                    // $('#supplier_name').val('Supplier Name');
                    // $('#supplier_name').val("{{($Supplier ? $Supplier->getSuppliers->supplier_name : '')}}");
                }
            });
        }
    });

    $('#po_list_receiving').change(function(){
        $.ajax({
            url: "{{route('get.supplier.name')}}",
            data: {
                canvass_id : $(this).val()
            },
            success: function(data){
                $('#supplier_name').val(data);
            },
        });
    });
    let = rules = {
        purchase_no: {
            required: true
        },
        date: {
            required: true
        },
        // receipt_no: {
        //     required: true
        // },
        quantity_received: {
            required: true
        },
        remarks: {
            required: true
        }
    };
    $('#receiving_form').registerFields(rules);

    // function validquantity(selector, id, quantity){
    //     var accepted = '#qty_input' + id;    
    //     var remaining = 0;
    //     var total_accrej = (parseInt($(accepted).val()) || 0);
    //     if(total_accrej > quantity){
    //     var check_id = $(selector).attr('id');
    //         $(selector).val(quantity);
    //     }
    // }
    function decimalToFraction(value, donly = true) {
            var tolerance = 1.0E-6; // from how many decimals the number is rounded
            var h1 = 1;
            var h2 = 0;
            var k1 = 0;
            var k2 = 1;
            var negative = false;
            var i;

            if (parseInt(value) == value) { // if value is an integer, stop the script
                return value;
            } else if (value < 0) {
                negative = true;
                value = -value;
            }

            if (donly) {
                i = parseInt(value);
                value -= i;
            }

            var b = value;

            do {
                var a = Math.floor(b);
                
                var aux = h1;
                h1 = a * h1 + h2;
                h2 = aux;
                aux = k1;
                k1 = a * k1 + k2;
                k2 = aux;
                b = 1 / (b - a);
            } while (Math.abs(value - h1 / k1) > value * tolerance);

            return (negative ? "-" : '') + ((donly & (i != 0)) ? i + ' ' : '') + (h1 == 0 ? '' : h1 + "/" + k1);
    }
    function fractionTodecimal(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
        }
        else {
            fraction_eval = parseInt(fraction);
        }
        return fraction_eval;
    }
    function validquantity(selector, id, quantity,evt){
        var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 47 && charCode > 32
            && (charCode < 48 || charCode > 57))
            return false;
                var accepted = '#qty_input' + id;
                var remaining = 0;
                var total_accrej = $(selector).val();
                $('.qtyRecei'+id).val(fractionTodecimal(total_accrej));
                if(fractionTodecimal(total_accrej) > quantity){
                    console.log(quantity);
                    console.log(fractionTodecimal(total_accrej));
                    var check_id = $(selector).attr('id');
                    $(selector).val(decimalToFraction(quantity));
                    $('.qtyRecei'+id).val(decimalToFraction(quantity));
                }
        return true;
        
    }
</script>
@endpush
