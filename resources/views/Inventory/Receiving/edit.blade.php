@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Inventory Receiving</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Inventory Receiving Details</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('receiving.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  id="edit_receiving_form" action="{{ route('receiving.update',$inventory_receiving->id)}}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Purchase Order No.</label>
                            <input type="text" class="form-control" value="{{ App\Common::getPRNumberFormat('purchase_orders', $inventory_receiving).App\Common::getAlphabet($inventory_receiving, true, $ids) }}" name="purchase_no"  id="po_list_receiving" readonly>
                            <input type="hidden" value="{{$inventory_receiving->po_id}}" name="purchase_order_id">
                        </div>
                        <div class="form-group col-md-6">
                            <label >Date Received</label>
                            <input type="text" name="date" id="date" class="form-control datetimepicker" value="{{ App\Common::convertWordDateFormat($inventory_receiving->date_received) }}"> 
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Supplier Name</label>
                            <input type="text" class="form-control" id="supplier_name" name="supplier_name" readonly value="{{$inventory_receiving->purchaseOrder->getCanvass->getSuppliers->supplier_name}}"> 
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr align="center">
                            <th width="10%">Item No.</th>
                            <th width="20%">Description</th>
                            <th width="10%">Quantity</th>
                            <th width="15%">Unit</th>
                            <th width="12%">Qty. Received</th>
                            <th width="17%">Remarks</th>
                            <th width="16%">Inventory Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach($inventory_receiving->getReceivingItems as $data => $val)
                                <tr align="center">
                                    <input type='hidden' name='for_insert_receiving[]' value="{{ $val->id }}">
                                    <td style="vertical-align: middle">{{ ++$data }}</td>
                                    <td style="vertical-align: middle">{{ $val->getCanvassItem->getPRItems->getInventoryName->description }}</td>
                                    <td style="vertical-align: middle">{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->getCanvassItem->available_quantity) }}</td>
                                    <td style="vertical-align: middle">{{ $val->getCanvassItem->getPRItems->getUnits->unit_name }}</td>
                                    <td style="vertical-align: middle"><input type='hidden' name='qtyRecei{{$val->id}}' id='qtyRecei{{$val->id}}' class='qtyRecei{{$val->id}}' value="{{ $val->quantity_received }}"><input name='quantity_received{{ $val->id }}' type="text" class="form-control newInput text-center" onkeyup=' return validquantity(this,"{{$val->id}}","{{ $val->getCanvassItem->available_quantity }}",event)'  {{ $val->quantity_received == $val->getCanvassItem->available_quantity?'readonly':''}} value="{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity_received) }}"></td>
                                    <td style="vertical-align: middle"><input type="text" name='remarks{{ $val->id }}' class="form-control newInput" value="{{ $val->remarks }}"></td>
                                    <td style="vertical-align: middle">
                                        <select class="form-control newInput" name="inventory_type{{ $val->id }}" id="inventory_type{{ $val->id }}">
                                            <option value="">Select Inventory Type</option>
                                            <option value="capex" {{ ($val->inventory_type == 'capex'?'selected':'') }}>Capital Expenditures</option>
                                            <option value="opex" {{ ($val->inventory_type == 'opex'?'selected':'') }}>Operating Expense</option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection


<style>
    .table{
        margin-top: 20px;
    }
    .margin-top{
        margin-top:50px;
    }
</style>
@push('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>
        function qtyValidation(selector,id,quantity,val) {
            var accepted = '#qty_input' + id;
            var remaining = 0;
            var total_accrej = (parseInt($(selector).val()) || 0);
            console.log(quantity);
            if(total_accrej > quantity){
                var check_id = $(selector).attr('id');
                $(selector).val(val);
            }
        }
        let = rules = {
            purchase_no: {
                required: true
            },
            date: {
                required: true
            },
            // receipt_no: {
            //     required: true
            // },
        };
        $('#edit_receiving_form').registerFields(rules, false);
        $('.newInput').each( function () {
            $(this).rules('add',{
                'required' : true
            })
        });
        $('#po_list_receiving').change(function(){
            var url = $('option:selected', this).attr('data-url');
            if(url == null){
                $('#receiving_details').empty();
                $('#supplier_name').val("");
            }
            else{
                $.ajax({
                type: "GET",
                url: url,
                success: function(html){
                    $('#receiving_details').empty().html(html);
                    $('#supplier_name').val("{{$Supplier->getSuppliers->supplier_name}}");
                }
            });
            }
            
        });
    function decimalToFraction(value, donly = true) {
        var tolerance = 1.0E-6; // from how many decimals the number is rounded
        var h1 = 1;
        var h2 = 0;
        var k1 = 0;
        var k2 = 1;
        var negative = false;
        var i;

        if (parseInt(value) == value) { // if value is an integer, stop the script
            return value;
        } else if (value < 0) {
            negative = true;
            value = -value;
        }

        if (donly) {
            i = parseInt(value);
            value -= i;
        }

        var b = value;

        do {
            var a = Math.floor(b);
            
            var aux = h1;
            h1 = a * h1 + h2;
            h2 = aux;
            aux = k1;
            k1 = a * k1 + k2;
            k2 = aux;
            b = 1 / (b - a);
        } while (Math.abs(value - h1 / k1) > value * tolerance);

        return (negative ? "-" : '') + ((donly & (i != 0)) ? i + ' ' : '') + (h1 == 0 ? '' : h1 + "/" + k1);
    }
    function fractionTodecimal(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
        }
        else {
            fraction_eval = parseInt(fraction);
        }
        return fraction_eval;
    }
    function validquantity(selector, id, quantity,evt){
        var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 47 && charCode > 32
            && (charCode < 48 || charCode > 57))
            return false;
                var accepted = '#qty_input' + id;
                var remaining = 0;
                var total_accrej = $(selector).val();
                $('.qtyRecei'+id).val(fractionTodecimal(total_accrej));
                if(fractionTodecimal(total_accrej) > quantity){
                    var check_id = $(selector).attr('id');
                    $(selector).val(decimalToFraction(quantity));
                    $('.qtyRecei'+id).val(decimalToFraction(quantity));
                }
        return true;
        
    }
    </script>
@endpush