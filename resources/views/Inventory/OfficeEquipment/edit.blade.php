@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">CAPEX</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Capital Expenditures</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('office-equipment.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  id="equipment_edit_form" class="form-horizontal" action="{{ route('office-equipment.update', $asset->id) }}" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}
                    <div class="errors"></div>
                    <div class="alert alert-info" role="alert">
                    <b>Note:</b> Other information to update like <b>Description, Category, Unit Cost and Image</b> must be affected the other related Asset Details.
                    </div>
                    <div class="center">
                        <div class="default">
                            <img onclick="onImage()" src="{{ $asset->getInventories->image == null ? asset('images/default.png') : route('inventory.image',$asset->inventory_id)}}" height="100" width="100" class="rounded-circle" id="im" name="im">
                            <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                            <input name="old_image" id="old_image" value="{{(!$asset?'':($asset->getInventories->image=null?'':$asset->getInventories->image))}}" hidden>
                        </div>
                        <span class="icon-camera"> <i class="fa fa-camera"></i></span>
                    </div>
                    <h5 >Asset Details</h5><hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Asset No</label>
                            <input type="text" class="form-control" id="name" readonly name="asset_no" value="{{$asset_tag}}" autocomplete="off">
                            <input type="text" class="form-control"  name="inventory_id" value="{{$asset->getInventories->id}}" hidden>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="position">Description</label>
                            <input type="text" class="form-control" id="position" name="description" value="{{$asset->getInventories->getInventoryName->description}}" autocomplete="off">
                            <input type="hidden" class="form-control" id="inventory_name_id" name="inventory_name_id" value="{{$asset->getInventories->getInventoryName->id}}" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Status</label>
                            <select class="form-control selectpicker" name="inventory_status">
                            @foreach($inventory_status as $status)
                                <option value="{{ $status->id }}"  data-subtext="{{ $status->notes }}" 
                                    {{ ($status->id == $asset->inventory_status_id ? "selected" : "") }}>
                                    {{ $status->status_name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Category</label>
                            <select class="form-control selectpicker" name="categ_name">
                            @foreach($category as $value)
                                <option value="{{ $value->id }}" data-subtext="{{($value->category_id != NULL? $value->getCategory->category_name : '' )}}" 
                                    {{ ($value->id == $category_id->category_id ? "selected" : "") }} >
                                    {{ $value->category_name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Serial Number</label>
                            <input type="text" class="form-control" id="serial" name="serial" value="{{ $asset->serial_number }}" autocomplete="off" placeholder="Serial Number">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date Acquired</label>
                            <input type="text" class="form-control datetimepicker" id="date_acquired" name="date_acquired" value="{{ App\Common::convertWordDateFormat($asset->date_acquired) }}" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Unit Cost</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">₱</span>
                                </div>
                                <input type="text" readonly class="form-control total_amount" id="unit_cost" name="unit_cost" value="{{App\Http\Controllers\OfficeEquipmentController::canvcost($asset) }}" autocomplete="off">
                            </div>
                            <!-- <input type="text" readonly class="form-control total_amount" id="unit_cost" name="unit_cost" value="{{App\Http\Controllers\OfficeEquipmentController::canvcost($asset) }}" autocomplete="off"> -->
                        </div>
                        <div class="form-group col-md-6">
                            <label>Unit</label>
                            <select class="form-control selectpicker" data-live-search="true" name="unit">
                            @foreach($units as $unit)
                                <option value="{{ $unit->id }}" {{ ($unit->id == $category_id->unit_id ? "selected" : "") }}>{{ $unit->unit_name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Location">Location</label>
                            <input type="text" class="form-control" id="location" name="location" value="{{$asset->location }}" autocomplete="off" placeholder="Location">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Custodian">Custodian</label>
                            <input type="text" class="form-control" id="custodian" name="custodian" value="{{$asset->custodian }}" autocomplete="off" placeholder="Custodian">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Other Details">Other Details</label>
                            <textarea class="form-control" name="other_details" style="height:150px;">{{$asset->other_details }}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Other Details">Remark</label>
                            <textarea class="form-control" name="remark" style="height:150px;">{{$asset->remark }}</textarea>
                        </div>
                    </div>                    
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script> 
        $('.selectpicker').selectpicker();
        $(".total_amount").number(true,2);
        let rules = {
            asset_no : {
                required: true
            },
            description : {
                required: true
            },
            serial : {
                required: true
            },
            date_acquired : {
                required: true
            },
            unit_cost : {
                required: true
            },
        };
        $('#equipment_edit_form').registerFields(rules, false);
        function onImage() {
            $('#image').click();
        }

        function onFileSelected() {
            var selectedFile = event.target.files[0];
            var files = event.target.files;
            var reader = new FileReader();
            if(selectedFile && selectedFile.size < 2097152)
            {
                var imgtag = document.getElementById("im");
                imgtag.title = selectedFile.name;
                reader.onload = function(event) {
                    imgtag.src = event.target.result;
                };
                reader.readAsDataURL(selectedFile);
            }
        }

        
    </script>
@endpush
@endsection

<style>
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .default{
        position: relative;
    }
    .icon-camera{
        height: 20px;
        width: 20px;
        position: absolute;
        margin-top: -16px;
        margin-left: 10px;
        font-size: 10px;
        border-radius: 50%;
        color: #fff;
        background-color: #3490dc;
    }
    .fa-camera{
        margin-top: 6px;
    }
</style>