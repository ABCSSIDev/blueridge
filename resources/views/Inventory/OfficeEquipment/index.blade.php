@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">CAPEX</li>
        </ol>
    </nav>
    <h4 class="form-head" style="margin: 0px 0px 50px 0px">Capital Expenditures (CAPEX) List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div align="right" style="margin: 0px 0px 20px 0px;">
        <button class="btn btn-primary print-form-css" data-form="print_all_asset"><i class="fa fa-print" aria-hidden="true"></i> PRINT ALL ASSET TAG</button>
    </div>
    <div class="row justify-content-center">
        <!-- <div class="col-md-12">
            <div align="right" style="margin: 50px 0px 10px 0px"><a href="#"><button class="btn btn-primary">Import CSV File</button></a> &nbsp <a href="#"><button class="btn btn-primary">Export CSV File</button></a></div>
        </div> -->
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover table-mediascreen" id="equipments_list">
                <thead class="thead-dark">
                    <tr align="center">
                    <th>Image</th>
                    <th>Asset No.</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Category</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody align="center">
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="print_all_asset" hidden>
    @foreach($Equipmentlist->chunk(3) as $data)
    <div class="col-12" style="display: flex">
        @foreach($data as $Equipmentlist)
        <div class="col-4">
            <div style="border: 2px solid black; height: 140px">
                <img src="{{ asset('images/blueridge-asset.png') }}" width="100%" height="30px">
                <hr style="margin-top: 5px; border-bottom: 1px solid black;margin-bottom: 0px"> 
                <span>&nbsp;Product #:{{ App\Common::AssetTagCode($Equipmentlist,$Equipmentlist->inventory_id) }}<span>
                <hr style="margin-top: 0px; border-bottom: 1px solid black;margin-bottom: 0px"> 
                <span>&nbsp;Category:{{ $Equipmentlist->getInventories->getInventoryName->description }}<span>
                <hr style="margin-top: 0px; border-bottom: 1px solid black;margin-bottom: 0px"> 
                <span>&nbsp;Description:{{ $Equipmentlist->getInventories->getInventoryName->description}}<span>
                <hr style="margin-top: 0px; border-bottom: 1px solid black;margin-bottom: 0px"> 
                <span>&nbsp;Property Cost:{{ $Equipmentlist->getInventories->unit_price}}<span>
                <hr style="margin-top: 0px; border-bottom: 1px solid black;margin-bottom: 0px"> 
                <span>&nbsp;Date Acquired: {{ App\Common::convertWordDateFormat($Equipmentlist->date_acquired) }}<span>
                <hr style="margin-top: 0px; border-bottom: 1px solid black;margin-bottom: 0px"> 
            </div>
        </div>
        @endforeach
    </div>
    <div style="margin-bottom: 15px;"></div>  
    @endforeach
    
      
</div>   
            
@push('scripts')
    <script>
    // $(document).ready(function(){
        var oTable = $('#equipments_list').DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('equipment.getEquipments',0000) !!}",
                columns:[
                    {data: "image","name":"image",orderable:false,searchable:false,"width":"10%", className:"v-align text-center"},
                    {data: "asset_no","name":"asset_no","width":"25%", className:"v-align text-center"},
                    {data: "description","name":"description","width":"25%", className:"v-align text-center"},
                    {data: "status","name":"status","width":"25%", className:"v-align text-center"},
                    {data: "category","name":"category","width":"25%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"15%", className:"v-align text-center"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("equipment.getEquipments", ': year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    // });
         
    </script>
@endpush
@endsection
<!-- <style>
    /* .mb-64 {
        margin-bottom: 64px;
    } */
    .mb-65 {
        margin-bottom: 65px;
    }
    .mb-67 {
        margin-bottom: 67px;
    }
    .mb-70 {
        margin-bottom: 70px;
    }
    .pt-2px {
        padding-top: 2px;
    }
    .pt-3px {
        padding-top: 3px;   
    }
    .pt-20 {
        padding-top: 20px;
    }
    .pt-25 {
        padding-top: 25px;
    }
    .pl-20 {
        padding-left: 20px;
    }
    .pl-10 {
        padding-left: 10px;
    }
    .pl-30 {
        padding-left: 30px;
    }
    .pl-59 {
        padding-left: 59px;
    }
    .pl-55 {
        padding-left: 55px;
    }
</style> -->