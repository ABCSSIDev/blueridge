@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">CAPEX</li>
        </ol>
    </nav>
    <h4 class="form-head">View Capital Expenditures</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{ route('office-equipment.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <div class="tab-content">
                    <div class="center">
                        <img src="{{ $asset->getInventories->image == null ? asset('images/default.png') : route('inventory.image',$asset->getInventories->id)}}" class="rounded-circle" height="100" width="100"> 
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Asset No:</label>
                            <p>{{ $asset_tag }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Description:</label>
                            <p>{{ $asset->getInventories->getInventoryName->description }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Status:</label>
                            <p>{{ $asset->getInvetoryStatus->status_name }} 
                            <span class="badge badge-info">{{$asset->getInvetoryStatus->notes }}</span></p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Category:</label>
                            <p>{{ $asset->getInventories->getCategory->category_name }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Serial Number:</label>
                            <p>{{ empty($asset->serial_number) ? "N/A" : $asset->serial_number}} </p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date Acquired:</label>
                            <p>{{ App\Common::convertWordDateFormat($asset->date_acquired) }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Unit Cost:</label>
                            <p>₱{{ number_format($asset->getInventories->unit_price,2) }} </p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Unit:</label>
                            <p>{{ $asset->getInventories->getUnits->unit_name}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Location:</label>
                            <p>{{ empty($asset->location) ? "N/A" : $asset->location }} </p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Custodian:</label>
                            <p>{{ empty($asset->custodian) ? "N/A" : $asset->custodian }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6" style="margin-bottom: 6rem;">
                            <label>Other Details:</label>
                            <p>{{ empty($asset->other_details) ? "N/A" : $asset->other_details }} </p>
                        </div>
                        <div class="form-group col-md-6" style="margin-bottom: 6rem;">
                            <label>Other Details:</label>
                            <p>{{ empty($asset->remark) ? "N/A" : $asset->remark }} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
</style>
