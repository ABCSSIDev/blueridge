<form method="POST" class="form-horizontal" id="delete_equipment_form" enctype="multipart/form-data" action="{{route('equipment.functionRemoveEquipment',$equipment_list->id)}}">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    <h6> Are you sure you want to delete {{$asset_tag }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>

@push('scripts')
<script>
let rules = {
  };
$('#delete_equipment_form').registerFields(rules);
</script>
@endpush