@extends('layouts.app')

@section('content')


<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Assign Assets & Supplies</li>
        </ol>
    </nav>
    <h4 class="form-head">View Assign Assets & Supplies</h4>
    <div class="row justify-content-center">
        <div class="col-md-12 margin-bottom">
            <div align="right" style="margin-bottom:20px"><a href="{{ route('assign-equipment.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" action="{{route('assign-equipment.store')}}" id="assignequ_form" enctype="multipart/form-data">
                    <div class="errors"></div>
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>Assign To.</b></label><br>
                            <p>&nbsp;{{$GeneralUser->name}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Contact No.</b></label><br>
                            <p>&nbsp;{{($GeneralUser->contact_number != null?$GeneralUser->contact_number:'N/A')}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>Address.</b></label><br>
                            <p>&nbsp;{{$GeneralUser->address}}</p>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr align="center">
                                <th scope="col">Description</th>
                                <th scope="col">Quantity</th>
                            </tr>
                        </thead>
                        <tbody id="table_append">
                            @foreach($AssignedSupplyItemsData as $val)
                            <tr align="center" style="border-style:hidden;">
                                <td>{{ ($val->AssestTagDetails?$val->InventoryDetails->getInventoryName->description."-".$val->AssestTagDetails->asset_tag."-".$val->AssestTagDetails->serial_number:$val->InventoryDetails->getInventoryName->description)}}</td>
                                <td>{{ $val->quantity }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
<style>
    .margin-top{
        margin-top:50px;
    }

</style>
