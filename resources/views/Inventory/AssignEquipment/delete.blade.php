<form method="POST" class="form-horizontal" enctype="multipart/form-data" action="{{route('functionRemoveAssign',$data->id)}}">
    <input type="hidden" name="_method" value="PUT">
    <div class="errors"></div>
    {{csrf_field()}}
    <h6> Are you sure you want to delete "{{ $data->name }}"?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>