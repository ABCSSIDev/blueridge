@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Assign Assets & Supplies</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Assign Assets & Supplies</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('assign-equipment.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" action="{{route('assign-equipment.store')}}" id="assignequ_form" enctype="multipart/form-data">
                    <div class="errors"></div>
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Assign To.</label>
                            <input type="text" class="form-control" id="assignto" name="assignto" placeholder="Assign To" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact No.</label>
                            <input type="number" maxlength="11" class="form-control" id="contactno" name="contactno" placeholder="Contact No" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Address</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Address" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Supplies and Equipments</label>
                            <input type="text" class="form-control" id="name" name="name" hidden>
                            <select class="form-control" id="supp_equ" name="supp_equ" onchange="SupplAndEquip(this.value)">
                                <option value="" >Select Supplies or Equipments</option>
                               
                            </select>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr align="center">
                                <th scope="col">Description</th>
                                <th scope="col">Available Quantity</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="table_append">
                            <tr align="center" style="border-style:hidden;" id="nodatatr">
                                <td colspan="4">No Data Available</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span> SUBMIT</button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
<style>
    .margin-top{
        margin-top:50px;
    }

</style>

@push('scripts')
    <script>
    let rules = {
        assignto : {
            required: true
        },
        contactno: {
            required: true
        },
        address : {
            required: true
        },
        supp_equ: {
            required: true
        },
    };
    $('#assignequ_form').registerFields(rules);  
    function validquantity(selector, id, quantity){
        var accepted = '#qty_input' + id;
        var remaining = 0;
        var total_accrej = (parseInt($(selector).val()) || 0);
        if(total_accrej > quantity){
            var check_id = $(selector).attr('id');
            $(selector).val(quantity);
        }
    }
    var inventory_ids = [];
    var auto_name = "";
    var auto_obj = "";
    var auto_data = "";
    $('#po_list_receiving').change(function(){
        $.ajax({
            url: "{{route('get.supplier.name')}}",
            data: {
                canvass_id : $(this).val()
            },
            success: function(data){
                $('#supplier_name').val(data);
            },
        });
    });
    $(document).ready(function() {
        $( "#assignto" ).autocomplete({
            source: function(request, response) {
                    $.ajax({
                    url: "{{route('assign.autocomplete')}}",
                    data: {
                            term : request.term
                    },
                    dataType: "json",
                    success: function(data){

                    var resp = $.map(data,function(obj){
                        return obj.name;
                    }); 
                    response(resp);
                    auto_data = data;
                    
                    }
                });
            },
            select: function(event,ui){
                $.map(auto_data,function(obj){
                    console.log(ui.item.label);
                    if(ui.item.label == obj.name){
                    
                        $('#contactno').val(obj.contact_number);
                        $('#address').val(obj.address);
                        $("#name").val(obj.id); 
                        $.ajax({
                            url: "{{route('assign.suppandequi')}}",
                            data: {
                                id : obj.id
                            },
                            success: function(data){
                                $("#supp_equ").html(data); 
                            },
                        });
                    }
                }); 
              
                
            }
        });
    });
    function reloadData(data){
        //this function uses for reloading data needed
        $("#supp_equ").empty();
        $("#supp_equ").append('<option value="" >Select Supplies or Equipments</option>');
    }
    function SupplAndEquip(id) {
        if(!inventory_ids.includes(id)){
            inventory_ids.push(id);
            $.ajax({
                url: "{{route('assign.suppandequitable')}}",
                data: {
                        id : id,
                        name :$('#name').val()

                },
                method: "GET",
                success: function(data){
                    // alert($('#for_savings').val());
                    
                    $('#nodatatr').hide(); 
                    $('#table_append').append(); 
                    $("#table_append").append(data);
                }
            });
        }

    }
  
    </script>
@endpush