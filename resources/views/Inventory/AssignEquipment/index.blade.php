@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Inventory</li>
            <li class="breadcrumb-item active" aria-current="page">Assign Assets & Supplies</li>
        </ol>
    </nav>
    <h4 class="form-head">Assign Assets & Supplies List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('assign-equipment.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
                <table class="table table-striped table-bordered table-hover table-mediascreen" id="assign_list">
                    <thead class="thead-dark">
                        <tr align="center">
                        <th scope="col">Assigned To</th>
                        <th scope="col">Assigned Materials</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                    </tbody>
                </table>
        <div>
    <div>
<div>



@push('scripts')
    <script>
    // $(document).ready(function(){
        var oTable = $('#assign_list').DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('assign.getAssign',0000) !!}",
                columns:[
                    {data: "assigned_to","name":"assigned_to","width":"30%"},
                    {data: "assigned_materials","name":"assigned_materials","width":"30%"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"30%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
    // });
    $('#budget_year').change(function(){
        var year = $(this).val();
        var url = '{{ route("assign.getAssign", ':year') }}';
        url = url.replace(':year', $(this).val());
        oTable.ajax.url(url).load();
    });
    </script>
@endpush
@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
