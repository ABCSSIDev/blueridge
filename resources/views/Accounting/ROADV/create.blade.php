@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item" >Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Liquidation/DV</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Liquidation/DV</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('roa-dv.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
        <div class="tab-content">
            <form method="POST"  class="form-horizontal" id="roa_form" action="{{route('disbursement-voucher.store')}}" enctype="multipart/form-data">
                <div class="errors"></div>
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-12"> 
                        <div class="tab-content">
                            <form method="POST"  class="form-horizontal" id="roa_form" action="{{route('disbursement-voucher.store')}}" enctype="multipart/form-data">
                                <div class="errors"></div>
                                {{ csrf_field() }}
                                <legend>Liquidation Details</legend>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Obligation No.</label>
                                        <input type="text" class="form-control" placeholder="Obligation No." name="obligation_number" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description of Obligation</label>
                                        <select class="form-control" name="ledger_id">
                                            <option value="">Select Description of Obligation</option>
                                            @foreach($account_ledger as $ledger)  
                                            <option value="{{$ledger->id}}">{{$ledger->ledger_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Date of Resolution</label>
                                        <input type="" name="resolution_date" placeholder="Date of Resolution" class="form-control datetimepicker" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Date of Approval</label>
                                        <input type="" name="approval_date" placeholder="Date of Approval" class="form-control datetimepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <label>Resolution Number</label>
                                        <input type="" name="resolution_number" placeholder="Resolution Number" class="form-control" autocomplete="off" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Total Amount</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">₱</span>
                                            </div>
                                            <input type="text" placeholder="Total Amount"  name="total_amount" id="total_amount" class="form-control total_amount">
                                        </div>
                                        <!-- <input type="text" placeholder="Total Amount"  name="total_amount" id="total_amount" class="form-control total_amount" autocomplete="off"> -->
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Bids & Awards Resolution Number</label>
                                        <input type="text" name="bids_resolution_number" id="bids_resolution_number" placeholder="Bids & Awards Resolution Number" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Bids & Awards Mode</label>
                                        <select class="form-control" name="bids_mode" id="bids_mode">
                                            <option value="">Select Mode</option>
                                            <option value="Bidding">Bidding</option>
                                            <option value="Shopping">Shopping</option>
                                            <option value="Negotiated">Negotiated</option>
                                            <option value="Over the Counter">Over the Counter</option>
                                            <option value="Limited Source">Limited Source</option>
                                            <option value="Repeat Order">Repeat Order</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Kagawad Signatory</label>
                                        <select class="form-control"  id="signatory" name="signatory">
                                            <option value="Kgd. Thomas J.T.F. de Castro">Kgd. Thomas J.T.F. de Castro</option>
                                            <option value="Kgd. Ayjell Acejas- de Castro">Kgd. Ayjell Acejas- de Castro</option>
                                        </select>
                                    </div>
                                </div>
                                <table style="width:100%">
                                    <tr>
                                        <td>Total Amount Spent</td>
                                        <td></td> 
                                        <td></td>
                                        <td><label id="total_amount_spent">₱ 0.00</label></td>
                                    </tr>
                                    <tr>
                                        <td>Amount of Cash Advance Per DV NO.</td>
                                        <td><input type="text" name="cash_advance_voucher_no" placeholder="Voucher No." class="form-control col-md-8" autocomplete="off"></td>
                                        <td><input type="text" name="cash_advance_date" placeholder="Date" class="form-control col-md-8 datetimepicker" autocomplete="off"></td>
                                        <td><input type="text" name="cash_advance_amount" id="cash_advance_amount" placeholder="₱ 0.00" class="form-control total_amount" autocomplete="off"></td>
                                    </tr>
                                    <tr>
                                        <td>Amount Refunded Per O.R NO.</td>
                                        <td><input type="text" name="amount_refund_or_no" placeholder="O.R No." class="form-control col-md-8" autocomplete="off"></td>
                                        <td><input type="text" name="amount_refund_date" placeholder="Date" class="form-control col-md-8 datetimepicker" autocomplete="off"></td>
                                        <td><label ><label id="total_refund_or_no">₱ 0.00</label></label></td>
                                    </tr>
                                    <tr>
                                        <td>Amount of Cash Advance Per DV NO.</td>
                                        <td></td> 
                                        <td></td>
                                        <td><label >₱ 0.00</label></td>
                                    </tr>
                                </table>
                                <hr class="hr1">
                                <legend>Disbursement Voucher Details</legend>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Voucher No.</label>
                                        <input type="text" class="form-control" name="voucher_no" placeholder="Voucher No." value="{{$last_id}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Check No.</label><a class="popup_form" data-toggle="modal" data-url="{{route('roa-dv.functionORPad')}}" title="Check No Settings" style="color: #006cc5"><span class="fa fa-cog"></span></a>
                                        <input type="text" class="form-control" name="check_no" id="check_no" placeholder="Check No." autocomplete="off" value="{{ $check_number }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Cheque Date</label>
                                        <input type="text" class="form-control datetimepicker" id="cheque_date" name="cheque_date" placeholder="Cheque Date" autocomplete="off" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Name of Claimant</label>
                                        <input type="text" class="form-control" id="claimant" name="claimant" placeholder="Name of Claimant" autocomplete="off">
                                        <input type="hidden" class="form-control" id="claimant" name="role_id" value="{{$role->id}}"> 
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Address of Claimant</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Address of Claimant" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>TIN</label>
                                        <input type="text" class="form-control" id="tin" name="tin" placeholder="TIN" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label>Due Date</label>
                                        <input type="text" class="form-control datetimepicker" id="due_date" name="due_date" placeholder="Due Date" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Mode</label>
                                        <select class="form-control" name="mode_type" id="mode_type">
                                            <option value="">Select Mode</option>
                                            <option value="To Pay">To Pay</option>
                                            <option value="To Withdraw">To Withdraw</option>
                                            <option value="To Reimburse">To Reimburse</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Invoice No.</label>
                                        <input type="text" class="form-control" id="invoice_no" name="invoice_no" placeholder="Invoice No." autocomplete="off" data-role="tagsinput">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>RER</label>
                                        <input type="text" class="form-control" id="rer" name="rer" placeholder="RER" autocomplete="off" data-role="tagsinput">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description" style="height:150px;"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" align="right" >
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</div>

@push('scripts')

<script>
     $('#check_no').change(function(){
        $.ajax({
            url: "{{route('roa-dv.autocompleteOr')}}",
            data: {
                OR : $(this).val()
            },
            dataType: "json",
            success: function(data){
                if(data == "duplicat"){
                    toastMessage('Warning!',"This Check Number already Existed!","warning");
                }
                if(data == "maxorpad"){
                    toastMessage('Warning!',"Sorry, you have already used all the Check no. Please create new ones!","warning");
                }
                if(data == "outofrange"){
                    toastMessage('Warning!',"Sorry, your Check is out of Check Number Range!","warning");
                }
                if(data == "zero"){
                    toastMessage('Warning!',"Sorry, but you cant center 0 as a Check Number!","warning");
                }
                
            }
        });
    })
    $(".total_amount").number(true,2);
    var auto_name = "";
    var auto_obj = "";
    var auto_data = "";
    $( "#total_amount" ).keyup(function(e){
        // $(".label-info").remove('');
        
        var num =Number($(this).val());
        var num2 = 0;
         if($('#cash_advance_amount').val() != null){
            var num2 = Number($('#cash_advance_amount').val());
        }
        var total =  num;
        var totalrefunded  =  num - num2;
        document.getElementById("total_amount_spent").innerHTML = '	₱ '+parseFloat(total, 10).toFixed(2);
        document.getElementById("total_refund_or_no").innerHTML = '	₱ '+parseFloat(totalrefunded, 10).toFixed(2);
    });
    $( "#cash_advance_amount" ).keyup(function(e){
        var num =Number($(this).val());
        var num2 = 0;
         if($('#total_amount').val() != null){
            var num2 = Number($('#total_amount').val());
        }
        var total =  num2;
        var totalrefunded  =  num2 - num;
        document.getElementById("total_amount_spent").innerHTML = '	₱ '+parseFloat(total, 10).toFixed(2);
        document.getElementById("total_refund_or_no").innerHTML = '	₱ '+parseFloat(totalrefunded, 10).toFixed(2);
    });
    $(document).ready(function() {
        $( "#claimant" ).autocomplete({
            source: function(request, response) {
                    $.ajax({
                    url: "{{route('dv.autocompleteClaimant')}}",
                    data: {
                            term : request.term
                    },
                    dataType: "json",
                    success: function(data){

                    var resp = $.map(data,function(obj){
                        return obj.name;
                    }); 
                    response(resp);
                    auto_data = data;
                    
                    }
                });
            },
            select: function(event,ui){
                $.map(auto_data,function(obj){
                    console.log(ui.item.label);
                    if(ui.item.label == obj.name){
                        $('#address').val(obj.address);
                    }
                }); 
            }
        });
    });

    let rules = {
        ledger_id : {
            required: true
        },
        resolution_date : {
            required: true
        },
        approval_date: {
            required: true
        },
        total_amount : {
            required: true
        },
        voucher_no : {
            required: true
        },
        check_no: {
            required: true
        },
        claimant : {
            required: true
        },
        address : {
            required: true
        },
        resolution_number : {
            required: true
        },
        cheque_date : {
            required: true
        },
        due_date: {
            required: true
        },
        mode_type: {
            required: true
        }
    };
$('#roa_form').registerFields(rules,true);
</script>
@endpush
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .default{
        position: relative;
    }
    .icon-camera{
        height: 20px;
        width: 20px;
        position: absolute;
        margin-top: -16px;
        margin-left: 10px;
        font-size: 10px;
        border-radius: 50%;
        color: #fff;
        background-color: #3490dc;
    }
    .fa-camera{
        margin-top: 6px;
    }
</style>