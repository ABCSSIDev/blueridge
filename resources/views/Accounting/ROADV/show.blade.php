@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item" >Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Liquidation/DV</li>
        </ol>
    </nav>
    <h4 class="form-head">View Liquidation/DV</h4>
    <div class="row justify-content-center">
        <div class="col-md-12 margin-bottom">
            <div align="right" style="margin-bottom:20px"><a href="{{ route('roa-dv.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <legend>Liquidation Details</legend>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Obligation No.:</label>
                        <p>{{ $roas->getROA->obligation_number }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Description of Obligation:</label>
                        <p>{{ $roas->getROA->getLedger->ledger_name }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Date of Resolution:</label>
                        <p>{{ App\Common::convertWordDateFormat($roas->getROA->resolution_date) }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Date of Approval:</label>
                        <p>{{ App\Common::convertWordDateFormat($roas->getROA->approval_date) }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Resolution number:</label>
                        <p>{{ $roas->getROA->resolution_number }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Total Amount:</label>
                        <p>₱ {{ number_format($roas->getROA->total_amount,2) }}</p>
                    </div>
                </div>
                <table style="width:100%">
                    <tr>
                        <td><label>Total Amount Spent</label></td>
                        <td></td> 
                        <td></td>
                        <td>₱ {{number_format($roas->getROA->total_amount,2)}}</td>
                    </tr>
                    <tr>
                        <td><label>Amount of Cash Advance Per DV NO.</label></td>
                        <td>{{$roas->cash_advance_voucher_no}}</td>
                        <td>{{$roas->cash_advance_date}}</td>
                        <td>₱ {{number_format($roas->getROA->cash_advance_amount,2)}}</td>
                    </tr>
                    <tr>
                        <td><label>Amount Refunded Per O.R NO.</label></td>
                        <td>{{$roas->amount_refund_or_no}}</td>
                        <td>{{$roas->amount_refund_date}}</td>
                        <td>₱ {{number_format($roas->getROA->total_amount - $roas->getROA->cash_advance_amount,2)}}</td>
                    </tr>
                    <tr>
                        <td><label>Amount of Cash Advance Per DV NO.</label></td>
                        <td></td> 
                        <td></td>
                        <td>₱ 0.00</td>
                    </tr>
                </table>
                <hr class="hr1">
                <legend>Disbursement Voucher Details</legend>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Voucher No.:</label>
                        <p>{{ $roas->voucher_no }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Check No.:</label>
                        <p>{{ $roas->check_number }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Cheque Date:</label>
                        <p>{{ App\Common::convertWordDateFormat($roas->cheque_date) }}</p>   
                    </div>
                    <div class="form-group col-md-6">
                        <label>Name of Claimant:</label>
                        <p>{{ $roas->getClaimant->name }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Address of Claimant:</label>
                        <p>{{ $roas->claimant_address }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>TIN:</label>
                        <p>{{ $roas->tin ? $roas->tin : "N/A" }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Invoice No.:</label>
                        <input  data-role="tagsinput" value="{{ $roas->invoice_no }}" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label>RER:</label>
                        <input  data-role="tagsinput" value="{{ $roas->rer }}" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Description:</label>
                        <p>{{ $roas->description }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>OR No.:</label>
                        <p>{{ ($roas->getROA->or_id == null?'N/A':$roas->getROA->getInvoice->or_number) }}</p>
                    </div>
                </div>
                <!-- <hr class="hr1"> -->
                @if(!is_null($roas->getPayment))
                    <legend>Payment Details</legend>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Amount Received:</label>
                            <p>PHP {{ number_format($roas->getPayment->amount_received,2) }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Received Date:</label>
                            <p>{{ App\Common::convertWordDateFormat($roas->getPayment->date_received) }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Paid From:</label>
                            <p>{{ $roas->getPayment->getBank->name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Payment Method:</label>
                            <p>{{ $roas->getPayment->getPaymentMethod->name }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Paid Into:</label>
                            <p>{{ $roas->getPayment->paid_into }}</p>
                        </div>
                    </div>
                    @if($roas->getPayment->is_cancelled == 1)
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <b>{{ $roas->getPayment->is_cancelled ? "Cancel Payments" : "Not Cancel" }}</b>
                        </div>
                    </div>
                    @endif
                @endif
            </div>
            <div class="form-group" align="right" >
                @if(is_null($roas->getPayment))
                    <a type="submit" href="{{ route('payment.create',['type' => 'disbursement-voucher','id' => $roas->id]) }}" class="btn btn-primary" data-form="print-invoice"><i class="fa fa-print"></i>&nbsp;PAYMENTS</a>
                @endif
                <button type="submit" class="btn btn-primary print-form-report" data-form="print-roa-dv"><span class="fa fa-print"></span>&nbsp;PRINT</button>
            </div>
        </div>
    </div>
</div>
<div id="print-roa-dv" hidden>
    <!-- @include('layouts.header') -->
    <div class="margin-t">@include('Docs.request-obligation-allotment')</div>
    <div class="break-page"></div>
    <!-- @include('layouts.header') -->
    <div class="margin-t">@include('Docs.disbursement-voucher')</div>
    <div class="break-page"></div>
    <!-- @include('layouts.header') -->
    <div>@include('Docs.resolution-authorizing')</div>
    <div class="break-page"></div>
    <!-- @include('layouts.header') -->
    <div class="">@include('Docs.resolution-reimbursement')</div>
    <div class="break-page"></div>
    <!-- @include('layouts.header') -->
    <div class="margin-t">@include('Docs.emergency-purchase')</div>
    <div class="break-page"></div>
      <style>
        .margin-t{
            margin-top: 40px;
        }
        .break-page{
            page-break-after: always;
        }
      </style>
</div>

@endsection

@push('scripts')
    <script>
        @if(\Illuminate\Support\Facades\Session::has('message'))
            let type  = '{{ \Illuminate\Support\Facades\Session::get('alert-type','info') }}';
            toastMessage('Warning!','{{ Illuminate\Support\Facades\Session::get('message') }}',type);
        @endif
    </script>
@endpush

<style>
    .footer {
        page-break-after: always;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .margin-bottom {
        margin-bottom: 50px
    }
    .margin-top{
            margin-top:50px;
        }
    .container.pr-container{
        max-width: none!important;
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
        border: 1px solid #9c98985c;
        -webkit-box-shadow: 1px 1px 10px #808080de;
        box-shadow: 1px 1px 10px #808080de;
        margin-bottom: 20px;
    }
    .tab_header {
        background-color: #323739 !important;
        color: #fff !important;
        text-align: center !important;
    }
    table.dataTable {
        clear: both;
        margin-top: 6px !important;
        margin-bottom: 6px !important;
        max-width: none !important;
        border-collapse: separate !important;
        border-spacing: 0;
    }
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #dee2e6;
    }
    .table-sm td {
        padding: 1rem!important;
    }
    .table-sm th{
        padding: 0.3rem!important;
    }
    thead tr th {
        font-weight: 500;
    }
    /* tr{
        text-align: center;
    } */
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
    .pt-03{
        padding: 0.3rem;
    }
    .mt-2r{
        margin-top: 2rem;
    }
    .hr1{
        border: 1!important;
    }
    .table-sm th.pt2{
        padding: 0.5rem!important;
    }
    .print-roa{
        -webkit-box-flex: 0;
        flex: 0 0 auto;
        max-width: 93%;
    }
    .width50rm{
        width: 50rem!important;
    }
    .print-dv{
        -webkit-box-flex: 0;
        flex: 0 0 auto;
        max-width: 93%;
    }
    .pt-1rm{
        padding-top: 1rem;
    }
    .pt-1-2rm{
        padding-top: 1.3rem;
    }
    .mt-1-5rm{
        margin-top: 1.5rem;
    }
   
</style>
