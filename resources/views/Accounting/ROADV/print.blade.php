@extends('layouts.app')

@section('content')


<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Liquidation/DV</li>
        </ol>
    </nav>
    <h4 class="form-head">Print Check</h4>
    <div align="right">
        <a href="{{route('roa-dv.index')}}"><button class="btn btn-secondary">BACK</button></a>
    </div>
    <table class="table-print-check tableheader mt-15 mb-15">
        <tr>
            <td width="71%"></td>
            <td width="5%"><b>DATE</b></td>
            <td width="24%" class="bottomborder">&nbsp;&nbsp;&nbsp;{{ \App\Common::convertWordDateFormat($dv->cheque_date) }}</td>
        </tr>
    </table>
    <table class="table-print-check tableheader mt-15 mb-15">
        <tr>
            <td width="9%"><b>PAY TO THE<br>ORDER OF</b></td>
            <td width="1%"></td>
            <td width="62%" class="bottomborder">&nbsp;&nbsp;&nbsp;{{ $dv->getClaimant->name }}</td>
            <td width="2%"></td>
            <td width="26%" class="b_strong" style="border: 1px solid #000; padding-bottom: 7px;">&nbsp; P &nbsp;&nbsp;&nbsp;{{ number_format($dv->getRoa->total_amount,2) }}</td>
        </tr>
    </table>
    <table class="table-print-check tableheader mb-15">
        <tr>
            <td width="8%"><b>PESOS</b></td>
            <td width="1%"></td>
            <td width="80%" class="bottomborder">&nbsp;&nbsp;&nbsp;{{ \App\Common::NumberToWords($dv->getRoa->total_amount,true) }} </td>
        </tr>
    </table>
    <div align="right">
        <button type="submit" class="btn btn-primary print-form" data-form="print-check"><span class="fa fa-print"></span>&nbsp;Print</button>
    </div>
</div>

<div id="print-check" hidden>
<table class="mt-15">
</table>
<table class="table-print-check tableheader" style="margin-top: -10px">
        <tr>
            <td width="67%"></td>
            <td width="25%">&nbsp;&nbsp;&nbsp;{{ $dv->cheque_date }}</td>
            <td width="8%"></td>
        </tr>
    </table>
    <table class="table-print-check tableheader mb-10" style="margin-top: 5px">
        <tr>
            <td width="9%"></td>
            <td width="60%">&nbsp;&nbsp;&nbsp;{{ $dv->getClaimant->name }}</td>
            <td width="2%"></td>
            <td width="27%" class="b_strong">&nbsp;&nbsp;&nbsp;{{ number_format($dv->getRoa->total_amount,2) }}</td>
            <td width="2%"></td>
        </tr>
    </table>
    <table class="table-print-check tableheader mb-10">
        <tr>
            <td width="5%"></td>
            <td width="80%">&nbsp;&nbsp;&nbsp;{{ \App\Common::NumberToWords($dv->getRoa->total_amount,true) }} </td>
            <td width="5%"></td>
        </tr>
    </table>
</div>

<style>
    .bottomborder {
        border-bottom: 1px solid #000;
    }
    .table-print-check {
        width: 100%;
        overflow-x: auto;
    }
    .mt-15 {
        margin-top: 15px;
    }
    .mb-15 {
        margin-bottom: 15px;
    }
    .mb-10 {
        margin-bottom: 10px;
    }
</style>

@endsection