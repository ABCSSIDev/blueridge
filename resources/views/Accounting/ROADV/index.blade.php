@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Liquidation/DV</li>
        </ol>
    </nav>
    <h4 class="form-head">Liquidation/DV List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('roa-dv.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
                <table class="table table-striped table-bordered table-hover table-mediascreen" id="voucher_list">
                    <thead class="thead-dark">
                        <tr align="center">
                        <th>Obligation No.</th>
                        <th>Voucher No.</th>
                        <th>Check No.</th>
                        <th>Name of Claimant</th>
                        <th>Due Date</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
    .table > tbody > tr > td  {
        vertical-align: middle;
    }
</style>
@push('scripts')
    <script>
          var oTable = $("#voucher_list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                // serverSide: true,
                type: "GET",
                ajax:"{!! route('disbursement-voucher.list',0000) !!}",
                columns:[
                    {data: "obligation_number","name":"obligation_number","width":"20%", className:"text-center"},
                    {data: "voucher_number","name":"voucher_number","width":"20%", className:"text-center"},
                    {data: "check_number","name":"check_number","width":"20%", className:"text-center"},
                    {data: "claimant_name","name":"claimant_name","width":"20%", className:"text-center"},
                    {data: "due_date","name":"due_date","width":"20%", className:"text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"25%", className:"text-center"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("disbursement-voucher.list", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush