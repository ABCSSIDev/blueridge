@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Liquidation/DV</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Liquidation/DV</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('roa-dv.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" action="{{ route('disbursement-voucher.update', $roas->id) }}" id="roa_form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT" >
                    <div class="tab-content">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Obligation No.:</label>
                                <input type="text" class="form-control" name="obligation_number" value="{{$roas->getROA->obligation_number}}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Description of Obligation:</label>
                                <select class="form-control" name="ledger_id">
                                @foreach($account_ledger as $ledger)
                                    <option value="{{ $ledger->id }}" {{ ($ledger->id == $roas->getROA->ledger_id ? "selected" : "") }}>{{$ledger->ledger_name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Date of Resolution:</label>
                                <input type="text" class="form-control datetimepicker" name="resolution_date" value="{{ App\Common::convertWordDateFormat($roas->getROA->resolution_date) }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date of Approval:</label>
                                <input type="text" class="form-control datetimepicker" name="approval_date" value="{{ App\Common::convertWordDateFormat($roas->getROA->approval_date) }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Resolution Number</label>
                                <input type="" name="resolution_number" placeholder="Resolution Number" value="{{ $roas->getROA->resolution_number }}" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Total Amount:</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control total_amount" name="total_amount" id="total_amount" value="{{$roas->getROA->total_amount}}" autocomplete="off">
                                </div>
                                <!-- <input type="text" class="form-control total_amount" name="total_amount" id="total_amount" value="{{$roas->total_amount}}" autocomplete="off"> -->
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Bids & Awards Resolution Number</label>
                                <input type="text" class="form-control" name="bids_resolution_number" value="{{ $roas->getROA->bids_resolution_number }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Bids Mode</label>
                                <select class="form-control" name="bids_mode" id="bids_mode">
                                    <option value="">Select Mode</option>
                                    <option value="Bidding" {{ ($roas->getROA->bids_mode == "Bidding" ? "selected" : "") }}>Bidding</option>
                                    <option value="Shopping" {{ ($roas->getROA->bids_mode == "Shopping" ? "selected" : "") }}>Shopping</option>
                                    <option value="Negotiated" {{ ($roas->getROA->bids_mode == "Negotiated" ? "selected" : "") }}>Negotiated</option>
                                    <option value="Over the Counter" {{ ($roas->getROA->bids_mode == "Over the Counter" ? "selected" : "") }}>Over the Counter</option>
                                    <option value="Limited Source" {{ ($roas->getROA->bids_mode == "Limited Source" ? "selected" : "") }}>Limited Source</option>
                                    <option value="Repeat Order" {{ ($roas->getROA->bids_mode == "Repeat Order" ? "selected" : "") }}>Repeat Order</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kagawad Signatory</label>
                                <select class="form-control"  id="signatory" name="signatory">
                                    <option value="Kgd. Thomas J.T.F. de Castro" {{ $roas->getRoa->signatory == "Kgd. Thomas J.T.F. de Castro" ? 'selected' : ''}}>Kgd. Thomas J.T.F. de Castro</option>
                                    <option value="Kgd. Ayjell Acejas- de Castro" {{ $roas->getRoa->signatory == "Kgd. Ayjell Acejas- de Castro" ? 'selected' : ''}}>Kgd. Ayjell Acejas- de Castro</option>
                                </select>
                            </div>
                        </div>
                        <table style="width:100%">
                            <tr>
                                <td>Total Amount Spent</td>
                                <td></td> 
                                <td></td>
                                <td><label id="total_amount_spent">₱ {{$roas->getROA->total_amount}}</label></td>
                            </tr>
                            <tr>
                                <td>Amount of Cash Advance Per DV NO.</td>
                                <td><input type="number" name="cash_advance_voucher_no" placeholder="Voucher No." class="form-control col-md-8 newInput" autocomplete="off" value="{{$roas->getROA->cash_advance_voucher_no}}"></td>
                                <td><input type="text" name="cash_advance_date" placeholder="Date" class="form-control col-md-8 datetimepicker" autocomplete="off" value="{{$roas->getROA->cash_advance_date}}"></td>
                                <td><input type="text" name="cash_advance_amount" id="cash_advance_amount" placeholder="₱ 0.00" class="form-control total_amount" autocomplete="off" value="{{$roas->getROA->cash_advance_amount}}"></td>
                            </tr>
                            <tr>
                                <td>Amount Refunded Per O.R NO.</td>
                                <td><input type="number" name="amount_refund_or_no" placeholder="O.R No." class="form-control col-md-8 newInput" autocomplete="off" value="{{$roas->getROA->amount_refund_or_no}}"></td>
                                <td><input type="text" name="amount_refund_date" placeholder="Date" class="form-control col-md-8 datetimepicker" autocomplete="off" value="{{$roas->getROA->amount_refund_date}}"></td>
                                <td><label ><label id="total_refund_or_no">₱ {{$roas->getROA->total_amount - $roas->getROA->cash_advance_amount}}</label></label></td>
                            </tr>
                            <tr>
                                <td>Amount of Cash Advance Per DV NO.</td>
                                <td></td> 
                                <td></td>
                                <td><label >₱ 0.00</label></td>
                            </tr>
                        </table>
                        <hr class="hr1">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Voucher No.:</label>
                                <input type="text" class="form-control" name="voucher_no" value="{{ $roas->voucher_no }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Check No.:</label>
                                <input type="text" class="form-control" name="check_no" id="check_no" value="{{$roas->check_number}}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Cheque Date</label>
                                <input type="text" class="form-control datetimepicker" id="cheque_date" name="cheque_date" value="{{ App\Common::convertWordDateFormat($roas->cheque_date) }}" placeholder="Cheque Date" autocomplete="off">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Mode</label>
                                <select class="form-control" name="mode_type" id="mode_type">
                                    <option value="">Select Mode</option>
                                    <option value="To Pay" {{ ($roas->mode == "To Pay" ? "selected" : "") }}>To Pay</option>
                                    <option value="To Withdraw" {{ ($roas->mode == "To Withdraw" ? "selected" : "") }}>To Withdraw</option>
                                    <option value="To Reimburse" {{ ($roas->mode == "To Reimburse" ? "selected" : "") }}>To Reimburse</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Name of Claimnant:</label>
                                <input type="text" class="form-control" value="{{$roas->getClaimant->name}}" id="claimant" name="claimant" autocomplete="off">
                                <input type="hidden" class="form-control" id="claimant" name="role_id" value="{{$role->id}}">
                                <input type="hidden" class="form-control" id="roa_id" name="roa_id" value="{{$roas->roa_id}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Address of Claimnant:</label>
                                <input type="text" class="form-control" value="{{$roas->claimant_address}}" id="address" name="address" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>TIN</label>
                                <input type="text" class="form-control" id="tin" name="tin" value="{{ $roas->tin }}" placeholder="TIN" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Invoice No.</label>
                                <input type="text" class="form-control" id="invoice_no" name="invoice_no" placeholder="Invoice No." autocomplete="off" data-role="tagsinput" value="{{ $roas->invoice_no }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>RER</label>
                                <input type="text" class="form-control" id="rer" name="rer" placeholder="RER" autocomplete="off" data-role="tagsinput" value="{{ $roas->rer }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Description</label>
                            <textarea class="form-control" name="description" style="height:150px;">{{ $roas->description }}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label>OR</label>
                            <select class="form-control selectpicker" name="or_id" id="or_id" data-error=".error-container" title="Select OR" >
                                <option value="">Select OR</option>
                                @foreach($Invoice as $value)
                                    @if($value->getPayment)
                                        <option value="{{ $value->id }}"  data-subtext="{{$value->getPayee->name}} " {{ $value->id == $roas->or_id ?'selected':''}}>{{ $value->or_number }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span>&nbsp;UPDATE</button>
                    </div>
                </form>
            </diV>
        </diV>
    </diV>
</diV>


@push('scripts')

<script>
     $('#check_no').change(function(){
        $.ajax({
            url: "{{route('roa-dv.autocompleteOr')}}",
            data: {
                OR : $(this).val()
            },
            dataType: "json",
            success: function(data){
                if(data == "duplicat"){
                    toastMessage('Warning!',"This OR Number already Existed!","warning");
                }
                if(data == "maxorpad"){
                    toastMessage('Warning!',"Sorry, you have already used all the OR no. Please create new ones!","warning");
                }
                if(data == "outofrange"){
                    toastMessage('Warning!',"Sorry, your OR is out of OR Number Range!","warning");
                }
                if(data == "zero"){
                    toastMessage('Warning!',"Sorry, but you cant center 0 as a OR Number!","warning");
                }
                
            }
        });
    })
    $(".total_amount").number(true,2);
    var auto_name = "";
    var auto_obj = "";
    var auto_data = "";

    $(document).ready(function() {
        $( "#claimant" ).autocomplete({
            source: function(request, response) {
                    $.ajax({
                    url: "{{route('dv.autocompleteClaimant')}}",
                    data: {
                            term : request.term
                    },
                    dataType: "json",
                    success: function(data){

                    var resp = $.map(data,function(obj){
                        return obj.name;
                    }); 
                    response(resp);
                    auto_data = data;
                    
                    }
                });
            },
            select: function(event,ui){
                $.map(auto_data,function(obj){
                    console.log(ui.item.label);
                    if(ui.item.label == obj.name){
                        $('#address').val(obj.address);
                    }
                }); 
            }
        });
    });

    let rules = {
        ledger_id : {
            required: true
        },
        resolution_date : {
            required: true
        },
        approval_date: {
            required: true
        },
        total_amount : {
            required: true
        },
        voucher_no : {
            required: true
        },
        check_no: {
            required: true
        },
        claimant : {
            required: true
        },
        address : {
            required: true
        },
        resolution_number : {
            required: true
        },
        cheque_date : {
            required: true
        },
        mode_type : {
            required: true
        }
    };
    $( "#total_amount" ).keyup(function(e){
        var num =Number($(this).val());
        var num2 = 0;
         if($('#cash_advance_amount').val() != null){
            var num2 = Number($('#cash_advance_amount').val());
        }
        var total =  num;
        var totalrefunded  =  num - num2;
        document.getElementById("total_amount_spent").innerHTML = '	₱ '+parseInt(total,10).toFixed(2);
        document.getElementById("total_refund_or_no").innerHTML = '	₱ '+parseInt(totalrefunded,10).toFixed(2);
    });
    $( "#cash_advance_amount" ).keyup(function(e){
        var num =Number($(this).val());
        var num2 = 0;
         if($('#total_amount').val() != null){
            var num2 = Number($('#total_amount').val());
        }
        var total =  num2;
        var totalrefunded  =  num2 - num;
        document.getElementById("total_amount_spent").innerHTML = '	₱ '+parseInt(total,10).toFixed(2);
        document.getElementById("total_refund_or_no").innerHTML = '	₱ '+parseInt(totalrefunded,10).toFixed(2);
    });
$('#roa_form').registerFields(rules,false);
</script>
@endpush
@endsection

<style>
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .margin-bottom {
        margin-bottom: 50px
    }
    .margin-top{
            margin-top:50px;
        }
    .container.pr-container{
        max-width: none!important;
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
        border: 1px solid #9c98985c;
        -webkit-box-shadow: 1px 1px 10px #808080de;
        box-shadow: 1px 1px 10px #808080de;
        margin-bottom: 20px;
    }
    .tab_header {
        background-color: #323739 !important;
        color: #fff !important;
        text-align: center !important;
    }
    table.dataTable {
        clear: both;
        margin-top: 6px !important;
        margin-bottom: 6px !important;
        max-width: none !important;
        border-collapse: separate !important;
        border-spacing: 0;
    }
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #dee2e6;
    }
    .table-sm td {
        padding: 1rem!important;
    }
    .table-sm th{
        padding: 0.3rem!important;
    }
    thead tr th {
        font-weight: 500;
    }
    /* tr{
        text-align: center;
    } */
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
    .pt-03{
        padding: 0.3rem;
    }
    .mt-2r{
        margin-top: 2rem;
    }
    .hr1{
        border: 1!important;
    }
    .table-sm th.pt2{
        padding: 0.5rem!important;
    }
</style>
