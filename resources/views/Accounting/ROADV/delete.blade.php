<form method="POST" action="{{ route('disbursement-voucher.destroy', $id) }}" class="form-horizontal" enctype="multipart/form-data">
{{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE" >
    <h6> Are you sure you want to delete Obligation No. {{ $voucher->getRoa->obligation_number }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>