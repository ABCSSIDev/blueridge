@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item">Invoice</li>
            <li class="breadcrumb-item active" aria-current="page">Official Receipt</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Official Receipt</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('invoice.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST" id="invoice_form" class="form-horizontal" action="{{route('invoice.update',$invoice->id)}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT" >
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Payee</label>
                                <input type="text" class="form-control" id="payee" name="payee" placeholder="Payee" value="{{$invoice->getPayee->name}}" autocomplete="off">
                                <input type="hidden" class="form-control" id="invoice_id" name="invoice_id" placeholder="Payee" value="{{$invoice->getInvoice->invoice_id}}">
                                <input type="hidden" class="form-control" id="role_id" name="role_id" value="{{$role->id}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date</label>
                                <input type="" class="form-control datetimepicker" name="invoice_date" value="{{ App\Common::convertWordDateFormat($invoice->invoice_date) }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{$invoice->getPayee->address}}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>OR No.</label>
                                <input type="text" class="form-control" id="or_no" name="or_no" placeholder="OR No." value="{{$invoice->or_number}}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Ledger</label>
                                <select class="form-control" name="ledger">
                                    <option value="">Select Ledger</option>
                                    @foreach($account_ledger as $ledger)
                                        <option value="{{ $ledger->id }}" {{ ($ledger->id == $invoice->ledger_id ? "selected" : "") }}>{{ $ledger->ledger_name }}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Nature of Collection</label>
                                <input type="text" class="form-control" id="nature_of_collection" name="nature_of_collection" placeholder="Nature of Collection" value="{{$invoice->collection_nature}}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Due Date</label>
                                <input type="text" class="form-control datetimepicker" id="due_date" name="due_date" placeholder="Due Date" value="{{ App\Common::convertWordDateFormat($invoice->due_date) }}" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Unit Price</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control price" id="unit_price" name="unit_price" placeholder="Unit Price" value="{{$invoice->getInvoice->unit_price}}" autocomplete="off">
                                </div>
                                <!-- <input type="text" class="form-control price" id="unit_price" name="unit_price" placeholder="Unit Price" value="{{$invoice->getInvoice->unit_price}}" autocomplete="off"> -->
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
</style>

@push('scripts')
<script>
    $(".price").number(true);

    var auto_name = "";
    var auto_obj = "";
    var auto_data = "";

    $(document).ready(function() {
        $('#or_no').change(function(){
            $.ajax({
                url: "{{route('invoice.autocompleteOr')}}",
                data: {
                    OR : $(this).val()
                },
                dataType: "json",
                success: function(data){
                    if(data == "duplicat"){
                        toastMessage('Warning!',"This OR Number already Existed!","warning");
                    }
                    if(data == "maxorpad"){
                        toastMessage('Warning!',"Sorry, you have already used all the OR no. Please create new ones!","warning");
                    }
                    if(data == "outofrange"){
                        toastMessage('Warning!',"Sorry, your OR is out of OR Number Range!","warning");
                    }
                    if(data == "zero"){
                        toastMessage('Warning!',"Sorry, but you cant center 0 as a OR Number!","warning");
                    }
                    
                }
            });
        })
        $( "#payee" ).autocomplete({
            source: function(request, response) {
                    $('#address').val("");
                    $.ajax({
                    url: "{{route('invoice.autocompletePayee')}}",
                    data: {
                            term : request.term
                    },
                    dataType: "json",
                    success: function(data){

                    var resp = $.map(data,function(obj){
                        return obj.name;
                    }); 
                    response(resp);
                    auto_data = data;
                    
                    }
                });
            },
            select: function(event,ui){
                $.map(auto_data,function(obj){
                    console.log(ui.item.label);
                    if(ui.item.label == obj.name){
                        $('#address').val(obj.address);
                    }
                }); 
            }
        });
        $(".ui-autocomplete-clear").click(function () {
            $('#address').val("");
        });
    });

    var availableTags = [
        "Complaint Fee",
        "Delivery Fee",
        "Utility Clearance",
        "Court Rental",
        "Donation",
        "RPTS",
        "CTC Share",
        "Damage Post",
        "Contruction Clearance",
        "Brgy. Hall Rental",
        "Activity Area",
        "Pressure Washer",
        "Business Clearance",
        "Penalty Business",
        "Brgy. Clearance",
        "Refund",
        "Brgy. ID",
        "kalingang Q.C. Financial Assistant",
        "IRA",
        "Conference Room",
        "Session Hall",
        "Parking Area",
        "Violation"
    ];

    $( "#nature_of_collection" ).autocomplete({
        source: availableTags,
        delay: 0,
        clearButton: true
    });

    let rules = {
        payee : {
            required: true
        },
        invoice_date : {
            required: true
        },
        address : {
            required: true
        },
        or_no: {
            required: true
        },
        quantity : {
            required: true
        },
        unit_price: {
            required: true
        },
        ledger : {
            required: true
        },
        nature_of_collection: {
            required: true
        }
    };
    $('#invoice_form').registerFields(rules);
</script>
@endpush
