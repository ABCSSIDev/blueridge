@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item">Invoice</li>
            <li class="breadcrumb-item active" aria-current="page">Cedula</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Cedula</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('cedula.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST" class="form-horizontal" id="cedula_form" action="{{route('cedula.update',$cedula_data->id)}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT" >
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" autocomplete="off" value="{{ $cedula_data->userData->first_name }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date Issue</label>
                                <input type="" class="form-control datetimepicker" id="issued_date" name="issued_date" placeholder="Date Issue" autocomplete="off" value="{{ date('M d Y',strtotime($cedula_data->issued_date)) }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Middle Initial</label>
                                <input type="text" class="form-control" id="middle_initial" name="middle_initial" placeholder="Middle Initial" autocomplete="off" value="{{ $cedula_data->userData->middle_initial }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" autocomplete="off" value="{{ $cedula_data->userData->last_name }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address" autocomplete="off" value="{{ $cedula_data->userData->address }}">
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Date of Birth</label>
                                <input type="" class="form-control datetimepicker" name="birth_date" id="birth_date"  placeholder="Date of Birth" autocomplete="off" value="{{ date('M d Y',strtotime($cedula_data->userData->birth_date)) }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Place of Birth</label>
                                <input type="text" class="form-control" id="birth_place" name="birth_place" placeholder="Place of Birth" autocomplete="off" value="{{ $cedula_data->userData->birth_place }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Citizenship</label>
                                <input type="text" class="form-control" id="citizenship" name="citizenship" placeholder="Citizenship" autocomplete="off" value="{{ $cedula_data->userData->citizenship }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Sex</label>
                                <select class="form-control" name="sex" id="sex">
                                    <option value="">Select Gender</option>
                                    <option value="0" {{ ($cedula_data->userData->sex == 0 ?'selected':'') }}>Famale</option>
                                    <option value="1" {{ ($cedula_data->userData->sex == 1 ?'selected':'') }}>Male</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Civil Status</label>
                                <select class="form-control" name="civil_status" id="civil_status">
                                    <option value="">Select Civil Status</option>
                                    <option value="1" {{ ($cedula_data->userData->civil_status == 1 ?'selected':'') }}>Single</option>
                                    <option value="2" {{ ($cedula_data->userData->civil_status == 2 ?'selected':'') }}>Married</option>
                                    <option value="3" {{ ($cedula_data->userData->civil_status == 3 ?'selected':'') }}>Widowed</option>
                                    <option value="4" {{ ($cedula_data->userData->civil_status == 4 ?'selected':'') }}>Divorced</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tin</label>
                                <input type="text" class="form-control" id="tin" name="tin" placeholder="Tin" autocomplete="off" value="{{ $cedula_data->userData->tin }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>CTC NO</label>
                                <input type="text" class="form-control" id="ctc" name="ctc" placeholder="Tin" autocomplete="off" value="{{ $cedula_data->ctc }}" readonly>
                            </div>
                        </div>
                        <div class="error-container"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table-sm display responsive table-striped table-bordered dataTable no-footer" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                    <thead class="tab_header">
                                        <tr align="center">
                                            <th style="text-align: center; width: 60%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Profession/Occupation/Business</th>
                                            <th style="text-align: center; width: 20%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Taxable Amount</th>
                                            <th style="text-align: center; width: 20%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Community Tax Due</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="table_purchase">
                                        <tr style="background-color:#0000000d">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="0">
                                            <td style="vertical-align: middle"><label>A. BASIC COMMUNITY TAX (₱ 5.00)</label>&nbsp;Voluntary or Exempted (₱ 1.00)</td>
                                            <td style="vertical-align: middle">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_6" name="item_6" value="{{ $cedula_data->item_6 }}">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_1" name="item_1" value="{{ $cedula_data->item_1 }}">
                                                </div>
                                                <!-- <input  type="number" class="form-control amount items price" autocomplete="off" id="item_1" name="item_1" value="{{ $cedula_data->item_1 }}"> -->
                                            </td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="1">
                                            <td style="vertical-align: middle"><label>B. ADDITIONAL COMMUNITY TAX</label>&nbsp;(Tax not to exceed ₱ 5000.00)</td>
                                            <td style="vertical-align: middle">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_7" name="item_7" value="{{ $cedula_data->item_7 }}">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_2" name="item_2" value="{{ $cedula_data->item_2 }}">
                                                </div>
                                                <!-- <input  type="number" class="form-control amount items price" autocomplete="off" id="item_2" name="item_2" value="{{ $cedula_data->item_2 }}"> -->
                                            </td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="2">
                                            <td style="vertical-align: middle">1. Gross Receipts or Earning Derived from Business during the Preceding Year</td>
                                            <td style="vertical-align: middle">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_8" name="item_8" value="{{ $cedula_data->item_8 }}">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_3" name="item_3" value="{{ $cedula_data->item_3 }}">
                                                </div>
                                                <!-- <input  type="number" class="form-control amount items price" autocomplete="off" id="item_3" name="item_3" value="{{ $cedula_data->item_3 }}"> -->
                                            </td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="3">
                                            <td style="vertical-align: middle">2. Salaries or Gross Receipts or Earning Derived from Exercise of Profession</td>
                                            <td style="vertical-align: middle">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_9" name="item_9" value="{{ $cedula_data->item_9 }}">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_4" name="item_4" value="{{ $cedula_data->item_4 }}">
                                                </div>
                                                <!-- <input type="number" class="form-control amount items price" autocomplete="off" id="item_4" name="item_4" value="{{ $cedula_data->item_4 }}"> -->
                                            </td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="4">
                                            <td style="vertical-align: middle">3. Income from Real Property (₱ 1.00 for every ₱ 1000.00)</td>
                                            <td style="vertical-align: middle">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price" autocomplete="off" id="item_10" name="item_10" value="{{ $cedula_data->item_10 }}">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₱</span>
                                                    </div>
                                                    <input type="number" class="form-control amount items price"  autocomplete="off" id="item_5" name="item_5" value="{{ $cedula_data->item_5 }}">
                                                </div>
                                                <!-- <input type="number" class="form-control amount items price"  autocomplete="off" id="item_5" name="item_5" value="{{ $cedula_data->item_5 }}"> -->
                                            </td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td colspan="3" align="right">Total:&nbsp;₱&nbsp;<input class="for_savings " readonly style="border: none;background-color: transparent;" name="totalamount" id="totalamount" value="{{  number_format($cedula_data->item_5 + $cedula_data->item_4 +$cedula_data->item_3 +$cedula_data->item_2 + $cedula_data->item_1,2) }}"></td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td style="vertical-align: middle"></td>
                                            <td align="right">Interest:</td> 
                                            <td style="vertical-align: middle"><input type="number" class="form-control amount interest"  autocomplete="off" id="interest" name="interest" value="{{  $cedula_data->interest }}"></td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td colspan="3" align="right">Total Amount Paid:&nbsp;₱&nbsp;<input class="for_savings" readonly style="border: none;background-color: transparent;" name="amount_paid" id="amount_paid" value="{{  number_format($totalamount,2) }}"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary mt-2"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection
<style>
    .margin-top{
            margin-top:50px;
        }
    .container.pr-container{
        max-width: none!important;
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .input {
        border: none;
        background-color: transparent;
    }
    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
        border: 1px solid #9c98985c;
        -webkit-box-shadow: 1px 1px 10px #808080de;
        box-shadow: 1px 1px 10px #808080de;
        margin-bottom: 20px;
    }
    .tab_header {
        background-color: #323739 !important;
        color: #fff !important;
        text-align: center !important;
    }
    table.dataTable {
        clear: both;
        margin-top: 6px !important;
        margin-bottom: 6px !important;
        max-width: none !important;
        border-collapse: separate !important;
        border-spacing: 0;
    }
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #dee2e6;
    }
    .table-sm td {
        padding: 1rem!important;
    }
    .table-sm th{
        padding: 0.3rem!important;
    }
    thead tr th {
        font-weight: 500;
    }
    tr{
        text-align: center;
    }
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
    .pt-03{
        padding: 0.3rem;
    }
    .table-sm th.pt2{
        padding: 0.5rem!important;
    }
</style>
@push('scripts')
<script>
$('.amount').on('keyup', function () {
    var total =0;   
    var interest =0;   
    var amountpaid =0;   
    $('.items').each(function(){
        total += Number($(this).val());
    });
    $('.interest').each(function(){
        interest = Number($(this).val());
    });
    amountpaid =  Number(total * interest);
    Final = Number(total + amountpaid);
    console.log(total);
    $('#totalamount').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
    $('#amount_paid').val(Final.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));

}); 
let rules = {
        first_name : {
            required: true
        },
        issued_date : {
            required: true
        },
        address : {
            required: true
        },
        last_name : {
            required: true
        },
        birth_date : {
            required: true
        },
        birth_place : {
            required: true
        },
        citizenship : {
            required: true
        },
        civil_status : {
            required: true
        },
        sex : {
            required: true
        },
        item_1 : {
            required: true
        },
        item_3 : {
            required: true
        },
    };
    $('#cedula_form').registerFields(rules,false);

</script>
@endpush
