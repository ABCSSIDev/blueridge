
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tab-content">
                
                    <form method="POST" class="form-horizontal" id="invoice_ctcpad_form" action="{{route('cedulas.functionCreateCTCPad')}}" enctype="multipart/form-data">
                    @if($or_settings == null)
                        <div class="errors"></div>
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>CTC Pad Start</label>
                                <input type="text" class="form-control pad_start" id="pad_start" name="pad_start" placeholder="CTC Pad Start" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>CTC Pad Quantity</label>
                                <input type="text" class="form-control" id="pad_end" name="pad_end" placeholder="CTC Pad Quantity" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    @else
                        <table class="table  table-responsive" style="width: 100%">
                            <tr>
                                <td>Total CTC Count</td>
                                <td>{{ $or_pads->sum('cd_pad_quantity') }}</td>
                            </tr>
                            <tr>
                                <td>Beginning Balance</td>
                                <td>{{ $or_pads->first()['cd_pad_start'] }}</td>
                            </tr>
                            <tr>
                                <td>Issued Cedula </td>
                                <td>{{ $Cedula->count() }}</td>
                            </tr>
                            <tr>
                                <td>End Balance</td>
                                <td>{{ $or_pads->first()['cd_pad_start'] + $or_pads->sum('cd_pad_quantity') - 1 }}</td>
                            </tr>
                        </table>
                        <div id="append_form"></div>
                        <a href="#" id="add_replenishment"><p><span class="fa fa-plus"></span> Add Replenishment</p></a>
                        <a href="#"><p><span class="fa fa-undo"></span> Reset OR Settings</p></a>
                    @endif
                    </form>
                
            </div>
        </div>
    </div>
    <script>
        $('#add_replenishment').on('click',function () {
            let append_html =
            '{{csrf_field()}}' + 
            ' <div class="form-row">\n' +
            '   <div class="form-group col-md-6">\n' +
            '       <label>CTC Pad Start</label>\n' +
            '       <input type="text" class="form-control newInput pad_start" id="pad_start" name="pad_start" placeholder="CTC Pad Start" autocomplete="off">\n' +
            '   </div>\n' +
            '   <div class="form-group col-md-6">\n' +
            '       <label>CTC Pad Quantity</label>\n' +
            '       <input type="text" class="form-control newInput" id="pad_end" name="pad_end" placeholder="CTC Pad Quantity" autocomplete="off">\n' +
            '   </div>\n' +
            '</div>' +
            '<div class="form-group" align="right" >\n' +
            '   <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>\n' +
            '</div>'
            $('#append_form').html(append_html);
            $('.newInput').each( function () {
                $(this).rules('add',{
                    'required' : true
                })
            });
            $('.pad_start').change(function(){
                if($(this).val() != null){
                    $.ajax({
                        url: "{{route('invoice.autocompleteORPad')}}",
                        data: {
                            OR : $(this).val()
                        },
                        dataType: "json",
                        success: function(data){
                            if(data>0){
                                toastMessage('Warning!',"This OR Pad Number already Existed!","warning");
                            }
                            
                        }
                    });
                }
            })
        })
    $('.pad_start').change(function(){
        if($(this).val() != null){
             $.ajax({
                url: "{{route('invoice.autocompleteORPad')}}",
                data: {
                    OR : $(this).val()
                },
                dataType: "json",
                success: function(data){
                    if(data == "duplicat"){
                        toastMessage('Warning!',"This OR Pad Number already Existed!","warning");
                    }
                    // if(data == "maxorpad"){
                    //     toastMessage('Warning!',"Sorry But","warning");
                    // }
                    
                }
            });
        }
    })
     $(document).ready(function(){
        let pad_rules = {
            pad_start : {
                required: true
            },
            pad_end : {
                required: true
            }
        }; 
        $('#invoice_ctcpad_form').registerFields(pad_rules);
     });
    </script>