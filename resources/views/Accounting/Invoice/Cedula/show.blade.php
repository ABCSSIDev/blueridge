@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item">Invoice</li>
            <li class="breadcrumb-item active" aria-current="page">Cedula</li>
        </ol>
    </nav>
    <h4 class="form-head">View Cedula</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('cedula.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST" class="form-horizontal" id="cedula_form" action="{{route('cedula.update',$cedula_data->id)}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <p>{{ $cedula_data->userData->first_name }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date Issue</label>
                                <p>{{ date('M d Y',strtotime($cedula_data->issued_date)) }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Middle Initial</label>
                                <p>{{ $cedula_data->userData->middle_initial }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <p>{{ $cedula_data->userData->last_name }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Address</label>
                                <p>{{ $cedula_data->userData->address }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Date of Birth</label>
                                <p>{{ date('M d Y',strtotime($cedula_data->userData->birth_date)) }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Place of Birth</label>
                                <p>{{ $cedula_data->userData->birth_place }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Citizenship</label>
                                <p>{{ $cedula_data->userData->citizenship }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Sex</label>
                                <p>{{ ($cedula_data->userData->sex == 0 ?'Famale':'Male') }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Civil Status</label>
                                <p>{{ ($cedula_data->userData->civil_status == 1 ? 'Single' : ($cedula_data->userData->civil_status == 2 ? 'Married' : ($cedula_data->userData->civil_status == 3 ? 'Widowed' :  'Divorced' )))}}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tin</label>
                                <p>{{ $cedula_data->userData->tin }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>CTC NO</label>
                                <p>{{ $cedula_data->ctc}}</p>
                            </div>
                        </div>
                        <div class="error-container"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table-sm display responsive table-striped table-bordered dataTable no-footer" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                    <thead class="tab_header">
                                        <tr align="center">
                                            <th style="text-align: center; width: 60%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Profession/Occupation/Business</th>
                                            <th style="text-align: center; width: 20%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Taxable Amount</th>
                                            <th style="text-align: center; width: 20%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Community Tax Due</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="table_purchase">
                                        <tr style="background-color:#0000000d">
                                            <td style="vertical-align: middle"><label>A. BASIC COMMUNITY TAX (₱ 5.00)</label>&nbsp;Voluntary or Exempted (₱ 1.00)</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_6 == null ? '0.00' : $cedula_data->item_6 }}</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_1 == null ? '0.00' : $cedula_data->item_1 }}</td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td style="vertical-align: middle"><label>B. ADDITIONAL COMMUNITY TAX</label>&nbsp;(Tax not to exceed ₱ 5000.00)</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_7 == null ? '0.00' : $cedula_data->item_7 }}</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_2 == null ? '0.00' : $cedula_data->item_2 }}</td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td style="vertical-align: middle">1. Gross Receipts or Earning Derived from Business during the Preceding Year</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_8 = null ? '0.00' : $cedula_data->item_8 }}</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_3 = null ? '0.00' : $cedula_data->item_3 }}</td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td style="vertical-align: middle">2. Salaries or Gross Receipts or Earning Derived from Exercise of Profession</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_9 = null ? '0.00' : $cedula_data->item_9 }}</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_4 = null ? '0.00' : $cedula_data->item_4 }}</td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="4">
                                            <td style="vertical-align: middle">3. Income from Real Property (₱ 1.00 for every ₱ 1000.00)</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_10 = null ? '0.00' : $cedula_data->item_10 }}</td>
                                            <td style="vertical-align: middle">₱&nbsp;{{ $cedula_data->item_5 = null ? '0.00' : $cedula_data->item_5 }}</td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td colspan="3" align="right">Total:&nbsp;₱&nbsp;<input class="for_savings" readonly style="border: none;background-color: transparent;" name="totalamount" id="totalamount" value="{{  number_format($cedula_data->item_5 + $cedula_data->item_4 +$cedula_data->item_3 +$cedula_data->item_2 + $cedula_data->item_1,2) }}"></td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td colspan="3" align="right">Interest: <input class="for_savings" readonly style="border: none;background-color: transparent;" name="" id="" value="{{  $cedula_data->interest }}%"></td>
                                        </tr>
                                        <tr style="background-color:#0000000d">
                                            <td colspan="3" align="right">Total Amount Paid:&nbsp;₱&nbsp;<input class="for_savings" readonly style="border: none;background-color: transparent;" name="" id="" value="{{  number_format($totalamount,2) }}"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- @if(!is_null($cedula_data->getPayment))
                            <legend>Payment Details</legend>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Amount Received:</label>
                                    <p>PHP {{ number_format($cedula_data->getPayment->amount_received,2) }}</p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Received Date:</label>
                                    <p>{{ App\Common::convertWordDateFormat($cedula_data->getPayment->date_received) }}</p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Paid Into:</label>
                                    <p>{{ $cedula_data->getPayment->getBank->name }}</p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Payment Method:</label>
                                    <p>{{ $cedula_data->getPayment->getPaymentMethod->name }}</p>
                                </div>
                            </div>
                        @endif -->
                        <div class="form-group" align="right" >
                            <!-- @if(is_null($cedula_data->getPayment))
                                <a type="submit" href="{{ route('payment.create',['type' => 'cedula','id' => $cedula_data->id]) }}" class="btn btn-primary" data-form="print-invoice"><i class="fa fa-print"></i>&nbsp;PAYMENTS</a>
                            @endif -->
                            <button type="submit" class="btn btn-primary print-form mt-2" data-form="print-cedula"><i class="fa fa-print"></i>&nbsp;PRINT</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<div id="print-cedula" hidden>
    <table width="100%" style="margin-top: 25px; font-size: 14px">
        <tr>
            <td width="7%"></td>
            <td width="4%" class="text-right">{{ App\Common::convertLastYearFormat($cedula_data->created_at) }}</td>
            <td width="28%">Barangay Blue Ridge B</td>
            <td width="9%" class="text-left">{{$cedula_data->issued_date}}</td>
            <td width="52%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top: -3px; font-size: 14px">
        <tr>
            <td width="45%">{{$cedula_data->userData->last_name}}    {{$cedula_data->userData->first_name}}    {{$cedula_data->userData->middle_initial}} </td>
            <td width="35%" style="padding-top: 15px;">{{$cedula_data->userData->tin}}</td>
            <td width="20%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top: -10px; font-size: 14px">
        <tr>
            <td width="7%"></td>
            <td width="47%">{{$cedula_data->userData->address}}</td>
            <td width="46%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top: 3px;font-size: 14px">
        <tr>
            <td width="7%"></td>
            <td width="15%">{{$cedula_data->userData->citizenship}}</td>
            <td width="8%"></td>
            <td width="30%">{{$cedula_data->userData->birth_place}}</td>
            <td width="40%"></td>
        </tr>
    </table>
    <table width="100%" style="font-size: 14px; margin-top: 2px">
        <tr>
            <td width="45%"></td>
            <td width="30%">{{$cedula_data->userData->birth_date}}</td>
            <td width="25%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top:25px; font-size: 14px">
        <tr>
            <td width="50%"></td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_6}}</td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_1}}</td>
            <td width="18%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top:3px;font-size: 14px">
        <tr>
            <td width="50%"></td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_7}}</td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_2}}</td>
            <td width="18%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top:3px;font-size: 14px">
        <tr>
            <td width="50%"></td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_8}}</td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_3}}</td>
            <td width="18%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top:2px;font-size: 14px">
        <tr>
            <td width="50%"></td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_9}}</td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_4}}</td>
            <td width="18%"></td>
        </tr>
    </table>
    <table width="100%" style="margin-top:2px;font-size: 14px">
        <tr>
            <td width="50%"></td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_10}}</td>
            <td width="16%">₱&nbsp;{{$cedula_data->item_5}}</td>
            <td width="18%"></td>
        </tr>
    </table>  
    <table width="100%" style="margin-top:3px;font-size: 14px">
        <tr>
            <td width="35%"></td>
            <td width="27%"></td>
            <td width="28%">₱&nbsp;{{ number_format($cedula_data->item_10 + $cedula_data->item_9 + $cedula_data->item_8 + $cedula_data->item_7 + $cedula_data->item_6 + $cedula_data->item_5 + $cedula_data->item_4 +$cedula_data->item_3 +$cedula_data->item_2 + $cedula_data->item_1,2) }}</td>
            <td width="10%"></td>
        </tr>
    </table> 
    <table width="100%" style="margin-top:3px;font-size: 14px">
        <tr>
            <td width="25%"></td>
            <td width="27%"></td>
            <td width="28%"></td>
            <td width="20%"></td>
        </tr>
    </table> 
    <table width="100%" style="margin-top:15px;font-size: 14px">
        <tr>
            <td width="35%"></td>
            <td width="27%"></td>
            <td width="28%">₱&nbsp;{{ number_format($cedula_data->item_10 + $cedula_data->item_9 + $cedula_data->item_8 + $cedula_data->item_7 + $cedula_data->item_6 + $cedula_data->item_5 + $cedula_data->item_4 +$cedula_data->item_3 +$cedula_data->item_2 + $cedula_data->item_1,2) }}</td>
            <td width="20%"></td>
        </tr>
    </table> 
    <table width="100%" style="margin-top:12px;font-size: 14px">
        <tr>
            <td width="23%"></td>
            <td width="35%"></td>
            <td width="20%">{{ App\Common::NumberToWords($cedula_data->item_10 + $cedula_data->item_9 + $cedula_data->item_8 + $cedula_data->item_7 + $cedula_data->item_6 + $cedula_data->item_5 + $cedula_data->item_4 +$cedula_data->item_3 +$cedula_data->item_2 + $cedula_data->item_1,2) }}</td>
            <td width="22%"></td>
        </tr>
    </table> 
</div>

@endsection
<style>
    .margin-top{
            margin-top:50px;
        }
    .container.pr-container{
        max-width: none!important;
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .input {
        border: none;
        background-color: transparent;
    }
    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
        border: 1px solid #9c98985c;
        -webkit-box-shadow: 1px 1px 10px #808080de;
        box-shadow: 1px 1px 10px #808080de;
        margin-bottom: 20px;
    }
    .tab_header {
        background-color: #323739 !important;
        color: #fff !important;
        text-align: center !important;
    }
    table.dataTable {
        clear: both;
        margin-top: 6px !important;
        margin-bottom: 6px !important;
        max-width: none !important;
        border-collapse: separate !important;
        border-spacing: 0;
    }
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #dee2e6;
    }
    .table-sm td {
        padding: 1rem!important;
    }
    .table-sm th{
        padding: 0.3rem!important;
    }
    thead tr th {
        font-weight: 500;
    }
    tr{
        text-align: center;
    }
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
    .pt-03{
        padding: 0.3rem;
    }
    .table-sm th.pt2{
        padding: 0.5rem!important;
    }
</style>
@push('scripts')
<script>
$('.amount').on('keyup', function () {
    var total =0;   
    $('.amount').each(function(){
        total += Number($(this).val());
    });
    console.log(total);
    $('#totalamount').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
}); 
let rules = {
        first_name : {
            required: true
        },
        issued_date : {
            required: true
        },
        address : {
            required: true
        },
        middle_initial : {
            required: true
        },
        last_name : {
            required: true
        },
        birth_date : {
            required: true
        },
        birth_place : {
            required: true
        },
        citizenship : {
            required: true
        },
        civil_status : {
            required: true
        },
        sex : {
            required: true
        },
        tin : {
            required: true
        },
        item_1 : {
            required: true
        },
        item_2 : {
            required: true
        },
        item_3 : {
            required: true
        },
        item_4 : {
            required: true
        },
        item_5 : {
            required: true
        },
    };
    $('#cedula_form').registerFields(rules);

</script>
@endpush
