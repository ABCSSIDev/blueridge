@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Accounting</li>
        <li class="breadcrumb-item">Transactions</li>
        <li class="breadcrumb-item">Invoice</li>
        <li class="breadcrumb-item active" aria-current="page">Cedula</li>
    </ol>
    </nav>
    <h4 class="form-head">Cedula List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('cedula.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
                <table class="table table-striped table-bordered table-hover text-center table-mediascreen" id="cedula_list">
                    <thead class="thead-dark">
                        <tr align="center">
                            <th width="20%">First Name</th>
                            <th width="20%">Last Name</th> 
                            <th width="20%">CTC No.</th>
                            <th width="20%">Tin</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div>
</div>
@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
@push('scripts')
    <script>
        var oTable = $("#cedula_list").DataTable({
            pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('get.cedula.list',0000) !!}",
                columns:[
                    {data: "first_name","name":"first_name","width":"20%", className:"v-align text-center"},
                    {data: "last_name","name":"last_name","width":"20%", className:"v-align text-center"},
                    {data: "ctc","name":"ctc","width":"20%", className:"v-align text-center"},
                    {data: "tin","name":"tin","width":"20%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
                ],
                select: {
                    style: "multi"
                }
        });
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("get.cedula.list", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush
