@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item">Invoice</li>
            <li class="breadcrumb-item active" aria-current="page">Official Receipt</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Official Receipt</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('invoice.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST" class="form-horizontal" id="invoice_form" action="{{route('invoice.store')}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Payee</label>
                                <input type="text" class="form-control" id="payee" name="payee" placeholder="Payee" autocomplete="off">
                                <input type="hidden" class="form-control" id="role_id" name="role_id" value="{{$role->id}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date</label>
                                <input type="" class="form-control datetimepicker" name="invoice_date" placeholder="Date" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>OR No.</label> <a class="popup_form" data-toggle="modal" data-url="{{route('invoice.functionORPad')}}" title="OR Pad Settings" style="color: #006cc5"><span class="fa fa-cog"></span></a>
                                @if($or_number == null)
                                    <input type="text" class="form-control pad_start" id="or_no" name="or_no" placeholder="OR No." autocomplete="off">
                                @else
                                    <input type="text" class="form-control pad_start" id="or_no" name="or_no" placeholder="OR No." autocomplete="off" value="{{ $or_number }}">
                                @endif
                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nature of Collection</label>
                                <select class="form-control" id="nature_of_collection" name="nature_of_collection">
                                    <option value="">Select Nature of Collection</option>
                                    <option value="Complaint Fee">Complaint Fee</option>
                                    <option value="Delivery Fee">Delivery Fee</option>
                                    <option value="Utility Clearance">Utility Clearance</option>
                                    <option value="Court Rental">Court Rental</option>
                                    <option value="Donation">Donation</option>
                                    <option value="RPTS">RPTS</option>
                                    <option value="CTC Share">CTC Share</option>
                                    <option value="Contruction Clearance">Contruction Clearance</option>
                                    <option value="Brgy. Hall Rental">Brgy. Hall Rental</option>
                                    <option value="Pressure Washer">Pressure Washer</option>
                                    <option value="Business Clearance">Business Clearance</option>
                                    <option value="Penalty Business">Penalty Business</option>
                                    <option value="Brgy. Clearance">Brgy. Clearance</option>
                                    <option value="Activity Area">Activity Area</option>
                                    <option value="Damage Post">Damage Post</option>
                                    <option value="Closure business">Closure business</option>
                                    <option value="Refund">Refund</option>
                                    <option value="Brgy. ID">Brgy. ID</option>
                                    <option value="kalingang Q.C. Financial Assistant">kalingang Q.C. Financial Assistant</option>
                                    <option value="IRA">IRA</option>
                                    <option value="Conference Room">Conference Room</option>
                                    <option value="Session Hall">Session Hall</option>
                                    <option value="Parking Area">Parking Area</option>
                                    <option value="Violation">Violation</option>
                                    <option value="Rental for Chairs">Rental for Chairs</option>
                                    <option value="Rental for Tables">Rental for Tables</option>
                                    <option value="Rental for Sound System">Rental for Sound System</option>
                                    <option value="Rental for Tent (big)">Rental for Tent (big)</option>
                                    <option value="Rental for Tent (small)">Rental for Tent (small)</option>
                                    <option value="Rental for Chainsaw">Rental for Chainsaw</option>
                                    <option value="Rainbow Park">Rainbow Park</option>
                                    <option value="Shooting">Shooting</option>
                                    <option value="Shooting">Payment for CCTV</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Ledger</label>
                                <select class="form-control" name="ledger">
                                    <option value="">Select Ledger</option>
                                    @foreach($account_ledger as $ledger)  
                                        <option value="{{$ledger->id}}">{{$ledger->ledger_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            {{--<div class="form-group col-md-6">--}}
                                {{--<label>Quantity</label>--}}
                                {{--<input type="text" class="form-control price" id="quantity" name="quantity" placeholder="Quantity" autocomplete="off">--}}
                            {{--</div>--}}
                            <div class="form-group col-md-6">
                                <label for="">Due Date</label>
                                <input type="text" class="form-control datetimepicker" id="due_date" name="due_date" placeholder="Due Date" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Unit Price</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control price" id="unit_price" name="unit_price" placeholder="Unit Price" autocomplete="off">
                                </div>
                                <!-- <input type="text" class="form-control price" id="unit_price" name="unit_price" placeholder="Unit Price" autocomplete="off"> -->
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary" id="btns-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(".price").number(true,2);
    $("#no_data").show();
    var auto_name = "";
    var auto_obj = "";
    var auto_data = "";

    $(document).ready(function() {
        $('#or_no').change(function(){
            $.ajax({
                url: "{{route('invoice.autocompleteOr')}}",
                data: {
                    OR : $(this).val()
                },
                dataType: "json",
                success: function(data){
                    if(data == "duplicat"){
                        toastMessage('Warning!',"This OR Number already Existed!","warning");
                    }
                    if(data == "maxorpad"){
                        toastMessage('Warning!',"Sorry, you have already used all the OR no. Please create new ones!","warning");
                    }
                    if(data == "outofrange"){
                        toastMessage('Warning!',"Sorry, your OR is out of OR Number Range!","warning");
                    }
                    if(data == "zero"){
                        toastMessage('Warning!',"Sorry, but you cant center 0 as a OR Number!","warning");
                    }
                    
                }
            });
        })
        $( "#payee" ).autocomplete({
            source: function(request, response) {
                    $('#address').val("");
                    $.ajax({
                    url: "{{route('invoice.autocompletePayee')}}",
                    data: {
                            term : request.term
                    },
                    
                    dataType: "json",
                    success: function(data){

                    var resp = $.map(data,function(obj){
                        return obj.name;
                    }); 
                    response(resp);
                    auto_data = data;
                    
                    }
                });
                
            },
            select: function(event,ui){
                $.map(auto_data,function(obj){
                    console.log(ui.item.label);
                    if(ui.item.label == obj.name){
                        $('#address').val(obj.address);
                    }
                }); 
            },
        });
        $(".ui-autocomplete-clear").click(function () {
            $('#address').val("");
        });
    });

    $("#add_inventory").click(function(e){
        $("#no_data").hide();
    });

    var availableTags = [
        "Complaint Fee",
        "Delivery Fee",
        "Utility Clearance",
        "Court Rental",
        "Donation",
        "RPTS",
        "CTC Share",
        "Contruction Clearance",
        "Brgy. Hall Rental",
        "Pressure Washer",
        "Business Clearance",
        "Penalty Business",
        "Brgy. Clearance",
    ];
    // $('.pad_start').change(function(){
    //     if($(this).val() != null){
    //          $.ajax({
    //             url: "{{route('invoice.autocompleteORPad')}}",
    //             data: {
    //                 OR : $(this).val()
    //             },
    //             dataType: "json",
    //             success: function(data){
    //                 if(data == "duplicat"){
    //                     toastMessage('Warning!',"This OR Pad Number already Existed!","warning");
    //                 }
    //                 if(data == "maxorpad"){
    //                     toastMessage('Warning!',"Sorry But","warning");
    //                 }
                    
    //             }
    //         });
    //     }
    // })
    // $( "#nature_of_collection" ).autocomplete({
    //     source: availableTags,
    //     delay: 0,
    //     clearButton: true
    // });

    let rules = {
        payee : {
            required: true
        },
        invoice_date : {
            required: true
        },
        address : {
            required: true
        },
        or_no: {
            required: true
        },
        due_date : {
            required: true
        },
        unit_price: {
            required: true
        },
        ledger : {
            required: true
        },
        nature_of_collection: {
            required: true
        }
    };
    $('#invoice_form').registerFields(rules);

   
</script>
@endpush
