@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item">Transactions</li>
            <li class="breadcrumb-item">Invoice</li>
            <li class="breadcrumb-item active" aria-current="page">Official Receipt</li>
        </ol>
    </nav>
    <h4 class="form-head">View Official Receipt</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('invoice.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="errors"></div>
                {{csrf_field()}}
                <fieldset>
                    <legend>Official Receipt Details</legend>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Payee:</label>
                            <p>{{ $invoice->getPayee->name }} <br><small>({{ (is_null($invoice->getPayment) ? "Open" : "Paid") }})</small></p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>OR Date:</label>
                            <p>{{ App\Common::convertWordDateFormat($invoice->invoice_date) }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Address:</label>
                            <p>{{ $invoice->getPayee->address }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>OR No.:</label>
                            <p>{{ $invoice->or_number }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Ledger:</label>
                            <p>{{ $invoice->getLedger->ledger_name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Nature of Collection:</label>
                            <p>{{ $invoice->collection_nature }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Due Date:</label>
                            <p>{{ \App\Common::formatDate("F d, Y",$invoice->due_date) }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Unit Price:</label>
                            <p>₱&nbsp;{{ number_format($invoice->getInvoice->unit_price,2) }}</p>
                        </div>
                    </div>
                </fieldset>
                @if(!is_null($invoice->getPayment))
                    <fieldset>
                        <legend>Payment Details</legend>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Amount Received:</label>
                                <p>₱{{ number_format($invoice->getPayment->amount_received,2) }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Received Date:</label>
                                <p>{{ App\Common::convertWordDateFormat($invoice->getPayment->date_received) }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Paid Into:</label>
                                <p>{{ $invoice->getPayment->getBank->name }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Payment Method:</label>
                                <p>{{ $invoice->getPayment->getPaymentMethod->name }}</p>
                            </div>
                        </div>
                    </fieldset>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group mt-3" align="right" >
        @if(is_null($invoice->getPayment))
            <a type="submit" href="{{ route('payment.create',['type' => 'invoice','id' => $invoice->id]) }}" class="btn btn-primary" data-form="print-invoice"><i class="fa fa-print"></i>&nbsp;PAYMENTS</a>
        @endif
        <button type="submit" class="btn btn-primary print-form" data-form="print-invoice"><i class="fa fa-print"></i>&nbsp;PRINT</button>
    </div>
</div>

<div id="print-invoice" class="col-12" align="center"  hidden>
    <table class="table-print-invoice" style="margin-top: 8.5em">
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>    
            <td>&nbsp;&nbsp; <br>&nbsp;&nbsp;{{ App\Common::convertWordDateFormat($invoice->invoice_date) }}</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table-print-invoice" style="margin-top: 15px">
        <tr>
            <td><br>&nbsp;&nbsp;{{$invoice->getPayee->name}}</td>
        </tr>
    </table>
    <table class="table-print-invoice" style="margin-top: 48px">
        <tr>
            <td></td>
            <td></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td width="20%">&nbsp;&nbsp;{{$invoice->collection_nature}}</td>
            <td width="10%">&nbsp;&nbsp;{{$invoice->getLedger->account_code}}</td>
            <td width="7%">&nbsp;{{$invoice->getInvoice->unit_price}}</td>
            <td width="63%"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>&nbsp;{{$invoice->getInvoice->unit_price}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4"></td>
        </tr>
    </table>
    <table class="table-print-invoice" style="margin-top: 42px">
        <tr>
            <td colspan="4">&nbsp;&nbsp;{{ App\Common::NumberToWords($invoice->getInvoice->unit_price) }}</td>
        </tr>
    </table>


    <style>
        .margin-top{
            margin-top:50px;
        }
        .table-print-invoice {
            width: 100%;
        }
        fieldset{
            border: none!important;
        }
        
    </style>
</div>

@endsection

@push('scripts')
    <script>
        @if(\Illuminate\Support\Facades\Session::has('message'))
            let type  = '{{ \Illuminate\Support\Facades\Session::get('alert-type','info') }}';
            toastMessage('Warning!','{{ Illuminate\Support\Facades\Session::get('message') }}',type);
        @endif
    </script>
@endpush


