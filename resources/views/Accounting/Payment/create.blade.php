@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item" >Accounting</li>
                <li class="breadcrumb-item">Transactions</li>
                <li class="breadcrumb-item active" aria-current="page">Payment</li>
            </ol>
        </nav>
        <h4 class="form-head">Create Payment</h4>
        <div class="row justify-content-center">
            <div class="col-md-12">
                    <div align="right" style="margin: 0px 0px 20px 0px">
                    @if($model == 'App\Payroll')
                        <a href="{{route('payroll.show',$reference_id)}}">
                    @elseif($model == 'App\DisbursementVoucher')
                        <a href="{{route('roa-dv.show',$reference_id)}}">
                    @else($model == 'App\Invoice')
                        <a href="{{route('invoice.show',$reference_id)}}">
                    @endif
                    <button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a>
                    </div>
                <div class="tab-content">
                    <form method="POST" class="form-horizontal" id="payment_form" action="{{route('payment.store')}}" enctype="multipart/form-data">
                        @csrf
                        {!! $message !!}
                        <input type="hidden" value="{{ $model }}" name="model">
                        <input type="hidden" name="reference_id" value="{{ $reference_id }}">
                        <input type="hidden" name="type" value="{{ $type }}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="amount">Amount Received</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control" value="{{ number_format($total,2) }}" id="amount" name="amount" readonly>
                                </div>
                                <!-- <input type="text" class="form-control" value="{{ number_format($total,2) }}" id="amount" name="amount" readonly> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date Received</label>
                                <input type="text" class="form-control datetimepicker" name="received_date" placeholder="Date Received" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="bank_id">Paid From</label>
                                <select name="bank_id" class="form-control" id="bank_id">
                                    <option value="">Select Bank</option>
                                    @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}" {{strcasecmp("Landbank", $bank->name)==0?"selected":""}}>{{ $bank->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="payment_id">Payment Method</label>
                                <select name="payment_id" class="form-control" id="payment_id">
                                    <option value="">Select Payment Method</option>
                                    @foreach($payment_methods as $method)
                                        <option value="{{ $method->id }}">{{ $method->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="bank_id">Paid Into</label>
                                <input type="text" class="form-control" id="paid_into" name="paid_into" placeholder="Paid To" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="cancelled" name="cancelled">
                            <label class="form-check-label" for="cancelled">Cancelled</label>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
                </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        let paidInto = [
            'BDO',
            'EastWest',
            'MetroBank',
            'Security Bank',
            'RCBC',
            'MERALCO',
            'Bayad Center',
            'Barangay Blue Ridge B',
            'BPI'
        ];
        let rules = {
            amount : {
                required: true
            },
            received_date : {
                required: true
            },
            bank_id : {
                required: true
            },
            payment_id : {
                required: true
            },
            paid_into : {
                required: true
            },
        };
        $('#payment_form').registerFields(rules);

        $(document).ready(function() {
            $( "#paid_into" ).autocomplete({
                source: paidInto
            });
        });
    </script>
@endpush
