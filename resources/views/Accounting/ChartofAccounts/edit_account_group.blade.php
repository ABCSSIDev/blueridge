@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item" >Accounting</li>
                <li class="breadcrumb-item active" aria-current="page">Chart of Account</li>
            </ol>
        </nav>
        <h4 class="form-head">Edit Chart of Account</h4>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div align="right" style="margin: 0px 0px 20px 0p"><a href="{{route('chart-account.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
                <div class="tab-content">
                    <form method="POST"  class="form-horizontal" action="{{route('update.chart-account-group', $group_data->id)}}" enctype="multipart/form-data" id="account_group_update">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT" >
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="status_name">Group Name</label>
                                <input type="text" class="form-control" name="group_name" placeholder="Group Name" value="{{$group_data->group_name}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="status_type">Parent Group (Optional)</label>
                                <select class="form-control" name="parent_group" >
                                    <option value="">Select Parent Group</option>
                                    @foreach($all_account_groups as $account_group)
                                        <option value="{{$account_group->id}}" {{$account_group->id==$group_data->group_id?"selected":""}}>{{$account_group->group_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Description (Optional)</label>
                                <textarea class="form-control" name="description" placeholder="Description">{{$group_data->description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
@push('scripts')
    <script>
        let rules = {
            group_name : { required: true },
        };
        $('#account_group_update').registerFields(rules, false);
    </script>
@endpush
