@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Accounting</li>
        <li class="breadcrumb-item active" aria-current="page">Chart of Accounts</li>
    </ol>
    </nav>
    <h4 class="form-head">Chart of Account List</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('chart-account.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table  table-bordered table-hover table-sm table-mediascreen">
                <thead class="thead-dark">
                    <tr align="center">
                    <th>Account Group / Ledgers</th>
                    <th>Account Code</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody >
                    {!! $table !!}
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>

