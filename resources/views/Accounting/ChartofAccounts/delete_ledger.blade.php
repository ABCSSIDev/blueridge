<form method="POST" action="{{route('archive.accountLedger', $ledger_data->id)}}" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE" >
    <h6><strong>Are you sure you want to archive {{$ledger_data->ledger_name}}?</strong></h6>
    <h6><em>Please take note that this action will also archive all groups and/or ledgers under {{$ledger_data->ledger_name}}</em></h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>
