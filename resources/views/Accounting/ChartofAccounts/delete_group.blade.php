<form method="POST" action="{{route('archive.accountGroup', $group_data->id)}}" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE" >
    <h6><strong>Are you sure you want to delete {{$group_data->group_name}} ?</strong> </h6>
    <p><em>Please take note that this action will also delete all groups and/or ledgers under {{$group_data->group_name}}</em> </p>

    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>
