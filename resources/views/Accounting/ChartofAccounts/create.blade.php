@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item active" aria-current="page">Chart of Accounts</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Chart of Account</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('chart-account.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-nav-tab">
                                <ul class="nav nav-tabs nav-tab-category" role="tablist">
                                    <li class="nav-items"><a href="#account_group" class="nav-link active" data-toggle="tab">Account Group</a></li>
                                    <li class="nav-items"><a href="#account_ledger" class="nav-link" data-toggle="tab">Account Ledger</a></li>
                                </ul>
                                <div class="tab-content-about">
                                    <div class="tab-pane fade active show" id="account_group">
                                        <form method="POST"  class="form-horizontal" action="{{route('store.accountGroup')}}" enctype="multipart/form-data" id="account_group_form">
                                                {{ csrf_field() }}
                                                <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                        <label for="status_name">Group Name</label>
                                                        <input type="text" class="form-control" name="group_name" placeholder="Group Name">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="status_type">Parent Group (Optional)</label>
                                                        <select class="form-control" name="parent_group" >
                                                            <option value="">Select Parent Group</option>
                                                            @foreach($all_account_groups as $account_group)
                                                                <option value="{{$account_group->id}}">{{$account_group->group_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label >Description (Optional)</label>
                                                        <textarea class="form-control" name="description" placeholder="Description"></textarea>
                                                    </div>
                                                </div>
                                            <div class="form-group" align="right" >
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="account_ledger">
                                        <form method="POST" id="account_ledger_form" class="form-horizontal" enctype="multipart/form-data" action="{{route('store.accountLedger')}}">
                                            {{ csrf_field() }}
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="status_type">Account Group</label>
                                                    <select class="form-control" name="group_name">
                                                        <option value="">Select Account Group</option>
                                                        @foreach($account_groups as $account_group)
                                                            <option value="{{$account_group->id}}">{{$account_group->group_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Ledger Name</label><br>
                                                    <input type="text" class="form-control" name="ledger_name" placeholder="Ledger Name">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Account Code</label><br>
                                                    <input type="text" class="form-control" name="account_code" placeholder="Account Code">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="status_type">Ledger Group (Optional)</label>
                                                    <select class="form-control" name="parent_ledger">
                                                        <option value="">Select Ledger Group</option>
                                                        @foreach($account_ledgers as $account_ledger)
                                                            <option value="{{$account_ledger->id}}">{{$account_ledger->ledger_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="account_type">Account Type</label>
                                                    <select class="form-control" name="account_type" id="account_type">
                                                        <option value="">Select Account Type</option>
                                                        <option>Debit</option>
                                                        <option>Credit</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label >Description (Optional)</label>
                                                    <textarea class="form-control" name="description" placeholder="Description"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <h6>Would you like to generate a budget from an existing account?</h6>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="switch" >
                                                        <input type="checkbox"  onclick="genBudget()" name="assign_budget">
                                                        <span class="slider"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="sample_id" style="display: none">
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Account</label>
                                                        <select class="form-control" name="assign_budget_account">
                                                            <option value="">Select an Account Group & Ledger</option>
                                                            @foreach($all_account_groups as $account_group)
                                                                <option value="0-{{$account_group->id}}">Group: {{$account_group->group_name}}</option>
                                                            @endforeach
                                                            @foreach($account_ledgers as $account_ledger)
                                                                <option value="1-{{$account_ledger->id}}">Ledger: {{$account_ledger->ledger_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Budget Percentage</label>
                                                        <input type="text" class="form-control" value="" max="100" name="assign_percentage" placeholder="%">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Attachment Name</label>
                                                        <input type="text" class="form-control" value="" name="attachment_name" placeholder="Budget Percentage">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Attachments</label><br>
                                                        <input type="file" name="attachments">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" align="right" >
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
@endsection

@push('scripts')
    <script>
        let group_rules = {
            group_name : { required: true },
        };
        $('#account_group_form').registerFields(group_rules);

        let ledger_rules = {
            ledger_name : { required: true},
            group_name : { required: true},
            account_code : { required: true},
            account_type : { required: true}
        };
        $('#account_ledger_form').registerFields(ledger_rules);
        function genBudget() {
            var x = document.getElementById("sample_id");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

    </script>
@endpush
<style>
    h6{
        line-height: 36px !important;
    }
    .margin-top{
        margin-top:50px;
    }
    /* Tabs for Chart of Accounts */
    .about-nav-tab .nav-tabs {
        margin-bottom: 40px;
    }
    .about-nav-tab .nav-tabs .nav-link.active {
        border-color: #132644 !important;
    }
    .about-nav-tab .nav-tabs .nav-link.active {
        color: white;
        background-color: #132644;
    }
    .about-nav-tab .nav-tabs .nav-link {
        color: black;
        border: none;
    }
    .about-nav-tab .nav-tabs .nav-items .nav-link {
        font-weight: 600;
        padding: 18px 30px;
        line-height: 1;
        border-color: #132644;
        font-size: 20px;
    }
    .nav-tab-category {
        border-bottom: 3px solid #132644 !important
    }
    .tab-content-about > .tab-pane.active {
        display: block;
    }
    .about-nav-tab .tab-content-about .tab-pane p {
        margin-bottom: 25px;
        font-size: 18px;
        color: aliceblue;
    }
    .about-nav-tab .tab-content-about .tab-pane ul {
        margin-bottom: 43px;
    }
    .tab-content-about>.tab-pane {
        display: none;
    }


    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }
</style>
