@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item" >Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Payroll</li>
        </ol>
    </nav>
    <h4 class="form-head">View Payroll</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{ route('payroll.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" id="" action="" enctype="multipart/form-data">
                <div align="center"><h5>Period Covered</h5></div>
                <div align="center">
                    <h3><strong>{{App\Common::convertWordDateFormat($payroll->date_from)}} - {{App\Common::convertWordDateFormat($payroll->date_to)}}</strong></h3>
                </div>
                    <div class="form-row">      
                        <div class="form-group col-md-6">
                            <label><b>Barangay</b></label><br>
                            <p>&nbsp;{{ $payroll->barangay }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>City/Municipality</b></label><br>
                            <p>&nbsp;{{ $payroll->city_municipality }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>Barangay Treasurer</b></label><br>
                            <p>&nbsp;{{ $payroll->treasurer }}</p>
                        </div>
                        <div class="form-group col-md-4">
                            <label><b>Province</b></label><br>
                            <p>&nbsp;{{ $payroll->province }}</p>
                        </div>
                        <div class="form-group col-md-2" >
                            <label><b>Payroll No.</b></label><br>
                            <p>&nbsp;{{ $payroll->payroll_number }}</p>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table-sm display responsive table-striped table-bordered dataTable no-footer" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                <thead class="tab_header">
                                    <tr align="center">
                                        
                                        <th style="text-align: center; width: 30.2px;"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">No.</th>
                                        <th style="text-align: center; width: 230.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Name</th>
                                        <th style="text-align: center; width: 20.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Position</th>
                                        <th style="text-align: center; width: 170.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Salaries & Wages</th>
                                        <th style="text-align: center; width: 130.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Honoraria</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Other Benefits</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Total</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">BIR w/holding Tax</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Total</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Net Amount Due</th>
                                        <!-- <th style="text-align: center; width: 80.2px;" class="pt2" rowspan="1" colspan="1" style="width: 83.2px;" aria-label="Action">Estimated Cost</th> -->
                                    </tr>
                                </thead>
                                <tbody id="table_purchase">
                                    @foreach($payroll->getPayrollItems as $key => $payrollItem)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $payrollItem->name }}</td>
                                            <td>{{ $payrollItem->position }}</td>
                                            <td>{{ number_format($payrollItem->salary,2) }}</td>
                                            <td>{{ number_format($payrollItem->honoraria,2) }}</td>
                                            <td>{{ number_format($payrollItem->other_benefits,2) }}</td>
                                            <td>{{ number_format($payrollItem->salary + $payrollItem->honoraria + $payrollItem->other_benefits,2) }}</td>
                                            <td>{{ number_format($payrollItem->bir,2) }}</td>
                                            <td>{{ number_format($payrollItem->bir,2) }}</td>
                                            <td>{{ number_format($payrollItem->salary + $payrollItem->honoraria + $payrollItem->other_benefits - $payrollItem->bir,2) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div align="right">
                                <label class="total-cost">Total&nbsp;<span><b>₱{{ $total_payroll }}</b></span></label>
                            </div>
                        </div>
                    </div>
                    @if(!is_null($payroll->getPayment))
                        <fieldset>
                            <legend>Payment Details</legend>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Amount Received:</label>
                                    <p>₱ {{ $total_payroll }}</p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Received Date:</label>
                                    <p>{{ App\Common::convertWordDateFormat($payroll->getPayment->date_received) }}</p>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Paid Into:</label>
                                    <p>{{ $payroll->getPayment->getBank->name }}</p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Payment Method:</label>
                                    <p>{{ $payroll->getPayment->getPaymentMethod->name }}</p>
                                </div>
                            </div>
                        </fieldset>
                    @endif
                </form>
            </div>
            <div class="form-group" align="right" >
                @if(is_null($payroll->getPayment))
                    <a type="submit" href="{{ route('payment.create',['type' => 'payroll','id' => $payroll->id]) }}" class="btn btn-primary" data-form="print-invoice"><i class="fa fa-print"></i>&nbsp;PAYMENTS</a>
                @endif

                <button class="btn btn-primary print-form-css" data-form="print_payroll"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
            </div>
        </div>
    </div>
    <div id="print_payroll" hidden>
        @include('Accounting.Payroll.print')
    </div>
</div>

@endsection

<style>
    .input {
        border: none;
        background-color: transparent;
    }
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
</style>