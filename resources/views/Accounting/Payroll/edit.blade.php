@extends('layouts.app')

@section('content')
<div class="container pb-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item" >Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Payroll</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Payroll</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('payroll.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <form method="POST"  class="form-horizontal" id="payroll_form" action="{{ route('payroll.update', $payroll->id) }}" enctype="multipart/form-data">
                <div class="errors"></div>
                <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}
                <div align="center"><h5>Period Covered</h5></div>
                <div class="form-row">
                    <div class="form-group col-md-6" align="right">   
                        <input type="" name="date_from" id="date_from" value="{{ App\Common::convertWordDateFormat($payroll->date_from) }}" placeholder="Date From" class="form-control datetimepicker" autocomplete="off" style="width:50%">
                    </div>
                    <span></span>
                    <div class="form-group col-md-6">              
                        <input type="" name="date_to" id="date_to" value="{{ App\Common::convertWordDateFormat($payroll->date_to) }}" placeholder="Date To" class="form-control datetimepicker" autocomplete="off" style="width:50%">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Barangay</label>
                        <input type="text" class="form-control" name="barangay" id="barangay" value="{{ $payroll->barangay }}" placeholder="Barangay" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label>City/Municipality</label>
                        <input type="text" class="form-control" name="municipality" id="municipality" value="{{ $payroll->city_municipality }}" placeholder="City/Municipality" autocomplete="off">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Barangay Treasurer</label>
                        <input type="text" class="form-control" name="treasurer" id="treasurer" value="{{ $payroll->treasurer }}" placeholder="Barangay Treasurer" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Province</label>
                        <input type="text" class="form-control" name="province" id="province" value="{{ $payroll->province }}" placeholder="Province" autocomplete="off">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Payroll No.</label>
                        <input class="form-control payrollNovali" onkeypress="return isNumberKey(event);" name="payroll_number" id="payroll_number" value="{{ $payroll->payroll_number }}" placeholder="Payroll Number" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Due Date</label>
                        <input type="text" class="form-control datetimepicker" name="due_date" id="due_date" value="{{ App\Common::convertWordDateFormat($payroll->due_date) }}" placeholder="Due Date" autocomplete="off">
                    </div>
                </div>
                
                <div class="error-container"></div>
                <div style="overflow: auto;">
                    <table class="table ss table-striped table-bordered table-hover table-responsive dragscroll" id="table_canvassing">
                        <thead class="thead-dark">
                            <tr align="center">
                                <th style="text-align: center; width: 5%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending"></th>
                                <th width="5%" style="vertical-align:middle;"></th>
                                <th width="5%" style="vertical-align:middle;"></th>
                                <th width="10%" style="vertical-align:middle;"></th>
                                <th width="10%" style="vertical-align:middle;" colspan="4" class="ptpb-0rm">Compensation</th>
                                <th width="10%" style="vertical-align:middle;" colspan="2" class="ptpb-0rm">Deductions</th>
                                <th width="15%" style="vertical-align:middle;"></th>
                            </tr>
                            <tr align="center">
                            <th style="text-align: center; width: 5%"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending"></th>
                                <th width="5%" style="vertical-align:middle;" rowspan="2">No.</th>
                                <th width="5%" style="vertical-align:middle;" rowspan="2">Name</th>
                                <th width="10%" style="vertical-align:middle;" rowspan="2">Position</th>
                                <th width="10%" style="vertical-align:middle;">Salary Wages</th>
                                <th width="10%" style="vertical-align:middle;">Honoraria</th>
                                <th width="15%" style="vertical-align:middle;">Other Benefits</th>
                                <th width="10%" style="vertical-align:middle;">Total</th>
                                <th width="15%" style="vertical-align:middle;">BIR w/ holding tax</th>
                                <th width="10%" style="vertical-align:middle;">Total</th>
                                <th width="25%" style="vertical-align:middle;" rowspan="2">Net Amount Due</th>
                            </tr>
                        </thead>
                        <tbody id="appendRowData" name="appendRowData">
                            @foreach($payroll_items as $data => $payroll_item)
                                <tr>
                                    <input type="hidden" class="for_editing" name="for_editing[]" value="{{ $payroll_item->id }}">
                                    <td>&nbsp;</td>
                                    <td class="item_no v-align" style="text-align: center;">{{ $count = ++$data }}</td>
                                    <td><input type="text" name="name{{ $payroll_item->id }}" id="name{{ $payroll_item->id }}" class="form-control" data-error=".error-container" value="{{ $payroll_item->name }}" style="width: 200px"></td>
                                    <td><input type="text" name="position{{ $payroll_item->id }}" id="position{{ $payroll_item->id }}" class="form-control" data-error=".error-container" value="{{ $payroll_item->position }}" style="width: 150px"></td>
                                    <td><input type="text" name="salary{{ $payroll_item->id }}" id="salary{{ $payroll_item->id }}" class="payroll salary price form-control" data-error=".error-container" value="{{ number_format($payroll_item->salary,2) }}" style="width: 100px"></td>
                                    <td><input type="text" name="honoraria{{ $payroll_item->id }}" id="honoraria{{ $payroll_item->id }}" class="payroll honoraria price form-control" data-error=".error-container" value="{{ number_format($payroll_item->honoraria,2) }}" style="width: 100px"></td>
                                    <td><input type="text" name="other{{ $payroll_item->id }}" id="other{{ $payroll_item->id }}" class="payroll other price form-control" data-error=".error-container" value="{{ number_format($payroll_item->other_benefits,2) }}" style="width: 100px"></td>
                                    <td><input type="text" name="total{{ $payroll_item->id }}" id="total{{ $payroll_item->id }}" class="subttotal form-control" value="{{ $payroll_item->salary + $payroll_item->honoraria + $payroll_item->other_benefits }}" readonly style="width: 150px"></td>
                                    <td><input type="text" name="bir{{ $payroll_item->id }}" id="bir{{ $payroll_item->id }}" class="payroll bir price form-control" data-error=".error-container" value="{{ number_format($payroll_item->bir) }}" style="width: 100px"></td>
                                    <td><input type="text" name="ttotal{{ $payroll_item->id }}" id="ttotal{{ $payroll_item->id }}" class="payroll subtotal form-control" value="{{ $payroll_item->bir }}" readonly style="width: 150px"></td>
                                    <td><input type="text" name="net_amount{{ $payroll_item->id }}" id="net_amount{{ $payroll_item->id }}" class="subnettotal form-control" value="{{ $payroll_item->salary + $payroll_item->honoraria + $payroll_item->other_benefits - $payroll_item->bir }}" readonly style="width: 150px"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-md-6" align="left">
                        <button class="btn btn-primary addNew" type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus-square" aria-hidden="true"></i> ADD ROW</button>
                    </div>
                    <div class="col-5" align="right">
                        <label class="total-cost">Total&nbsp;:&nbsp;₱&nbsp; <input class="input" type="text" id="totalcost" name="totalcost" readonly value="{{ $total_payroll }}" style="margin-right: -180px;"></b></span></label>
                    </div>
                </div>
                <div class="form-group" align="right" >
                    <button type="submit" class="btn btn-primary" name="btn-submit" id="btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .ptpb-0rm{
        padding-top: 0rem!important;
        padding-bottom: 0rem!important;
    }
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
    .input {
        border: none;
        background-color: transparent;
    }
</style>
@push('scripts')
<script>
    let rules = {
        barangay : {
            required: true
        },
        municipality: {
            required: true
        },
        treasurer : {
            required: true
        },
        province: {
            required: true
        },
        payroll_number : {
            required: true
        },
        date_from: {
            required: true
        },
        date_to : {
            required: true
        },
        name0 : {
            required: true
        },
        position0 : {
            required: true
        },
        honoraria0 : {
            required: true
        },
    };
    $('#payroll_form').registerFields(rules,false);
    $('.payrollNovali').on('keyup', function () {
        $.ajax({
            url: "{{route('payroll.validation')}}",
            data: {
                id : $('.payrollNovali').val()
            },
            method: "GET",
            success: function(data){
                if(data == 'true'){
                    toastMessage('Oopps!','This Payroll No. already taken , Please Use Another Payroll No',"error");
                }
            }
            
            
        });
    });
    $('#appendRowData').on('keyup', '.payroll', function () {
        let total = 0;
        let compensation = 0;
        let deductions = 0;
        let bir;
        let salary = 0;
        let honoraria = 0;
        let other = 0;
        if($(this).hasClass('salary') || $(this).hasClass('honoraria') || $(this).hasClass('other')){
            if($(this).hasClass('salary')){
                salary = Number($(this).val());
                honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
                other = Number($(this).parent().siblings('td').children('.other').val());
            };
            if($(this).hasClass('honoraria')){
                honoraria = Number($(this).val());
                salary = Number($(this).parent().siblings('td').children('.salary').val());
                other = Number($(this).parent().siblings('td').children('.other').val());
            };
            if($(this).hasClass('other')){
                other = Number($(this).val());
                salary = Number($(this).parent().siblings('td').children('.salary').val());
                honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
            };
            deductions = Number($(this).parent().siblings('td').children('.bir').val());
            let compensation = salary + honoraria + other;
            $(this).parent().siblings('td').children('.subttotal').val(compensation);
            let nettoatl = salary + honoraria + other - deductions;
            if(nettoatl < 0){
                toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                $('#btn-submit').prop('disabled', true);
            }else{
                $('#btn-submit').prop('disabled', false);
            }
            $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);

        }
        if($(this).hasClass('bir')){
            if($(this).hasClass('bir')){
                bir = Number($(this).val());
            };      
            compensation = Number($(this).parent().siblings('td').children('.subttotal').val());
            $(this).parent().siblings('td').children('.subtotal').val(bir);
            let nettoatl = compensation - bir;
            if(nettoatl < 0){
                toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                $('#btn-submit').prop('disabled', true);
            }else{
                $('#btn-submit').prop('disabled', false);
            }
            $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);

        }
        
        $('.subnettotal').each(function(){
            
            total +=  Number($(this).val());
        });
        
        $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
    });
    var count = '{{ $count + 1 }}' ;
    var countItem = 2;
    $(".addNew").click(function(e){
        e.preventDefault();
        $('#nodata').empty();
        $('#appendRowData').append('<tr class="appendRow" align="center"><input type="hidden"  class="for_payroll" name="for_payroll[]" value="'+count+'">'+
            '<td>'+
                '<button class="btn btn-primary RemoveBtn" type="button" id="dropdownMenuButton"  aria-haspopup="true" aria-expanded="false" >'+
                    '<i class="fa fa-minus"></i>'+
                '</button>'+              
            '</td>'+
            '<td class="item_no v-align">'+
                countItem+
            '</td>'+
            '<td name="name">'+
                '<input type="text" name="name'+count+'" id="name'+count+'" class="form-control newInput" data-error=".error-container" placeholder="Name">'+      
            '</td>'+
            '<td name="position">'+
                '<input type="text" name="position'+count+'" id="position'+count+'" class="form-control newInput" data-error=".error-container" placeholder="Position">'+      
            '</td>'+
            '<td name="salary">'+
                '<input type="number" name="salary'+count+'" id="salary'+count+'" class="payroll salary form-control newInput" placeholder="Salary Wages">'+      
            '</td>'+
            '<td name="honoraria">'+
                '<input type="number"  name="honoraria'+count+'" id="honoraria'+count+'" class="payroll honoraria form-control newInput" data-error=".error-container" placeholder="Honoraria">'+      
            '</td>'+
            '<td name="other">'+
                '<input  type="number" name="other'+count+'" id="other'+count+'" class="payroll other form-control newInput" data-error=".error-container" placeholder="Other Benefits">'+      
            '</td>'+
            '<td name="total1">'+
                '<input type="number" class="subttotal form-control" placeholder="Total" readonly>'+      
            '</td>'+
            '<td name="bir">'+
                '<input type="number" name="bir'+count+'" id="bir'+count+'" class="payroll bir form-control newInput" data-error=".error-container" placeholder="BIR w/ holding tax">'+      
            '</td>'+
            '<td name="total2">'+
                '<input type="number" class="subtotal form-control" placeholder="Total" readonly>'+      
            '</td>'+
            '<td name="net_amount">'+
                '<input type="number"  class="subnettotal form-control" placeholder="Net Amount Due" readonly>'+      
            '</td>'+
        '</tr>'
        );
        updateItem(); // update item number
        count ++;
        countItem ++; 
        $('.newInput').each( function () {
            $(this).rules('add',{
                'required' : true
            })
        });
        $('#appendRowData').on('keyup', '.payroll', function () {
            let total = 0;
            let compensation = 0;
            let deductions = 0;
            let bir;
            let salary = 0;
            let honoraria = 0;
            let other = 0;
            if($(this).hasClass('salary') || $(this).hasClass('honoraria') || $(this).hasClass('other')){
                if($(this).hasClass('salary')){
                    salary = Number($(this).val());
                    honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
                    other = Number($(this).parent().siblings('td').children('.other').val());
                };
                if($(this).hasClass('honoraria')){
                    honoraria = Number($(this).val());
                    salary = Number($(this).parent().siblings('td').children('.salary').val());
                    other = Number($(this).parent().siblings('td').children('.other').val());
                };
                if($(this).hasClass('other')){
                    other = Number($(this).val());
                    salary = Number($(this).parent().siblings('td').children('.salary').val());
                    honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
                };
                deductions = Number($(this).parent().siblings('td').children('.bir').val());
                $(this).parent().siblings('td').children('.subttotal').val(salary + honoraria + other);
                let nettoatl = salary + honoraria + other - deductions;
                if(nettoatl < 0){
                    toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                    $('#btn-submit').prop('disabled', true);
                }else{
                    $('#btn-submit').prop('disabled', false);
                }
                $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);
            }
            if($(this).hasClass('bir')){
                if($(this).hasClass('bir')){
                    bir = Number($(this).val());
                };      
                compensation = Number($(this).parent().siblings('td').children('.subttotal').val());
                $(this).parent().siblings('td').children('.subtotal').val(bir);
                let nettoatl = compensation - bir;
                if(nettoatl < 0){
                    toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                    $('#btn-submit').prop('disabled', true);
                }else{
                    $('#btn-submit').prop('disabled', false);
                }
                 $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);
            }
            $('.subnettotal').each(function(){
                total += Number($(this).val());
            });
            $('.totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
        });
        
        $("#appendRowData").on('click','.RemoveBtn',function(e){
            e.preventDefault();
            updateItem();
            let total = 0;
            $(this).parent().parent().remove();     
            $('.subnettotal').each(function(){
                total += Number($(this).val());
            });
            $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
        });
    });
    function updateItem(){
        var rowCount = 0;
        var ctr = 1;
        var table = document.getElementById("appendRowData");
        rowCount = table.rows.length;
        $('.item_no').each( function () {
            var temp = rowCount - ctr; 
            var temp2 = rowCount - temp;
            $(this).html(temp2);
            ctr++
            // console.log(temp);
            console.log(rowCount);
        });
        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
            console.log(charCode);
            if (charCode != 46 && charCode != 45 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $(".price").number(true,2);

</script>
@endpush
@endsection
