@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item" >Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Payroll</li>
        </ol>
    </nav>
    <h4 class="form-head">Payroll List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('payroll.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table table-striped table-bordered table-hover text-center table-mediascreen" id="payroll_list">
                <thead class="thead-dark">
                    <tr align="center">
                    <th>Date From</th>
                    <th>Date To</th>
                    <th>Payroll No.</th>
                    <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
        var oTable = $("#payroll_list").DataTable(
        {
            pagingType: "full_numbers",
            processing: true,
            //serverSide: true,
            type: "GET",
            ajax:"{!! route('get.payrollList',0000) !!}",
            columns:[
                {data: "date_from","name":"date_from","width":"25%", className:"v-align text-center"},
                {data: "date_to","name":"date_to","width":"25%", className:"v-align text-center"},
                {data: "payroll_no","name":"payroll_no","width":"25%", className:"v-align text-center"},
                {data: "action","name":"action",orderable:false,searchable:false,"width":"25%"},
            ],
            select: {
                style: "multi"
            }
        }
    );
    $('#budget_year').change(function(){
        var year = $(this).val();
        var url = '{{ route("get.payrollList", ':year') }}';
        url = url.replace(':year', $(this).val());
        oTable.ajax.url(url).load();
    });
</script>
@endpush
