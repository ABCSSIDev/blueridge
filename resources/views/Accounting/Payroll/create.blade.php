@extends('layouts.app')

@section('content')

<div class="container pb-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item" >Transactions</li>
            <li class="breadcrumb-item" >Bills</li>
            <li class="breadcrumb-item active" aria-current="page">Payroll</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Payroll</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('payroll.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <form method="POST"  class="form-horizontal" id="payroll_form" action="{{ route('payroll.store')}}" enctype="multipart/form-data">
                <div class="errors"></div>
                {{csrf_field()}}
                <div align="center"><h5>Period Covered</h5></div>
                <div class="form-row">
                    <div class="form-group col-md-6" align="right">   
                        <input type="" name="date_from" id="date_from" placeholder="Date From" class="form-control datetimepicker" autocomplete="off" style="width:50%">
                    </div>
                    <span></span>
                    <div class="form-group col-md-6">              
                        <input type="" name="date_to" id="date_to" placeholder="Date To" class="form-control datetimepicker" autocomplete="off" style="width:50%">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Barangay</label>
                        <input type="text" class="form-control" name="barangay" id="barangay" placeholder="Barangay" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label>City/Municipality</label>
                        <input type="text" class="form-control" name="municipality" id="municipality" placeholder="City/Municipality" autocomplete="off">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Barangay Treasurer</label>
                        <input type="text" class="form-control" name="treasurer" id="treasurer" placeholder="Barangay Treasurer" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Province</label>
                        <input type="text" class="form-control" name="province" id="province" placeholder="Province" autocomplete="off">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Payroll No.</label>
                        <input class="form-control" onkeypress="return isNumberKeyDash(event);" name="payroll_number" id="payroll_number" placeholder="Payroll Number" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Due Date</label>
                        <input type="text" class="form-control datetimepicker" name="due_date" id="due_date" placeholder="Due Date" autocomplete="off">
                    </div>
                </div>
                
                    <div class="error-container"></div>
                    <div style="overflow: auto;">
                    <table class="table table-striped table-bordered table-hover table-responsive dragscroll no-wrap text-center" id="table_canvassing">
                        <thead class="thead-dark">
                            <tr align="center">
                                <th width="5%" rowspan="2"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending"></th>
                                <th width="5%" class="v-align" rowspan="2">No.</th>
                                <th width="5%" class="v-align" rowspan="2">Name</th>
                                <th width="10%" class="v-align" rowspan="2">Position</th>
                                <th width="10%" class="v-align" colspan="4" class="ptpb-0rm">Compensation</th>
                                <th width="10%" class="v-align" colspan="2" class="ptpb-0rm">Deductions</th>
                                <th width="25%" class="v-align" rowspan="2">Net Amount Due</th>
                            </tr>
                            <tr align="center">
                                <th width="10%" class="v-align">Salary Wages</th>
                                <th width="10%" class="v-align">Honoraria</th>
                                <th width="15%" class="v-align">Other Benefits</th>
                                <th width="10%" class="v-align">Total</th>
                                <th width="15%" class="v-align">BIR w/ holding tax</th>
                                <th width="10%" class="v-align">Total</th>
                            </tr>
                        </thead>
                        <tbody id="appendRowData" name="appendRowData">
                            <tr>
                                <input type="hidden" class="for_payroll" name="for_payroll[]" value="0">
                                <td></td>
                                <td class="v-align">1</td>
                                <td><input type="text" name="name0" id="name0"  class="form-control" data-error=".error-container" style="width: 200px" placeholder="Name"></td>
                                <td><input type="text" name="position0" id="position0" class="form-control" data-error=".error-container" style="width: 150px" placeholder="Position"></td>
                                <td><input type="number" name="salary0" id="salary0" class="payroll salary price form-control" placeholder="Salary Wages"></td>
                                <td><input type="number" name="honoraria0" id="honoraria0" class="payroll honoraria price form-control" data-error=".error-container" style="width: 100px" placeholder="Honoraria"></td>
                                <td><input type="number" name="other0" id="other0" class="payroll other price form-control" data-error=".error-container" placeholder="Other Benefits"></td>
                                <td><input type="number" name="total0" id="total0" class="subttotal form-control" readonly style="width: 150px" placeholder="Total"></td>
                                <td><input type="number" name="bir0" id="bir0" class="payroll bir price form-control" data-error=".error-container" placeholder="BIR w/ holding tax	"></td>
                                <td><input type="number" name="ttotal0" id="ttotal0" class="payroll subtotal form-control" readonly style="width: 150px" placeholder="Total"></td>
                                <td><input type="number" name="net_amount0" id="net_amount0" class="subnettotal form-control" readonly style="width: 150px" placeholder="Net Amount Due"></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>

                <div class="row">
                    <div class="col-md-6" align="left">
                        <button class="btn btn-primary addNew" type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus-square" aria-hidden="true"></i> ADD ROW</button>
                    </div>
                    <div class="col-md-6" align="right">
                        <label class="total-cost">Total&nbsp;:&nbsp;₱&nbsp;<b> <input class="input" type="text" id="totalcost" name="totalcost" readonly style="margin-right: -150px;"></b></span></label>
                    </div>
                </div>
                <div class="form-group" align="right" style="margin-top: 2rem;">
                    <button type="submit" class="btn btn-primary" name="btn-submit" id="btn-submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .input {
        border: none;
        background-color: transparent;
    }
</style>

@push('scripts')
<script>
    $('#totalcost').val('0.00');
    let rules = {
        barangay : {
            required: true
        },
        municipality: {
            required: true
        },
        treasurer : {
            required: true
        },
        province: {
            required: true
        },
        payroll_number : {
            required: true
        },
        date_from: {
            required: true
        },
        date_to : {
            required: true
        },
        name0 : {
            required: true
        },
        position0 : {
            required: true
        },
        salary0 : {
            required: true
        },
        honoraria0 : {
            required: true
        },
    };
    $('#payroll_form').registerFields(rules);
    var count = 1;
    var countItem = 2;
    $('#appendRowData').on('keyup', '.payroll', function () {
        let total = 0;
        let compensation = 0;
        let deductions = 0;
        let bir;
        let salary = 0;
        let honoraria = 0;
        let other = 0;
        if($(this).hasClass('salary') || $(this).hasClass('honoraria') || $(this).hasClass('other')){
            if($(this).hasClass('salary')){
                salary = Number($(this).val());
                honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
                other = Number($(this).parent().siblings('td').children('.other').val());
            };
            if($(this).hasClass('honoraria')){
                honoraria = Number($(this).val());
                salary = Number($(this).parent().siblings('td').children('.salary').val());
                other = Number($(this).parent().siblings('td').children('.other').val());
            };
            if($(this).hasClass('other')){
                other = Number($(this).val());
                salary = Number($(this).parent().siblings('td').children('.salary').val());
                honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
            };
            deductions = Number($(this).parent().siblings('td').children('.bir').val());
            $(this).parent().siblings('td').children('.subttotal').val(salary + honoraria + other);
            let nettoatl = salary + honoraria + other - deductions;
            if(nettoatl < 0){
                toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                $('#btn-submit').prop('disabled', true);
            }else{
                $('#btn-submit').prop('disabled', false);
            }
            $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);

        }
        if($(this).hasClass('bir')){
            if($(this).hasClass('bir')){
                bir = Number($(this).val());
            };      
            compensation = Number($(this).parent().siblings('td').children('.subttotal').val());
            $(this).parent().siblings('td').children('.subtotal').val(bir);
            let nettoatl = compensation - bir;
            if(nettoatl < 0){
                toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                $('#btn-submit').prop('disabled', true);
            }else{
                $('#btn-submit').prop('disabled', false);
            }
            $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);

        }
        $('.subnettotal').each(function(){
            total += Number($(this).val());
        });
        $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
    });
    $(".addNew").click(function(e){
        e.preventDefault();
        $('#nodata').empty();
        $('#appendRowData').append('<tr class="appendRow"><input type="hidden"  class="for_payroll" name="for_payroll[]" value="'+count+'">'+
            '<td>'+
                '<button class="btn btn-primary RemoveBtn" type="button" id="dropdownMenuButton"  aria-haspopup="true" aria-expanded="false" >'+
                    '<i class="fa fa-minus"></i>'+
                '</button>'+              
            '</td>'+
            '<td class="item_no">'+
                countItem+
            '</td>'+
            '<td name="name">'+
                '<input type="text" placeholder="Name" name="name'+count+'" id="name'+count+'" class="form-control newInput" data-error=".error-container">'+      
            '</td>'+
            '<td name="position">'+
                '<input type="text" placeholder="Position" name="position'+count+'" id="position'+count+'" class="form-control newInput" data-error=".error-container">'+      
            '</td>'+
            '<td name="salary">'+
                '<input type="number" placeholder="Salary Wages" name="salary'+count+'" id="salary'+count+'" class="payroll salary form-control newInput">'+      
            '</td>'+
            '<td name="honoraria">'+
                '<input type="number" placeholder="Honoraria"  name="honoraria'+count+'" id="honoraria'+count+'" class="payroll honoraria form-control newInput" data-error=".error-container">'+      
            '</td>'+
            '<td name="other">'+
                '<input  type="number" placeholder="Other Benefits" name="other'+count+'" id="other'+count+'" class="payroll other form-control newInput">'+      
            '</td>'+
            '<td name="total1">'+
                '<input type="number" placeholder="Total" class="subttotal form-control" readonly>'+      
            '</td>'+
            '<td name="bir">'+
                '<input type="number" placeholder="BIR w/ holding tax" name="bir'+count+'" id="bir'+count+'" class="payroll bir form-control newInput">'+      
            '</td>'+
            '<td name="total2">'+
                '<input type="number" placeholder="Total" class="subtotal form-control" readonly>'+      
            '</td>'+
            '<td name="net_amount">'+
                '<input type="number" placeholder="Net Amount Due" class="subnettotal form-control" readonly>'+      
            '</td>'+
        '</tr>'
        );  
        $('.newInput').each( function () {
            $(this).rules('add',{
                'required' : true
            })
        });
        updateItem(); // update item number
        $('#appendRowData').on('keyup', '.payroll', function () {
            let total = 0;
            let compensation = 0;
            let deductions = 0;
            let bir;
            let salary = 0;
            let honoraria = 0;
            let other = 0;
            if($(this).hasClass('salary') || $(this).hasClass('honoraria') || $(this).hasClass('other')){
                if($(this).hasClass('salary')){
                    salary = Number($(this).val());
                    honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
                    other = Number($(this).parent().siblings('td').children('.other').val());
                };
                if($(this).hasClass('honoraria')){
                    honoraria = Number($(this).val());
                    salary = Number($(this).parent().siblings('td').children('.salary').val());
                    other = Number($(this).parent().siblings('td').children('.other').val());
                };
                if($(this).hasClass('other')){
                    other = Number($(this).val());
                    salary = Number($(this).parent().siblings('td').children('.salary').val());
                    honoraria = Number($(this).parent().siblings('td').children('.honoraria').val());
                };
                deductions = Number($(this).parent().siblings('td').children('.bir').val());
                $(this).parent().siblings('td').children('.subttotal').val(salary + honoraria + other);
                let nettoatl = salary + honoraria + other - deductions;
                if(nettoatl < 0){
                    toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                    $('#btn-submit').prop('disabled', true);
                }else{
                    $('#btn-submit').prop('disabled', false);
                }
                $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);
            }
            if($(this).hasClass('bir')){
                if($(this).hasClass('bir')){
                    bir = Number($(this).val());
                };      
                compensation = Number($(this).parent().siblings('td').children('.subttotal').val());
                $(this).parent().siblings('td').children('.subtotal').val(bir);
                let nettoatl = compensation - bir;
                if(nettoatl < 0){
                    toastMessage('Warning!',"Deductions is greater than Compensation?","warning");
                    $('#btn-submit').prop('disabled', true);
                }else{
                    $('#btn-submit').prop('disabled', false);
                }
                 $(this).parent().siblings('td').children('.subnettotal').val(nettoatl);
            }
            $('.subnettotal').each(function(){
                total += Number($(this).val());
            });
            $('.totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
        });
        
        $("#appendRowData").on('click','.RemoveBtn',function(e){
            e.preventDefault();
            updateItem();
            let total = 0;
            $(this).parent().parent().remove();     
            $('.subnettotal').each(function(){
                total += Number($(this).val());
            });
            $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
        });
    count ++;
    countItem ++; 
    });
    function updateItem(){
        var rowCount = 1;
        var ctr = 2;
        var table = document.getElementById("appendRowData");
        rowCount = table.rows.length;
        $('.item_no').each( function () {
            var temp = rowCount - ctr; 
            var temp2 = rowCount -temp;
            $(this).html(temp2);
            ctr++
            console.log(rowCount);
        });
    }

    function isNumberKeyDash(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
            console.log(charCode);
            if (charCode != 46 && charCode != 45 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
@endpush
@endsection