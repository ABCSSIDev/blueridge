@include('layouts.header')
<table class="table table-bordered margin-top">
    <tbody>
        <tr align="center">
            <td colspan="11"><b>PAYROLL</b></br></br>Period Covered: <b>{{App\Common::convertWordDateFormat($payroll->date_from)}} - {{App\Common::convertWordDateFormat($payroll->date_to)}}</b>
        
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div>
                    <p>Barangay: <u>{{ $payroll->barangay }}</u></p>
                    <p>Barangay Treasurer: <u>{{ $payroll->treasurer }}</u></p>
                </div>
            </td>   
            <td colspan="4" style="border-left-style: hidden;border-right-style:hidden">
                <div>
                    <p>City/Municipality: <u>{{ $payroll->city_municipality }}</u></p>
                    <p>Province: <u>{{ $payroll->province }}</u></p>
                </div>
            </td> 
            <td colspan="3" style="vertical-align:bottom;text-align:right">  
                <p>Payroll No: <u>{{ $payroll->payroll_number }}</u></p>
            </td>     
        </tr> 
        <tr align="center">
            <td class="v-align" rowspan="2">No.</td>
            <td class="v-align" rowspan="2">Name</td>
            <td class="v-align" rowspan="2">Position</td>
            <td class="v-align" colspan="4" class="ptpb-0rm">Compensation</td>
            <td class="v-align" colspan="2" class="ptpb-0rm">Deductions</td>
            <td class="v-align" rowspan="2">Net Amount Due</td>
            <td class="v-align" rowspan="2">Signature of Recipient</td>
        </tr>
        <tr align="center">
            <td class="v-align">Salary & Wages</td>
            <td class="v-align">Honoraria</td>
            <td class="v-align">Other Benefits</td>
            <td  class="v-align">Total</td>
            <td class="v-align">BIR w/ holding tax</td>
            <td class="v-align">Total</td>
        </tr>
        @php $total_salary = 0; $total_honoraria = 0; $total_benefits = 0; $total_com = 0; $total_bir = 0; $total_net = 0; @endphp 
        @foreach($payroll->getPayrollItems as $payrollItem)
            @php
                $total_salary += $payrollItem->salary;
                $total_honoraria += $payrollItem->honoraria;
                $total_benefits += $payrollItem->other_benefits;
                $total_com += $payrollItem->salary + $payrollItem->honoraria + $payrollItem->other_benefits;
                $total_bir += $payrollItem->bir;
                $total_net += $payrollItem->salary + $payrollItem->honoraria + $payrollItem->other_benefits - $payrollItem->bir;
            @endphp
            <tr align="center">
                <td>{{ $payrollItem->id }}</td>
                <td>{{ $payrollItem->name }}</td>
                <td>{{ $payrollItem->position }}</td>
                <td>{{ number_format($payrollItem->salary,2) }}</td>
                <td>{{ number_format($payrollItem->honoraria,2) }}</td>
                <td>{{ number_format($payrollItem->other_benefits,2) }}</td>
                <td>{{ number_format($payrollItem->salary + $payrollItem->honoraria + $payrollItem->other_benefits,2) }}</td>
                <td >{{ number_format($payrollItem->bir,2) }}</td>
                <td>{{ number_format($payrollItem->bir,2) }}</td>
                <td>{{ number_format($payrollItem->salary + $payrollItem->honoraria + $payrollItem->other_benefits - $payrollItem->bir,2) }}</td>
                <td></td>
            </tr>
        @endforeach
        <tr align="center">
            <td ></td>
            <td>TOTAL</td>
            <td></td>
            <td>₱ {{ number_format($total_salary,2) }}</td>
            <td>₱ {{ number_format($total_honoraria,2) }}</td>
            <td>₱ {{ number_format($total_benefits,2) }}</td>
            <td>₱ {{ number_format($total_com,2) }}</td>
            <td></td>
            <td>₱ {{ number_format($total_bir,2) }}</td>
            <td>₱ {{ number_format($total_net,2) }}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <p><b>A. Certified</b> as to availability of appropriation for obligation in the amount of <b><u>₱{{ number_format($total_net,2) }}</u></b></p><br><br>
                    <p>Signature:______________</p>
                    <p>Printed Name: <b><u>Anna Francesca L. Maristela</u></b></p>
                    <p>Position: <u>Chairman, Com. on Appropriations</u></p>
                    <p>Date:___________________ </p>
                </div>
            </td>
            <td colspan="3">
                <div>
                    <p><b>B. Certified</b> as to availability of funds, and completeness and propriety of supporting documents.</p><br><br><br>
                    <p>Signature:______________</p>
                    <p>Printed Name: <b><u>Michelle V. Meniano</u></b></p>
                    <p>Position: <u>Barangay Treasurer</u></p>
                    <p>Date:___________________ </p>
                </div>
            </td>
            <td colspan="3">
                <div>
                    <p><b>C. Certified</b> as to validity, propriety, and legality of claim and approved for payment
                    </p><br><br><br>
                    <p>Signature:______________</p>
                    <p>Printed Name: <b><u>ESPERANZA CASTRO-LEE</u></b></p>
                    <p>Position: <u>Punong Barangay</u></p>
                    <p>Date:___________________ </p>
                </div>
            </td>
            <td colspan="3">
            <div>
                    <p><b>D. Certified</b> that each official/employee whose name appears on the above roll has been paid the amount stated opposite his name</p><br>
                    <p>Signature:______________</p>
                    <p>Printed Name: <b><u>Michelle V. Meniano</u></b></p>
                    <p>Position: <u>Barangay Treasurer</u></p>
                    <p>Date:___________________ </p>
                </div>
            </td>
        </tr>
    </tbody>
</table>    

<style>
    .margin-top{
        margin-top: 30px;
    }
    
    .v-align {
        vertical-align: middle !important;
    }
</style>