@extends('layouts.app')

@section('content')
<div class="container">
  <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item active" aria-current="page">Budget Allocation</li>
        </ol>
    </nav>
    <h4 class="form-head">View Budget Allocation</h4>
    <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('budget-allocation.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
    <div class="row justify-content-center mt-5 mb-sm-3">
      <table class="table  table-bordered table-hover table-sm">
        <tr>
          <td><b>TOTAL ESTIMATED FUNDS AVAILABLE FOR APPROPRIATION</b></td>
          <td class="right-align">₱&nbsp;{{ number_format($debit,2) }}</td>
        </tr>
        <tr>
          <td><b>TOTAL EXPENDITURE PROGRAM</b></td>
          <td class="right-align">₱&nbsp;{{ number_format($credit,2) }}</td>
        </tr>
        <tr>
          <td><b>ENDING BALANCE</b></td>
          <td class="right-align">₱&nbsp;{{ number_format((int)$debit - (int)$credit,2) }}</td>
        </tr>
      </table>
      <table class="table  table-bordered table-hover table-sm">
          <thead class="thead-dark">
              <tr align="center">
              <th width="60%">Account Group / Ledgers</th>
              <th width="10%">Account Code</th>
              <th width="30%">Action</th>
              </tr>
          </thead>
          <tbody >
            {!! $table !!}
          </tbody>
      </table>
    </div>
    <div class="form-group pb-4 mr-n14" align="right" >
        <button type="submit" class="btn btn-primary print-form" data-form="print-budget-allocation"><i class="fa fa-print"></i>&nbsp;PRINT</button>
    </div>
</div>

<div id="print-budget-allocation" hidden>
  @include('Accounting.BudgetAllocation.print')
</div>

@endsection


<style>

/* Tabs for Categories */
.about-nav-tab .nav-tabs {
  margin-bottom: 40px;
}
.about-nav-tab .nav-tabs .nav-link.active {
  border-color: #132644 !important;
}
.about-nav-tab .nav-tabs .nav-link.active {
    color: white;
    background-color: #132644;
}
.about-nav-tab .nav-tabs .nav-link {
  color: black;
  border: none;
}
.about-nav-tab .nav-tabs .nav-items .nav-link {
  font-weight: 600;
  padding: 18px 30px;
  line-height: 1;
  border-color: #132644;
  font-size: 20px;
}
.nav-tab-category {
  border-bottom: 3px solid #132644 !important
}
.tab-content-about > .tab-pane.active {
  display: block;
}
.about-nav-tab .tab-content-about .tab-pane p {
  margin-bottom: 25px;
  font-size: 18px;
  color: aliceblue;
}
.about-nav-tab .tab-content-about .tab-pane ul {
  margin-bottom: 43px;
}
.tab-content-about>.tab-pane {
  display: none;
}
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}
.switch input {display:none;}
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #8593ae;
  -webkit-transition: .4s;
  transition: .4s;
}
.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}
input:checked + .slider {
  background-color: rgba(10, 122, 211, 0.82);
}
input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}
input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}
.slider.round {
  border-radius: 34px;
}
.slider.round:before {
  border-radius: 50%;
}
.sw-top{
  margin-top: 20px;
}

.margin-top{
        margin-top:50px;
    }
.btn-color{
    background-color: #00007c !important;
    color: #fff !important;
}
.btn-pad{
    padding: 6px 25px 6px 25px !important;
}
.btn-text{
    color: #fff !important;
}
.text-indent{
    text-indent: 10px;
}
.indent-two{
    text-indent: 20px;
}
.indent-three{
    text-indent: 30px;
}
.indent-four{
    text-indent: 40px;
}
.tb-color{
    background-color:#0000000d;
}
ol.basic{
    margin-bottom: 0px !important;
}
.mr-n14{
  margin-right: -14px;
}
</style>
