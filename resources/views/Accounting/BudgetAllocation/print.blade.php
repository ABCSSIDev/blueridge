
<div class="print-budget-allocation">
    @include('layouts.header')
    <div align="center">
        <p class="pad">APPROPRIATION ORDINANCE NO. 01<br>Series of {{ $allocation->year }}</p>
        <p class="margin-b"><b>AN ORDINANCE AUTHORIZING THE ANNUAL BUDGET OF BARANGAY BLUE RIDGE B FOR<br> 
        FISCAL YEAR {{ $allocation->year+1 }} IN THE TOTAL AMOUNT OF {{ strtoupper(App\Common::NumberToWords($debit,true)) }} (Php {{ number_format($debit,2) }}) AND APPROPRIATING THE NECESSARY FUNDS FOR THE PURPOSE</b></p>
        <hr>
        <p>Introduced by: Kagawad Katherine T. de Jesus<br>Kagawad Anna Francesca L. Maristela</p>
        <hr>
    </div>
    <!-- <div class="break-page">
        @include('layouts.header')
        <p class="margin-t"><b>Section 3:</b> Expenditures Program cover the following areas of operations:</p><br>
        <table width="100%">
            <thead>
                <tr align="center">
                    <th width="50%">PART II: EXPENDITURE PROGRAM</th>
                    <th width="25%">Account Code</th>
                    <th width="25%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr class="tb-color">
                <td class="text-indent">|&nbsp;<b>GENERAL ADMINISTRATION PROGRAM (GAP)</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="text-indent">|&nbsp;CURRENT OPERATING EXPENDITURES</td>
                <td></td>
                <td></td>
                </tr>
                <tr class="tb-color">
                <td class="text-indent"><b>|&nbsp;Personal Services (PS)</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="text-indent">|&nbsp;Other Compensation</td>
                <td align="center">5-01-02</td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;Honoraria</td>
                <td align="center">5-01-02-100</td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;Cash Gift</td>
                <td align="center">5-01-02-150</td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;Year End Bonus</td>
                <td align="center">5-01-02-140</td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr>
                <td class="text-indent">|&nbsp;Personnel Benefit Contributions</td>
                <td align="center">5-01-03</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Retirement and Life Insurance Premiums</td>
                <td align="center">5-01-03-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="text-indent">|&nbsp;Other Personnel Benefits</td>
                <td align="center">5-01-04</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Other Personnel Benefits</td>
                <td align="center">5-01-04-990</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr class="tb-color">
                <td class="text-indent">|&nbsp;<b>Total PS</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>

                <tr class="tb-color">
                <td class="text-indent"><b>|&nbsp;Maintenance & Other Operating Expenses (MOOE)</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Travelling Expenses - Local</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Fuel, Oil and Lubricants Expenses</td>
                <td align="center">5-02-03-090</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Training and Scholarship Expenses</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Training Expenses</td>
                <td align="center">5-02-02-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Supplies and Materials Expenses</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Office Supplies Expenses</td>
                <td align="center">5-02-03-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Accountable Forms Expenses</td>
                <td align="center">5-02-03-020</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Other Supplies and Materials Expenses</td>
                <td align="center">5-02-03-990</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Printing and Publication Expenses</td>
                <td align="center">5-02-99-020</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Utility Expenses</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Water Expenses</td>
                <td align="center">5-02-04-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Electricity Expenses</td>
                <td align="center">5-02-04-020</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Communication Expenses</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Postage and Courier Services</td>
                <td align="center">5-02-05-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Telephone Expenses</td>
                <td align="center">5-02-05-020</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Professional Services</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Other Professional Services</td>
                <td align="center">5-02-11-990</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>General Services</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Environment/Sanitary Services</td>
                <td align="center">5-02-12-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Other General Services</td>
                <td align="center">5-02-12-990</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Repairs and Maintenance</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Repairs and Maintenance - Machinery and Equipment</td>
                <td align="center">5-02-13-050</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;Repairs and Maintenance - Other Property, Plant and Equipment</td>
                <td align="center">5-02-13-990</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Taxes, Insurance Premiums and Other Fees</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Fidelity Bond Premiums</td>
                <td align="center">5-02-16-020</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Legal Services</td>
                <td align="center">5-02-11-010</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Other MOOE</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Other Maintenance and Operating Expenses</td>
                <td align="center">5-02-99-990</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Membership Dues and Contributions to Organizations</td>
                <td align="center">5-02-99-060</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Extraordinary and Miscellaneous Expenses</td>
                <td align="center">5-02-10-030</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Transportation and Delivery Expenses</td>
                <td align="center">5-02-99-040</td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr class="tb-color">
                <td class="indent-three">|&nbsp;<b>Total MOOE</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;<b>Financial Expenses</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;<b>Total Financial Expenses</b></td>
                <td></td>
                <td align="center">	₱0</td>
                </tr>
                <tr class="tb-color">
                <td class="text-indent"><b>|&nbsp;Capital Outlay (CO)</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;<b>Total Capital Outlay</b></td>
                <td></td>
                <td align="center">	₱0</td>
                </tr>
                <tr class="tb-color">
                <td class="text-indent"><b>|&nbsp;Special Purpose Appropriation (SPA)</b></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;Appropriation for Sangguniang Kabataan</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;Appropriation for Infrastructure (20% Devt Fund)</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-two">|&nbsp;Appropriation for BDRRMF</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td class="indent-three">|&nbsp;Disaster Preparedness, Prevention & Mitigation</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr>
                <td class="indent-four">|&nbsp;Response, Rehabilitation & Recovery</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr class="tb-color">
                <td class="indent-three">|&nbsp;<b>Total SPA</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr class="tb-color">
                <td class="text-indent"><b>|&nbsp;TOTAL GENERAL APPROPRIATION PROGRAM</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
            </tbody>
        </table>
    </div> -->
    <!-- <div class="break-page">
        @include('layouts.header')
        <p class="margin-t"><b>Section 4:</b> General Provisions for the following Basic Services and Facilities are also adopted:</p>
        <table width="100%">
            <thead>
                <tr align="center">
                    <th width="50%">BASIC SERVICES AND FACILITIES PROGRAM</th>
                    <th width="25%">Account Code</th>
                    <th width="25%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td><ol class="basic" type="a"><li>Day Care</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><ol class="basic" type="a" start="2"><li>Health & Nutrition</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;MOOE</td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><ol class="basic" type="a" start="3"><li>BPOS/BADAC</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;MOOE</td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><ol class="basic" type="a" start="4"><li>Agricultural Support</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><ol class="basic" type="a" start="5"><li>Katarungang Pambarangay</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><ol class="basic" type="a" start="6"><li>Information & Reading Center</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><ol class="basic" type="a" start="7"><li>Other Services</li></ol></td>
                <td></td>
                <td></td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;Senior Citizens</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;PWDs</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;GAD</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;BCPC (less attribution from 
                BDRRM & BADAC)</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;Children's Fund</td>
                <td></td>
                <td align="center">₱10,000,000</td>
                </tr>
                <tr >
                <td class="indent-four">|&nbsp;<b>Total</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>
                <tr>
                <td><b>TOTAL BASIC SERVICES & FACILITIES PROGRAM</b></td>
                <td></td>
                <td align="center">	₱10,000,000</td>
                </tr>   
            </tbody>
        </table>
        <br><br><br><br><br> -->
        <div>
            <table width="100%">
                    <!-- <thead>
                    </thead> -->
                    <tbody>
                        {!! $table !!}
                    </tbody>
            </table>
            <br/><br/>
            <table width="100%">
                <tr>
                    <td><b>TOTAL ESTIMATED FUNDS AVAILABLE FOR APPROPRIATION</b></td>
                    <td class="right-align">₱&nbsp;{{ number_format($debit,2) }}</td>
                </tr>
                <tr>
                    <td><b>TOTAL EXPENDITURE PROGRAM</b></td>
                    <td class="right-align">₱&nbsp;{{ number_format($credit,2) }}</td>
                </tr>
                <tr>
                    <td><b>ENDING BALANCE</b></td>
                    <td class="right-align">₱&nbsp;{{ number_format((int)$debit - (int)$credit,2) }}</td>
                </tr>
            </table>
        </div>
</div>

<style>
     .break-page{
        page-break-after: always;
    }
</style>
