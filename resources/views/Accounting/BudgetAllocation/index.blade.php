@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item active" aria-current="page">Budget Allocation</li>
        </ol>
    </nav>
    <h4 class="form-head">Budget Allocation List</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"> <a href="{{ route('budget-allocation.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table table-striped table-bordered table-hover table-mediascreen" id="budget_allocation_list">
                <thead class="thead-dark">
                    <tr align="center">
                    <th>Year</th>
                    <th>Action</th>
                    </tr>
                </thead>
              
            </table>
        </div>
    </div>
</div>
@endsection


@push('scripts')

<script>
         $("#budget_allocation_list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                // serverSide: true,
                type: "GET",
                ajax:"{!! route('get.budgetallocation.list') !!}",
                columns:[
                    {data: "year","name":"year","width":"30%", "className": "text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%" , className:"text-center"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
    </script>
@endpush

