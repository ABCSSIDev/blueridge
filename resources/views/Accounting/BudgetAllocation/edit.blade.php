@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item active" aria-current="page">Budget Allocation</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Budget Allocation</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('budget-allocation.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST" id="budget_form"  class="form-horizontal" action="{{ route('budget-allocation.update', $budget_id) }}" enctype="multipart/form-data">
                    <div class="errors"></div>
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT" >
                    <div class="col-md-4">
                        <label>
                            <b>YEAR: {{$year}}</b>
                        </label>
                    </div>
                    <div class="col-md-12">
                        <table class="table  table-bordered table-hover table-sm">
                            <thead class="thead-dark">
                              <tr align="center">
                              <th width="50%">Account Group / Ledgers</th>
                              <th width="20%">Account Code</th>
                              <th width="30%">Action</th>
                              </tr>
                            </thead>
                            <tbody >
                                {!! $table !!}
                            </tbody>
                        </table>
                        <table class="table  table-bordered table-hover table-sm" style="margin: 50px 0 0 0">
                            <tbody>
                                <tr>
                                <td><b>TOTAL ESTIMATED FUNDS AVAILABLE FOR APPROPRIATION</b></td>
                                <td id="debit">₱&nbsp;{{$debit}}</td>
                                </tr>
                                <tr>
                                <td><b>TOTAL EXPENDITURE PROGRAM</b></td>
                                <td id="credit">₱&nbsp;{{$credit}}</td>
                                </tr>
                                <tr>
                                <td><b>ENDING BALANCE</b></td>
                                <td id="ending_balance">₱&nbsp;{{$ending_balance}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group pb-4" align="right" >
                            <button type="submit" class="btn btn-primary mt-4"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>
    let rules = {
    };
    $('#budget_form').registerFields(rules,false);
    $('.sum_ledgers').number(true, 2);
    $('#debit').number(true, 2);
    $('#credit').number(true, 2);
    $('#ending_balance').number(true, 2);

    $('.compute-total').click(function(){
        let id = $(this).attr('data-id');
        let remove_total = $(this).attr('remove');
        let total = 0;
        var total_id = '.group_total-' + id;

        if(remove_total === "true"){
            $(this).attr('remove', false).text("Compute Total");
            $(total_id).html("");
            $(total_id).val("");
            $(total_id).attr('remove', false);
        }else {
            $('.group-' + id).each(function (index) {
                console.log($(this).val());
                total += Number($(this).val());
            });
            $(this).attr('remove', true).text("Remove Total");
            $(total_id).number(total, 2);
            $(total_id).val(total);
            $(total_id).attr('remove', true);
        }

    });

    $('.sum_ledgers').keyup(function(e){
        e.stopImmediatePropagation();
        $data_arr = $(this).attr('data-arr');
        $parent_ledger = $(this).attr('data-ledger');
        $groups = $data_arr.split(","); // convert data-arr value into array
        $groups.pop(); // remove the last values in the array
        $el_val = $(this).val();
        if($el_val == "" || $el_val == 0){
            $el_val = "0";
        }
        $ledger_total = 0;
        //loop through all groups in array and sum its total
        for(var i = 0; i <= $groups.length; i++){
            var total = 0;
            $('.group-'+$groups[i]).each(function(){
                total += Number($(this).val());
            });
            //computation and assignment of preset budget
            var budget_group = $('.budget_group-'+$groups[i]);
            budget_group.each(function(){
                var computed_budget = total*(Number($(this).attr('data-budget'))/100);
                $(this).val(computed_budget);
            });
            var budget_ledger = $('.budget_ledger-'+$(this).attr('data-ledger'));
            budget_ledger.each(function(){
                var computed_budget_ledger = Number($el_val)*(Number($(this).attr('data-budget'))/100);
                $(this).val(computed_budget_ledger);
            });
            let group_total = $('.group_total-'+$groups[i]);
            if(group_total.attr('remove') === "true"){
                //if attr remove is true, show total into group_total with number format of two decimal places
                group_total.number(total, 2);
            }

           
        }
        $('.ledger_group_'+$parent_ledger).each(function(){
                $ledger_total += Number($(this).val());
            });
        $('.ledger-'+$parent_ledger).html("<strong>"+$ledger_total.toFixed(2)+"</strong>");
        computeBalance(); //execute function for debit, credit, and ending balance's values
        if($el_val == "" || $el_val == 0){
            $(this).val("0.00");
        }
    });

    function computeBalance(){
        var values = [];
        values['debit'] = 0;
        values['credit'] = 0;

        //iterate through all ledgers and sum its corresponding total (0 = debit; 1 = credit)
        $('.sum_ledgers').each(function(){
            $(this).attr('a-type') === "0"?values['debit']+=Number($(this).val()):values['credit']+=Number($(this).val());
        });

        //subtract credit from debit to get the ending balance
        values['ending_balance'] = values['debit'] - values['credit'];

        //assign corresponding values to its row (number format with 2 decimal places)
        $('#debit').number(values['debit'], 2);
        $('#credit').number(values['credit'], 2);
        $('#ending_balance').number(values['ending_balance'], 2);
    }
</script>
@endpush
