@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item active" aria-current="page">Bank</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Bank</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('bank.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                    <form method="POST" class="form-horizontal" id="bank_form" action="{{route('bank.store')}}" enctype="multipart/form-data">
                        <div class="errors"></div>
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Bank Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Bank Name" autocomplete="off">
                                <input type="hidden" class="form-control" id="created_by" name="created_by" value="{{ Auth::user()->id }}">
                                <input type="hidden" class="form-control" id="created_at" name="created_at" value="{{ date('Y/m/d H:i:s') }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Contact Number</label>
                                <input type="number" class="form-control price" id="contact_number" name="contact_number" placeholder="Contact Number" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Address" autocomplete="off">
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Account Name</label>
                                <input type="text" class="form-control" id="account_name" name="account_name" placeholder="Account Name" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Account Number</label>
                                <input type="number" class="form-control price" id="account_number" name="account_number" placeholder="Account Number" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Account Type</label>
                                <select class="form-control" name="account_type" id="account_type">
                                    <option value="">Select Account Type</option>
                                    <option value="1">Savings</option>
                                    <option value="2">Current</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Product Type</label>
                                <select class="form-control" name="product_type" id="product_type">
                                    <option value="">Select Product Type</option>
                                    <option value="1">Passbook</option>
                                    <option value="2">ATM</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Initial Deposit</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control number" id="initial_deposit" name="initial_deposit" placeholder="Initial Deposit" autocomplete="off">
                                </div>
                                <!-- <input type="text" class="form-control number" id="initial_deposit" name="initial_deposit" placeholder="Initial Deposit" autocomplete="off"> -->
                            </div>
                        </div>
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
// $("#initial_deposit").number(true,2);
let rules = {
        name : {
            required: true
        },
        address : {
            required: true
        },
        account_name : {
            required: true
        },
        account_number : {
            required: true
        },
        account_type : {
            required: true
        },
        initial_deposit : {
            required: true
        },
    };
    $('#bank_form').registerFields(rules);

    $('input.number').keyup(function(event) {
    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
        // format number
        $(this).val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });

</script>
@endpush
