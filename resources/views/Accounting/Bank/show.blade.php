@extends('layouts.app')


@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Accounting</li>
            <li class="breadcrumb-item active" aria-current="page">Bank</li>
        </ol>
    </nav>
    <h4 class="form-head">View Bank</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('bank.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST" class="form-horizontal" id="bank_form" action="" enctype="multipart/form-data">
                    <div class="errors"></div>
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Bank Name</label>
                            <p>{{ $bank_data->name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Number</label>
                            <p>{{ $bank_data->contact_number }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Address</label>
                            <p>{{ $bank_data->address }}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Account Name</label>
                            <p>{{ $bank_data->account_name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Account Number</label>
                            <p>{{ $bank_data->account_number }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Account Type</label>
                            <p>{{ ($bank_data->account_type == 1?'Savings':'Current')}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Product Type</label>
                            <p>{{ ($bank_data->product_type == 1?'Passbook':'ATM')}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Initial Deposit</label>
                            <p>₱&nbsp;{{ number_format($bank_data->initial_deposit, 2) }}</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
// $("#initial_deposit").number(true,2);
let rules = {
    name : {
        required: true
    },
    address : {
        required: true
    },
    contact_number : {
        required: true
    },
    account_name : {
        required: true
    },
    account_number : {
        required: true
    },
    account_type : {
        required: true
    },
    product_type : {
        required: true
    },
    initial_deposit : {
        required: true
    },
};
$('#bank_form').registerFields(rules);

</script>
@endpush
