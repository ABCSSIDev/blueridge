<div class="margin-top-print">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="2" colspan="2"  class="text-center"><span class="f-size ">LIQUIDATION REPORT</span><br><br><br><span class="italic">Barangay, City/Municipality, Province</span></th>
                <th class="text-left" colspan="2">No: <span class="line"></span></th>
            </tr>
            <tr align="left">
                <th colspan="2">Date: <span class="line"></span></th>
            </tr>
        </thead>
        <tbody>
            <tr align="center">
                <td colspan="2"><b>PARTICULARS</b></td>
                <td colspan="2"><b>AMOUNT</b></td>
            </tr>
             <tr>
                <td colspan="2"><br><br><br><br><br></td>
                <td colspan="2"><br><br><br><br><br></td>
            </tr>
            <tr  style="border-bottom: 1px solid #dee2e6">
                <td colspan="2" style="border-style: none;">TOTAL AMOUNT SPENT </td>
                <td colspan="2" style="border-style: none;"><img src="{{asset('images/arrow-symbol.png')}}" width="30px" height="30px"></td>
            </tr>
            <tr  style="border-bottom: 1px solid #dee2e6">
                <td colspan="2" style="border-style: none;">AMOUNT OF CASH ADVANCE PER DV NO.____ DTD.____</td>
                <td colspan="2" style="border-style: none;"><img src="{{asset('images/arrow-symbol.png')}}" width="30px" height="30px"></td>
            </tr>
           <tr style="border-bottom: 1px solid #dee2e6">
                <td colspan="2" style="border-style: none;">AMOUNT REFUNDED PER O.R NO._______ DTD._______</td>
                <td colspan="2" style="border-style: none;"><img src="{{asset('images/arrow-symbol.png')}}" width="30px" height="30px"></td>
            </tr>
            <tr style="border-bottom: 1px solid #dee2e6">
                <td colspan="2" style="border-style: none;">AMOUNT TO BE REIMBURSED</td>
                <td colspan="2" style="border-style: none;"><img src="{{asset('images/arrow-symbol.png')}}" width="30px" height="30px"></td>
            </tr>
            <tr>
                <td style="border-style: none;border-right:1px solid #dee2e6"><b>Submitted by:</b></td>
                <td colspan="3" style="border-style: none;"><b>Received by:</b></td>
            </tr>
            <tr align="center">
                <td style="border-style: none;border-right:1px solid #dee2e6">
                    <p class="bt-liquidation"></p>
                    Accountable Officer
                    <p class="bt-liquidation"></p>
                    Date
                </td>
                <td colspan="3" style="border-style: none;">
                    <p class="bt-liquidation"></p>
                    Accounting Unit
                    <p class="bt-liquidation"></p>
                    Date
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center" style="border-top: 2px solid"><b>ACCOUNTING ENTRIES</b></td>
            </tr>
            <tr align="center">
                <td>Account Titles</td>
                <td>Account Code</td>
                <td>Debit</td>
                <td>Credit</td>
            </tr> 
            <tr>
                <td><br><br><br><br></td>
                <td><br><br><br><br></td>
                <td><br><br><br><br></td>
                <td><br><br><br><br></td>
            </tr>
            <tr>
                <td style="border-style: none;"><b>Prepared by:</b></td>
                <td colspan="3" style="border-style: none;"><b>Approved by:</b></td>
            </tr>
            <tr align="center">
                <td style="border-style: none;">
                    <p class="bt-liquidation"></p>
                    Barangay Book keeper
                    <p class="bt-liquidation"></p>
                    Date
                </td>
                <td colspan="3" style="border-style: none;">
                    <p class="bt-liquidation"></p>
                    City/Municipal Accountant
                    <p class="bt-liquidation"></p>
                    Date
                </td>
            </tr>
        </tbody>
    </table>

</div>



<style>
    .margin-top-print {
        margin-top: 40px;
    }
    .line{
        border-bottom: 1px solid #000;
        width: 20%;
    }
    .italic{
        font-style: italic;
        border-top: 1px solid #000;
    }
    .f-size{
        font-size: 20px;
    }
    .m-left{
        margin-left: 10%;
    }
    .b-top{
        border-top: 1px solid #000;
        width: 20px;
    }
    .left1{
        margin-left: 35%;
    }
    .left2{
        margin-left: 45%;
    }
    .bt-liquidation {
        border-bottom: 1px solid; 
        position: inherit; 
        width: 200px;
        margin-bottom: 0;
        margin-top: 30px;
        margin-bottom: 5px;
    }
</style>