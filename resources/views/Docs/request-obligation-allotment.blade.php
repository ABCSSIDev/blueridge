<form class="form-horizontal" role='form' method="POST" enctype="multipart/form-data">
    <table class="table table-bordered text-center" width="100%">
        <thead>
            <tr>
                <td colspan="4" rowspan="2" width="60%" style="vertical-align: middle;">
                    <h3><b>LIQUIDATION REPORT</b></h3> 
                    <p><b>BLUE RIDGE B, QUEZON CITY, METRO MANILA</b></p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 5.5rem; width: 68.5%; margin-top: -1rem;"></p>
                    <p>Barangay, City/Municipality, Province</p>
                </td>
                <td style="text-align: left;">
                    <p style="position: relative; top: 10px;">No.: <underline style="border-bottom: 2px solid">{{ $roas->getRoa->obligation_number }}</underline></p>
                    <!-- <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem;"></p> -->
                </th>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left;" width="20%">
                <p style="position: relative; top: 10px;">Date: <underline style="border-bottom: 2px solid">{{ App\Common::convertWordDateFormat($roas->getRoa->resolution_date) }}</underline></p>
                <!-- <p style="border-bottom: 2px solid; position: relative; margin-left: 2.5rem;"></p></td> -->
            </tr>
            <tr>
                <td colspan="4"><b>PARTICULARS</b></td>
                <td colspan="2"><b>AMOUNT</b></td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 2rem;"><b>{{ $roas->getRoa->getLedger->ledger_name }}</b></td>
                <td colspan="2" style="vertical-align: middle;"><b>₱{{ number_format($roas->getRoa->total_amount,2) }}</b></td>
            </tr>
            <tr>
                <td style="text-align: left;" colspan="4">TOTAL AMOUNT SPENT</td>
                <td colspan="2" style="border-left-style: hidden;">₱{{ number_format($roas->getRoa->total_amount,2) }}</td>
            </tr>
            <tr>
                <td style="text-align: left;" colspan="2">AMOUNT OF CASH ADVANCE PER DV NO.</td>
                <td style="border-left-style: hidden; text-align: left; text-decoration: underline;">{{$roas->getRoa->cash_advance_voucher_no}}</td>
                <td style="border-left-style: hidden; text-decoration: underline;">{{$roas->getRoa->cash_advance_date}}</td>
                <td colspan="2" style="border-left-style: hidden;">₱ {{ number_format($roas->getRoa->cash_advance_amount,2) }}</td>
            </tr>
            <tr>
                <td style="text-align: left;">AMOUNT REFUNDED PER O.R. NO.</td>
                <td style="border-left-style: hidden; text-decoration: underline;">{{$roas->getRoa->amount_refund_or_no}}</td>
                <td style="border-left-style: hidden;">DTD</td>
                <td style="border-left-style: hidden; text-decoration: underline;">{{$roas->getRoa->amount_refund_date}}</td>
                <td style="border-left-style: hidden;" colspan="2">₱ {{ number_format($roas->getRoa->total_amount - $roas->getRoa->cash_advance_amount,2) }}</td>
            </tr>
            <tr>
                <td style="text-align: left;" colspan="4">AMOUNT TO BE REIMBURSED</td>
                <td colspan="2" style="border-left-style: hidden;">₱ 0.00</td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: 600;">
                    <p style="text-align: left">Submitted by:</p>
                    <p style="margin-top: 30px;">MICHELL V. MENIANO</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: -1rem;"></p>
                    <p style="margin-top: -1rem;">Accountable Officer</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: 30px;"></p>
                    <p style="margin-top: -1rem;">Date</p>
                </td>
                <td colspan="3" style="font-weight: 600;">
                    <p style="text-align: left">Received by:</p>
                    <!-- <p style="margin-top: 30px; text-align: center;"></p> -->
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: 50px;"></p>
                    <p style="margin-top: -1rem; text-align: center;">Accounting Unit</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: 30px;"></p>
                    <p style="margin-top: -1rem; text-align: center;">Date</p>
                </td>
            </tr>
            <tr>
                <td colspan="6"><b>ACCOUNTING ENTRIES</b></td>
            </tr>
            <tr>
                <td colspan="2">Account Titles</td>
                <td>Account Code</td>
                <td>Debit</td>
                <td>Credit</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: 600">
                    <p style="text-align: left">Prepared by:</p>
                    <p style="margin-top: 30px;">&nbsp;</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: -1rem;"></p>
                    <p style="margin-top: -1rem;">Barangay Bookkeeper</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: 30px;"></p>
                    <p style="margin-top: -1rem;">Date</p>
                </td>
                <td colspan="3" style="font-weight: 600">
                    <p style="text-align: left">Approved by:</p>
                    <p style="margin-top: 30px; text-align: center;">&nbsp;</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: -1rem;"></p>
                    <p style="margin-top: -1rem; text-align: center;">City/Municipal Accountant</p>
                    <p style="border-bottom: 2px solid; position: relative; margin-left: 2rem; width: 85%; margin-top: 30px;"></p>
                    <p style="margin-top: -1rem; text-align: center;">Date</p>
                </td>
            </tr>
        </thead>
    </table>
</form>
<style>
     .table td, .table th {
        padding: 10px !important;
    }
</style>