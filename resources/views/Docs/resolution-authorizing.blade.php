<!-- <div class="margin-top">
    <div align="left">
        <h6 class="fw-700">BARANGAY RESOLUTION NO. {{(!empty($roas) ? $roas->getRoa->resolution_number : '091-S-2019') }}</h6>
        <h6 class="fw-700">RESOLUTION AUTHORIZING MICHELL V. MENIANO TO WITHDRAW THE AMOUNT OF <p style="text-decoration: underline;">{{(!empty($roas) ? $amount_in_words : 'forty seven thousand seven hundred ninety nine pesos only')}} ₱{{ (!empty($roas) ? number_format($roas->getROA->total_amount,2) : ' 47,799.00' ) }}</p> AS PAYMENT FOR {{ (!empty($roas) ? $roas->getRoa->getLedger->ledger_name : 'Supplies and materials expenses') }}</h6><br>
        <h6>WHEREAS, the Annual Budget FY {{ (!empty($roas) ? $year: '2019') }} was approved and confirmed by the Secretary of the City;</h6><br>
        <p>NOW BE IT RESOLVED, AS IT IS HEREBY RESOLVED that MICHELL V. MENIANO, Barangay Treasurer, be authorized to withdraw the amount of <b style="text-decoration: underline;">{{(!empty($roas) ? $amount_in_words : 'forty seven thousand seven hundred ninety nine pesos only')}} ₱{{ (!empty($roas) ? number_format($roas->getROA->total_amount,2) : ' 47,799.00' ) }}</b> for the above mentioned expense under the MOOE of the Annual Budget FY <b style="text-decoration: underline;">{{ (!empty($roas) ? $year: '2019') }}</b></p><br>
        <p>Approved on <b style="text-decoration: underline;">{{ (!empty($roas) ? App\Common::convertWordDateFormat($roas->getRoa->approval_date) : '03 April, 2019' ) }}</b> in Quezon City.</p><br>
    </div>
</div>
<div class="col-12" align="right">
    <span class="sign-name" ref="#sign-position1">ESPERANZA CASTRO-LEE</span><br>
    <span class="sign-position" id="sign-position1">Punong Barangay</span>
</div>
<br>
<br>
<div class="col-12">
    <div class="row">
        <div class="col-8 sign-name" ref="#sign-position2"></div>
        <div class="col-4 sign-name" ref="#sign-position3"></div>
    </div>
    <div class="row">
        <div class="col-8 sign-position" id="sign-position2"></div>
        <div class="col-4 sign-position" id="sign-position3"></div>
    </div>
</div>
<br>
<div class="col-12">
    <div class="row">
        <div class="col-8 sign-name" ref="#sign-position4"></div>
        <div class="col-4 sign-name" ref="#sign-position5"></div>
    </div>
    <div class="row">
        <div class="col-8 sign-position" id="sign-position4"></div>
        <div class="col-4 sign-position" id="sign-position5"></div>
    </div>
</div>
<br>
<div class="col-12">
    <div class="row">
        <div class="col-8 sign-name" ref="#sign-position6"></div>
        <div class="col-4 sign-name" ref="#sign-position7"></div>
    </div>
    <div class="row">
        <div class="col-8 sign-position" id="sign-position6"></div>
        <div class="col-4 sign-position" id="sign-position7"></div>
    </div>
</div>
<br>
<div class="col-12">
    <div class="row">
        <div class="col-8 sign-name" ref="#sign-position8"></div>
    </div>
    <div class="row">
        <div class="col-8 sign-position" id="sign-position8"></div>
    </div>
</div>
<br>
<br>
<div class="col-12">
    <p>Attested by:</p><br>
    <p style="margin-bottom: auto; text-indent: 2rem;" class="sign-name" ref="#sign-position9"></p>
    <p style="text-indent: 2rem;" class="sign-position" id="sign-position9"></p>
</div> -->


<table class="text-center margin-top-print" width="100%">
    <thead>
        <tr>
            <td>
                <p><b>BARANGAY RESOLUTION NO. <b style="text-decoration: underline;">{{(!empty($roas) ? $roas->getRoa->resolution_number : '091-S-2019') }}</b></b></p>
                <p style="margin: 0;text-align: justify"><b>A RESOLUTION AUTHORIZING<b style="text-decoration: underline;">&nbsp;MICHELL V. MENIANO</b> <b style="text-transform: uppercase;">{{$roas->mode}} THE AMOUNT <b>₱{{ number_format($roas->getRoa->total_amount,2) }} for {{ $roas->getRoa->getLedger->ledger_name }}</b> {{(!empty($po_mode) ? 'as mode of procurement '. $po_mode->procurement_mode : '')}} for the Month of {{ App\Common::convertWordDateFormat($roas->getRoa->approval_date) }}</b></p>
                <p style="margin: 0;text-align: justify"><b></p>
                <!-- <p style="margin-top: 0;text-align: justify"><b> for the Month of {{ App\Common::convertWordDateFormat($roas->getRoa->approval_date) }}</b></p> -->
                <p style="text-align: justify">WHEREAS the Annual Budget of {{ App\Common::convertToYear($roas->getRoa->resolution_date) }} was approved and confirmed by the Secretary of the City; NOW BE IT RESOLVED, AS IT IS HEREBY RESOLVED that <span style="text-decoration: underline;">Michell V. Meniano</span></p>
                <p style="margin: 0;text-align: justify"><span style="text-decoration: underline;">Barangay Treasurer</span><span> be authorized <b style="text-transform: lowercase;">{{$roas->mode}}</b> the amount of ₱{{ number_format($roas->getRoa->total_amount,2) }} for the above mentioned expense under MOOE of the Annual Budget FY {{ App\Common::convertToYear($roas->getRoa->approval_date) }}.</span></p>
                <!-- <p style="margin: 0;"><b>₱{{ number_format($roas->getRoa->total_amount,2) }} for the above</b></p> -->
                <!-- <p style="margin-top: 0;">mentioned expense under MOOE of the Annual Budget FY 2020.</p> -->
                <p style="margin-top: 3rem;">Approved on {{ App\Common::convertWordDateFormat($roas->getRoa->approval_date) }} at Quezon City</p>
                <p style="margin-top: 5rem; margin-bottom: 0;">ESPERANZA CASTRO-LEE</p>
                <p style="margin-top: 0;">Punong Barangay</p>
                <table style="width: 100%;">
                    <tr>
                        <td><b><p style="margin-top: 3rem;">Katherine T. de Jesus</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                        <td><b><p style="margin-top: 3rem;">Anna Francesca L. Maristela</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                    </tr>
                    <tr>
                        <td><b><p style="margin-top: 3rem;">Maria Elena M. Ruiz</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                        <td><b><p style="margin-top: 3rem;">Anthony O. Viray</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                    </tr>
                    <tr>
                        <td><b><p style="margin-top: 3rem;">Agusto D. Ilagan</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                        <td>
                            <b>
                                <p style="margin-top: 3rem;">{{ $roas->getRoa->signatory == null ? 'Kgd. Thomas J.T.F. de Castro': $roas->getRoa->signatory }}</p>
                                <p style="margin-top: -1rem;">Kagawad</p>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td><b><p style="margin-top: 4rem; margin-right: 5rem;">Attested by:</p></b></td>
                    </tr>
                    <tr>
                        <td style="border: none;"><b><p>Rovie Rose M. Bernabe</p><p style="margin-top: -1rem; margin-right: 1.5rem;">Barangay Secretary</p></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </thead>
</table>
<style>
    .fw-700 {
        font-weight: 700;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>
