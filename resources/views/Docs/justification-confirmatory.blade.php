<form class="form-horizontal margin-top-print" role='form' method="POST" enctype="multipart/form-data" style="font-size: 24px">
    <div align="center">
        <p style="margin-top: 2rem;"><b>Justification and Confirmatory Report on Emergency Purchases</b></p>
    </div>
    <p>Requirements</p>
    <!-- <table class="table table-bordered">
        <thead>
            <tr align="center">
                <th>Item No</th>
                <th>Unit</th>
                <th>Description</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            <tr align="center">
                <td>{{ (!empty($po) ? $item_no : '18') }}</td>
                <td>Various</td>
                <td>{{ $item->getPRItems->getPurchaseRequest->RoaDetails->getLedger->ledger_name }}</td>
                <td>&#8369; {{ (!empty($po) ? number_format($total,2) : '47,799.00') }}</td>
            </tr>
            <tr align="center">
                <td></td>
                <td></td>
                <td><b>TOTAL</b></td>
                <td>&#8369; {{ (!empty($po) ? number_format($total,2) : '47,799.00') }}</td>
            </tr>
        </tbody>
    </table> -->
    <table class="table table-bordered">
        <tr align="center">
            <th style="width: 15%;">Unit</th>
            <th style="width: 30%;">Particulars</th>
            <th style="width: 15%;">Quantity</th>
            <th style="width: 20%;">Unit Cost</th>
            <th style="width: 20%;">Amount</th>
        </tr>
        @php $total = 0;  @endphp
        @if(!empty($canvass_item))
            @foreach($canvass_item as $canvass_value => $item)
                @php
                    $est_cost = $item->available_quantity * $item->unit_price;
                    $total += $item->unit_price * $item->available_quantity;
                @endphp
                <tr align="center">
                    <td>{{ $item->getPRItems->getUnits->unit_symbol }}</td>
                    <td>{{ $item->getPRItems->getInventoryName->description }}</td>
                    <td>{{ number_format($item->available_quantity) }}</td>
                    <td>₱&nbsp;{{ number_format($item->unit_price,2) }}</td>
                    <td>₱&nbsp;{{ number_format($est_cost,2) }}</td>
                </tr>
            @endforeach
        @else
            <tr align="center">
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
            </tr>
            <tr align="center">
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
            </tr>
        @endif
    </table>
    <div class="margin">
        <p>
            1. That the emergency purchase of {{ $item->getPRItems->getPurchaseRequest->RoaDetails->getLedger->ledger_name }} for Barangay Blue Ridge B,
            Quezon City was made to maintain operational requirement in performing regular duties and responsibilities,
            specifically to deliver basic barangay services.
        </p>
        <p>
            2. That the expenses incurred is certified by the Barangay Treasurer and approved by the Sangguniang Barangay through
            Resolution # {{(!empty($po) ? $po->getPurchaseRequest->RoaDetails->resolution_number : '091-S-2019') }}.
        </p>
        <p>
            3. That the said emergency purchase was paid by and purchased from {{ (!empty($canvass->getSuppliers) ? $canvass->getSuppliers->supplier_name : 'SHEIN HARDWARE') }}.
        </p>
        <p>
            4. That the saide emergency purchase was necessary in the timely delivery of services to the constitutents
            of the Barangay and in the discharge of duties and responsibilities of the Barangay Office staff.
        </p>
    </div>
    <table width="100%" class="mt-02">
        <tr>
            <td width="45%" class="border-n">Prepared by:</td>
            <td width="50%" class="border-n">Confirmed by:</td>
        </tr>
    </table>
    <table width="100%" class="mt-02">
        <tr>
            <td width="45%" class="border-n align-left">
                <p class="sign-name mb-0" ref="#sign-position1">Michell V. Meniano</p>
                <p class="bt-po justify-line mb-0" style="right: 5rem;"></p>
                <p class="sign-position" id="sign-position1" style="margin-top: -.5rem;">Barangay Treasurer</p>
            </td>
            <td width="50%" class="border-n align-left">
                <p class="sign-name mb-0" ref="#sign-position2">ESPERANZA CASTRO-LEE</p>
                <p class="bt-po justify-line mb-0" style="right: 5rem;"></p>
                <p class="sign-position" id="sign-position2" style="margin-top: -.5rem;">Punong Barangay</p>
            </td>
        </tr>
    </table>
</form>

<style>
    .text-center {
        text-align: center;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>
