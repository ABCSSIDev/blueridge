<div align="center" class="margin-top-print">
    <p style="margin-top: -1rem;"><b>Emergency Purchase</b></p>
</div>
<div class="margin">
    <p>The need for the purchase of {{ $roas->description }} including its quality, was urgent and absolutely indispensable to public service. 
    There was no time to procure these through public bidding due to the urgency of the requirements.</p>
</div>
<br>
<div class="margin">
    <div align="right">
        <p style="font-weight: 600;">ESPERANZA CASTRO-LEE</p>
        <p class="indent" style="margin-top: -1rem;">Punong Barangay</p>
    </div>
</div>
<div class="margin">
    <p align="center" style="margin-top: 3rem;"><b>CONFIRMATORY REPORT</b></p>
</div>
<div class="">
    <p>
        1.  The said items which were through emergency purchase directed by the Punong Barangay are execeptionally 
        urgent and indispendable to prevent immediate danger to or loss of life and property.
    </p>
    <p>
        2. There was no material time to acquire said items in the usual procedure of canvassing.
    </p>
    <p>
        3. Prices paid for were the lowest obtaible in the open market at the time of the procurement of same.
    </p>
    <p>
        4. The said items were furnished by various outlets under various invoice numbers in the amount stated therein.
    </p>
    <p>Approved on <b style="text-decoration: underline;">{{ (!empty($roas) ? App\Common::convertWordDateFormat($roas->getRoa->approval_date) : '' ) }}</b> in Quezon City.</p><br>
</div>
<div class="margin">
    <p align="center"><b>ESPERANZA CASTRO-LEE</b><br>Punong Barangay</p>
</div>


<style>
    .indent{
        text-indent: 40px;
        direction: rtl; 
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>
