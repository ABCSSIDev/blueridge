<div class="margin">
    <p align="center"><b>INSPECTION REPORT</b></p>
</div>
<table width="100%">
    <tr>
        <td width="41%"><p class="align-left mb-0">Supplier: {{ (!empty($inventory_receiving) ? $inventory_receiving->purchaseOrder->getCanvass->getSuppliers->supplier_name :'') }}</p></td>
        <td width="50%"><p class="align-left mb-0">Address: {{ (!empty($inventory_receiving) ? $inventory_receiving->purchaseOrder->getCanvass->getSuppliers->address :'') }}</p></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width="45%"><p class="align-left">Ledger Name: {{ (!empty($po) ? $po->getPurchaseRequest->RoaDetails->getLedger->ledger_name : '') }}</p></td>
        <td width="40%"><p class="align-left">Date: {{ (!empty($po) ? App\Common::convertWordDateFormat($po->po_date): '') }}</p></td>
        <td width="15%"><p class="align-left">Total: {{ (!empty($po) ? number_format($total,2) : '' ) }}</p></td>
    </tr>
</table>
<table class="table table-bordered">
    <tr>
        <td class="align-left">PR No.</td>
        <td class="align-left">{{ (!empty($po) ? $pr_no : '') }}</td>
        <td class="align-left">Date</td>
        <td class="align-left">{{ (!empty($po) ? App\Common::convertWordDateFormat($po->getPurchaseRequest->request_date) : '' ) }}</td>
        <td class="align-left">Amount</td>
        <td class="align-left">&#8369; {{ (!empty($po) ? number_format($po->getPurchaseRequest->RoaDetails->total_amount,2) : '' ) }}</td>
    </tr>
    <tr>
        <td class="align-left">PO No.</td>
        <td class="align-left">{{ (!empty($po) ? App\Common::getPRNumberFormat("purchase_requests", $pr_view).App\Common::getAlphabet($pr_view, true, $ids) : '') }}</td>
        <td class="align-left">Date</td>
        <td class="align-left">{{ (!empty($po) ? App\Common::convertWordDateFormat($po->po_date) : '' ) }}</td>
        <td class="align-left">Amount</td>
        <td class="align-left">&#8369; {{ (!empty($po) ? number_format(App\Http\Controllers\ReceivingController::POtotal($id),2) : '' ) }}</td>
    </tr>
    <tr> 
        <td class="align-left">Delivered at</td>
        <td class="align-left" colspan="5">Blue Ridge B, Quezon City</td>
    </tr>
    <tr>
        <td class="align-left">Accepted on</td>
        <td class="align-left" colspan="5">{{ (!empty($po) ? App\Common::convertWordDateFormat($po->po_date): '') }}</td>
    </tr>
    <tr>
        <td class="align-left">Inspection Requested on</td>
        <td class="align-left" colspan="2">{{ (!empty($po) ? App\Common::convertWordDateFormat($po->po_date): '') }}</td>
        <td class="align-left">Inspection on</td>
        <td class="align-left" colspan="2">{{ (!empty($po) ? App\Common::convertWordDateFormat($po->po_date): '') }}</td>
    </tr>
</table>
<div class="margin">
    <p><b>Remarks:</b> Ocular inspection was conducted by the undersigned and found said project/activity is existing and 100% complied as to be reflected in the Report of Collection and Deposit of the Barangay</p>
</div>
<table width="100%" class="mt-02">
    <tr>
        <td width="33%" class="border-n" style="text-align: left;">Inspected by:</td>
        <td width="33%" class="border-n" style="text-align: left;">Certified by:</td>
        <td width="33%" class="border-n" style="text-align: left;">Noted by:</td>
    </tr>
</table>
<table width="100%" class="mt-02">
    <tr>
        <td width="33%" class="border-n">
            <p style="margin-left: 5rem;" class="sign-name mb-0 mg-left-5" ref="#sign-position1"></p>
            <p class="bt-po mb-0 inspect-line"></p>
            <p class="sign-position p-inspect" id="sign-position1">Chairman, Committee on Inspection</p>
        </td>
        <td width="33%" class="border-n">
            <p class="sign-name mg-left-9 mb-0" ref="#sign-position2"></p>
            <p class="bt-po mb-0 mg-left-5 width-70"></p>
            <p class="sign-position m-position" id="sign-position2">Barangay Treasurer</p>
        </td>
        <td width="33%" class="border-n">
            <p class="sign-name mg-left-9 mb-0" ref="#sign-position3"></p>
            <p class="bt-po mb-0 mg-left-5 width-70"></p>
            <p class="sign-position m-position" id="sign-position3">Punong Barangay</p>
        </td>
    </tr>
</table>