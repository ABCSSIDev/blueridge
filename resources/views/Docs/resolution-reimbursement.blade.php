<table class="text-center margin-top-print" width="100%">
    <thead>
        <tr>
            <td>
                <p><b>Bids & Awards Committee Resolution No. {{ $roas->getROA->bids_resolution_number == null ? 'N/A' : $roas->getROA->bids_resolution_number }}</b></p>
                <p style="margin: 0;"><b>A RESOLUTION ADOPTING {{ $roas->getROA->bids_mode == null ? 'N/A' : $roas->getROA->bids_mode }} AS THE MODE OF PROCUREMENT FOR</b></p>
                <p style="margin: 0;"><b>{{ $roas->getRoa->getLedger->ledger_name }}</b></p>

                <p style="text-align: left;">WHEREAS the Barangay Council has approved to procure the necessary fuel to ensure continued barangay operations and services to benefit the various constituents of Barangay Blue Ridge B;</p>

                <p style="text-align: left;">WHEREAS the Bids and Awards Committee finds the use of REIMURSEMENT as the mode of procurement for said requirements to be the most convenient and beneficial to the Barangay;</p>

                <p style="text-align: left;">NOW BE IT RESOLVED AS IT IS HERBEBY RESOLVED that REIMBURSEMENT is the approved mode of procurement for above-stated requirements;</p>

                <p style="text-align: left;">RESOLVED FURTHER that copies of this Resolution be forwarde to the authorities concerned for information, guidance and proper disposition;</p>

                <span>Approved on <span style="text-decoration: underline;">{{ App\Common::convertWordDateFormat($roas->getRoa->approval_date) }}</span> in Quezon City</span>
                <p style="margin-top: 2rem; margin-bottom: 0;">Thomas J.T.F. de Castro</p>
                <p style="margin-top: 0;">BAC Chairman</p>
                <table style="width: 100%;">
                    <tr>
                        <td><b><p style="margin-top: 2rem;">Katherine T. de Jesus</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                        <td><b><p style="margin-top: 2rem;">Anna Francesca L. Maristela</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                    </tr>
                    <tr>
                        <td><b><p style="margin-top: 2rem;">Maria Elena M. Ruiz</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                        <td><b><p style="margin-top: 2rem;">Anthony O. Viray</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                    </tr>
                    <tr>
                        <td><b><p style="margin-top: 2rem;">Agusto D. Ilagan</p><p style="margin-top: -1rem;">Kagawad</p></b></td>
                    </tr>
                    <tr>
                        <td><b><p style="margin-top: 5rem; margin-right: 5rem;">Attested by:</p></b></td>
                        <td><b><p style="margin-top: 5rem; margin-right: 5rem;">Certified True and Correct:</p></b></td>
                    </tr>
                    <tr>
                        <td><b><p>Rovie Rose M. Bernabe</p><p style="margin-top: -1rem;">Barangay Secretary</p></b></td>
                        <td><b><p style="margin-right: 4rem;">ESPERANZA CASTRO-LEE</p><p style="margin-top: -1rem; margin-right: 9.2rem;">Punong Barangay</p></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </thead>
</table>

<style>
    .margin-top-print {
        margin-top: 40px;
    }
</style>