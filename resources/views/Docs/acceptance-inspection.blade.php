<div class="margin-top-print">
    <table class="table table-bordered">
        <tbody>
            <tr align="center">
                <th colspan="6">
                    ACCEPTANCE AND INSPECTION REPORT
                </th>
            </tr>
            <tr align="left">
                <td colspan="6">
                    <div class="row pb-2" style="margin-left: 2px">
                        <div class="col-5">
                            <span>Barangay: <b>Blue Ridge B</b></span>
                        </div>
                        <div class="col-7" style="margin-left: 3em">
                            <span>City/Municipality: <b>Quezon City</b></span>
                        </div>
                    </div>
                    <div class="row pb-2" style="margin-left: 2px">
                        <div class="col-5">
                            <span>Tel. No.:</span>
                        </div>
                        <div class="col-7" style="margin-left: 9.5em">
                            <span>Province: <b>NCR</b></span>
                        </div>
                    </div>    
                </td>
            </tr>
            <tr align="left">
                <td>
                    Supplier: <b>{{ (!empty($inventory_receiving) ? $inventory_receiving->purchaseOrder->getCanvass->getSuppliers->supplier_name :'') }}</b>
                    <br>PO No.: <b>{{ $po->po_no }}</b>
                    <br>Date: <b>{{ (!empty($inventory_receiving) ? App\Common::convertDateFormat($inventory_receiving->purchaseOrder->po_date) : '') }}</b>
                </td>
                <td>
                    Invoice No.: <b>{{$inventory_receiving->receipt_number == null ? 'N/A' : $inventory_receiving->receipt_number}}</b>
                    <br> RER No.: <b>{{$inventory_receiving->rer_number == null ? 'N/A' : $inventory_receiving->rer_number}}</b>
                    <br>Date: <b>{{ (!empty($inventory_receiving) ? App\Common::convertDateFormat($inventory_receiving->date_received) : '') }}</b>
                </td>
                <td>
                    AIR No.: <b>{{ $po->po_no }}</b>
                    <br>Date: <b>{{ (!empty($inventory_receiving) ? App\Common::convertDateFormat($inventory_receiving->purchaseOrder->po_date) : '') }}</b>
                </td>
                <td>
                    RIS No.:<br>Date:
                </td>
            </tr> 
            <table class="table table-bordered mt-n3">
                <tr align="center">
                    <td style="18%">
                        Unit
                    </td>
                    <td style="width: 55%">
                        Description
                    </td>
                    <td style="width: 25.5%">
                        Quantity
                    </td>
                </tr>
                @if(!empty($inventory_receiving))
                    @foreach($inventory_receiving->getReceivingItems as $data => $val)
                    <tr align="center">
                        <td style="18%">
                            <b>{{ $val->getCanvassItem->getPRItems->getUnits->unit_name }}</b>
                        </td>
                        <td style="width: 55%">
                            <b>{{ $val->getCanvassItem->getPRItems->getInventoryName->description }}</b>
                        </td>
                        <td style="width: 25.5%">
                            <b>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity_received) }}</b>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr align="center">
                        <td style="18%"></td>
                        <td style="width: 55%"></td>
                        <td style="width: 25.5%"></td>
                    </tr>
                @endif
                <table class="table table-bordered mt-n3">
                    <tr align="center">
                        <td class="wdth50">
                            ACCEPTANCE
                        </td>
                        <td class="wdth50">
                            INSPECTION
                        </td>
                    </tr>
                    <tr>
                        <td class="pt-3">
                            <div>
                                Date Received:
                                <b>{{ (!empty($inventory_receiving) ? App\Common::convertDateFormat($inventory_receiving->date_received) : '') }}</b>
                            </div>
                            <div class="pt-4">
                                <span>
                                    <input type=checkbox disabled="disabled" {{ (!empty($inventory_receiving) ? ($totalcount == $total_recs ? 'checked': '' ) : '') }} class="checkbox-form"/>
                                </span>
                                <span class="lbl-chckbox2 ">Complete</span>
                                <br>
                                <span>
                                    <input type=checkbox disabled="disabled" {{ (!empty($inventory_receiving) ? ($totalcount != $total_recs ? 'checked': ''  ) : '') }} class="checkbox-form mt-4"/>
                                </span>
                                <span class="lbl-chckbox2">Partial(Pls. specify quantity)</span>
                                <br class="lheight0">
                                <span class="pl-6rm">received <b style="text-decoration: underline">
                                {{$total_recs}}</b></span>
                                <!-- {{ (!empty($inventory_receiving) ? ($totalcount != $total_recs ? $total_recs : ''  ) : '') }}</b></span> -->
                            </div>
                            <div class="pt-4rm pb-md-3" align="center">
                                <underline style="border-bottom: 2px solid">Michell V. Meniano</underline><br>
                                <span class="sign-name" ref="#sign-position1">Signature over Printed Name</span><br>
                                <span class="sign-position" id="sign-position1">Barangay Treasure</span>
                            </div>
                        </td>
                        <td class="pt-3">
                            <div>
                                Date Inspected:
                                <b>{{ (!empty($inventory_receiving) ? App\Common::convertDateFormat($inventory_receiving->date_received) : '') }}</b>
                            </div>
                            <div class="pt-4">
                                <span>
                                    <input type="checkbox" disabled="disabled" checked class="checkbox-form"/>
                                </span>
                                <span class="lbl-chckbox2">Inspected, verified as to quantity</span>
                                <br class="lheight0">
                                <span class="pl-6rm">and specifications</span>
                                <br><br><br><br>
                            </div>
                            <div class="pt-4rm pb-md-3" align="center" style="margin-top: -10px">
                                <underline style="border-bottom: 2px solid">{{ $inventory_receiving->inspector == null ? 'Kgd. Thomas J.T.F. de Castro': $inventory_receiving->inspector }}</underline><br>
                                <span class="sign-name" ref="#sign-position1">Signature over Printed Name</span><br>
                                <span class="sign-position" id="sign-position1">Authorized Inspector</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </table>
        </tbody>
    </table>
</div>
<style> 
    .pl-13rm{
        padding-left: 13rem;
    }
    .height-14{
        height:14rem;
    }
    .overline{
        text-decoration: overline;
    }
    .checkbox-form{
        width: 5rem;
        height: 2rem;
    }
    .pl-6rm{
        padding-left: 6rem;
    }
    .pt-6rm{
        padding-top: 6rem;
    }
    .pt-4rm{
        padding-top: 4rem;
    }
    .lbl-chckbox1{
        position: absolute;
        line-height: 0rem;
    }
    .lbl-chckbox2{
        display: flex;
        margin-top: -2rem;
        padding-left: 5rem;
    }
    .lheight0{
        line-height: 0rem;
    }
    .wdth50{
        width: 50%;
    }
    .margin-top{
        margin-top: 20px !important;
    }
    .width-50rm{
        width: 50rem!important;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid black !important;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>
