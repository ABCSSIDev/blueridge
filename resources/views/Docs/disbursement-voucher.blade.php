<table class="table table-bordered margin-top-print">
  <thead>
    <tr>
      <th colspan="2" class="text-center"><b>Disbursement Voucher</b></th>
      <td>DV No.: <b>{{ $roas->voucher_no }}</b></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-left td-border">Barangay: <b>Blue Ridge B</b></td>
      <td class="text-left">City/Municipality: <b>Quezon City</b></td>
      <td class="text-left">Date: 
        <b>@php if( !empty($roas))
            echo App\Common::convertDateFormat($roas->getROA->approval_date); 
            else
            echo '08-Apr-19';
        @endphp</b>
      </td>
    </tr>
    <tr>
      <td class="text-left">Payee: <b>{{(!empty($roas) ? $roas->getClaimant->name : 'YOLANDA M. LEGARA')}}</b></td>
      <td class="text-left">Province: <b>NCR</b></td>
      <td class="text-left">Fund:  
        <b>{{ $roas->getRoa->getLedger->ledger_name }}</b>
        <!-- <b>{{ (!empty($fund) ? number_format($fund->amount,2) : '')}}</b>  -->
      </td>
    </tr>
    <tr>
      <td class="text-left">Address: <b>{{(!empty($roas) ? $roas->getClaimant->address : 'Quezon City')}}</b></td>
      <td class="text-left">TIN:   <b>{{(!empty($roas) ? $roas->tin : '')}}</b></td>
      <td class="text-left"></td>
    </tr>
    <tr>
      <th colspan="2" class="text-center">Particulars</th>
      <th class="text-center">Amount</th>
    </tr>
    <tr>
      <!-- <td colspan="2" style="padding: 8px"><b>{{App\Common::NumberToWords($roas->getROA->total_amount, true)}} for {{ $roas->getRoa->getLedger->ledger_name }}</b></td> -->
      <td colspan="2"><b>{{ $roas->description }}</b></td>
      <td><b>₱ {{(!empty($roas) ? number_format($roas->getROA->total_amount,2) : '47,799.00' ) }}</b></td>
    </tr>
    <tr>
      <td style="width: 33%">
        <p style="text-align: left;"><b>A. Certified</b> as to existence of appropriation for obligation</p>
        <p style="margin-top: 4rem;">Anna Francesca L. Maristela</p>
        <p class="a-cert"></p>
        <p style="margin-top: -1rem;">(Signature Over Printed Name)</p>
        <p style="margin-top: -1rem;">Chairman,&nbsp;Committee&nbsp;on<br>Appropriation</p>
        <p style="text-align: left;">Date:<p class="a-dv-date"></p></p>
      </td>
      <td style="width: 33%">
        <p style="text-align: left;"><b>B. Certified</b> as to availability of funds for the purpose and completeness and propriety of supporting documents</p>
        <p style="margin-top: 2rem;">Michell V. Meniano</p>
        <p class="a-cert"></p>
        <p style="margin-top: -1rem;">(Signature&nbsp;Over&nbsp;Printed&nbsp;Name)</p>
        <p style="margin-top: -1rem;">Barangay&nbsp;Treasurer</p>
        <p style="text-align: left;">Date:<p class="a-dv-date"></p></p>
      </td>
      <td style="width: 34%">
        <p style="text-align: left;"><b>C. Certified</b> as to validity, propriety and legality of claim and approved for payment</p>
        <p style="margin-top: 3rem;"><b>ESPERANZA CASTRO-LEE</b></p>
        <p class="a-cert"></p>
        <p style="margin-top: -1rem;">(Signature&nbsp;Over&nbsp;Printed&nbsp;Name)</p>
        <p style="margin-top: -1rem;">Punong&nbsp;Barangay</p>
        <p style="text-align: left;">Date:<p class="a-dv-date"></p></p>
    </td>
    </tr>
    <tr>
      <td style="width: 33%; border-right-style: hidden;">
        <p style="text-align: left;"><b>D. Received Payment:</b></p>
        <p style="margin-top: 4rem;">Michell V. Meniano</p>
        <p class="a-cert"></p>
        <p style="margin-top: -1rem;">(Signature&nbsp;Over&nbsp;Printed&nbsp;Name)</p>
      </td>
      <td style="width: 33%; border-right-style: hidden; text-align: left;">
        <p style="margin: 0; margin-top: 1rem;">Check No.: <b style="text-decoration: underline;">{{ !empty($roas) ? $roas->check_number: '672163' }}</b></p>
        <p style="margin: 0;">Bank Name: <b style="text-decoration: underline;">Landbank</b></p>
        <p style="margin: 0;">RER No.:&nbsp;{{ $roas->rer == null ? 'N/A' : $roas->rer }}</p>
        <p style="margin: 0;">O.R No.: <div style="word-break: break-all;">{{ ($roas->invoice_no == null ? 'N/A' : $roas->invoice_no) }}</div></p>
      </td>
      <td style="width: 34%;"><p style="text-align: left; margin-top: 3rem;">Date: <b style="text-decoration: underline;"> {{ App\Common::convertDateFormat($roas->cheque_date) }}</b></p><p class="a-dv-date"></p></td>
    </tr>
    <tr>
      <th colspan="3" class="text-left">E. Accounting Entries</th>
    </tr>
  </tbody>
    <table class="table table-bordered wdth-table" style="margin-top: -16px;">
    <thead>
      <tr>
          <th class="text-center" style="width:40%">Account</th>
          <th class="text-center" style="width:20%">Account Code</th>
          <th class="text-center" style="width:20%">Debit</th>
          <th class="text-center" style="width:20%">Credit</th>
      </tr>
      <tr>
          <td><b>{{ (!empty($roas) ? $roas->getRoa->getLedger->ledger_name : 'Supplies and materials expenses') }}</b></td>
          <td><b>{{ (!empty($roas) ? $roas->getRoa->getLedger->account_code : '5-02-03-010') }}</b></td>
          <td><b>₱ {{ (!empty($roas) ? number_format($roas->getRoa->total_amount,2) : '47,799.00') }}</b></td>
          <td></td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td><b>₱ {{ (!empty($roas) ? number_format($roas->getROA->total_amount,2) : '47,799.00') }}</b></td>
      </tr>
      <tr>
          <td width="30%" colspan="1"></td>
          <td width="70%" colspan="4">
            <table width="100%">
                <tr>
                  <td style="border: none !important; text-align: left;"><b>Prepared By:</b></td>
                </tr>
                <tr>
                    <td width="50%" style="border: none !important;"><p></p><p class="a-cert"></p><p style="margin-top: -1rem;">Barangay Bookkeeper</p></td>
                    <td width="50%" style="border: none !important;"><p></p><p class="a-cert"></p><p style="margin-top: -1rem;">Date</p></td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                  <td style="border: none !important; text-align: left;"><b>Approved By:</b></td>
                </tr>
                <tr>
                    <td width="50%" style="border: none !important;"><p></p><p class="a-cert"></p><p style="margin-top: -1rem;">City/Municipal Accountant</p></td>
                    <td width="50%" style="border: none !important;"><p></p><p class="a-cert"></p><p style="margin-top: -1rem;">Date</p></td>
                </tr>
            </table>
          </td> 
      </tr>
    </thead>
  </table>
</table>
<!-- <form class="form-horizontal" role='form' method="POST" enctype="multipart/form-data">
  <table class="table table-bordered text-center" width="100%">
      <thead>
          <tr>
              <td style="padding: 0; padding-left: 0.75rem;" colspan="3"><p style="text-align: left; margin: 0;">Republic of the Philippines</p><p>DISBURSEMENT VOUCHER</p></td>
              <td style="padding: 0; padding-left: 0.75rem;" colspan="3"><p style="margin: 0;">VOUCHER NUMBER:</p><p>{{ $roas->voucher_no }}</p></td>
          </tr>
          <tr>
              <td colspan="6" style="padding: 0; padding-left: 0.75rem;"><p style="text-align: left; margin: 0;">Paying Agency: <b>BARANGAY BLUE RIDGE B</b></p></td>
          </tr>
          <tr>
              <td colspan="6" style="padding: 0; padding-left: 0.75rem;"><p style="text-align: left; margin: 0;"><b>Name of Claimant:</b> <b>{{ $roas->name }}</b></p></td>
          </tr>
          <tr>
              <td colspan="2" style="padding: 0; padding-left: 0.75rem;"><p style="text-align: left; margin: 0;">Address of Claimant: {{ $roas->claimant_address }}</p></td>
              <td style="text-align: left; margin: 0;">Account No.</td>
              <td style="border-left-style: hidden;">&nbsp;</td>
              <td style="border-left-style: hidden;">&nbsp;</td>
              <td style="border-left-style: hidden;">&nbsp;</td>
          </tr>
          <tr>
              <td colspan="4" style="padding: 0; padding-left: 0.75rem;"><p style="letter-spacing: 6px; font-weight: 600; margin: 0;">PARTICULARS OF PAYMENT</p></td>
              <td colspan="2" style="font-weight: 600; vertical-align: middle; padding: 0; padding-left: 0.75rem;">AMOUNT</td>
          </tr>
          <tr>
              <td colspan="4" style="padding: 0; padding-left: 0.75rem;"><p style="text-align: left; margin: 0;">{{ $roas->description }}</p></td>
              <td colspan="2" style="padding: 0; padding-left: 0.75rem; font-weight: 600; vertical-align: middle;">₱{{ number_format($roas->getRoa->total_amount,2) }}</td>
          </tr>
          <tr>
              <td colspan="4" style="padding: 0; padding-left: 0.75rem;"><p style="text-align: left; margin: 0;">&nbsp;</p></td>
              <td colspan="2" style="padding: 0; padding-left: 0.75rem; font-weight: 600; vertical-align: middle; margin: 0;">&nbsp;</td>
          </tr>
          <tr>
              <td colspan="6" style="padding: 0; padding-left: 0.75rem; border-bottom-style: hidden;"><p style="text-align: left; width: 50%; margin: 0;">CERTIFIED: Expenses/Cash Advance necessary, lawful and incurred under my direct supervision.</p></td>
          </tr>
          <tr>
              <td colspan="3"></td>
              <td colspan="3" style="padding: 0; padding-left: 0.75rem; font-weight: 600; vertical-align: middle; border-left-style: hidden;"><p style="font-weight: 700; font-size: 15px;">ESPERANZA CASTRO-LEE</p><p style="margin-top: -1rem;">Punong Barangay</p></td>
          </tr>
          <tr>
              <td colspan="6" style="padding: 0; padding-left: 0.75rem;"><p style="letter-spacing: 6px; font-weight: 600; margin: 0;">ACCOUNTING ENTRIES</p></td>
          </tr>
          <tr>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 5px;">OBLIGATION NO.</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 5px;">F.P.A.</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 5px;">OJECT CODE</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 5px;">ACCOUNT CODE</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 5px;">DEBIT</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 5px;">CREDIT</p></td>
          </tr>
          <tr>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 0;">{{ $roas->getRoa->obligation_number }}</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 0;"></p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 0;"></p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 0;">{{ $roas->getRoa->getLedger->account_code }}</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 0;">₱{{ number_format($roas->getRoa->total_amount,2) }}</p></td>
              <td style="padding: 0; padding-left: 0.75rem;"><p style="margin: 0;"></p></td>
          </tr>
          <tr>
              <td style="padding: 0; padding-left: 0.25rem;" colspan="2"><p style="margin: 10px 0px; text-align: left;">Journalized by/ date:</p></td>
              <td style="padding: 0; padding-left: 0.25rem;" colspan="4"><p style="margin: 10px 0px; text-align: left;">Indexed by date:</p></td>
          </tr>
          <tr>
              <td style="padding: 0; padding-left: 0.25rem; border-bottom-style: hidden;" colspan="3"><p style="margin: 10px 0px; text-align: left;">CERTIFIED: Adequate funds/budgetary allotment in the amount of <b style="text-decoration: underline;">₱{{ number_format($roas->getRoa->total_amount,2) }}</b> expenditure properly certified, supported by documents mark (x) per checklist on the back hereof; account codes proper; previous cash advance liquidated/accounted for.</p></td>
              <td style="padding: 0; padding-left: 0.25rem;" colspan="3"><p style="margin: 10px 0px; text-align: left;">APPROVED</p></td>
          </tr>
          <tr>
            <td colspan="3">
                <table style="width: 100%;">
                <tr>
                    <td style="border: none;"><div style="top: 2rem; position: relative;"><p style="font-weight: 700;">Michelle V. Meniano</p><p style="font-weight: 700; margin-top: -1rem;">Barangay Treasurer</p></div></td>
                    <td style="border: none;"><div style="top: 2rem; position: relative;"><p style="font-weight: 700;">Anna Francesca L. Maristela</p><p style="font-weight: 700; margin-top: -1rem;">Accountant Officer</p></div></td>
                </tr>
                </table>
            </td>
            <td colspan="3">
                <table style="width: 100%;">
                    <tr>
                        <td style="border: none;"><div style="top: 2rem; position: relative;"><p style="font-weight: 700; font-size: 15px;">ESPERANZA&nbsp;CASTRO-LEE&nbsp;</p><p style="font-weight: 700; margin-top: -1rem;">Punong Barangay</p></div></td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr>
              <td colspan="2">
                  <p style="letter-spacing: 6px; font-weight: 600;">CHECK</p>
              </td>
              <td>
                  <p>RECIEVED FROM:</p>
              </td>
              <td colspan="3">
                  <p>FOR COA USE ONLY</p>
              </td>
          </tr>
          <tr>
              <td>Check No.</td>
              <td>{{ $roas->check_number }}</td>
              <td rowspan="6" style="vertical-align: middle">
                <p style="margin: 0; position: relative;font-weight: 600; font-size: 14px;">MICHELLE V. MENIANO</p>
                <p style="margin: 0; position: relative;font-weight: 600;">Barangay Treasurer</p></td>
              <td rowspan="6" colspan="3" style="vertical-align: middle">ALLOWED IN AUDIT</td>
          </tr>
          <tr>
              <td>Amt</td>
              <td>₱{{ number_format($roas->getRoa->total_amount,2) }}</td>
          </tr>
          <tr>
              <td>Date</td>
              <td>{{ App\Common::convertWordDateFormat($roas->getRoa->approval_date) }}</td>
          </tr>
          <tr>
              <td colspan="2" style="padding: 0; font-weight: 600;">OFFICIAL RECEIPT</td>
          </tr>
          <tr>
              <td>OR No.</td>
              <td>{{ ($roas->or_id == null?'N/A':$roas->getInvoice->or_number) }}</td>
          </tr>
          <tr>
              <td style="vertical-align: middle;">Date</td>
              <td style="vertical-align: middle;">{{ App\Common::convertWordDateFormat($roas->getRoa->approval_date) }}</td>
              <td>
                  <p style="margin: 0; position: relative; top: 1rem; font-weight: 600; font-size: 14px;">MICHELL V. MENIANO</p>
                  <p style="margin: 0; position: relative; top: 1rem; font-weight: 600;">Barangay Treasurer</p>
              </td>
              <td colspan="3"><p style="position: relative; top: 1rem;">ALLOWED IN AUDIT</p></td>
          </tr>
      </thead>
  </table>
</form> -->
<style>
  .table-bordered th, .table-bordered td {
    border: 1px solid black !important;
  }
  .margin-top-print {
    margin-top: 40px;
  }
  .table td, .table th {
    padding: 5px !important;
  }
</style> 