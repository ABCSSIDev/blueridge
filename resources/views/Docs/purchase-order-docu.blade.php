<form class="form-horizontal margin-top-print" role='form' method="POST" enctype="multipart/form-data" style="font-size: 24px">
    <table class="table table-bordered purchase-order">
        <tbody>
            <tr align="center">
                <th colspan="5">PURCHASE ORDER</th>
            </tr>
            <tr class="align-left">
                <td colspan="2">
                    <p>Barangay:<b><u>&nbsp;&nbsp;Blue Ridge B&nbsp;&nbsp;</u></b></p>
                    <p class="pb-2">Tel. No.:<b><u>&nbsp;&nbsp;(8)535-9822&nbsp;&nbsp;</u></b></p>
                </td>
                <td colspan="3">
                    <p>City/Municipality:<b><u>&nbsp;&nbsp;Quezon City&nbsp;&nbsp;</u></b></p>
                    <p>Province: <b><u>&nbsp;&nbsp;NCR&nbsp;&nbsp;</u></b></p>
                </td>
            </tr>
            <tr class="align-left">
                <td colspan="2">
                    <p>Supplier:<b><u>&nbsp;&nbsp;{{ empty($canvass) ? "" : $canvass->getSuppliers->supplier_name }}&nbsp;&nbsp;</u></b></p>
                    <p>Address:<b><u>&nbsp;&nbsp;{{ empty($canvass->getSuppliers->address) ? "N/A" : $canvass->getSuppliers->address }}&nbsp;&nbsp;</u></b></p>
                    <p>TIN:<b><u>&nbsp;&nbsp;{{ empty($canvass->getSuppliers->tin) ? "N/A" : $canvass->getSuppliers->tin }}&nbsp;&nbsp;</u></b> </p>
                </td>
                <td colspan="3">
                    <p>PO No.: <b><u>&nbsp;&nbsp;{{ empty($po) ? "" : $po->po_no }}&nbsp;&nbsp;</u></b></p>
                    <p>Date: <b><u>&nbsp;&nbsp;@php echo empty($po->po_date) ? "" : App\Common::convertWordDateFormat($po->po_date); @endphp&nbsp;&nbsp;</u></b></p>
                    <p class="pb-2">Mode of Procurement: </p>
                    @if(!empty($po))
                        <div class="row" style="margin-bottom: 30px;margin-top: 30px">
                            <input type="checkbox" {{ ($po->procurement_mode == "Bidding" ? 'checked': '') }} style="position: relative; left: 3.5rem; bottom: 1rem;">
                            <b style="margin-right: 1rem; margin-left: 1rem;">Bidding</b>
                            <input type="checkbox" {{ ($po->procurement_mode == "Negotiated" ? 'checked': '') }} style="position: relative; left: 4.5rem; bottom: 1rem;">
                            <b style="margin-right: 1rem; margin-left: 2rem;">Negotiated</b>
                            <input type="checkbox" {{ ($po->procurement_mode == "Over the Counter" ? 'checked': '') }} style="position: relative; left: 5rem; bottom: 1rem;">
                            <b style="margin-left: 1rem;">Over the Counter</b>   
                        </div> 
                        <div class="row">
                            <input type="checkbox" {{ ($po->procurement_mode == "Shopping" ? 'checked': '') }} style="position: relative; left: 3.5rem; bottom: 1rem;">
                            <b style="margin-right: 1rem; margin-left: 1rem;">Shopping</b>
                            <input type="checkbox" {{ ($po->procurement_mode == "Repeat Order" ? 'checked': '') }} style="position: relative; left: 4rem; bottom: 1rem;">
                            <b style="margin-right: 1rem; margin-left: 1rem;">Repeat Order</b>
                            <input type="checkbox" {{ ($po->procurement_mode == "Limited Source" ? 'checked': '') }} style="position: relative; left: 4rem; bottom: 1rem;">
                            <b style="margin-right: 1rem;">Limited Source</b>
                        </div>
                    @else
                        <div class="row">
                            <input type="checkbox" style="position: relative; left: 5rem; bottom: 1rem;">
                            <p>Bidding</p>
                            <input type="checkbox" style="position: relative; left: 45px; bottom: 1rem;">
                            <p>Negotiated</p>
                            <input type="checkbox" style="position: relative; left: 45px; bottom: 1rem;">
                            <p>Over the Counter</p>
                        </div>
                        <div class="row" style="margin-top: 1rem;">
                            <input type="checkbox" style="position: relative; left: 5rem; bottom: 1rem;">
                            <p>Shopping</p>
                            <input type="checkbox" style="position: relative; left: 39px; bottom: 1rem;">
                            <p style="margin-right: 2rem;">Repeat Order</p>
                            <input type="checkbox" style="position: relative; left: 45px; bottom: 1rem;">
                            <p>Limited Source</p>
                        </div>
                    @endif
                </td>
            </tr>
            <tr class="align-left">
                <td colspan="5">
                    <b>Gentlemen:</b>
                    <p style="text-indent: 10px; margin-top: 10px;">Please deliver to this office the following article subject to the terms and conditions cobatined herein</p>
                </td>
            </tr>
            <tr class="align-left">
                <td colspan="2">
                    <p>Place of Delivery:  <b><u>&nbsp;&nbsp;{{ $po->place_of_delivery == null ? "" : $po->place_of_delivery }}&nbsp;&nbsp;</u></b></p>
                    <p>Date of Delivery: <b><u>&nbsp;&nbsp;{{ $po->delivery_date == null ? "" : App\Common::convertWordDateFormat($po->delivery_date) }}&nbsp;&nbsp;</u></b></p>
                </td>
                <td colspan="3">
                    <p>Delivery Term: <b><u>&nbsp;&nbsp;{{ empty($po->delivery_term) ? "" : $po->delivery_term }}&nbsp;&nbsp;</u></b></p>
                    <p>Payment Term: <b><u>&nbsp;&nbsp;{{ empty($po->payment_term) ? "" : $po->payment_term }}&nbsp;&nbsp;</u></b></p>
                </td>
            </tr>
            <tr align="center">
                <th style="width: 15%;">Unit</th>
                <th style="width: 30%;">Particulars</th>
                <th style="width: 15%;">Quantity</th>
                <th style="width: 20%;">Unit Cost</th>
                <th style="width: 20%;">Amount</th>
            </tr>
            @php $total = 0;  @endphp
            @if(!empty($canvass_item))
                @foreach($canvass_item as $canvass_value => $item)
                    @php
                        $est_cost = $item->available_quantity * $item->unit_price;
                        $total += $item->unit_price * $item->available_quantity;
                    @endphp
                    <tr align="center">
                        <td>{{ $item->getPRItems->getUnits->unit_symbol }}</td>
                        <td>{{ $item->getPRItems->getInventoryName->description }}</td>
                        <td>{{ number_format($item->available_quantity) }}</td>
                        <td>₱&nbsp;{{ number_format($item->unit_price,2) }}</td>
                        <td>₱&nbsp;{{ number_format($est_cost,2) }}</td>
                    </tr>
                @endforeach
            @else
                <tr align="center">
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                </tr>
                <tr align="center">
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                </tr>
            @endif
            <tr class="align-left">
                <td colspan="5">(Total Amount in words) <b>@php echo App\Common::NumberToWords($total); @endphp</b></td>
            </tr>
            <tr  class="align-left">
                <td colspan="5">
                    <p style="text-indent: 2rem">
                        In case of failure to make full delivery within the time specified above, a penalty of one-tenth (1/10) of one percent for everyday of delay shall be imposed.
                    </p>
                    <p class="p-very p-truly" style="padding-top: 3rem">Very truly yours,</p>
                    <p style="left: 40.5rem; position: relative; padding-top: 10px;">ESPERANZA CASTRO-LEE</p>
                    <p class="btm-border"></p>
                    <p style="left: 45rem; position: relative;">Punong Barangay</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-row">
                        <div class="col-md-12">
                            <p class="align-left">Conforme: </p>
                        </div>
                    </div>
                    <div class="pt-sm-5" align="center" style="margin-top: 1.5rem">
                        <p class="supplier-po"></p>
                        <p style="margin-bottom: 0; position: relative; bottom: 5px;" class="sign-name">(Signature over Printed Name)</p>
                        <p class="sign-position" style="position: relative; top: -10px; padding-bottom: 10px;">Supplier</p>
                    </div>
                    <div align="center" style="margin-top: .5rem">
                        <p class="date-po"></p>
                        <p>Date</p>
                    </div>
                </td>
                <td colspan="3">
                    <div class="form-row">
                        <div class="col-md-12">
                            <p class="align-left">Existence of Available Appropriations of </p>
                        </div>
                    </div>
                    <div class="pt-sm-5" align="center">
                        <p class="com-po">Anna Francesca L. Maristela</p>
                        <p style="margin-bottom: 0; position: relative; bottom: 5px;" class="sign-name">(Signature over Printed Name)</p>
                        <p class="sign-position" style="position: relative; top: -10px; padding-bottom: 10px;">Chairman, Committee on Appropriations</p>
                    </div>
                    <div align="center">
                        <p class="date-po"></p>
                        <p>Date</p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</form>
<style>
    table { 
        page-break-inside:auto
    }
    tr { 
        page-break-inside:avoid; page-break-after:auto
    }
    thead { 
        display:table-header-group
    }
    tfoot { 
        display:table-footer-group
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid black !important;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style> 
