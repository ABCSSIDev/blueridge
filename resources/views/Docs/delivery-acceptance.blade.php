<div align="center" class="margin-top-print">
    <p style="margin-top: 2rem;"><b>Certificate of Delivery and Acceptance</b></p>
</div>
<!-- <div class="margin">
    <p align="center"><b>DELIVERY ACCEPTANCE</b></p>
</div> -->
<div class="margin">
    <p>To Whom It May Concern:</p>
</div>
<div class="margin">
    @if($inventories)
    <p style="text-indent: 3rem; text-align: justify;">
        This is to certify that {{$total_rec_in_words}}
        ({{ (!empty($inventory_receiving) ? $total_rec : '18' ) }}) 
        {{ $disbursement->description }}</p>

    <table class="table table-bordered">
        <thead>
            <tr align="center">
                <th><b>UNIT</b></th>
                <th><b>QTY</b></th>
                <th><b>ITEMS</b></th>
            </tr>
        </thead>
        <tbody>
            @foreach($inventories as $inventory)
            <tr align="center">
                <td>{{ $inventory->getCanvassItem->getPRItems->getUnits->unit_name }}</td>
                <td>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($inventory->quantity_received) }}</td>
                <td>{{ $inventory->getCanvassItem->getPRItems->getInventoryName->description }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    <p style="text-align: justify;">
        have been delivered to this barangay. The articles were accepted last {{ (!empty($inventory_receiving) ? App\Common::convertDateFormat($inventory_receiving->date_received) : '05-Apr-19') }} by the Barangay Chairperson representing the Barangay Council of Blue Ridge B with the following {{$inventory_receiving->receipt_number == null ? '' : 'OR No. ' .$inventory_receiving->receipt_number }} {{$inventory_receiving->rer_number == null ? '' : 'RER No. ' .$inventory_receiving->rer_number }} amounting to ₱ {{ (!empty($inventory_receiving) ? number_format($total_accept,2) : '47,990.00') }}.
    </p>
    @endif
</div>
<div class="margin-top-signatory">
    <div class="row" align="center">
        <div class="col-4 sign-name" ref="#sign-position1">Rovie Rose M. Bernabe</div>
        <div class="col-4 sign-name" ref="#sign-position2">Michell V. Meniano</div>
        <div class="col-4 sign-name" ref="#sign-position3">ESPERANZA CASTRO-LEE</div>
    </div>
    <div class="row" align="center">
        <div class="col-4 sign-position" id="sign-position1">Barangay Secretary</div>
        <div class="col-4 sign-position" id="sign-position2">Barangay Treasurer</div>
        <div class="col-4 sign-position" id="sign-position3">Punong Barangay</div>
    </div>
</div>
<style>
    .margin-top{
        margin: 100px 0 100px 0;
    }
    .margin-top-signatory{
        margin: 200px 0 200px 0;
    }
    .margin{
        margin: 30px 0 30px 0;
    }
    .margin-top-print {
        margin-top: 40px;
    }
    table { 
        page-break-inside:auto
    }
    tr { 
        page-break-inside:avoid; page-break-after:auto
    }
    thead { 
        display:table-header-group
    }
    tfoot { 
        display:table-footer-group
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid black !important;
    }
</style>