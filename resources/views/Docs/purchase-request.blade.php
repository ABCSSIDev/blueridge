
<form class="form-horizontal " role='form' method="POST" enctype="multipart/form-data">
    <table class="table table-bordered print-request">
        <tbody>
            <tr align="center">
                <th colspan="6">
                    PURCHASE REQUEST
                </th>
            </tr>
            <tr align="left" colspan="6">
                <td colspan="3" style="width: 50%" class="text-left">
                    <div class="pt-3 pb-2">
                        <p>Barangay: <b>Barangay Blue Ridge B</b></p>
                    </div>
                    <div class="pb-2">
                        <p>City/Municipality: <b>Quezon City</b></p>
                    </div>
                    <div class="pb-2">
                        <p>Province: <b>NCR</b></p> 
                    </div>    
                </td>
                <td colspan="3" style="width: 50%" class="text-left"> 
                    <div class="pt-3 pb-2">
                        <p>P.R. No.: <b>{{ (!empty($PurchaseRequestDetails) ? $PurchaseRequestDetails->pr_no : '' ) }}</b></span></p>
                    </div>
                    <div class="pb-2">
                        <p>Date: <b>@php if( !empty($PurchaseRequestDetails))
                                    echo App\Common::convertWordDateFormat($PurchaseRequestDetails->request_date); 
                                    else
                                    echo '';
                                @endphp</b></p>
                    </div>
                </td>
            </tr>
            <tr align="center">
                <th colspan="6">
                    REQUISITION
                </th> 
            </tr>
            <tr align="center">
                <td class="p-leftright">
                    Item<br>Number
                </td>
                <td class="p-4 p-leftright">
                    QTY
                </td>
                <td class="p-leftright">
                    Unit of<br>Measurement
                </td>
                <td class="p-leftright">
                    Item Description
                </td>
                <td class="p-leftright">
                    Etimated<br>Unit Cost
                </td>
                <td class="p-leftright">
                    Estimated<br>Amount
                </td>
            </tr>
            @if(!empty($PurchaseRequestDetails))
            @foreach($subPurchaseRequestDetails as $data => $val)
            <tr align="center" class="">
                <td class="p-leftright"><b>{{ ++$data }}</b></td>
                <td class="p-leftright"><b>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity) }}</b></td>
                <td class="p-leftright"><b>{{ $val->getUnits->unit_symbol }}</b></td>
                <td class="p-leftright"><b>{{ $val->getInventoryName->description }}</b></td>
                <td class="p-leftright"><b>₱&nbsp;{{ number_format($val->unit_cost, 2) }}</b></td>
                <td class="p-leftright"><b>₱&nbsp;{{ number_format($val->quantity * $val->unit_cost , 2)}}</b></td>
            </tr>
            @endforeach
            <tr>
                <td colspan="6" class="text-left">
                    Total Estimated Amount: ₱
                    <b>@php if(!empty($PurchaseRequestDetails))
                            echo App\Http\Controllers\PurchaseRequestController::PurchaseRequestTotal($PurchaseRequestDetails->id);
                    @endphp</b>
                </td>
            </tr>
            @else
            <tr align="center" class="">
                <td class="p-leftright">
                    <br>
                </td>
                <td class="p-4 p-leftright">
                    
                </td>
                <td class="p-leftright">
                    <br>
                </td>
                <td class="p-leftright">
                    
                </td>
                <td class="p-leftright">
                    <br>
                </td>
                <td class="p-leftright">
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="text-left">
                    Total Estimated Amount
                </td>
            </tr>
            @endif
        </tbody>
        <table class="table table-bordered mt-n3 print-request">
            <tbody>
                <tr class="text-left">
                    <td colspan="6" class="height-6">
                        Purpose: {{$PurchaseRequestDetails->purpose}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width: 50%">
                        <div class="text-left">
                            Requested by:
                        </div>
                        <div class="pt-sm-5">
                            <span class="pr-overline sign-name" ref="#sign-position1">Michelle V. Meniano</span><br>
                            <span class="pr-overline sign-name" ref="#sign-position1">Signature over Printed Name</span><br>
                            <span class="sign-position" id="sign-position1">Barangay Treasurer</span>
                        </div>
                        <div class="pt-sm-5 pb-3">
                            <p>Date <span class="top-prline mt-n4"></span></p>
                        </div>
                    </td>
                    <td colspan="3" style="width: 50%"> 
                        <div class="text-left">
                            Approved:
                        </div>
                        <div class="pt-sm-5">
                            <span class="pr-overline sign-name" ref="#sign-position1">ESPERANZA CASTRO-LEE</span><br>
                            <span class="pr-overline sign-name" ref="#sign-position2">Signature over Printed Name</span><br>
                            <span class="sign-position" id="sign-position2">Punong Barangay</span>
                        </div>
                        <div class="pt-sm-5 pb-3">
                            <p>Date <span class="top-prline mt-n4"></span></p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </table>
</form>
<style>
    table { 
        page-break-inside:auto
    }
    tr { 
        page-break-inside:avoid; page-break-after:auto
    }
    thead { 
        display:table-header-group
    }
    tfoot { 
        display:table-footer-group
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid black !important;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style> 
