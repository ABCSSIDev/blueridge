<form class="form-horizontal margin-top-print" role='form' method="POST" enctype="multipart/form-data" style="font-size: 24px">
    <div class="margin">
        <p>The City Auditor</p>
        <p>COA, Quezon City</p>
    </div>
    <div class="margin">
        <p align="center"><b>NOTICE OF DELIVERY</b></p>
    </div>
    <div class="margin-tb">
        <p class="indent-2">This is to inform your office that {{ (!empty($po) ? '('.$item_no.')' : 'Eighteen (18)') }}  items of purchase of {{ $item->getPRItems->getPurchaseRequest->RoaDetails->getLedger->ledger_name }} were delivered to this Barangay per Purchase 
        Order #{{ (!empty($po) ? $po->po_no : '2020-01-01A' ) }} dated {{ (!empty($po) ? App\Common::convertWordDateFormat($po->po_date) : 'April 03, 2019' ) }} amounting to {{ App\Common::NumberToWords($total) }} (₱ {{ (!empty($po) ? number_format($total,2) : '' ) }})
        </p>
    </div>
    <table class="table table-bordered">
        <tr align="center">
            <th style="width: 15%;">Unit</th>
            <th style="width: 30%;">Particulars</th>
            <th style="width: 15%;">Quantity</th>
            <th style="width: 20%;">Unit Cost</th>
            <th style="width: 20%;">Amount</th>
        </tr>
        @php $total = 0;  @endphp
        @if(!empty($canvass_item))
            @foreach($canvass_item as $canvass_value => $item)
                @php
                    $est_cost = $item->available_quantity * $item->unit_price;
                    $total += $item->unit_price * $item->available_quantity;
                @endphp
                <tr align="center">
                    <td>{{ $item->getPRItems->getUnits->unit_symbol }}</td>
                    <td>{{ $item->getPRItems->getInventoryName->description }}</td>
                    <td>{{ number_format($item->available_quantity) }}</td>
                    <td>₱&nbsp;{{ number_format($item->unit_price,2) }}</td>
                    <td>₱&nbsp;{{ number_format($est_cost,2) }}</td>
                </tr>
            @endforeach
        @else
            <tr align="center">
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
            </tr>
            <tr align="center">
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td>&nbsp</td>
            </tr>
        @endif
    </table>
    <table width="100%" class="mt-02 text-center">
        <tr>
            <td width="60%"></td>
            <td width="40%"><p class="sign-name p-position-name" ref="#sign-position1" style="border-bottom: 2px solid">ESPERANZA CASTRO-LEE</p></td>
        </tr>
        <tr>
            <td width="60%"></td>
            <td width="40%"><p class="sign-position" id="sign-position1" style="margin-top: 0px;">Punong Barangay</p></td>
        </tr>
        <!-- <tr>
            <td width="80%"></td>
            <td width="30%"><p class="sign-position" id="sign-position1" style="margin-top: 0px;">Punong Barangay</p></td>
        </tr> -->
    </table>
</form>

<style>
    table { 
        page-break-inside:auto
    }
    tr { 
        page-break-inside:avoid; page-break-after:auto
    }
    thead { 
        display:table-header-group
    }
    tfoot { 
        display:table-footer-group
    }
    .text-center {
        text-align: center;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>