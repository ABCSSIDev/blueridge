@if(!empty($canvass))
@foreach($canvass as $key => $value)
<div class="col-md-12 margin-top-print">
    <div class="">
        <div class="form-row">
            <div class="col-md-6">
                <p style="margin-bottom: auto;">Supplier Name: <span class="fw-700">{{$value->getSuppliers->supplier_name}}</span></p>
                <p>Supplier Address: {{$value->getSuppliers->address}}</p>
            </div>
            <div class="col-md-4">
                <!-- <p>Date: {{ App\Common::convertWordDateFormat($value->created_at)}}</p> -->
                <p>Date: </p>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-12">
                <p>Sir/Madam:</p>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-12">
                <p style="text-indent: 2rem;">
                    Please quote your lowest government net price/s tas/es included on the item/s
                    listened hereunder specifying terms of sales, delivery and applicability of quotation
                    and submit to this office within two (2) government working days for repair and/or job order receipt hereof.
                </p><br>
            </div>
        </div>
    </div>
    <table class="table table-bordered text-center">
        <tr> 
            <th>ITEM</th>
            <th>QTY</th> 
            <th>UNIT</th> 
            <th>DESCRIPTION OF ARTICLES</th> 
            <th>UNIT PRICE</th> 
            <th>Available QTY</th> 
            <th>AMOUNT</th>
        </tr>
        @php ($count = 1) ; $total = 0; @endphp
        @foreach($canvass_items as $items)
        @if($value->id == $items->canvass_id)
            @foreach($purchase_request_items as $pr_item) 
            @if($pr_item->id == $items->pr_item_id)
                @php $total = $total + ($items->available_quantity * $items->unit_price); @endphp
                <tr align="center">
                    <td>{{$count++}}</td>
                    <td>{{number_format($pr_item->quantity)}}</td>
                    <td>{{$pr_item->getUnits->unit_symbol}}</td>
                    <td>{{$pr_item->getInventoryName->description}}</td>
                    <td>₱&nbsp;{{number_format($items->unit_price,2)}}</td>
                    <td>{{number_format($items->available_quantity)}}</td>
                    <td>₱&nbsp;{{number_format($items->available_quantity * $items->unit_price,2)}}</td>
                </tr>
            @endif
            @endforeach
        @endif
        @endforeach
        <tr>
            <td class="fw-700" colspan="6" align="right">TOTAL</td>
            <td class="fw-700">₱&nbsp;{{ number_format($total,2) }}</td>
        </tr>
    </table>
    <div class="form-row">
        <div class="form-group col-md-5">
            <p>Attested by:</p><br>
            <p>Rovie Rose M. Bernabe</p>
            <p style="margin-top: -10px">Barangay Secretary / Requesting Official</p>
        </div>
        <div class="form-group col-md-4 m-signatory">
            <p style="margin-left: 70px">Actual Canvass</p>
            <p style="margin-left: -5px; text-decoration: overline; margin-top: 3rem;">Signature of Proprietor/Representative</p>
        </div>
    </div>
</div>
<div class="footer"></div>
@endforeach
@else
<div class="col-md-12">
    <div class="container">
        <div class="form-row">
            <div class="col-md-8">
                <p class="fw-700" style="margin-bottom: auto;">SHEIN HARDWARE</p>
                <p>386 B Serrano Rd., Libis, Quezon City</p>
            </div>
            <div class="col-md-4">
                <br><p>Date:</p>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-12">
                <p>Sir/Madam:</p>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-12">
                <p style="text-indent: 2rem;">
                    Please quote your lowest government net price/s tas/es included on the item/s
                    listened hereunder specifying terms of sales, delivery and applicability of quotation
                    and submit to this office within two (2) government working days for repair and/or job order receipt hereof.
                </p><br>
            </div>
        </div>
    </div>
    <table class="table table-bordered text-center">
        <tr> 
            <th>ITEM</th>
            <th>QTY</th> 
            <th>UNIT</th> 
            <th>DESCRIPTION OF ARTICLES</th> 
            <th>UNIT PRICE</th> 
            <th>AMOUNT</th>
        </tr>
        <tr>
            <td>1</td>
            <td>8</td>
            <td>pairs</td>
            <td>Bolt w/ 4 nut & $ washer</td>
            <td>₱28.00</td>
            <td>₱224.00</td>
        </tr>
        <tr>
            <td class="fw-700" colspan="5" align="right">TOTAL</td>
            <td class="fw-700">₱47,799.00</td>
        </tr>
    </table>
    <div class="form-row">
        <div class="form-group col-md-6">
            <p>Attested by:</p><br>
            <p style="margin-bottom: auto; text-indent: 2rem;" class="sign-name" ref="#sign-position1">Rovie Rose M. Bernabe</p>
            <p style="text-indent: 3rem;" class="sign-position" id="sign-position1">Barangay Secretary</p>
        </div>
        <div class="form-group col-md-6">
            <br><p style="margin-bottom: auto;">Actual Canvass</p>
            <p style="margin-left: -5px; text-decoration: overline; margin-top: 3rem;">Signature of Proprietor/Representative</p>
        </div>
    </div>
</div>
@endif
<style>
    table { 
        page-break-inside:auto
    }
    tr { 
        page-break-inside:avoid; page-break-after:auto
    }
    thead { 
        display:table-header-group
    }
    tfoot { 
        display:table-footer-group
    }
    .footer {
        page-break-after: always;
    }
    .table > tbody > tr > td  {
        vertical-align: middle;
    }
    .table > tbody > tr > th  {
        vertical-align: middle;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid black !important;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>