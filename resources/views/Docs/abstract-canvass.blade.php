<div align="center" class="margin-top-print">
    <div class="form-group col-md-12">
        <b>ABSTRACT OF CANVASS</b>
    </div>
</div>
<table class="table table-bordered text-center" style="width: 100%;margin-top: 20px">
    <tr> 
        <th rowspan="3" class="fs-14 pad-0rm">ITEM</th>
        <th rowspan="3" class="fs-14 pad-0rm">QTY</th> 
        <th rowspan="3" class="fs-14 pad-0rm">UNIT</th> 
        <th rowspan="3" class="fs-14 pad-0rm">DESCRIPTION OF ARTICLE</th> 
        <td colspan="3" class="fs-14 pad-0rm" style="text-align: center;">QUOTATION</td> 
        <th rowspan="3" class="fs-14 pad-0rm">AWARDED TO:</th>
    </tr>
    <tr>
    @if(!empty($canvass))
    @foreach($canvass as $value) 
        @foreach($suppliers as $supplier)
            @if($value->supplier_id == $supplier->id)
            <th class="fs-14 pad-0rm">{{$supplier->supplier_name}}</th>
            @endif
        @endforeach
    @endforeach
    @else
        <th class="fs-14 pad-0rm">SUPPLIER 1</th>
        <th class="fs-14 pad-0rm">SUPPLIER 2</th>
        <th class="fs-14 pad-0rm">SUPPLIER 3</th>
    @endif
    </tr>
    <tr>
        @if(!empty($canvass))
            @foreach($canvass as $data)
                <th class="fs-14 pad-0rm">Supplier Address: <p style="font-size: 12px">{{$data->getSuppliers->address}}</p></th>
            @endforeach
        @else
            <td class="fs-14 pad-0rm">Supplier Address:</td>
            <td class="fs-14 pad-0rm">Supplier Address:</td>
            <td class="fs-14 pad-0rm">Supplier Address:</td>
        @endif
    </tr>
    @if(!empty($canvass))
    @foreach($grouped_canvass_items as $key =>  $grouped_item) 
        @if($group_id == $grouped_item->group_id)
        <tr align="center">
                @foreach($purchase_request_items as $pr_item) 
                    @if($pr_item->id == $grouped_item->pr_item_id)
                    <td class="fs-14 pad-0rm">{{$key+1}}</td>
                    <td class="fs-14 pad-0rm">{{number_format($pr_item->quantity)}}</td>
                    <td class="fs-14 pad-0rm">{{$pr_item->getUnits->unit_symbol}}</td>
                    <td class="fs-14 pad-0rm">{{$pr_item->getInventoryName->description}}</td>
                    @endif
                @endforeach
            @foreach($canvass_items as $canvass_item_key => $item) 
                @if($item->getCanvasses->group_id == $grouped_item->group_id && $item->pr_item_id == $grouped_item->pr_item_id)
                    <td class="fs-14 pad-0rm">₱&nbsp;{{number_format($item->available_quantity * $item->unit_price,2)}}</td>
                @endif
            @endforeach
            <td class="fs-14 pad-0rm">{{$supplier_awarded->getSuppliers->supplier_name}}</td>
        </tr>
        @endif
    @endforeach
        <tr>
            <td colspan="4" style="text-align: right;" class="fs-14 pad-0rm">Total</td>
            @foreach($supplier_total as $total)
                <td class="fs-14 pad-0rm" style="text-align: center;">₱&nbsp;{{ number_format($total,2) }}</td>
            @endforeach
            <td class="fs-14 pad-0rm"></td>
        </tr>
    @else
    <tr>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: right;" class="fs-14 pad-0rm">Total</td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
        <td class="fs-14 pad-0rm"></td>
    </tr>
    @endif
</table>
<div class="form-row">
    <div class="form-group col-md-12">
        <p>We hereby certify that the foregoing is the abstract price of quotations and the award is hereby made to <b>{{ (!empty($awarded) ? $awarded->supplier_name : 'Shien Hardware' ) }}</b></p><br>
    </div>
</div>
<table width="100%">
    <tbody>
        <tr class="">          
            <td colspan="2" style="width: 33%">
                <p style="margin-bottom: 40px;">Prepared By:</p>
                <div style="text-align: center;">
                    <div class="form-group col-md-6">
                        <p style="margin-bottom: 0;" class="sign-name" ref="#sign-position1">Rovie Rose M. Bernabe</p>
                        <p style="margin-top: 0;" class="sign-position" id="sign-position1">Barangay Secretary</p>
                    </div>
                </div>
            </td>
    
            <td colspan="2" style="width: 33%">
                <p style="margin-bottom: 40px;">Approved By:</p>
                <div style="text-align: center;">
                    <div class="form-group col-md-6">
                        <p style="margin-bottom: 0;" class="sign-name" ref="#sign-position1">Michelle V. Meniano</p>
                        <p style="margin-top: 0;" class="sign-position" id="sign-position1">Barangay Treasurer</p>
                    </div>
                </div>
            </td>
            
            <td colspan="3" style="width: 34%">
                <p style="margin-bottom: 40px;">Noted By:</p>
                <div class="form-group col-md-6">
                    <div style="text-align: center;">
                        <p style="margin-bottom: 0;" class="sign-name mb-0" ref="#sign-position2">ESPERANZA CASTRO-LEE</p>
                        <p style="margin-top: 0;" class="sign-position" id="sign-position2">Punong Barangay</p>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<style>
    table { 
        page-break-inside:auto
    }
    tr { 
        page-break-inside:avoid; page-break-after:auto
    }
    thead { 
        display:table-header-group
    }
    tfoot { 
        display:table-footer-group
    }
    .table > tbody > tr > td  {
        vertical-align: middle;
    }
    .table > tbody > tr > th  {
        vertical-align: middle;
    }
    .table-abstract-canvass {
        display: block;
        width: 100%;
        overflow-y: auto;
    }
    .mb-0 {
        margin-bottom: 0;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid black !important;
    }
    .margin-top-print {
        margin-top: 40px;
    }
</style>