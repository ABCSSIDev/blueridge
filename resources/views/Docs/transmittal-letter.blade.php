
<div class="margin-t">
    <p>Date:</p>
</div>
<div class="margin">
    <p>To The City/Municipal Accountant</p>
    <p>City/Municipality:</p>
</div>
<div class="margin">
    <p>Sir/Madam:</p>
</div>
<div class="margin">
    <p>We submit herewith the following documents:   
    a) certified copy of the cashbook;
    b) copy of PBCs issued; and
    c) original of the Disbursement Voucher/payroll issued for the (month and year) duly acknowledged by the payees:</p>
</div>
<table class="table table-bordered text-center"> 
    <tr> 
        <th colspan="2">A. DV/Payroll</th>
        <th colspan="2">Check</th> 
        <th rowspan="2">Payee</th> 
        <th rowspan="2">Amount</th> 
        <th colspan="2">PB Certification</th>
    </tr> 
    <tr>
        <td>Date</td>
        <td>No</td>
        <td>Date</td>
        <td>No</td>
        <td>Date</td>
        <td>No</td>
    </tr>
    <tr> 
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left"> 
        <td colspan="8">B. RCDs and Supporting Documents (RCRs ORs and VDS)</td>
    </tr>
    <tr> 
        <td colspan="3">Date</td>
        <td colspan="3">No</td>
        <td colspan="3">Amount</td>
    </tr>
    <tr> 
        <td colspan="3"></td>
        <td colspan="3"></td>
        <td colspan="3"></td>
    </tr>
    <tr> 
        <td colspan="3"></td>
        <td colspan="3"></td>
        <td colspan="3"></td>
    </tr>
    <tr align="left"> 
        <td colspan="8">C. Other Reports</td>
    </tr>
    <tr> 
        <td colspan="4">Date</td>
        <td colspan="4">Type of Report</td>
    </tr>
    <tr> 
        <td colspan="4"></td>
        <td colspan="4"></td>
    </tr>
    <tr> 
        <td colspan="4"></td>
        <td colspan="4"></td>
    </tr>
</table>
<div class="margin">
    <p>Please acknowledge receipt hereof.</p>
</div>
<div align="right">
    <p style="margin-right: 3rem;">Very truly yours,</p><br>
    <p style="margin-right: 3rem;" class="sign-name" ref="#sign-position1"></p>
    <p style="margin-right: 3rem;" class="sign-position" id="sign-position1">Barangay Treasurer</p><br>
</div>
<div class="margin-top">
    <div class="row" align="left">
        <div class="col-6">Noted by:
        <br>
        <br>
        <br>
        <span class="sign-name" ref="#sign-position2"></span>
        </div>
        <div class="col-6">Received by:
        <br>
        <br>
        <br>
        <span class="sign-name" ref="#sign-position3"></span>
        </div>
    </div>
    <div class="row" align="left">
        <div class="col-6 sign-position" id="sign-position2">Punong Barangay</div>
        <div class="col-6 sign-position" id="sign-position3">Signature Name and Designation</div>
    </div>
</div>



<style>
    .margin-t{
        margin-top: 50px;
    }
</style>