@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Purchase Request</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Purchase Request</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('purchase-request.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" id="purchase_form" action="{{ route('purchase-request.update',$PurchaseRequestDetails->id)}}" enctype="multipart/form-data">
                <div class="errors"></div>
                    <input type="hidden" name="_method" value="PUT">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>To</label>
                            <input type="text" class="form-control" name="pur_to" id="pur_to" value="{{$PurchaseRequestDetails->to}}" placeholder="To" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Purchase Request No.</label>
                            <input type="text" class="form-control" name="pr_no"  autocomplete="off" placeholder="Purchase Request No." value="{{ $PurchaseRequestDetails->pr_no }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>From</label>
                            <input type="text" class="form-control" value="{{$PurchaseRequestDetails->from}}" name="pur_from" id="pur_from" placeholder="From" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Request Date</label>
                            <input type="text" class="form-control datetimepicker" name="pur_request_date" id="pur_request_date" placeholder="Request Date" autocomplete="off" value="{{ date('F d, Y', strtotime($PurchaseRequestDetails->request_date)) }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Delivery To</label>
                            <input type="text" class="form-control" name="pur_del_to" value="{{$PurchaseRequestDetails->delivery_to}}" id="pur_del_to" placeholder="Delivery To" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Period of Delivery</label>
                            <input type="text" class="form-control datetimepicker" name="pur_delivery_date" id="pur_delivery_date" placeholder="Period of Delivery" autocomplete="off" value="{{ date('F d, Y', strtotime($PurchaseRequestDetails->delivery_period)) }}">
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Resolution No.</label>
                            <select class="form-control newInput"  disabled>
                                <option value="">Select Resolution No.</option>
                                @foreach($roa as $value)
                                    <option value="{{ $value->id }}" {{ ($value->id == $PurchaseRequestDetails->roa_id?'selected':'')}}>{{$value->resolution_number}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>ROA Amount</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">₱</span>
                                </div>
                                <input type="text" class="form-control" name="pur_amount" id="pur_amount" readonly value="{{ number_format($PurchaseRequestDetails->RoaDetails->total_amount,2) }}">
                            </div>
                            <!-- <input type="text" class="form-control" name="pur_amount" id="pur_amount" readonly value="₱{{ number_format($PurchaseRequestDetails->RoaDetails->total_amount,2) }}"> -->
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purpose</label>
                            <input type="text" class="form-control" name="purpose" id="purpose" value="{{ $PurchaseRequestDetails->purpose }}">
                        </div>
                    </div>
                    <div class="error-container"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table-sm display responsive table-striped table-bordered dataTable no-footer" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                <thead class="tab_header">
                                    <tr align="center">
                                        <th style="text-align: center; width: 5%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending"></th>
                                        <th style="text-align: center; width: 5%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Item No.</th>
                                        <th style="text-align: center; width: 30%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Description</th>
                                        <th style="text-align: center; width: 10%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Unit</th>
                                        <th style="text-align: center; width: 13%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Category.</th>
                                        <th style="text-align: center; width: 11%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Qty</th>
                                        <th style="text-align: center; width: 13%" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Est. Unit Cost</th>
                                        <th style="text-align: center; width: 13%" class="pt2" rowspan="1" colspan="1" style="" aria-label="Action">Est. Cost</th>
                                    </tr>
                                </thead>
                                <tbody id="table_purchase_edit">
                                @foreach($subPurchaseRequestDetails as $data => $val)
                                    <tr align="center">
                                        <input type="hidden" class="for_editing" name="for_editing[]" value="{{$val->id}}" {{ ($Canvass?'disabled':'')}}>
                                        <td></td>
                                        <td class="item_no v-align">{{ $count = ++$data }}</td>
                                        <td class="v-align">
                                            <div class="ui-widget v-align">
                                                <input id="search{{$val->id}}"  name="search{{$val->id}}" data-error=".error-container" type="text" value="{{ $val->getInventoryName->description }}" class="form-control {{ ($Canvass?'':'search')}} newInput" placeholder="Search" autocomplete="off"/ readonly {{ ($Canvass?'disabled':'')}}>
                                                <input type="hidden" class="exist_id" name="exist_id{{$val->id}}" id="exist_id{{$val->id}}" value="{{ $val->getInventoryName->id }}">
                                            </div>
                                        </td>
                                        <td class="v-align">
                                            <select class="form-control pur_unit newInput" data-error=".error-container" name="pur_unit{{$val->id}}" id="pur_unit{{$val->id}}" disabled {{ ($Canvass?'disabled':'')}}>
                                                <option value="">Select Unit Type</option>
                                                @foreach($unit as $value)
                                                <option value="{{ $value->id }}" {{ ($value->id == $val->unit_id?'selected':'') }}>{{ $value->unit_symbol }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="v-align">
                                            <select class="form-control category newInput" data-error=".error-container" name="category{{$val->id}}" id="category{{$val->id}}" disabled {{ ($Canvass?'disabled':'')}}>
                                                <option value="">Select Category</option>
                                                @foreach($category as $value)
                                                <option value="{{ $value->id }}" {{ ($value->id == $val->getInventoryName->category_id?'selected':'') }}>{{ $value->category_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="v-align"><input type="text" onkeypress="return isNumberKeySlash(event);" data-error=".error-container" class="form-control subtotal qty newInput text-center" id="qty{{$val->id}}" name="qty{{$val->id}}" placeholder="Qty" autocomplete="off" data-validation="required" autocomplete="off" value="{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity) }}" {{ ($Canvass?'disabled':'')}}></td>
                                        <td hidden><input type="text" readonly class="form-control  qtyfrans" id="qtyfrans{{$val->id}}" name="qtyfrans{{$val->id}}"  value="{{ $val->quantity }}" {{ ($Canvass?'disabled':'')}}></td>
                                        <td class="v-align"><input type="text" data-error=".error-container" class="form-control subtotal cost newInput" id="cost{{$val->id}}" placeholder="Est. Unit Cost" name="cost{{$val->id}}" autocomplete="off" data-validation="required" autocomplete="off" value="{{ $val->unit_cost }}" {{ ($Canvass?'disabled':'')}}></td>
                                        <td class="v-align"><input type="text" class="form-control subtotalval" autocomplete="off" id="subtotal{{$val->id}}" name="subtotal{{$val->id}}" data-validation="required" readonly value="{{ number_format($val->quantity * $val->unit_cost , 2)}}"></td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div class="row">
                                @if(!$Canvass)
                                <div class="col-12" align="left">
                                    <button class="btn btn-primary addNew" type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus-square" aria-hidden="true"></i> ADD ROW</button>
                                </div>
                                @endif
                                <div class="col-12" align="right">
                                    <label class="total-cost">Total:&nbsp;<input class="input-total-pr" type="text" id="totalcost" name="totalcost" value="₱ {{ $totalamount }}" readonly></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
let rules = {
        pur_to : {
            required: true
        },
        pur_from : {
            required: true
        },
        pur_request_date: {
            required: true
        },
        pur_del_to : {
            required: true
        },
        pur_delivery_date: {
            required: true
        }, 
        search0: {
            required: true
        },
        pur_unit0 : {
            required: true
        },
        qty0: {
            required: true
        },
        pr_no: {
            required: true
        },
        category0 : {
            required: true
        },
        cost0: {
            required: true
        },
    };
    function isNumberKeySlash(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 47 && charCode > 32
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function fractionTodecimal(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
        }
        else {
            fraction_eval = parseInt(fraction);
        }
        return fraction_eval;
    }
$('#purchase_form').registerFields(rules, false);
$('#table_purchase_edit').on('keyup', '.subtotal', function (e) {
    var qty = 0;   
    var cost =0;
    var total =0;   
    if($(this).hasClass('cost')){
        cost = Number($(this).val());
            var qty = fractionTodecimal($(this).parent().siblings('td').children('.qty').val());
            var val = fractionTodecimal1($(this).parent().siblings('td').children('.qty').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
    }
    if( $(this).hasClass('qty') ){
            var qty = fractionTodecimal($(this).val());
            var val = fractionTodecimal1($(this).val());
            console.log(qty);
            if($(this).val() == ""){
                qty = 0;
            }
            cost = $(this).parent().siblings('td').children('.cost').val();
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
    }
    $(this).parent().siblings('td').children('.subtotalval').val(cost * qty);
    $('.subtotalval').each(function(){
        let finalcost = ($(this).val());
        finalcost = finalcost.replace(/,/g, '');
        total += Number(finalcost);
    }); 
    var locale = 'de';
    var options = { minimumFractionDigits: 2, maximumFractionDigits: 2};
    var formatter = new Intl.NumberFormat(locale, options);
    $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));

    // $('#totalcost').val(formatter.format(total));
        
            
}); 
function fractionTodecimal1(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        var decimalParts = '';
        
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
            var str=fraction_eval.toString();
            var numarray=str.split('.');
            var val1 = numarray[0];
            var val2 = decimalParts[0];
            var val3 = decimalParts[1];

            var step1 = (parseInt(val1) * parseInt(val3)) + parseInt(val2);

            return step1+','+val3;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
            var val2 = decimalParts[0];
            var val3 = decimalParts[1];
            return val2+','+val3;
        }
        else {
            var decimalParts = 1;
            fraction_eval = parseInt(fraction);
            return fraction_eval+','+decimalParts;

        }
       
    }
var count = '{{ $count + 1 }}' ;
var countItem = 2;
$(".addNew").click(function(e){
    e.preventDefault();
    $('#table_purchase_edit').append('<tr align="center" class="appendRow">'+
        '<input type="hidden"  class="for_savings" name="for_savings[]" value="'+count+'"><td>'+
            '<button class="btn btn-primary RemoveBtn" type="button" id="dropdownMenuButton"  aria-haspopup="true" aria-expanded="false" >'+
                '<i class="fa fa-minus  "></i>'+
            '</button>'+
        '</td>'+
        '<td class="item_no">'+countItem+'</td>'+
        '<td>'+
        '<div class="ui-widget"><input id="search'+count+'" data-error=".error-container" name="search'+count+'"" autocomplete="off" type="text" class="newInput form-control newInput search'+count+'" placeholder="Search" />'+
        '<input type="hidden" class="exist_id" name="exist_id'+count+'" id="exist_id'+count+'"></div>'+
        '</td>'+
        '<td>'+
            '<select class="form-control newInput pur_unit" data-error=".error-container" name="pur_unit'+count+'" id="pur_unit'+count+'" >'+
                '<option value="">Select Unit Type</option>'+
                @foreach($unit as $product )
                '<option value="{{ $product->id }}">{{ $product->unit_symbol }}</option>'+
                @endforeach
            '</select>'+
        '</td>'+
        '<td>'+
            '<select class="form-control newInput category" data-error=".error-container" name="category'+count+'" id="category'+count+'">'+
                '<option value="">Select Category</option>'+
                @foreach($category as $product )
                '<option value="{{ $product->id }}">{{ $product->category_name }}</option>'+
                @endforeach
        '</td>'+
        '<td hidden><input type="text" placeholder="Qty" class="form-control subtotals qtyfrans" autocomplete="off"  name="qtyfrans'+count+'" id="qtyfrans'+count+'"readonly></td>'+
        '<td><input type="text"  onkeypress="return isNumberKeySlash(event);" placeholder="Qty" data-error=".error-container" class="form-control subtotals qtys newInput text-center" autocomplete="off"  name="qty'+count+'" id="qty'+count+'" autocomplete="off" data-validation="required"></td>'+
        '<td><input type="number"autocomplete="off" data-error=".error-container" placeholder="Est. Unit Cost"class="form-control   subtotals costs newInput" name="cost'+count+'" id="cost'+count+'" autocomplete="off" data-validation="required"></td>'+
        '<td><input type="text" class="form-control subtotalval " readonly name="subtotal'+count+'" id="subtotal'+count+'" autocomplete="off" data-validation="required"></td>'+
    '</tr>');
    updateItem(); // update item number
    $('#table_purchase_edit').on('keyup', '.subtotals', function () {
        var qty = 0;   
        var cost =0;
        var total =0;   
        if($(this).hasClass('costs')){
            cost = Number($(this).val());
            var qty = fractionTodecimal($(this).parent().siblings('td').children('.qtys').val());
            var val = fractionTodecimal1($(this).parent().siblings('td').children('.qtys').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
        }
        // console.log(11);
        if( $(this).hasClass('qtys') ){
            var qty = fractionTodecimal($(this).val());
            var val = fractionTodecimal1($(this).val());
            cost = Number($(this).parent().siblings('td').children('.costs').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
        }
       
        $(this).parent().siblings('td').children('.subtotalval').val(cost * qty);
        
        $('.subtotalval').each(function(){
            let finalcost = ($(this).val());
            finalcost = finalcost.replace(/,/g, '');
            if(finalcost == ""){
                total += 0;
            }else{
                total += Number(finalcost);
            }
        });
        
        var locale = 'de';
        var options = { minimumFractionDigits: 2, maximumFractionDigits: 2};
        var formatter = new Intl.NumberFormat(locale, options);
        $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
        // $('#totalcost').val(formatter.format(total));
       
        
    }); 
    // $( ".search"+count ).keyup(function(){
    //     if($(this).val() == ''){
    //         $(this).siblings('.exist_id').val('');
    //     }
    // });
    $( ".search"+count ).autocomplete({
        select: function( event, ui ) {
            var el = this;
            $(this).prop('readonly',true);
           
            var val = $(this).parent().parent().parent().find('input[class="for_savings"]').val();
            $(this).parent().parent().siblings('td').children('.pur_unit').prop('disabled',true);
            $(this).parent().parent().siblings('td').children('.category').prop('disabled',true);
            $(this).siblings('.exist_id').val(ui.item.id);
            
        },
        source: function(request, response) {
            $.ajax({
            url: "{{route('pur.autocomplete')}}",
            data: {
                    term : request.term
            },
            dataType: "json",
            success: function(data){
            var resp = $.map(data,function(obj){
                
                return {
                            'label': obj.description,
                            'unit' : obj.unit_id,
                            'cat'  : obj.category_id,
                            'id'   : obj.id
                        };
                    
            }); 
            response(resp);
            }
        });
        },
        delay: 0,
        clearButton: true,
        minLength: 1
    });
    count ++;
    countItem ++;
    $('.newInput').each( function () {
        $(this).rules('add',{
            'required' : true
        })
    });
    
    $('.ui-autocomplete-clear').click(function(){

       var val = $(this).parent().parent().parent().find('input[class="for_savings"]').val();
       $('#pur_unit'+val).prop('disabled', false);
       $('#category'+val).prop('disabled', false);
       $('#exist_id'+val).val('');
       $(this).siblings('#search'+val).prop('readonly',false);

    });
    function fractionTodecimal1(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        var decimalParts = '';
        
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
            var str=fraction_eval.toString();
            var numarray=str.split('.');
            var val1 = numarray[0];
            var val2 = decimalParts[0];
            var val3 = decimalParts[1];

            var step1 = (parseInt(val1) * parseInt(val3)) + parseInt(val2);

            return step1+','+val3;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
            var val2 = decimalParts[0];
            var val3 = decimalParts[1];
            return val2+','+val3;
        }
        else {
            var decimalParts = 1;
            fraction_eval = parseInt(fraction);
            return fraction_eval+','+decimalParts;

        }
       
    }
    $("#table_purchase_edit").on('click','.RemoveBtn',function(e){
        e.preventDefault();
        updateItem();
        $(this).parent().parent().remove();
       
    });
  
   
});
$(document).ready(function() {
    $('.newInput').each( function () {
        $(this).rules('add',{
            'required' : true
        })
    });
    // $( ".search" ).keyup(function(){
    //     if($(this).val() == ''){
    //         $(this).siblings('.exist_id').val('');
    //     }
    // });
    $( ".search" ).autocomplete({
        select: function( event, ui ) {
            var el = this;
           
            $(this).parent().parent().siblings('td').children('.pur_unit').prop('disabled',true);
            $(this).parent().parent().siblings('td').children('.category').prop('disabled',true);
            $(this).siblings('.exist_id').val(ui.item.id);
            $(this).prop('readonly',true);

        },
        source: function(request, response) {
                $.ajax({
                url: "{{route('pur.autocomplete')}}",
                data: {
                        term : request.term
                },
                dataType: "json",
                success: function(data){  
                var resp = $.map(data,function(obj){
                    return {
                            'label': obj.description,
                            'unit' : obj.unit_id,
                            'cat'  : obj.category_id,
                            'id'   : obj.id
                        };
                }); 
                response(resp);
                }
            });
        },
        delay: 0,
        clearButton: true,
        minLength: 1
    });
    
    $('.ui-autocomplete-clear').click(function(){
        $(this).siblings('.search').prop('readonly',false);
        $(this).parent().parent().siblings('td').children('.pur_unit').prop('disabled',false);
        $(this).parent().parent().siblings('td').children('.category').prop('disabled',false);
        $(this).siblings('.exist_id').val("");
    });
    $('#pur_roa_no').change(function () {
        $.ajax({
            url: "{{route('pur.roadetails')}}",
            data: {
                    roa : $(this).val()
            },
            dataType: "json",
            success: function(data){
                $('#pur_amount').val(data.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')); 
            },
            
        });
    });
});
//update item no.
function updateItem(){
    var rowCount = 0;
    var ctr = 1;
    var table = document.getElementById("table_purchase_edit");
    rowCount = table.rows.length;
    $('.item_no').each( function () {
        var temp = rowCount - ctr; 
        var temp2 = rowCount -temp;
         $(this).html(temp2);
         ctr++
    });

}

function reloadData(data){
    //this function uses for reloading data needed
    $('#pur_request_no').val(data.new_pr);
    $("#pur_roa_no").empty().append(data.roa_select);
}
</script>
@endpush