@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Purchase Request</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Purchase Request</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('purchase-request.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" id="purchase_form" action="{{ route('purchase-request.store')}}" enctype="multipart/form-data">
                    <div class="errors"></div>
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>To</label>
                            <input type="text" class="form-control" name="pur_to" id="pur_to" value="LOCAL TREASURER" placeholder="To" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Purchase Request No.</label>
                            <input type="text" class="form-control" name="pur_request_no" id="pur_request_no" autocomplete="off" placeholder="Purchase Request No." value="{{ $last_id }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>From</label>
                            <input type="text" class="form-control" value="Barangay Blue Ridge B" name="pur_from" id="pur_from" placeholder="From" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Request Date</label>
                            <input type="text" class="form-control datetimepicker" name="pur_request_date" id="pur_request_date" placeholder="Request Date" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Delivery To</label>
                            <input type="text" class="form-control" name="pur_del_to" value="Barangay Blue Ridge B" id="pur_del_to" placeholder="Delivery To" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Period of Delivery</label>
                            <input type="text" class="form-control datetimepicker" name="pur_delivery_date" id="pur_delivery_date" placeholder="Period of Delivery" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purpose</label>
                            <input type="text" class="form-control" name="purpose" id="purpose" value="" placeholder="Purpose" autocomplete="off">
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Resolution No.</label>
                            <select class="form-control newInput" name="pur_roa_no" id="pur_roa_no" autocomplete="off" >
                                <option value="">Select Resolution No.</option>
                                @if($roa)
                                    @foreach($roa as $value)
                                        <option value="{{ $value->id }}" >{{$value->resolution_number}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>ROA Amount</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">₱</span>
                                </div>
                                <input type="text" class="form-control" name="pur_amount" id="pur_amount" placeholder="0.00" readonly>
                            </div>
                            <!-- <input type="text" class="form-control" name="pur_amount" id="pur_amount" placeholder="0.00" readonly> -->
                        </div>
                    </div>
                    <div class="error-container"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="search-table-outter">
                                <table class="table table-striped table-bordered table-hover table-responsive no-wrap">
                                    <thead class="tab_header">
                                        <tr align="center">
                                            <th></th>
                                            <th class="v-align" width="2%;">Item No.</th>
                                            <th class="v-align" width="30%">Description</th>
                                            <th class="v-align" width="14%">Unit</th>
                                            <th class="v-align" width="10%">Qty</th>
                                            <th class="v-align" width="24%">Category.</th>
                                            <th class="v-align" width="5%">Est. Unit Cost</th>
                                            <th class="v-align" width="13%">Est. Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_purchase">
                                        <tr align="center">
                                            <input type="hidden" class="for_savings" name="for_savings[]" value="0">
                                            <td class="empty-pr">&nbsp;&nbsp;&nbsp;</td>
                                            <td class="item-no-pr item_no v-align">1</td>
                                            <td class="item-description-pr">
                                                <div class="ui-widget">
                                                    <input id="search0" name="search0" type="text" data-error=".error-container" class="form-control search" placeholder="Description" autocomplete="off" title="Add a description">
                                                    <input type="hidden" name="exist_id0" id="exist_id0">
                                                </div>
                                            </td>
                                            <td class="unit-type-pr">
                                                <select class="form-control" name="pur_unit0" id="pur_unit0" data-error=".error-container" title="Select Unit Type">
                                                    <option value="">Select Unit Type</option>
                                                    @foreach($unit as $value)
                                                    <option value="{{ $value->id }}">{{ $value->unit_symbol }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="qty-pr"><input type="text" onkeypress="return isNumberKeySlash(event);" class="form-control subtotal qty text-center" id="qty0" name="qty0" placeholder="Qty" autocomplete="off"></td>    
                                            <td class="category-pr" hidden><input  type="text" readonly class="form-control  qtyfrans" id="qtyfran0" name="qtyfran0"></td>
                                            <td class="category-pr">
                                                <select class="form-control" name="category0" id="category0" data-error=".error-container" title="Select Category">
                                                    <option value="">Select Category</option>
                                                    @foreach($category as $value)
                                                    <option value="{{ $value->id }}">{{ $value->category_name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="est-unit-pr">
                                                <input type="text" class="form-control subtotal cost text-center" onkeypress="return isNumberKeySlash(event);" id="cost0" placeholder="Est. Unit Cost" name="cost0" autocomplete="off" data-error=".error-container" autocomplete="off" title="Estimated Unit Cost">
                                            </td>
                                            <td class="est-cost-pr"><input type="text" class="form-control subtotalval" autocomplete="off" id="subtotal0" name="subtotal0" data-error=".error-container" readonly title="Estimated Cost" placeholder="0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-7" align="left">
                                    <button class="btn btn-primary addNew" type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus-square" aria-hidden="true"></i> ADD ROW</button>
                                </div>
                                <div class="col-5" align="right">
                                    <label class="total-cost">Total&nbsp;: ₱<input class="input-total-pr" type="text" id="totalcost" name="totalcost" readonly></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
<style>
    .pt-03{
        padding: 0.3rem;
    }
</style>

@push('scripts')

<script>

$('#totalcost').val('0.00');
let rules = {
        pur_to : {
            required: true
        },
        pur_request_no: {
            required: true
        },
        pur_from : {
            required: true
        },
        pur_request_date: {
            required: true
        },
        pur_del_to : {
            required: true
        },
        pur_delivery_date: {
            required: true
        },
        pur_roa_no : {
            required: true
        },
        search0: {
            required: true
        },
        pur_unit0 : {
            required: true
        },
        qty0: {
            required: true
        },
        category0 : {
            required: true
        },
        cost0: {
            required: true
        },
        purpose: {
            required: true
        },
    };
    function number_test(n){
        var result = (n - Math.floor(n)) !== 0; 
        
        if (result)
            return 'fraction';
        else
            return 'wholenumber';
   }
    function fractionTodecimal(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
        }
        else {
            fraction_eval = parseInt(fraction);
        }
        return fraction_eval;
    }
    function fractionTodecimal1(fraction){
        var fraction_eval = 0;
        var string = fraction + '';
        var fractionParts = string.split('-');
        var decimalParts = '';
        
        if (fractionParts.length === 1) {
            /* try space as divider */
            fractionParts = string.split(' ');
        }
        if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
            var integer = parseInt(fractionParts[0]);
            var decimalParts = fractionParts[1].split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = integer + decimal;
            var str=fraction_eval.toString();
            var numarray=str.split('.');
            var val1 = numarray[0];
            var val2 = decimalParts[0];
            var val3 = decimalParts[1];

            var step1 = (parseInt(val1) * parseInt(val3)) + parseInt(val2);

            return step1+','+val3;
        }
        else if (fraction.indexOf('/') !== -1) {
            var decimalParts = fraction.split('/');
            var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
            fraction_eval = decimal;
            var val2 = decimalParts[0];
            var val3 = decimalParts[1];
            return val2+','+val3;
        }
        else {
            var decimalParts = 1;
            fraction_eval = parseInt(fraction);
            return fraction_eval+','+decimalParts;

        }
       
    }
$('#purchase_form').registerFields(rules);
$('#table_purchase').on('keyup', '.subtotal', function () {
        var qty = 0;
        var cost =0;
        var total =0;

        if($(this).hasClass('cost')){
            cost = Number($(this).val());
            var qty = fractionTodecimal($(this).parent().siblings('td').children('.qty').val());
            var val = fractionTodecimal1($(this).parent().siblings('td').children('.qty').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
        }

        if( $(this).hasClass('qty') ){
            var qty = fractionTodecimal($(this).val());
            var val = fractionTodecimal1($(this).val());
            cost = Number($(this).parent().siblings('td').children('.cost').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
        }
        
        $(this).parent().siblings('td').children('.subtotalval').val((cost * qty).toFixed(2));

        $('.subtotalval').each(function(){
            if($(this).val() == ""){
                total += 0;
            }else{
                total += Number($(this).val());
            }
        });

        $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));

});
var count = 1;
var countItem = 2;
$(".addNew").click(function(e){
    e.preventDefault();
    $('#table_purchase').append('<tr align="center" class="appendRow">'+
        '<input type="hidden"  class="for_savings v-align" name="for_savings[]" value="'+count+'"><td class="v-align">'+
            '<button class="btn btn-primary RemoveBtn" type="button" id="dropdownMenuButton"  aria-haspopup="true" aria-expanded="false" >'+
                '<i class="fa fa-minus  "></i>'+
            '</button>'+
        '</td>'+
        '<td class="item_no v-align">'+countItem+'</td>'+
        '<td>'+
        '<div class="ui-widget"><input id="search'+count+'" name="search'+count+'"" data-error=".error-container" autocomplete="off" type="text" class="form-control newInput search'+count+'" placeholder="Description" title="Add a description">'+
        '<input type="hidden" name="exist_id'+count+'" id="exist_id'+count+'"></div>'+
        '</td>'+
        '<td>'+
            '<select class="form-control newInput" name="pur_unit'+count+'" id="pur_unit'+count+'" data-error=".error-container" title="Select Unit Type">'+
                '<option value="">Select Unit Type</option>'+
                @foreach($unit as $product )
                '<option value="{{ $product->id }}">{{ $product->unit_symbol }}</option>'+
                @endforeach
            '</select>'+
        '</td>'+
        '<td><input type="text" onkeypress="return isNumberKeySlash(event);" placeholder="Qty" class="form-control subtotals qtys newInput text-center" autocomplete="off"  name="qty'+count+'" id="qty'+count+'" ></td>'+
        '<td hidden><input type="text"  onkeypress="return isNumberKeySlash(event);" placeholder="Qty" class="form-control subtotals qtyfrans" autocomplete="off"  name="qtyfran'+count+'" id="qtyfran'+count+'"readonly></td>'+
        '<td>'+
            '<select class="form-control newInput" name="category'+count+'" id="category'+count+'" data-error=".error-container" title="Select Category">'+
                '<option value="">Select Category</option>'+
                @foreach($category as $product )
                '<option value="{{ $product->id }}">{{ $product->category_name }}</option>'+
                @endforeach
        '</td>'+
        '<td><input type="text"autocomplete="off" placeholder="Est. Unit Cost"class="form-control subtotals costs newInput text-center" onkeypress="return isNumberKeySlash(event);" name="cost'+count+'" id="cost'+count+'" autocomplete="off" data-error=".error-container" title="Estimated Unit Cost"></td>'+
        '<td><input type="text" class="form-control subtotalval " readonly name="subtotal'+count+'" id="subtotal'+count+'" autocomplete="off" data-validation="required" title="Estimated Cost" placeholder="0.00"></td>'+
    '</tr>');
    updateItem(); // update item number
    $('#table_purchase').on('keyup', '.subtotals', function () {
        var qty = 0;
        var cost =0;
        var total =0;

        if($(this).hasClass('costs')){
            cost = Number($(this).val());
            var qty = fractionTodecimal($(this).parent().siblings('td').children('.qtys').val());
            var val = fractionTodecimal1($(this).parent().siblings('td').children('.qtys').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
        }
        // console.log(11);
        if( $(this).hasClass('qtys') ){
            var qty = fractionTodecimal($(this).val());
            var val = fractionTodecimal1($(this).val());
            cost = Number($(this).parent().siblings('td').children('.costs').val());
            if($(this).val() == ""){
                qty = 0;
            }
            $(this).parent().siblings('td').children('.qtyfrans').val(qty);
        }

        $(this).parent().siblings('td').children('.subtotalval').val(cost * qty);

        $('.subtotalval').each(function(){
            if($(this).val() == ""){
                total += 0;
            }else{
                total += Number($(this).val());
            }
        });

        $('#totalcost').val(total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));

    });

    $( ".search"+count ).autocomplete({
        select: function( event, ui ) {
            console.log(ui.item.label);
            var val = $(this).parent().parent().parent().find('input[class="for_savings"]').val();
            $('#pur_unit'+val).prop('disabled', true);
            $('#category'+val).prop('disabled', true);
            $('#exist_id'+val).val(ui.item.id);

        },
        source: function(request, response) {
            $.ajax({
            url: "{{route('pur.autocomplete')}}",
            data: {
                    term : request.term
            },
            dataType: "json",
            success: function(data){
            var resp = $.map(data,function(obj){
                return {
                            'label': obj.description,
                            'unit' : obj.unit_id,
                            'cat'  : obj.category_id,
                            'id'   : obj.id
                        };

            });
            response(resp);
            }
        });
        },
        delay: 0,
        clearButton: true,
        minLength: 1
    });
    count ++;
    countItem ++;
    $('.newInput').each( function () {
        $(this).rules('add',{
            'required' : true
        })
    });

    $('.ui-autocomplete-clear').click(function(){
       var val = $(this).parent().parent().parent().find('input[class="for_savings"]').val();
       $('#pur_unit'+val).prop('disabled', false);
       $('#category'+val).prop('disabled', false);
       $('#exist_id'+val).val('');
        console.log(val);
    });

    $("#table_purchase").on('click','.RemoveBtn',function(e){
        e.preventDefault();
        updateItem();
        $(this).parent().parent().remove();

    });


});
$(document).ready(function() {
    $( "#search0" ).autocomplete({
        select: function( event, ui ) {
            console.log(ui.item.label);
            $('#pur_unit0').prop('disabled', true);
            $('#category0').prop('disabled', true);
            $('#exist_id0').val(ui.item.id);

        },
        source: function(request, response) {
                $.ajax({
                url: "{{route('pur.autocomplete')}}",
                data: {
                        term : request.term
                },
                dataType: "json",
                success: function(data){
                var resp = $.map(data,function(obj){
                    return {
                            'label': obj.description,
                            'unit' : obj.unit_id,
                            'cat'  : obj.category_id,
                            'id'   : obj.id
                        };
                });
                response(resp);
                }
            });
        },
        delay: 0,
        clearButton: true,
        minLength: 1
    });
    $('.ui-autocomplete-clear').click(function(){
       var val = $(this).parent().parent().parent().find('input[class="for_savings"]').val();
       $('#exist_id0').val();
        //console.log(val);
    });
    $('#pur_roa_no').change(function () {
        $.ajax({
            url: "{{route('pur.roadetails')}}",
            data: {
                    roa : $(this).val()
            },
            dataType: "json",
            success: function(data){
                $('#pur_amount').val(data.total_amount.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
                $('#purpose').val(data.ledger);
            },

        });
    });
});
//update item no.
function updateItem(){
    var rowCount = 0;
    var ctr = 1;
    var table = document.getElementById("table_purchase");
    rowCount = table.rows.length;
    $('.item_no').each( function () {
        var temp = rowCount - ctr;
        var temp2 = rowCount -temp;
         $(this).html(temp2);
         ctr++
    });

}

function reloadData(data){
    //this function uses for reloading data needed
    $('#pur_request_no').val(data.new_pr);
    $("#pur_roa_no").empty().append(data.roa_select);
}

//to limit numbers ang slash only
function isNumberKeySlash(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 32 && (charCode < 47 || charCode > 57))
        return false;
    return true;
}
</script>
@endpush
