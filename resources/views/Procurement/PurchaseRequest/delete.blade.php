<form method="POST" id="remove_perchase_request"  class="form-horizontal" action="{{ route('pur.FunctionRemovePurchaseRequest',$purchase_request->id) }}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    <h6> Are you sure you want to delete PR ID. {{ $pr_no }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{route('purchase-request.index')}}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>