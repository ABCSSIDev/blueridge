@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Procurement</li>
        <li class="breadcrumb-item active" aria-current="page">Purchase Request</li>
    </ol>
    </nav>
    <h4 class="form-head">View Purchase Request</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('purchase-request.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div> 
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" id="purchase_form" action="" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>To</b></label><br>
                            <p>&nbsp;{{$PurchaseRequestDetails->to}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Purchase Request No.</b></label><br>
                            <p>&nbsp;{{ $PurchaseRequestDetails->pr_no }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>From</b></label><br>
                            <p>&nbsp;{{$PurchaseRequestDetails->from}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Request Date</b></label><br>
                            <p>&nbsp;{{ App\Common::convertWordDateFormat($PurchaseRequestDetails->request_date) }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>Delivery To</b></label><br>
                            <p>&nbsp;{{$PurchaseRequestDetails->delivery_to}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Period of Delivery</b></label><br>
                            <p>&nbsp;{{ App\Common::convertWordDateFormat($PurchaseRequestDetails->delivery_period) }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label><b>Resolution No.</b></label><br>
                            <p>&nbsp;{{$PurchaseRequestDetails->RoaDetails->resolution_number}}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label><b>Amount</b></label><br>
                            <p>₱&nbsp;{{number_format($PurchaseRequestDetails->RoaDetails->total_amount,2)}}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purpose</label>
                            <p>&nbsp;{{$PurchaseRequestDetails->purpose}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table-sm display responsive no-wrap table-striped table-bordered dataTable no-footer text-center" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                <thead class="tab_header">
                                    <tr align="center">
                                        <th style="width: 30.2px;"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Item No.</th>
                                        <th style="width: 230.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Description</th>
                                        <th style="width: 20.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Unit</th>
                                        <th style="width: 70.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Category</th>
                                        <th style="width: 30.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Qty</th>
                                        <th style="width: 80.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Est. Unit Cost</th>
                                        <th style="width: 80.2px;" class="pt2" rowspan="1" colspan="1" style="width: 83.2px;" aria-label="Action">Estimated Cost</th>
                                    </tr>
                                </thead>
                                <tbody id="table_purchase">
                                @foreach($subPurchaseRequestDetails as $data => $val)
                                    <tr align="center">
                                        <td>{{ ++$data }}</td>
                                        <td>{{ $val->getInventoryName->description }}</td>
                                        <td>{{ $val->getUnits->unit_symbol }}</td>
                                        <td>{{ $val->getInventoryName->Category->category_name}}</td>
                                        <td>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity) }}</td>
                                        <!-- <td>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($val->quantity,$val->quantity1) }}</td> -->
                                        <td>₱&nbsp;{{ number_format($val->unit_cost, 2) }}</td>
                                        <td>₱&nbsp;{{ number_format($val->quantity * $val->unit_cost , 2)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div align="right">
                            <label class="total-cost">Total&nbsp;<span style="margin-left: 1rem;"><b>₱ 
                                @php 
                                    echo App\Http\Controllers\PurchaseRequestController::PurchaseRequestTotal($PurchaseRequestDetails->id);
                                @endphp
                            </b></span></label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="form-group" align="right" >
                <button class="btn btn-primary print-form-css" data-form="print_purchaserequest"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
            </div>
        </div>
    </div>
    <div id="print_purchaserequest" hidden>
        <!-- <div style="margin-bottom: 50px;">
            @include('layouts.header')
        </div> -->
        @include('Docs.purchase-request')
                                                        
        <!-- <style>
            .print-request{
            -webkit-box-flex: 0;
            flex: 0 0 auto;
            max-width: 86.3333333333%;
                }
            .bt-prline1{
                border-bottom: 1px solid;
                position: inherit;
                width: 270px;
                display: inline-block;
            }
            .bt-prline1.bt-prline2{
                width: 220px;
            }
            .bt-prline1.bt-prline3{
                width: 277px;
            }
            .bt-prline1.bt-prline4{
                width: 220px;
            }
            .bt-prline1.bt-prline5{
                width: 220px;
                margin-left: 18px;
            }
            .pr-overline{
                text-decoration: overline;
            }
            .top-prline{
                border-top: 1px solid;
                position: inherit;
                width: 100px;
                display: block;
            }
        </style> -->
    </div>
</div>



@endsection

<style>
    .input {
        border: none;
        background-color: transparent;
    }
    .total-cost{
        font-size: 1.5rem;
        margin-right: 1rem;
    }
    .pt-03{
        padding: 0.3rem;
    }
</style>