@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Procurement</li>
        <li class="breadcrumb-item active" aria-current="page">Purchase Request</li>
    </ol>
    </nav>
    <h4 class="form-head">Purchase Request List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('purchase-request.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
            <table class="table table-striped table-bordered table-hover text-center table-mediascreen" id="purchase_request_list">
                <thead class="thead-dark">
                    <tr align="center">
                    <th>PR ID</th>
                    <th>To</th>
                    <th>From</th>
                    <th>Resolution Date</th>
                    <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>

@push('scripts')
    <script>
         var oTable = $("#purchase_request_list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('pur.getPurchaseRequest',0000) !!}",
                columns:[
                    {data: "prno","name":"prno","width":"20%", className:"v-align text-center"},
                    {data: "to","name":"to","width":"20%", className:"v-align text-center"},
                    {data: "from","name":"from","width":"20%", className:"v-align text-center"},
                    {data: "resolutiondate","name":"resolutiondate","width":"20%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("pur.getPurchaseRequest", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush