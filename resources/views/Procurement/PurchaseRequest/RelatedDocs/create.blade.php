@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 margin-top">
            <div class="card">
                <div class="card-header">Create Request of Obligation</div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div align="right" style=""><a href="{{route('related-docs.index')}}"><button class="btn btn-secondary">BACK</button></a></div>
                    </div>
                    <div class="col-12"> 
                        <div class="tab-content">
                            <form method="POST"  class="form-horizontal" action="" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Obligation No.</label>
                                        <input type="text" class="form-control" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Date of Obligation</label>
                                        <select class="form-control">
                                            <option>Select Chart of Account</option>
                                            <option>Chart of Accounts</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Resolution No.</label>
                                        <input type="text" class="form-control" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Date of Approval</label>
                                        <input type="text" class="form-control datetimepicker" >
                                    </div>
                                </div>
                                <div class="form-group" align="right" >
                                    <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span> SUBMIT</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
@endsection


<style>
    .margin-top{
        margin-top:50px;
    }
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
    .default{
        position: relative;
    }
    .icon-camera{
        height: 20px;
        width: 20px;
        position: absolute;
        margin-top: -16px;
        margin-left: 10px;
        font-size: 10px;
        border-radius: 50%;
        color: #fff;
        background-color: #3490dc;
    }
    .fa-camera{
        margin-top: 6px;
    }
</style>

@push('scripts')
<script>

$( ".datetimepicker" ).datepicker({
        format: 'YYYY-MM-DD',
});

</script>
@endpush