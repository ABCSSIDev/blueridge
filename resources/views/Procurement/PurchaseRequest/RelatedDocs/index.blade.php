@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 margin-top">
            <div class="card">
                <div class="card-header">Request of Obligation List</div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div align="right" style="margin: 0px 0px 10px 0px"><a href="{{route('related-docs.create')}}"><button class="btn btn-primary">CREATE</button></a></div>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover">
                                <thead class="thead-dark">
                                    <tr align="center">
                                        <th>Obligation No.</th>
                                        <th>Resolution No.</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <tr align="center">
                                        <td style="vertical-align: middle">1</td>
                                        <td style="vertical-align: middle">002-S-2020</td>
                                        <td><div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#"> View Record</a>
                                                <a class="dropdown-item" href=""> Edit Record</a>
                                                <a class="dropdown-item" href="#"> Delete Record</a>
                                            </div>
                                        </div></td>
                                    </tr>
                                    
                                    <tr align="center">
                                        <td style="vertical-align: middle">2</td>
                                        <td style="vertical-align: middle">003-S-2020</td>
                                        <td><div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#"> View Record</a>
                                                <a class="dropdown-item" href=""> Edit Record</a>
                                                <a class="dropdown-item" href="#"> Delete Record</a>
                                            </div>
                                        </div></td>
                                    </tr>
                                    <tr align="center">
                                        <td style="vertical-align: middle">3</td>
                                        <td style="vertical-align: middle">004-S-2020</td>
                                        <td><div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#"> View Record</a>
                                                <a class="dropdown-item" href=""> Edit Record</a>
                                                <a class="dropdown-item" href="#"> Delete Record</a>
                                            </div>
                                        </div></td>
                                    </tr>
                                    <tr align="center">
                                        <td style="vertical-align: middle">4</td>
                                        <td style="vertical-align: middle">005-S-2020</td>
                                        <td><div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#"> View Record</a>
                                                <a class="dropdown-item" href=""> Edit Record</a>
                                                <a class="dropdown-item" href="#"> Delete Record</a>
                                            </div>
                                        </div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>

