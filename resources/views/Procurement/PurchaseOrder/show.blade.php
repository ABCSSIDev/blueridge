@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Purchase Order</li>
        </ol>
    </nav>
    <h4 class="form-head">View Purchase Order</h4>
    <div class="row justify-content-center">
        <div class="col-md-12 mb-50">
            <div align="right" style="margin: 0px 0px 20px 0px;"><a href="{{ route('purchase-order.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Purchase Request ID:</label>
                        <p>{{ $po->getPurchaseRequest->pr_no }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Purchase Order No.:</label>
                        <p>{{ $po->po_no }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Supplier Name:</label>
                        <p>{{ $canvass->getSuppliers->supplier_name }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Purchase Order Date:</label>
                        <p>{{ App\Common::convertWordDateFormat($po->po_date) }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Address:</label>
                        <p>{{ empty($canvass->getSuppliers->address) ? "N/A" : $canvass->getSuppliers->address }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Mode of Procurement:</label>
                        <p>{{ $po->procurement_mode }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Purchase Request Date:</label>
                        <p>{{ App\Common::convertWordDateFormat($po->getPurchaseRequest->request_date) }}</p>
                    </div>
                </div>
                <hr class="hr1">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Place of Delivery:</label>
                        <p>{{ $po->place_of_delivery != null ? $po->place_of_delivery : '' }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Delivery Term:</label>
                        <p>{{ $po->delivery_term }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Date of Delivery:</label>
                        <p>{{ $po->delivery_date != null ? App\Common::convertWordDateFormat($po->delivery_date) : '' }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Payment Term:</label>
                        <p>{{ $po->payment_term }}</p>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-sm-12">
                        <table class="table-sm display responsive table-striped table-bordered text-center dataTable" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                            <thead class="tab_header">
                                <tr align="center">
                                    <th style="width: 10%;"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Item No.</th>
                                    <th style="width: 30%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Description</th>
                                    <th style="width: 15%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Unit</th>
                                    <th style="width: 10%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Quantiy</th>
                                    <th style="width: 20%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Est. Unit Cost</th>
                                    <th style="width: 20%;" class="pt2" rowspan="1" colspan="1" style="width: 83.2px;" aria-label="Action">Est. Cost</th>
                                </tr>
                            </thead>
                            
                            <tbody id="purchase_order_list">
                                @php $total = 0; $item_no = 0; @endphp 
                                @foreach($canvass_item as $canvass_value => $item)
                                    @php
                                        $item_no++;
                                        $est_cost = $item->available_quantity * $item->unit_price;
                                        $total += $item->unit_price * $item->available_quantity;
                                    @endphp
                                <tr>
                                    <td>{{ $item_no }}</td>
                                    <td>{{ $item->getPRItems->getInventoryName->description }}</td>
                                    <td>{{ $item->getPRItems->getUnits->unit_name }}</td>
                                    <td>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($item->available_quantity) }}</td>
                                    <td>₱&nbsp;{{ number_format($item->unit_price,2) }}</td>
                                    <td>₱&nbsp;{{ number_format($est_cost,2) }}</td>
                                </tr>
                                @endforeach
                                <tr class="align-right">
                                    <td colspan="5" style="text-align: right;">Total</td>
                                    <td class="text-center"><b>₱&nbsp;{{ number_format($total,2) }}</b></td>
                                </tr>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                <div class="form-group" align="right" >
                    <button class="btn btn-primary print-form-css mt-3" data-form="print_po"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                </div>
            </div>
        </div>
    </div>
    <!-- @include('Docs.purchase-order-docu') -->
</div>
<div id="print_po" hidden>
    <div style="margin-bottom: 20px;">  
        <!-- @include('layouts.header') -->
    </div>
    @include('Docs.purchase-order-docu')
    <div class="break-page"></div>
    <div style="margin-bottom: 20px;">  
        <!-- @include('layouts.header') -->
    </div>
    @include('Docs.notice-delivery')
    <div class="break-page"></div>
    <div style="margin-bottom: 20px;">  
        <!-- @include('layouts.header') -->
    </div>
    @include('Docs.justification-confirmatory')
</div>

@endsection