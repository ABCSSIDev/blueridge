@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Purchase Order</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Purchase Order</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{route('purchase-order.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST" class="form-horizontal" action="{{ route('purchase-order.update', $id) }}" enctype="multipart/form-data" id="poUpdateForm">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT" >
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purchase Request ID</label>
                            <input type="text" class="form-control" readonly="" value="{{ $purchaseOrder->getPurchaseRequest->pr_no }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Purchase Order ID</label>
                            <input type="text" class="form-control" name="po_num" value="{{ $purchaseOrder->po_no }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Supplier Name</label>
                            <input type="text" class="form-control" value="{{ $purchaseOrder->getCanvass->getSuppliers->supplier_name }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Purchase Order Date</label>
                            <input type="text" class="form-control datetimepicker" name="po_date" value="{{ App\Common::convertWordDateFormat($purchaseOrder->po_date) }}" >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Address</label>
                            <input type="text" class="form-control" value="{{ empty($purchaseOrder->getCanvass->getSuppliers->address) ? 'N/A' : $purchaseOrder->getCanvass->getSuppliers->address }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Mode of Procurement</label>
                            <select class="form-control" name="procurement_mode">
                                <option value="">Select Mode of Procurement</option>
                                <option value="Bidding" {{ ($purchaseOrder->procurement_mode == "Bidding" ? "selected" : "") }}>Bidding</option>
                                <option value="Shopping" {{ ($purchaseOrder->procurement_mode == "Shopping" ? "selected" : "") }}>Shopping</option>
                                <option value="Negotiated" {{ ($purchaseOrder->procurement_mode == "Negotiated" ? "selected" : "") }}>Negotiated</option>
                                <option value="Over the Counter" {{ ($purchaseOrder->procurement_mode == "Over the Counter" ? "selected" : "") }}>Over the Counter</option>
                                <option value="Limited Source" {{ ($purchaseOrder->procurement_mode == "Limited Source" ? "selected" : "") }}>Limited Source</option>
                                <option value="Repeat Order" {{ ($purchaseOrder->procurement_mode == "Repeat Order" ? "selected" : "") }}>Repeat Order</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purchase Request Date</label>
                            <input type="text" class="form-control datetimepicker" value="{{ App\Common::convertWordDateFormat($purchaseOrder->getPurchaseRequest->request_date) }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                        </div>
                    </div>
                    <hr class="hr1">
                    <div class="form-row mt-2r">
                        <div class="form-group col-md-6">
                            <label>Place of Delivery</label>
                            <input type="text" class="form-control" name="place_of_delivery" value="{{ $purchaseOrder->place_of_delivery }}" placeholder="Place of Delivery">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Delivery Term</label>
                            <input type="text" class="form-control" name="delivery_term" value="{{ $purchaseOrder->delivery_term }}" placeholder="Delivery Term">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Date of Delivery</label>
                            <input type="text" class="form-control datetimepicker" name="delivery_date" value="{{ $purchaseOrder->delivery_date == null ? '' : App\Common::convertWordDateFormat($purchaseOrder->delivery_date) }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Payment Term</label>
                            <select class="form-control" name="payment_term" value="{{ $purchaseOrder->payment_term }}">
                                <option value="">Payment Term</option>
                                <option value="COD" {{ ($purchaseOrder->payment_term == "COD" ? "selected" : "") }} >COD</option>
                                <option value="Cash" {{ ($purchaseOrder->payment_term == "Cash" ? "selected" : "") }} >Cash</option>
                                <option value="Check" {{ ($purchaseOrder->payment_term == "Check" ? "selected" : "") }} >Check</option>
                                <option value="Reimbursement" {{ ($purchaseOrder->payment_term == "Reimbursement" ? "selected" : "") }} >Reimbursement</option>
                                <option value="Post Paid" {{ ($purchaseOrder->payment_term == "Post Paid" ? "selected" : "") }} >Post Paid</option>
                            </select>
                        </div>
                    </div>
                    <div class="row pt-2" style="margin-bottom: 20px;">
                        <div class="col-sm-12">
                            <table class="table-sm display responsive table-striped table-bordered dataTable text-center" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                <thead class="tab_header">
                                    <tr align="center">
                                        <th style="text-align: center; width: 82.2px;"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Item No.</th>
                                        <th style="text-align: center; width: 298.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Description</th>
                                        <th style="text-align: center; width: 170.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Unit</th>
                                        <th style="text-align: center; width: 170.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Qty</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Est. Unit Cost</th>
                                        <th style="text-align: center; width: 180.2px;" class="pt2" rowspan="1" colspan="1" style="width: 83.2px;" aria-label="Action">Est. Cost</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach($canvass_item as $canvass => $item)
                                        @php
                                            $est_cost = $item->available_quantity * $item->unit_price;
                                            $total += $item->unit_price * $item->available_quantity;
                                        @endphp
                                    <tr>
                                        <td class="v-align">{{ ++$canvass }}</td>
                                        <td class="v-align">{{ $item->getPRItems->getInventoryName->description }}</td>
                                        <td class="v-align">{{ $item->getPRItems->getUnits->unit_name }}</td>
                                        <td class="v-align">{{ App\Http\Controllers\PurchaseRequestController::decToFraction($item->available_quantity) }}</td>
                                        <td class="v-align">₱&nbsp;{{ number_format($item->unit_price,2) }}</td>
                                        <td class="v-align">₱&nbsp;{{ number_format($est_cost,2) }}</td>
                                    </tr>
                                    @endforeach
                                    <tr class="align-right">
                                        <td colspan="5" class="v-align" style="text-align: right;">Total</td>    
                                        <td class="text-center v-align"><b>₱&nbsp;{{ number_format($total,2) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>

    let rules = {
        po_date : {
            required: true
        },
        procurement_mode : {
            required: true
        },
        place_of_delivery : {
            required: true
        },
        payment_term : {
            required: true
        },
        po_num : {
            required: true
        }
    };
    $('#poUpdateForm').registerFields(rules, false);
</script>
@endpush