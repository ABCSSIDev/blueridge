@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Purchase Order</li>
        </ol>
    </nav>
    <h4 class="form-head">Create Purchase Order</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px;"><a href="{{route('purchase-order.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST" class="form-horizontal" id="po_form" action="{{ route('purchase-order.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purchase Request No.</label>
                            <select class="form-control" name="pr_num" id="pr_num">
                                <option value="">Select Purchase Request ID.</option>
                                @if($purchase_request)
                                    @foreach($purchase_request as $pr)
                                        @if(count($pr->getCanvasses)>0)
                                            <option data-id="{{ App\Common::getPRNumberFormat('purchase_orders', $po) . App\Common::getAlphabet($pr->id) }}" data-url="{{ route('purchaseRequestID',$pr->id) }}" value="{{ $pr->id }}">{{ $pr->pr_no }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Purchase Order No.</label>
                            <input type="text" class="form-control" value="" name="po_num" id="po_num" placeholder="Purchase Order No.">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Supplier Name</label>
                            <select class="form-control" name="supplier_id" id="supplier_id">
                                <option value="">Select Supplier Name</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Purchase Order Date</label>
                            <input type="text" class="form-control datetimepicker" name="po_date" placeholder="Purchase Order Date">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Address</label>
                            <input type="text" class="form-control" name="address_supplier" id="address_supplier" placeholder="Address" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Mode of Procurement</label>
                            <select class="form-control" name="procurement_mode" id="procurement_mode">
                                <option value="">Select Mode of Procurement</option>
                                <option value="Bidding">Bidding</option>
                                <option value="Shopping">Shopping</option>
                                <option value="Negotiated">Negotiated</option>
                                <option value="Over the Counter">Over the Counter</option>
                                <option value="Limited Source">Limited Source</option>
                                <option value="Repeat Order">Repeat Order</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Purchase Request Date</label>
                            <input type="text" class="form-control datetimepicker" id="pr_date" placeholder="Purchase Request Date" disabled>
                        </div>
                    </div>
                    <hr class="hr1">
                    <div class="form-row mt-2r">
                        <div class="form-group col-md-6">
                            <label>Place of Delivery</label>
                            <input type="text" name="place_of_delivery" class="form-control" placeholder="Place of Delivery" value="Barangay Blue Ridge B">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Delivery Term</label>
                            <input type="text" name="delivery_term" class="form-control" placeholder="Delivery Term">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Date of Delivery</label>
                            <input type="text" name="date_of_delivery" class="form-control datetimepicker" placeholder="Date of Delivery">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Payment Term</label>
                            <select class="form-control" name="payment_term">
                                <option value="">Select Payment Term</option>
                                <option value="COD">COD</option>
                                <option value="Cash">Cash</option>
                                <option value="Check">Check</option>
                                <option value="Reimbursement">Reimbursement</option>
                                <option value="Post Paid">Post Paid</option>
                            </select>
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-sm-12">
                            <table class="table-sm display responsive table-striped table-bordered dataTable text-center no-wrap" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%;">
                                <thead class="tab_header">
                                    <tr align="center">
                                        <th style="text-align: center; width: 10%;"  class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1" aria-sort="ascending">Item No.</th>
                                        <th style="text-align: center; width: 30%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Description</th>
                                        <th style="text-align: center; width: 15%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Unit</th>
                                        <th style="text-align: center; width: 10%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Quantity</th>
                                        <th style="text-align: center; width: 20%;" class="pt2" tabindex="0" aria-controls="advertisement_list" rowspan="1" colspan="1">Est. Unit Cost</th>
                                        <th style="text-align: center; width: 20%;" class="pt2" rowspan="1" colspan="1" style="width: 83.2px;" aria-label="Action">Est. Cost</th>
                                    </tr>
                                </thead>
                                <tbody id="purchase_order_list">
                                    <tr>
                                        <td colspan="6">No data available in table</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!--End of Listing for Purchase Order-->
                        </div>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span> SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    $("#supplier_id").prop('disabled', true);
    $('#pr_num').change(function (e) {
        var el = $(this).find(':selected').data('id');
        var url = $('option:selected', this).attr('data-url');
        if(url == null){
            $("#supplier_id").prop('disabled', true);
            $('#address_supplier').val('Address');
            $('#pr_date').val('Purchase Request Date');
            $('#supplier_id').prop('selectedIndex',0);
            $('#purchase_order_list').empty();
            $('#purchase_order_list').append('<tr><td colspan="6">No data available in table</td></tr>');
            $('#po_num').val('');
        }
        else{
            $.ajax({
                type: "GET",
                url: url,
                success: function(html){
                    $("#supplier_id").prop('disabled', false);
                    $('#supplier_id').empty().html(html);
                    $('#po_num').val(el);
                }
            });
        }
    });

    $('#supplier_id').change(function (e) {
        var url = $('option:selected', this).attr('data-url');
        if(url == null){
            $("#supplier_id").prop('disabled', true);
            $('#purchase_order_list').empty();
            $('#purchase_order_list').append('<tr><td colspan="6">No data available in table</td></tr>');
            $('#address_supplier').val('Address');
            $('#pr_date').val('Purchase Request Date');
            $('#pr_num').prop('selectedIndex',0);
            $('#po_num').val('');
        }
        else{
            $.ajax({
                type: "GET",
                url: url,
                success: function(html){
                    $('#purchase_order_list').empty().html(html);
                }
            });
        }
    });

    $('#pr_num').change(function () {
        $.ajax({
            url: "{{ route('requestDate') }}",
            data: {
                request_date : $(this).val()
            },
            success: function(data){
                $('#pr_date').val(data);
            },
            
        });
    });

    $('#supplier_id').change(function () {
        $.ajax({
            url: "{{ route('supplierNameDetails') }}",
            data: {
                supplier : $(this).val()
            },
            success: function(data){
                $('#address_supplier').val(data);
            },
            
        });
    });
    
    let rules = {
            pr_num : {
                required: true
            },
            supplier_id : {
                required: true
            },
            po_date : {
                required: true
            },
            procurement_mode : {
                required: true
            },
            delivery_term : {
                required: true
            },
            payment_term : {
                required: true
            },
            po_num : {
                required: true
            },
    };
    $('#po_form').registerFields(rules,true);
    function reloadData(data){
        //this function uses for reloading data needed
        $("#pr_num").empty();
        $("#pr_num").append(data.pr_select);
    }
</script>
@endpush