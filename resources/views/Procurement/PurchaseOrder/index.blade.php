@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Purchase Order</li>
        </ol>
    </nav>
    <h4 class="form-head">Purchase Order List</h4>
    <div class="row">
        <div class="col-md-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
                <!-- <option value="2021">2021</option> -->
            </select>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12"> 
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('purchase-order.create')}}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
                <table class="table table-striped table-bordered table-hover text-center table-mediascreen" id="po_list">
                    <thead class="thead-dark">
                        <tr align="center">
                            <th>PO ID</th>
                            <th>Supplier Name</th>
                            <th>PR ID</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
</style>
@push('scripts')
    <script>
         var oTable = $("#po_list").DataTable(
            {
                pagingType: "full_numbers",
                processing: true,
                //serverSide: true,
                type: "GET",
                ajax:"{!! route('purchase-order.getPurchaseOrders',0000) !!}",
                columns:[
                    {data: "id","name":"id","width":"20%", className:"v-align text-center"},
                    {data: "supplier_name","name":"supplier_name","width":"40%", className:"v-align text-center"},
                    {data: "pr_id","name":"pr_id","width":"20%", className:"v-align text-center"},
                    {data: "action","name":"action",orderable:false,searchable:false,"width":"20%"},
                ],
                select: {
                    style: "multi"
                }
            }
        );
        $('#budget_year').change(function(){
            var year = $(this).val();
            var url = '{{ route("purchase-order.getPurchaseOrders", ':year') }}';
            url = url.replace(':year', $(this).val());
            oTable.ajax.url(url).load();
        });
    </script>
@endpush

