@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Canvassing</li>
        </ol>
    </nav>
    <h4 class="form-head">Edit Canvassing</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{ route('canvassing.index') }}"><button class="btn btn-secondary">BACK</button></a></div>
            <div class="tab-content">
                <form method="POST"  class="form-horizontal" action="{{route('canvassing.update',$id)}}" enctype="multipart/form-data" id="canvassing_form">
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}
                    <div class="errors"></div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="purchase_request">Purchase Request ID.</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{ $purchase_request_id->getPurchaseRequest->pr_no }}" readonly>
                        </div>
                    </div>
                    <div style="overflow: auto; margin-bottom: 20px;">
                        <table class="table table-striped table-bordered table-hover no-wrap" id="table_canvassing">
                            <thead class="thead-dark">
                                <tr align="center">
                                    <th width="5%" style="vertical-align:middle;">Quantity</th>
                                    <th width="10%" style="vertical-align:middle;">Description</th>
                                    <th width="5%" style="vertical-align:middle;">Unit</th>
                                    @foreach($canvass as $value) 
                                        @foreach($suppliers as $supplier)
                                            @if($value->supplier_id == $supplier->id)
                                            <th width="25%" scope="col">
                                                <input type="text" id="name" name="name" class="form-control" value="{{$supplier->supplier_name}}" disabled>
                                                <div class="row mt-2">
                                                    <div class="col border1">Unit Price</div>
                                                    <div class="col border1">Quantity Available</div>
                                                    <div class="col border1">Total</div>
                                                </div>
                                            </th>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tr>
                                <tbody>
                                    @php ($count_y = 1)
                                    @foreach($grouped_canvass_items as $key =>  $grouped_item) 
                                    @if($group_id == $grouped_item->group_id)
                                    <tr align="center">
                                        @foreach($purchase_request_items as $pr_item) 
                                            @if($pr_item->id == $grouped_item->pr_item_id)
                                            <td class="v-align">{{ App\Http\Controllers\PurchaseRequestController::decToFraction($pr_item->quantity)}}</td>
                                            <td class="v-align">{{$pr_item->getInventoryName->description}}</td>
                                            <td class="v-align">{{$pr_item->getUnits->unit_name}}</td>
                                            @endif
                                        @endforeach
                                        @php ($count_x = 1)
                                        @foreach($canvass_items as $canvass_item_key => $item) 
                                            @if($item->getCanvasses->group_id == $grouped_item->group_id && $item->pr_item_id == $grouped_item->pr_item_id)
                                            <td class="v-align">
                                                <div class="row mt-2 canvass">
                                                
                                                    <input type="hidden" name="for_insert_canvass[]" value="{{$count_x . $count_y}}">
                                                    <input type="hidden" name="canvass_id{{$count_x . $count_y}}" value="{{$item->id}}">
                                                    <input type="hidden" name="canvass_item_id{{$count_x . $count_y}}" value="{{$item->canvass_id}}">
                                                    <input type="hidden" name="pr_item_id{{$count_x . $count_y}}" value="{{$item->pr_item_id}}">
                                                    <div class="col"><input type="text" class="form-control newInput price priceval unit_price text-center select_unit{{$grouped_item->pr_item_id}} " data-selector="{{$count_x . $count_y}}" name="unit_price{{$count_x . $count_y}}" for="unit_price" value="{{number_format($item->unit_price,2)}}" autocomplete="off" {{ ($po ? "disabled": "")}}></div>
                                                    <div class="col"><input type="text" class="form-control newInput price quantity_available text-center available_quantity{{$grouped_item->pr_item_id}} qtyvalidation"  data-selector="{{$count_x . $count_y}}" name="quantity_available{{$count_x . $count_y}}" for="quantity_available" value="{{ App\Http\Controllers\PurchaseRequestController::decToFraction($item->available_quantity)}}" onkeyup=" return validquantity(this,{{ $value->id}} , {{ App\Http\Controllers\CanvassingController::getValdationQuantitys($id,$group_id,$item->available_quantity,$item->pr_item_id)}},event,{{$count_x . $count_y}})"  autocomplete="off" {{ ($po ? "disabled": "")}} {{ (App\Http\Controllers\CanvassingController::QtyValidationCanvass($item->canvass_id,$item->pr_item_id) == 1 ? "readonly": "")}}></div>
                                                    <div class="col" hidden><input type="text" class="form-control qtycanss{{$count_x . $count_y}}" value="{{ $item->available_quantity }}" name="quantity_availablez_{{$count_x . $count_y}}" id="quantity_availablez_{{$count_x . $count_y}}"></div>
                                                    <div class="col" style="top: 7px;" id=""><input type="hidden" class="total_price_class{{$count_x}}" data-selector="" value="{{$item->unit_price * $item->available_quantity}}"><label class="total_price" value="">₱&nbsp;{{number_format($item->unit_price * $item->available_quantity,2)}}</label></div>
                                                </div>
                                            </td>
                                            <script>

                                            </script>
                                            @php($count_x++)
                                            @endif
                                        @endforeach
                                        @php($count_y++)
                                    </tr>
                                    @endif
                                    @endforeach
                                    @php ($total = 1)
                                    <tr>
                                        <td class="v-align" colspan="3"></td>
                                        <td class="v-align">
                                            <div class="row mt-2" style="width: 350px">
                                                <div class="col">Total</div>
                                                <div class="col align-right"><label class="total_amount{{$total}}"></label></div>
                                            </div>
                                        </td>
                                        <td class="v-align">
                                            <div class="row mt-2" style="width: 350px">
                                                <div class="col">Total</div>
                                                <div class="col align-right"><label class="total_amount{{$total+1}}"></label></div>
                                            </div>
                                        </td>
                                        <td class="v-align">
                                            <div class="row mt-2" style="width: 350px">
                                                <div class="col">Total</div>
                                                <div class="col align-right"><label class="total_amount{{$total+2}}"></label></div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </thead>
                        </table>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> UPDATE</button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
    total();

    let rules = {
        unit_price : {
            required: true
        },
        quantity_available : {
            required: true
        },
    };
    $('#canvassing_form').registerFields(rules, false);
    var formatter = new Intl.NumberFormat('en-IN', {
        minimumFractionDigits: 2,
    });
    // $("#table_canvassing").on("keyup", ".qtyvalidation", function () {
    //     alert($(this).val());
    // });
    $("#table_canvassing").on("keyup", ".price", function () {
        var selector = this.getAttribute("data-selector");
        var unit_price = 0;   
        var quantity_available =0;
        var total =0;   
        $(".priceval").number(true,2);
        
      

        if($(this).hasClass("unit_price")){
            unit_price = Number($(this).val());
            quantity_available = Number(fractionTodecimal($(this).parent().siblings().children(".quantity_available").val()));
            if($(this).parent().siblings().children(".quantity_available").val() == ""){
                quantity_available =0;
            }
        }

        if( $(this).hasClass("quantity_available") ){
            quantity_available = Number(fractionTodecimal($(this).val()));
            if($(this).val() == ""){
                quantity_available =0;
            }
            unit_price = Number($(this).parent().siblings().children(".unit_price").val() );
        }
        var total_amount = unit_price * quantity_available;
        $(this).parent().siblings().children(".total_price").html("₱ "+total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        $(this).parent().siblings().children(".total_price_class"+selector.charAt(0)).val(unit_price * quantity_available);

        $(".total_price_class"+selector.charAt(0)).each(function(){
            total += Number($(this).val());
        });
    
        $(".total_amount"+selector.charAt(0)).html("₱"+total.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    });

    function total(){
        for(var i = 1; i <= 3; i++){
            var total =0;
            $(".total_price_class"+i).each(function(){
                total += Number($(this).val());
            });

            var formatter = new Intl.NumberFormat('en-IN', {
                minimumFractionDigits: 2,
            });
            $(".total_amount"+i).html("₱ "+total.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));

        }
    }

    $('.newInput').each( function () {
        $(this).rules('add',{
            'required' : true
        })
    });

    // function validquantity(selector, id,quantity){
    //     var accepted = '#qty_input' + id;
    //     var remaining = 0;
    //     var total_accrej = (parseInt($(selector).val()) || 0);
    //     console.log(quantity);
    //     if(total_accrej > quantity){
    //         var check_id = $(selector).attr('id');
    //         $(selector).val(quantity);
    //     }
      
    // }

    function decimalToFraction(value, donly = true) {
                var tolerance = 1.0E-6; // from how many decimals the number is rounded
                var h1 = 1;
                var h2 = 0;
                var k1 = 0;
                var k2 = 1;
                var negative = false;
                var i;

                if (parseInt(value) == value) { // if value is an integer, stop the script
                    return value;
                } else if (value < 0) {
                    negative = true;
                    value = -value;
                }

                if (donly) {
                    i = parseInt(value);
                    value -= i;
                }

                var b = value;

                do {
                    var a = Math.floor(b);
                   
                    var aux = h1;
                    h1 = a * h1 + h2;
                    h2 = aux;
                    aux = k1;
                    k1 = a * k1 + k2;
                    k2 = aux;
                    b = 1 / (b - a);
                } while (Math.abs(value - h1 / k1) > value * tolerance);

                return (negative ? "-" : '') + ((donly & (i != 0)) ? i + ' ' : '') + (h1 == 0 ? '' : h1 + "/" + k1);
        }
        function fractionTodecimal(fraction){
            var fraction_eval = 0;
            var string = fraction + '';
            var fractionParts = string.split('-');
            if (fractionParts.length === 1) {
                /* try space as divider */
                fractionParts = string.split(' ');
            }
            if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
                var integer = parseInt(fractionParts[0]);
                var decimalParts = fractionParts[1].split('/');
                var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
                fraction_eval = integer + decimal;
            }
            else if (fraction.indexOf('/') !== -1) {
                var decimalParts = fraction.split('/');
                var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
                fraction_eval = decimal;
            }
            else {
                fraction_eval = parseInt(fraction);
            }
            return fraction_eval;
        }
        function validquantity(selector, id, quantity,evt,ids,id1){
            var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode != 47 && charCode > 32
                && (charCode < 48 || charCode > 57))
                return false;
                    var accepted = '#qty_input' + id;
                    var remaining = 0;
                    var total_accrej = $(selector).val();
                    $('.qtycanss'+ids).val(fractionTodecimal(total_accrej));
                    if(fractionTodecimal(total_accrej) > quantity){
                        var check_id = $(selector).attr('id');
                        $(selector).val(decimalToFraction(quantity));
                        $('.qtycanss'+ids).val(decimalToFraction(quantity));
                    }
            return true;
            
        }
    
    </script>
@endpush

@endsection
<style>
    .center{
        text-align: center; 
        margin: 30px 0px 50px 0px;
    }
</style>