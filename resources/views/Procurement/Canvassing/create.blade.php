@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item" >Procurement</li>
        <li class="breadcrumb-item active" aria-current="page">Canvassing</li>
    </ol>
    </nav>
    <h4 class="form-head">Create Canvassing</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{route('canvassing.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <form method="POST" class="form-horizontal" action="{{route('canvassing.store')}}" enctype="multipart/form-data" id="canvassing_form">
                    <div class="errors"></div>
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="purchase_request">Purchase Request #</label>
                            <select class="form-control" id="purchase_request" name="purchase_request">
                            <option value="" >Select P.R. #</option>
                            @if($purchase_request)
                                @foreach($purchase_request as $pr)  
                                    <option data-url="{{ route('getPurchaseRequest',$pr->id) }}" value="{{ $pr->id }}">{{$pr->pr_no }}</option>
                                @endforeach
                            @endif
                            </select>
                        </div>
                    </div>
                    <div style="overflow: auto">
                        <div class="error-container"></div>
                        <table class="table table-striped table-bordered table-hover table-responsive dragscroll no-wrap" id="table_canvassing">
                            <thead class="thead-dark">
                                <tr align="center">
                                    <th width="5%" style="vertical-align:middle;">Item</th>
                                    <th width="10%" style="vertical-align:middle;">Est. Unit Cost</th>
                                    <th width="5%" style="vertical-align:middle;">Quantity</th>
                                    <th width="5%" style="vertical-align:middle;">Remaining</th>
                                    <th width="10%" style="vertical-align:middle;">Description</th>
                                    <th width="5%" style="vertical-align:middle;">Unit</th>
                                    <th width="25%" scope="col">
                                        <input type='hidden' name="for_insert[]" value="0">
                                        <input type='hidden' name="awarded0" value="1">

                                        <select class="form-control newInput select-supplier" name="supplier0" id="supplier0" for="supplier0" data-error=".error-container">
                                        <option value="" >Select Dealer</option>
                                        @foreach($suppliers as $supplier) 
                                            <option value="{{$supplier->id}}">{{$supplier->supplier_name}} - {{$supplier->address}}</option>
                                        @endforeach
                                        </select>
                                        <div class="row mt-2" style="width: 350px">
                                            <div class="col">Unit Price</div>
                                            <div class="col">Quantity Available</div>
                                            <div class="col">Total</div>
                                        </div>
                                    </th>

                                    <th width="25%" scope="col">
                                        <input type='hidden' name="for_insert[]" value="1">
                                        <input type='hidden' name="awarded1" value="0">

                                        <select class="form-control newInput select-supplier" name="supplier1" id="supplier1" for="supplier1" data-error=".error-container">
                                            <option value="" >Select Dealer</option>
                                            @foreach($suppliers as $supplier)
                                                <option value="{{$supplier->id}}">{{$supplier->supplier_name}} - {{$supplier->address}}</option>
                                            @endforeach
                                        </select>
                                        <div class="row mt-2" style="width: 350px">
                                            <div class="col">Unit Price</div>
                                            <div class="col">Quantity Available</div>
                                            <div class="col">Total</div>
                                        </div>
                                    </th>
                                    
                                    <th width="25%" scope="col">
                                        <input type='hidden' name="for_insert[]" value="2">
                                        <input type='hidden' name="awarded2" value="0">

                                        <select class="form-control newInput select-supplier" name="supplier2" id="supplier2" for="supplier2" data-error=".error-container">
                                        <option value="">Select Dealer</option>
                                        @foreach($suppliers as $supplier) 
                                            <option value="{{$supplier->id}}">{{$supplier->supplier_name}} - {{$supplier->address}}</option>
                                        @endforeach
                                        </select>
                                        <div class="row mt-2" style="width: 350px">
                                            <div class="col">Unit Price</div>
                                            <div class="col">Quantity Available</div>
                                            <div class="col">Total</div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="appendRowData">
                                
                            </tbody>

                            <tbody id="purchase_request_no_data">
                                <tr align="center">
                                    <td colspan="9">No data available in table</td>
                                </tr>
                            </tbody>

                            <tbody id="no_data">
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group" align="right" >
                        <button type="submit" class="btn btn-primary btn-submit"><span class="fa fa-floppy-o"></span>&nbsp;SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        let supplier = ["","",""];
        let rules = {
            supplier0 : {
                required: true
            },
            supplier1 : {
                required: true
            },
            supplier2 : {
                required: true
            },
            purchase_request: {
                required: true
            }
        };
        $('#canvassing_form').registerFields(rules);

        $('.select-supplier').change(function(){
            let id = $(this).attr('id');
            let elem = $(this).attr('id');
            let index  = elem.replace("supplier","");
            let text = $('#'+elem+' option:selected').text();
            if ($.inArray($(this).val(),supplier) > -1){
                $('#'+elem).val('');
                supplier[index] = "";

                toastMessage('Warning!',"Supplier "+text+" is already selected","warning");
            }else{
                supplier[index] = $(this).val();
            }

        });
        
        $('#purchase_request').change(function(){
            var url = $('option:selected', this).attr('data-url');
            if(url == null){
                $('#appendRowData').empty();
                $('#purchase_request_no_data').show();
            }
            else{
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(html){
                        $('#appendRowData').empty().html(html);
                        $('#purchase_request_no_data').hide();
                        $('#no_data').empty().html();
                    }
                });
            }
        });

        $(".newInput").each( function () {
            $(this).rules("add",{
                "required" : true
            })
        });

        function reloadData(data){
            //this function uses for reloading data needed
            $("#no_data").empty().append(data.append_no_data);
        }
        function decimalToFraction(value, donly = true) {
                var tolerance = 1.0E-6; // from how many decimals the number is rounded
                var h1 = 1;
                var h2 = 0;
                var k1 = 0;
                var k2 = 1;
                var negative = false;
                var i;

                if (parseInt(value) == value) { // if value is an integer, stop the script
                    return value;
                } else if (value < 0) {
                    negative = true;
                    value = -value;
                }

                if (donly) {
                    i = parseInt(value);
                    value -= i;
                }

                var b = value;

                do {
                    var a = Math.floor(b);
                   
                    var aux = h1;
                    h1 = a * h1 + h2;
                    h2 = aux;
                    aux = k1;
                    k1 = a * k1 + k2;
                    k2 = aux;
                    b = 1 / (b - a);
                } while (Math.abs(value - h1 / k1) > value * tolerance);

                return (negative ? "-" : '') + ((donly & (i != 0)) ? i + ' ' : '') + (h1 == 0 ? '' : h1 + "/" + k1);
        }
        function fractionTodecimal(fraction){
            var fraction_eval = 0;
            var string = fraction + '';
            var fractionParts = string.split('-');
            if (fractionParts.length === 1) {
                /* try space as divider */
                fractionParts = string.split(' ');
            }
            if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
                var integer = parseInt(fractionParts[0]);
                var decimalParts = fractionParts[1].split('/');
                var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
                fraction_eval = integer + decimal;
            }
            else if (fraction.indexOf('/') !== -1) {
                var decimalParts = fraction.split('/');
                var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
                fraction_eval = decimal;
            }
            else {
                fraction_eval = parseInt(fraction);
            }
            return fraction_eval;
        }
        function validquantity(selector, id, quantity,evt,id,id1){
            var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode != 47 && charCode > 32
                && (charCode < 48 || charCode > 57))
                return false;
                    var accepted = '#qty_input' + id;
                    var remaining = 0;
                    var total_accrej = $(selector).val();
                    $('.qtycanss'+id+'_'+id1).val(fractionTodecimal(total_accrej));
                    // console.log(fractionTodecimal(total_accrej));
                    if(fractionTodecimal(total_accrej) > quantity){
                        var check_id = $(selector).attr('id');
                        $(selector).val(decimalToFraction(quantity));
                        $('.qtycanss'+id+'_'+id1).val(decimalToFraction(quantity));
                    }
            return true;
            
        }
    </script>
@endpush
@endsection
<style>
    .margin-top{
        margin-top:50px;
    }
</style>
