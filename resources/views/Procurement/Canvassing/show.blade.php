@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Procurement</li>
            <li class="breadcrumb-item active" aria-current="page">Canvassing</li>
        </ol>
    </nav>
    <h4 class="form-head">View Canvassing</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style=""><a href="{{ route('canvassing.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;BACK</button></a></div>
            <div class="tab-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-nav-tab">
                                <ul class="nav nav-tabs nav-tab-category" role="tablist">
                                    <li class="nav-items"><a href="#abstract_canvass" class="nav-link active" data-toggle="tab">Abstract of Canvass</a></li>
                                    <li class="nav-items"><a href="#suppliers_canvass" class="nav-link" data-toggle="tab">Suppliers Canvass</a></li>
                                </ul>
                                <div class="tab-content-about">
                                    <div class="tab-pane fade active show" id="abstract_canvass">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead class="thead-dark">
                                                <tr align="center">
                                                    <th width="5%" style="vertical-align:middle;">Item</th>
                                                    <th width="5%" style="vertical-align:middle;">Quantity</th>
                                                    <th width="18%" style="vertical-align:middle;">Description</th>
                                                    @foreach($canvass as $value)
                                                    @foreach($suppliers as $supplier)
                                                        @if($value->supplier_id == $supplier->id)
                                                            <th width="18%" style="vertical-align:middle;">Quotation<br>{{$supplier->supplier_name}}</th>
                                                        @endif
                                                    @endforeach
                                                    @endforeach
                                                    <th width="18%" style="vertical-align:middle;">Awarded to</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($grouped_canvass_items as $key =>  $grouped_item)
                                            @if($group_id == $grouped_item->group_id)
                                            <tr align="center">
                                                @foreach($purchase_request_items as $pr_item)
                                                    @if($pr_item->id == $grouped_item->pr_item_id)
                                                    <td>{{$key+1}}</td>
                                                    <td>{{ App\Http\Controllers\PurchaseRequestController::decToFraction($pr_item->quantity) }}</td>
                                                    <td>{{$pr_item->getInventoryName->description}}</td>
                                                    @endif
                                                @endforeach

                                                @foreach($canvass_items as $canvass_item_key => $item)
                                                    @if($item->getCanvasses->group_id == $grouped_item->group_id && $item->pr_item_id == $grouped_item->pr_item_id)
                                                        <td>₱&nbsp;{{number_format($item->unit_price * $item->available_quantity,2)}}</td>
                                                    @endif
                                                @endforeach
                                                <td>{{$supplier_awarded->getSuppliers->supplier_name}}</td>
                                            </tr>
                                            @endif
                                            @endforeach
                                            <tr align="center">
                                                <td colspan="3" style="text-align: right;">Total</td>
                                                @foreach($supplier_total as $total)
                                                    <td>₱&nbsp;{{ number_format($total,2) }}</td>
                                                @endforeach
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="form-group" align="right" >
                                            <button class="btn btn-primary print-form-css" data-form="print-abstract-canvass"><i class="fa fa-print" aria-hidden="true"></i> PRINT</button>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="suppliers_canvass">
                                        @foreach($canvass  as $key => $value)
                                        <div class="collapse-border">
                                            <div class="accordion-collapse" data-toggle="collapse" data-target="#supplier{{$value->id}}">{{$value->getSuppliers->supplier_name}}</div>
                                            <div id="supplier{{$value->id}}" class="collapse padding">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead class="thead-dark">
                                                        <tr align="center">
                                                            <th width="8%" style="vertical-align:middle;">Item No.</th>
                                                            <th width="7%" style="vertical-align:middle;">Quantity</th>
                                                            <th width="15%" style="vertical-align:middle;">Unit</th>
                                                            <th width="30%" style="vertical-align:middle;">Description</th>
                                                            <th width="20%" style="vertical-align:middle;">Unit Price</th>
                                                            <th width="7%" style="vertical-align:middle;">Available Quantity</th>
                                                            <th width="20%" style="vertical-align:middle;">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php ($count = 1); $total = 0; @endphp
                                                        @foreach($canvass_items as $items)
                                                        @if($value->id == $items->canvass_id)

                                                            @foreach($purchase_request_items as $pr_item)
                                                            @if($pr_item->id == $items->pr_item_id)
                                                            @php $total = $total + ($items->available_quantity * $items->unit_price); @endphp
                                                                <tr align="center">
                                                                    <td>{{$count++}}</td>
                                                                    <td>{{ number_format($pr_item->quantity) }}</td>
                                                                    <td>{{$pr_item->getUnits->unit_name}}</td>
                                                                    <td>{{$pr_item->getInventoryName->description}}</td>
                                                                    <td>₱&nbsp;{{number_format($items->unit_price,2)}}</td>
                                                                    <td>{{  App\Http\Controllers\PurchaseRequestController::decToFraction($items->available_quantity)}}</td>
                                                                    <td>₱&nbsp;{{number_format($items->available_quantity * $items->unit_price,2)}}</td>
                                                                </tr>
                                                            @endif
                                                            @endforeach

                                                        @endif
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="6" style="text-align: right;">Total</td>
                                                            <td align="center"><b>₱&nbsp;{{ number_format($total,2) }}</b></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="form-group" align="right" style="margin-top: 15px;">
                                            <button class="btn btn-primary print-form-css" data-form="print-supplier-canvass"><i class="fa fa-print" aria-hidden="true"></i> PRINT</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="print-abstract-canvass" hidden>   
    <div style="margin-bottom: 50px;">
        <!-- @include('layouts.header') -->
    </div>
    @include('Docs.abstract-canvass')
    <style>
        @page{
            margin: 0% 10%;
        }
       .fs-14{
           font-size: 14px;
       }
       .pad-0rm{
           padding: 2px!important;
       }
    </style>
</div>
<div id="print-supplier-canvass" hidden>
    <!-- @include('layouts.header')  -->
    <div class="mt-4">
        @include('Docs.suppliers-canvass')
    </div>
    <style>
        .m-signatory{
            padding-top:2.5rem; 
            margin-left:30rem; 
            position: absolute;
        }
    </style>
</div>


@endsection

<style>
    /* Tabs for Categories */
    .about-nav-tab .nav-tabs {
        margin-bottom: 40px;
    }
    .about-nav-tab .nav-tabs .nav-link.active {
        border-color: #132644 !important;
    }
    .about-nav-tab .nav-tabs .nav-link.active {
        color: white;
        background-color: #132644;
    }
    .about-nav-tab .nav-tabs .nav-link {
        color: black;
        border: none;
    }
    .about-nav-tab .nav-tabs .nav-items .nav-link {
        font-weight: 600;
        padding: 18px 30px;
        line-height: 1;
        border-color: #132644;
        font-size: 20px;
    }
    .nav-tab-category {
        border-bottom: 3px solid #132644 !important
    }
    .tab-content-about > .tab-pane.active {
        display: block;
    }
    .about-nav-tab .tab-content-about .tab-pane p {
        margin-bottom: 25px;
        font-size: 18px;
        color: aliceblue;
    }
    .about-nav-tab .tab-content-about .tab-pane ul {
        margin-bottom: 43px;
    }
    .tab-content-about>.tab-pane {
        display: none;
    }

    /*for accordion collapse */
    .accordion-collapse{
        background-color: #132644;
        color: #fff;
        padding: 18px;
        width: 100%;
        transition: 0.1s;
        border-bottom: 1px solid #fff;
    }
    .collapse-border{
        border: 1px solid #132644;

    }
    .padding{
        padding: 1rem 1rem 0 1rem;
    }
    .btn-print{
        margin-top: 1rem;
    }

</style>