@extends('layouts.app')

@section('content')


<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Reports</li>
        </ol>
    </nav>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-nav-tab">
                                <ul class="nav nav-tabs nav-tab-category" role="tablist">
                                    <li class="nav-items"><a href="#accounting_tab" class="nav-link active" data-toggle="tab">Accounting</a></li>
                                    <!-- <li class="nav-items"><a href="#procurement_tab" class="nav-link" data-toggle="tab">Procurement</a></li> -->
                                    <li class="nav-items"><a href="#inventory_tab" class="nav-link" data-toggle="tab">Inventory</a></li>
                                </ul>
                                <div class="tab-content-about">
                                    <div class="tab-pane fade active show" id="accounting_tab">                   
                                        <form method="POST"  class="form-horizontal" action="" enctype="multipart/form-data" id="accounting_tab_form">
                                            <div class="form-row">
                                                <!-- <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-coins"></i></div>
                                                                <h2>Financial Statement Report</h2>
                                                            </div>
                                                            <div class="drop"><p>VIEW</p>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fa fa-bar-chart"></i></div>
                                                                <h2>Profit and Loss Report</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <p>VIEW</p>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fa fa-line-chart"></i></div>
                                                                <h2>Record of Estimated and Actual Income</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('estimated_actual_income.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fa fa-line-chart"></i></div>
                                                                <h2>Cedula</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('cedula.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-file-alt"></i></div>
                                                                <h2>Transmittal Letter</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('transmital_letter.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-file-invoice"></i></div>
                                                                <h2 class="txt-ellipsis">Registration of Obligation Personal Services, MOOE and Capital Outlay Expenses Classification</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('registration_obligation.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            
                                            </div>
                                            <br>
                                            <div class="form-row mt-n4">
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-book"></i></div>
                                                                <h2>Cashbook</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('cashbook.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-credit-card"></i></div>
                                                                <h2>Statement of Appropriations Obligations & Balances</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('obligations_balances.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fa fa-bank"></i></div>
                                                                <h2>RBI</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('rbi_form.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-id-card"></i></div>
                                                                <h2>Accountability Forms</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('accountability_form.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-row mt-n4">
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">	
                                                                <div class="icon"><i class="fa fa-exchange"></i></div>
                                                                <h2 class="txt-ellipsis">Statement of Comparison of Budget and Actual Amounts</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('comparison_statement.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">	
                                                                    <div class="icon"><i class="fa fa-exchange"></i></div>
                                                                    <h2 class="txt-ellipsis">Reports of Collections and Deposits</h2>
                                                                </div>
                                                                <div class="drop">
                                                                    <a href="{{ route('official_receipt.report') }}"><p>VIEW</p></a>
                                                                    <div class="arrow"></div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fas fa-file-invoice"></i></div>
                                                                <h2 class="txt-ellipsis">Registration of Obligation Personal Services, MOOE and Capital Outlay Expenses Classification</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('registration_obligation.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->

                                                <!-- <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fa fa-line-chart"></i></div>
                                                                <h2>Record of Estimated and Actual Income</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('estimated_actual_income.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">
                                                                <div class="icon"><i class="fa fa-line-chart"></i></div>
                                                                <h2>Cedula</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('cedula.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <!-- <div class="form-row mt-n4">
                                                <div class="col-md-3 card-reports">
                                                    <div class="ui">
                                                        <div class="ui_box white">
                                                            <div class="ui_box__inner">	
                                                                <div class="icon"><i class="fa fa-exchange"></i></div>
                                                                <h2 class="txt-ellipsis">Reports of Collections and Deposits</h2>
                                                            </div>
                                                            <div class="drop">
                                                                <a href="{{ route('official_receipt.report') }}"><p>VIEW</p></a>
                                                                <div class="arrow"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>            -->
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="procurement_tab">
                                        <form method="POST"  class="form-horizontal" action="" enctype="multipart/form-data" id="procurement_tab_form">
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="inventory_tab">
                                        <form method="POST"  class="form-horizontal" action="" enctype="multipart/form-data" id="inventory_tab_form">
                                            <div class="form-row">
                                                <div class="col-md-3">
                                                    <div class="col-md-3 card-reports">
                                                        <div class="ui">
                                                            <div class="ui_box white">
                                                                <div class="ui_box__inner">
                                                                    <div class="icon"><i class="fa fa-industry"></i></div>
                                                                    <h2>Supplies and Materials</h2>
                                                                </div>
                                                                <div class="drop">
                                                                    <a href="{{ route('supplies_materials.report') }}"><p>VIEW</p></a>
                                                                    <div class="arrow"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

<style>
    .accounting-card:hover a {
        text-decoration: none;
        color: white;
    }
    .accounting-card:hover a h6:hover {
        text-decoration: none;
        color: white;
    }
    .lheight-normal{
        line-height: normal!important;
    }
    .accounting-card{
        height: 8rem; 
        width: 14rem;
        border: solid 1px!important;
        border-radius: 0.5rem!important;
    }
    .accounting-card:hover{
        -webkit-transform: scale(1.1);
        -ms-transform: scale(1.1);
        transform: scale(1.1);
        background: #132644;
        color: #fff;
        border-radius: 1rem!important;
        transition: 0.5s;
    }
    .pt-3rm {
        padding-top: 3rem;
    }
    .pt5-5rm{
        padding-top: 5.5rem;
    }
    .pt-5rm{
        padding-top: 5rem;
    }
    h6{
        line-height: 36px !important;
    }
    .margin-top{
        margin-top:50px;
    }
    /* Tabs for Chart of Accounts */
    .about-nav-tab .nav-tabs {
        margin-bottom: 40px;
    }
    .about-nav-tab .nav-tabs .nav-link.active {
        border-color: #132644 !important;
    }
    .about-nav-tab .nav-tabs .nav-link.active {
        color: white;
        background-color: #132644;
    }
    .about-nav-tab .nav-tabs .nav-link {
        color: black;
        border: none;
    }
    .about-nav-tab .nav-tabs .nav-items .nav-link {
        font-weight: 600;
        padding: 18px 30px;
        line-height: 1;
        border-color: #132644;
        font-size: 20px;
    }
    .nav-tab-category {
        border-bottom: 3px solid #132644 !important
    }
    .tab-content-about > .tab-pane.active {
        display: block;
    }
    .about-nav-tab .tab-content-about .tab-pane p {
        margin-bottom: 25px;
        font-size: 18px;
        color: aliceblue;
    }
    .about-nav-tab .tab-content-about .tab-pane ul {
        margin-bottom: 43px;
    }
    .tab-content-about>.tab-pane {
        display: none;
    }


    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }
</style>