@include('layouts.header')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <p>Fund: </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <p>Date: {{ $month }} {{ $month != null ?'1-31': ''}} {{ $year }}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <p>Name of Accountable Officer:ANTONIO A. SAUR </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <p>Report: no. 10</p>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered table-hover text-center" style="margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th colspan="15" style="text-align: left;"><b>A. COLLECTIONS</b></th>
            </tr>
            <tr>
                <td colspan="15" style="text-align: left;">1. For Collectors</td>
            </tr>
            <tr>
                <td rowspan="3" colspan="2" class="v-align" style="font-weight: 600;">Type of Form</td>
                <td colspan="7" style="font-weight: 600;">Official Receipt Serial Numbers</td>
                <td colspan="5" style="font-weight: 600;">PESOS</td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: 600;">QTY</td>
                <td colspan="2" style="font-weight: 600;">From</td>
                <td colspan="2" style="font-weight: 600;">To</td>
                <td colspan="1" style="border-left-style: hidden;"></td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="">&nbsp;</td>
                <td colspan="2" style="">&nbsp;</td>
                <td colspan="3" style="">&nbsp;</td>
                <td colspan="5" style="">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" rowspan="4" class="v-align" style="font-weight: 600;">CTC</td>
            </tr>
            <tr>
                <td colspan="2" style="">{{ (empty($CTCPadsData)?'':$CTCPadsData['quantity']) }}</td>
                <td colspan="2" style="">{{ (empty($CTCPadsData)?'':$CTCPadsData['first']) }}</td>
                <td colspan="3" style="">{{ (empty($CTCPadsData)?'':$CTCPadsData['lastdvs']) }}</td>
                <td colspan="5" style="">₱&nbsp;{{ (empty($CTCPadsData)?'':$totalitem) }}</td>
            </tr>
            <tr>
               <td colspan="2" style="">&nbsp;</td>
                <td colspan="2" style="">&nbsp;</td>
                <td colspan="3" style="">&nbsp;</td>
                <td colspan="5" style="">&nbsp;</td>
            </tr>
            <tr>
               <td colspan="2" style="">&nbsp;</td>
                <td colspan="2" style="">&nbsp;</td>
                <td colspan="3" style="">&nbsp;</td>
                <td colspan="5" style="">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="13" style="text-align: left; font-weight: 600;">2. For Liquidating Officers/Treasurers</td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: left; font-weight: 600;">Name of Accountable Officers</td>
                <td colspan="4" style="font-weight: 600;">Reference No.</td>
                <td colspan="4" style="font-weight: 600;">Amount</td>
            </tr>
            <tr>
                <td colspan="5"><p style="margin: 0; margin-top: 3rem; font-weight: 600;">Michelle V. Meniano</p><p style="margin: 0; font-weight: 600;">Barangay Treasurer</p></td>
                <td colspan="4" class="v-align">{{ $month }} {{ $month != null ?'1-31': ''}} {{ $year }}</td>
                <td colspan="4" class="v-align">₱&nbsp;{{ (empty($CTCPadsData)?'':$totalitem) }}</td>
            </tr>
            <tr>
                <th colspan="13" style="text-align: left;"><b>B. REMITTANCE/DEPOSITS</b></th>
            </tr>
            <tr>
                <td colspan="5" style="text-align: left; font-weight: 600;">Name of Accountable Officers</td>
                <td colspan="4">&nbsp;</td>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5"><p style="margin: 0; margin-top: 3rem; font-weight: 600;">ANTONIO A. SAUR</p><p style="margin: 0; font-weight: 600;">OIC Cash Division</p></td>
                <td colspan="4">&nbsp;</td>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <th colspan="15" style="text-align: left;"><b>C. ACCOUNTABILITY FOR ACCOUNTABLE FORMS</b></th>
            </tr>
            <tr>
                <td rowspan="3" style="vertical-align: middle; font-weight: 600;">Form&nbsp;Name&nbsp;& #</td>
                <td colspan="3" style="font-weight: 600;">Beginning Balance</td>
                <td colspan="3" style="font-weight: 600;">Received</td>
                <td colspan="3" style="font-weight: 600;">Issued</td>
                <td colspan="3" style="font-weight: 600;">Balance</td>
            </tr>
            <tr>
                <td colspan="3" style="font-weight: 600;">Serial Nos.</td>
                <td colspan="3" style="font-weight: 600;">Serial Nos.</td>
                <td colspan="3" style="font-weight: 600;">Serial Nos.</td>
                <td colspan="3" style="font-weight: 600;">Serial Nos.</td>
            </tr>
            <tr>
                <td style="font-weight: 600;">QTY</td>
                <td style="font-weight: 600;">From</td>
                <td style="font-weight: 600;">To</td>
                <td style="font-weight: 600;">QTY</td>
                <td style="font-weight: 600;">From</td>
                <td style="font-weight: 600;">To</td>
                <td style="font-weight: 600;">QTY</td>
                <td style="font-weight: 600;">From</td>
                <td style="font-weight: 600;">To</td>
                <td style="font-weight: 600;">QTY</td>
                <td style="font-weight: 600;">From</td>
                <td style="font-weight: 600;">To</td>
            </tr>
            <tr>
                <td style="text-align: left;">CTC</td>
                <td>{{ (empty($CTCVPadData)?'':$CTCVPadData['quantity']) }}</td>
                <td>{{ (empty($CTCVPadData)?'':$CTCVPadData['start']) }}</td>
                <td>{{ (empty($CTCVPadData)?'':$CTCVPadData['start']+$CTCVPadData['quantity'] - 1) }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>{{ (empty($CTCPadsData)?'':($CTCPadsData['quantity'] == 0 ?'':$CTCPadsData['quantity'])) }}</td>
                <td>{{ (empty($CTCPadsData)?'':($CTCPadsData['first'] == 0 ?'':$CTCPadsData['first'])) }}</td>
                <td>{{ (empty($CTCPadsData)?'':($CTCPadsData['lastdvs'] == 0 ?'':$CTCPadsData['lastdvs'])) }}</td>
                <td>{{ (empty($CTCVPadData)?'':$CTCVPadData['quantity'] - $CTCPadsData['quantity']) }}</td>
                <td>{{ (empty($CTCVPadData)?'':($CTCPadsData['lastdvs'] == 0?$CTCVPadData['start']:$CTCPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($CTCVPadData)?'':$CTCVPadData['start']+$CTCVPadData['quantity']- 1) }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
                <td colspan="4">&nbsp;</td>
                <td colspan="6" style="text-align: left; font-weight: 600;">List of Checks:</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; font-weight: 600;">Beginning Balance</td>
                <td colspan="4">NIL</td>
                <td style="text-align: left; font-weight: 600;">Bank</td>
                <td colspan="3" style="text-align: left; font-weight: 600;">Check No.</td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; font-weight: 600;">Add : Collections</td>
                <td colspan="4">₱&nbsp;{{ (empty($CTCPadsData)?'':$totalitem) }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; font-weight: 600;">Cash Deposited</td>
                <td colspan="4">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; font-weight: 600;">Applied Deposites (checks):</td>
                <td colspan="4">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right; font-weight: 600;">TOTAL</td>
                <td colspan="4">₱&nbsp;{{ (empty($CTCPadsData)?'':$totalitem) }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; font-weight: 600;">Less: Remittance Deposit to LB QC</td>
                <td colspan="4">₱&nbsp;{{ (empty($CTCPadsData)?'':$totalitem) }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; font-weight: 600;">Cash on Hand</td>
                <td colspan="4" style="font-weight: 600;">NIL</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: left;"><b>CERTIFICATION:</b></td>
                <td colspan="7" style="border-bottom-style: hidden;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: left;">
                    <p>I hereby certify that B depositsand accountability for accountable forms is true correct.</p>
                    <div style="text-align: center;"><p style="margin: 0; margin-top: 3rem;"><b>Michelle V. Meniano</p></b><p><b>Barangay Treasurer</b></p></div>
                </td>
                <td colspan="7">
                    <p>I hereby certify that B depositsand accountability for accountable forms is true correct.</p>
                    <div style="text-align: center;"><p style="margin: 0; margin-top: 3rem;"><b>ANTONIO A. SAUR</b></p><p><b>OIC Cash Division</b></p></div>
                </td>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
  
</div>


