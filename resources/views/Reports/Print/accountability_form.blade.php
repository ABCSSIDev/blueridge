<table width="100%">
    <tr>
        <td width="40%"><p>Barangay: BARANGAY BLUE RIDGE B</p></td>
        <td width="35%"><p>City/Municipality: QUEZON CITY</p></td>
        <td width="25%"><p>RAAF No.:</p></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width="27%"><p>Barangay Treasurer: MICHELLE V. MENIANO</p></td>
        <td width="40%"><p>Province: METRO MANILA</p></td>
    </tr>
</table>
<table class="table table-striped responsive table-bordered table-hover text-center" style="width: 100%;">
    <thead class="thead-dark">
        <tr>
            <th rowspan="3" style="vertical-align: middle;">Name of Form and No.</th>
            <th colspan="3">Beginning Balance</th>
            <th colspan="3">Receipt</th>
            <th colspan="3">Issued</th>
            <th colspan="3">Ending Balance</th>
        </tr>
        <tr>
            <th rowspan="2" style="vertical-align: middle;">Qty</th>    
            <th colspan="2">Inclusive Serial No.</th>
            <th rowspan="2" style="vertical-align: middle;">Qty</th>    
            <th colspan="2">Inclusive Serial No.</th>
            <th rowspan="2" style="vertical-align: middle;">Qty</th>    
            <th colspan="2">Inclusive Serial No.</th>
            <th rowspan="2" style="vertical-align: middle;">Qty</th>    
            <th colspan="2">Inclusive Serial No.</th>
        </tr>
        <tr>
            <th>From</th>
            <th>To</th>
            <th>From</th>
            <th>To</th>
            <th>From</th>
            <th>To</th>
            <th>From</th>
            <th>To</th>
        </tr>
        <tr>
            <td style="text-align: left;"><b>With Money Value:</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Cash Tickets</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: left;"><b>Without Money Value:</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Official Receipts</td>
            <td>{{ (empty($ORPad)?'':$ORPad['quantity']) }}</td>
            <td>{{ (empty($ORPad)?'':$ORPad['start']) }}</td>
            <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']) }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ (empty($ORPadsData)?'':$ORPadsData['quantity']) }}</td>
            <td>{{ (empty($ORPadsData)?'':$ORPadsData['first']) }}</td>
            <td>{{ (empty($ORPadsData)?'':$ORPadsData['lastdvs']) }}</td>
            <td>{{ (empty($ORPad)?'':$ORPad['quantity'] - $ORPadsData['quantity']) }}</td>
            <td>{{ (empty($ORPad)?'':($ORPadsData['lastdvs'] == 0?$ORPad['start']:$ORPadsData['lastdvs'] + 1)) }}</td>
            <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']) }}</td>
        </tr>
        <tr>
            <td>Checks</td>
            <td>{{ (empty($DVPad)?'':$DVPad['quantity']) }}</td>
            <td>{{ (empty($DVPad)?'':$DVPad['start']) }}</td>
            <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']) }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ (empty($DVPadsData)?'':$DVPadsData['quantity']) }}</td>
            <td>{{ (empty($DVPadsData)?'':$DVPadsData['first']) }}</td>
            <td>{{ (empty($DVPadsData)?'':$DVPadsData['lastdvs']) }}</td>
            <td>{{ (empty($DVPad)?'':$DVPad['quantity'] - $DVPadsData['quantity']) }}</td>
            <td>{{ (empty($DVPad)?'':($DVPadsData['lastdvs'] == 0?$DVPad['start']:$DVPadsData['lastdvs'] + 1)) }}</td>
            <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']) }}</td>
        </tr>
        <tr>
            <td class="" colspan="13">
                <div class="" style="margin: 1rem;">
                    <div class="row">
                        <h6 style="text-align: left;">CERTIFICATION</h6>
                        <p style="text-align: left; text-indent: 4rem;">I hereby certify that the foregoing is a true statement of all accountable forms received,
                            issued and transferred by me during the above-stated period and the correctness of the beginning balances.</p>
                    </div>
                    <table width="100%" style="margin-top: 3em">
                        <tr>
                            <td width="50%" style="border: none; font-weight: 600;">MICHELLE V. MENIANO<p class="bt-po" style="width: 80%; margin-left: 3rem; margin-top: -1px;"></p><p style="margin-bottom: -1rem; margin-top: -1px;">(Name and Signature)</p><p style="">Barangay Treasurer</p></td>
                            <td width="50%" style="border: none;">   {{ empty($year)?'':date('M Y',strtotime($year.'-'.$month.'')) }}<p class="bt-po" style="margin-left: 7rem; width: 50%; margin-top: -1px;"></p><p style="margin-left: 1.5rem; margin-top: -1px;">Date</p></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </thead>
</table>