

<table class="table table-bordered margin-top">
    <thead>
        <tr class="text-center">
            <th colspan="15"><b>CASHBOOK</b></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="15">
                <div>
                    <p>Barangay: BARANGAY BLUE RIDGE B</p><p class="btm-cashbook" style="width: 35%; left: 4rem;">
                    <p>Barangay Treasurer: MICHELLE V. MENIANO</p><p class="btm-cashbook" style="width: 30%;"></p>
                    <p>Calendar Year: <strong><input class="date-selected input-total-pr mon" id="mon" name="mon"></strong></p><p class="btm-cashbook" style="width: 32.5%; left: 6rem;">
                </div>
            </td>        
        </tr> 
        <tr align="center">
            <td rowspan="2">Date</td>
            <td rowspan="2">Particulars</td>
            <td rowspan="2">Reference</td>
            <td colspan="3">Cash in Local Treasury</td>
            <td colspan="3">Cash in Bank</td>
            <td colspan="3">Cash Advances</td>
            <td colspan="3">Pettycash</td>
        </tr>
        <tr>
            <td >Collection</td>
            <td >Deposit</td>
            <td >Balance</td>
            <td >Deposit</td>
            <td >Check Issued</td>
            <td >Balance</td>
            <td >Receipt</td>
            <td >Disbursements</td>
            <td >Balance</td>
            <td >Receipts/Replenishment</td>
            <td >Payments</td>
            <td >Balance</td>
        </tr>    
        <tbody class="report" name="report">
        </tbody>
        <tr >
            <td colspan="15">
                <div style="margin-left: 40%">
                    <p style="text-indent: 30px"><b>CERTIFICATION:</b></p>
                    <p>I hereby certify that the foregoing is a correct and complete record of all my collections, deposits/remittances and balances of my accounts in the cah - In Local Treasury, Cash in Bank, Cash advances, and Petty Cash as of <u style="color: red"><strong><input class="date-selected input-total-pr mon" id="mon" name="mon"></strong>.</u></p>
                </div>
                <div style="margin-left: 70%;margin-top: 50px">
                    <p style="text-indent: 30px"><b>MICHELLE V. MENIANO</b></p>
                    <p class="border-t" style="margin-top: -1rem;">Name and Signature</p>
                    <p>
                       <input class="for_savings" readonly style="border: none;background-color: transparent;" name="dateprint" id="dateprint">
                    </p>
                    <p class="border-t" style="margin-top: -1rem;">Date</p>
                </div>
            </td>
        </tr>
    </tbody>

</table>


<style>
    .border-t{
        border-top: 1px solid #000;
        width: 250px;
        text-align:center;
    }

    .border{
        border: 3px solid #000;
    }
    .table thead th, .table td {
        vertical-align: middle !important;
    }
    .input-total-pr { 
        border: none;
        background-color: transparent;
        
    }
    .border-b{
        border-bottom: 1px solid #000;
        width: 250px;
        text-align:center;
    }
    .margin-top{
        margin-top: 30px;
    }
    .btm-cashbook {
        border-bottom: 1px solid; 
        position: relative; 
        left: 8rem;
        margin-top: -1rem;
    }
</style>