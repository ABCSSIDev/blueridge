<table class="table table-bordered text-center mt-table">
    <thead>
        <tr align="center">
            <th colspan="10">REPORT ON INVENTORY OF SUPPLIES AND MATERIALS<br>
            As of <span style="text-decoration: underline"><strong><input class="date-selected input-total-pr"></strong></span>
            </th>
        </tr>
        <tr align="left">
            <th colspan="6" >Barangay: BLUE RIDGE B<br>Tel No.: (8)535-9822</th>
            <th colspan="4">City/Municipal: <span class="border-b">QUEZON CITY</span><br>Province:<span class="border-b">NCR</span></th>

        </tr>
        <tr align="left">
            <th colspan="10">For which <u style="margin-left: 20px">Michelle V. Meniano</u> <u style="margin-left: 20px">Barangay Treasurer</u> <span style="margin-left: 20px">is accountable, having assumed accountability on</span> <u><span style="text-decoration: underline"><strong><input class="date-selected input-total-pr"></strong></span></u><br><span style="margin-left: 85px">(Accountable Officer)</span><span style="margin-left: 40px">(Designation)</span><span style="margin-left: 365px">(Date of Assumption)</span></th>
        </tr>
        <tr align="center">
            <th rowspan="2" width="15%"><b>Article</b></th>
            <th rowspan="2" width="15%"><b>Description</b></th>
            <th rowspan="2" width="10%"><b>Stock Number</b></th>
            <th rowspan="2" width="10%"><b>Unit of Measure</b></th>
            <th rowspan="2" width="10%"><b>Unit Value</b></th>
            <th rowspan="2" width="10%"><b>Balance per Card (Quantity)</b></th>
            <th rowspan="2" width="10%"><b>On hand per Count (Quantity)</b></th>
            <th colspan="2" width="10%"><b>Shortage/Overage</b></th>
            <th rowspan="2" width="10%"><b>Remarks</b></th>
        </tr>
        <tr>
            <th ><b>Quantity</b></th>
            <th ><b>Value</b></th>
        </tr>
    </thead>
    <tbody class="report" name="report">
    </tbody>
    <tbody class="nodatatr" name="nodatatr">
        <tr align="center" >
            <td colspan="10">No Data Available</td>
        </tr>
    </tbody>
    <tbody>
        <table width="100%">
            <tr>
                <td style="border: none; text-align: left;"><b>Prepared By:</b></td>
                <td style="border: none; text-align: left; position: absolute; margin-left: 27rem;"><b>Approved By:</b></td>
            </tr>
            <tr>
                <td style="border: none;" align="center"><p style="margin-top: 1em;"></p><p style="margin-top: 2rem;">Michelle V. Meniano</p><p style="" class="bbm-supply"></p></td>
                <td style="border: none;" align="center"><p style="margin-top: 1em;"></p><p style="margin-top: 2rem;">Anna Francesca L. Maristela</p><p style="" class="bbm-supply"></p></td>
                <td style="border: none;" align="center"><p style="margin-top: 1em;"></p><p style="margin-top: 2rem; font-weight: 600;">ESPERANZA CASTRO-LEE</p><p style="" class="bbm-supply"></p></td>
            </tr>
            <tr>
                <td style="border: none;" align="center"><p style="margin-top: 1em;"></p><p>Barangay Treasurer</p></td>
                <td style="border: none;" align="center"><p style="margin-top: 1em;"></p><p>Chairman, Committee on Inspection</p></td>
                <td style="border: none;" align="center"><p style="margin-top: 1em;"></p><p>Punong Barangay</p></td>
            </tr>
        </table>
    </tbody>
</table>

<style>
    .border{
        border: 3px solid #000;
    }
    .table thead th, .table td {
        vertical-align: middle !important;
    }
    .input-total-pr { 
        border: none;
        background-color: transparent;
    }
    .border-b{
        border-bottom: 1px solid #000;
        width: 250px;
        text-align:center;
    }
   
</style>
