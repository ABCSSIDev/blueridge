@include('layouts.header')

<div class="col-md-12">
    <table class="table table-striped table-bordered table-hover table-mediascreen" style="margin-top: 50px">
        <thead class="thead-dark">
            <tr align="center">
            <th rowspan="2" width="15%">Date</th>
            <th rowspan="2" width="10%">Particulars</th>
            <th rowspan="2" width="10%">Reference No.</th>
            <th colspan="6" width="60%">Income Accounts</th>
            <th rowspan="2" width="10%">Total</th>
            </tr>
            <tr align="center">
            <th width="10%">RPT</th>
            <th width="10%">Business Tax</th>
            <th width="10%">Community Tax</th>
            <th width="10%">Share from IRA</th>
            <th width="10%">Other Service Income</th>
            <th width="10%">Grants and Donations in Cash</th>
            </tr>
        </thead>
        
        <tbody class="report state-app" name="report">
            <tr>
                <td colspan="10"><b>A. Income Estimates</b></td>
            </tr>
            <tr align="center" style="border-style:hidden;">
                <td colspan="15">No data available in table</td>
            </tr>
            <tr>
                <td colspan="10"><b>B. Actual Collections</b></td>
            </tr>
            <tr align="center" style="border-style:hidden;">
                <td colspan="15">No data available in table</td>
            </tr>
        </tbody>
    </table>
</div>
<table width="100%">
    <tr>
        <td width="60%"><p>Prepared by:</p></td>
        <td width="40%"><p>Noted by:</p></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width="60%"><p style="margin-left: 6.5rem;">Michelle V. Meniano</p></td>
        <td width="40%"><p style="margin-left: 5rem;"><b>ESPERANZA CASTRO-LEE</b></p></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width="60%"><p style="position: relative; margin-top: -1rem; margin-left: 7rem;">Barangay Tresurer</p></td>
        <td width="40%"><p style="position: relative; margin-top: -1rem; margin-left: 8rem;">Punong Barangay</p></td>
    </tr>
</table>


<style>
    th,td{
        vertical-align: middle !important;
    }
    .m-left{
        margin-left: 10%;
    }
</style>