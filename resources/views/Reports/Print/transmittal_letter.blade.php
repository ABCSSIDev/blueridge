
@include('layouts.header')
    <table width="100%">
        <tr>
            <td width="70%"><p>To: The City/Municpal Accountant</p></td>
            <td width="30%"><p>Date:</p></td>
        </tr>
    </table>
    <p>Sir/Madam:</p>
    <p>We submit herewith the following documents:
        a) certified copy of the cashbook;
        b) copy of PBCs issued
        c) original of the Disbursment Voucher/payroll issued for the {{ date('F Y', strtotime($data->created_at)) }} duly acknowledged by the payees:</p>
    <table class="table table-striped table-bordered text-center" width="100%" style="width: 100%; margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th colspan="2" style="text-align: left;">A. DV/Payroll</th>
                <th colspan="2">Check</th>
                <th rowspan="2" style="vertical-align: middle;">Payee</th>
                <th rowspan="2" style="vertical-align: middle;">Amount</th>
                <th colspan="2">PB Certification</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>No.</th>
                <th>Date</th>
                <th>No.</th>
                <th>Date</th>
                <th>No.</th>
            </tr>
            @if(empty($DisbursementVoucher))
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @else
                @forelse($DisbursementVoucher as $data)
                    <tr>
                        <td>{{ date('m/d/Y', strtotime($data->created_at)) }}</td>
                        <td>{{ App\Common::getPRNumberFormat('roas', $data->getROA) }}</td>
                        <td>{{ date('m/d/Y', strtotime($data->created_at)) }}</td>
                        <td>{{ $data->check_number }}</td>
                        <td>{{ ($data->or_id == null?'':$data->getInvoice->getPayee->name) }}</td>
                        <td>₱&nbsp;{{ $data->getROA->total_amount }}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
        <tr>
            <th colspan="8" style="text-align: left;">B. RCDs Supporting Documents (RCRs, Ors and VDS)</th>
        </tr>
        <tr>
            <td colspan="3">Date</td>
            <td colspan="3">No.</td>
            <td colspan="3">Amount</td>
        </tr>
        @if(!empty($RCDSuppDoc))
            <tr>
                <td colspan="3">{{ $RCDSuppDoc['Date'] }}</td>
                <td colspan="3">{{ $RCDSuppDoc['rangepad'] }}</td>
                <td colspan="3">₱&nbsp;{{ $RCDSuppDoc['total'] }}</td>
            </tr>
        @else
            <tr align="center">
                <td colspan="15">No data available in table</td>
            </tr>
        @endif
            <tr>
                <th colspan="8" style="text-align: left;">C. Other Reports</th>
            </tr>
            <tr>
                <td colspan="3">Date</td>
                <td colspan="5">Type of Report</td>
            </tr>
            @if(!empty($OtherReports))
                <tr>
                    <td colspan="3">{{ date('M Y',strtotime($year.'-'.$month))}}</td>
                    <td colspan="5">Report of Collection and Deposits</td>
                </tr>
                <tr>
                    <td colspan="3">{{ date('M Y',strtotime($year.'-'.$month))}}</td>
                    <td colspan="5">Report of Accountability for Accountable Forms</td>
                </tr>
                <tr>
                    <td colspan="3">{{ date('M Y',strtotime($year.'-'.$month))}}</td>
                    <td colspan="5">Deposits Slip</td>
                </tr>
            @else
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @endif
        </thead>
    </table>
    <p>Please acknowledge receipt hereof.</p>
    <table width="100%">
        <tr>
            <td width="50%"></td>
            <td width="50%">Very truly yours,</td>
        </tr>
    </table>
    <table width="100%" style="margin-top: 1em">
        <tr>
            <td width="50%"></td>
            <td width="50%"><p style="margin-left: 9.5rem; font-weight: 600;">MICHELLE V. MENIANO</p><p class="bt-po" style="width: 60%; margin-left: 6rem; margin-top: -1rem;"></p><p style="margin-left: 11rem; margin-top: -1px;">Barangay Treasurer</p></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td><p style="text-align: left;">Noted by:</p></td>
            <td><p style="text-align: left;">Received by:</p></td>
        </tr>
        <tr>
            <td width="50%"><p style="margin-left: 9.5rem; font-weight: 600;">Esperanza Castro-Lee</p><p class="bt-po" style="width: 60%; margin-left: 5rem; margin-top: -1rem;"></p><p style="margin-left: 11rem; margin-top: -1px;">Punong Barangay</p></td>
            <td width="50%"><p style="margin-left: 9.5rem; font-weight: 600;">Sample Signatory</p><p class="bt-po" style="margin-left: 5rem; width: 68%; margin-top: -1rem;"></p><p style="margin-left: 9rem; margin-top: -1px;">Signature, Name and Designation</p></td>
        </tr>
    </table>
    <p style="margin-top: 3rem;"><i><b>*related to COA Circular 2019-001</b></i></p>
    <div class="page-break"></div>
    @include('layouts.header')
    <table width="100%">
        <tr>
            <td width="70%"><p>Name of Barangay Treasurer:</p></td>
            <td width="30%"><p>Date:</p></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="70%"><p>Barangay:</p></td>
            <td width="30%"><p>RCD No.:</p></td>
        </tr>
    </table>
    <table class="table table-striped table-bordered text-center" style="margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th colspan="13" style="text-align: left;"><b>A. Collection</b></th>
            </tr>
            <tr>
                <td colspan="4">Official Receipt/RCR</td>
                <td colspan="4" rowspan="2" style="vertical-align: middle;">Payor</td>
                <td colspan="4" rowspan="2" style="vertical-align: middle;">Nature of Collection</td>
                <td colspan="4" rowspan="2" style="vertical-align: middle;">Amount</td>
            </tr>
            <tr>
                <td colspan="2">Date</td>
                <td colspan="2">Number</td>
            </tr>
            @if(empty($invoices))
            @else
                @foreach($invoices as $invoice)
                    <tr>
                        <td colspan="2">{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                        <td colspan="2">{{ $invoice->or_number }}</td>
                        <td colspan="4">{{ $invoice->getPayee->name }}</td>
                        <td colspan="4">{{ $invoice->collection_nature }}</td>
                        <td colspan="4">₱&nbsp;{{ number_format($invoice->getInvoice->unit_price, 2) }}</td>
                    </tr>
                @endforeach
            @endif
            <tr>
                <th colspan="13" style="text-align: left;"><b>B. DEPOSITS</b></th>
            </tr>
            <tr>
                <td colspan="4">Bank/Branch</td>
                <td colspan="5">Reference</td>
                <td colspan="4">Amount</td>
            </tr>
            @if(empty($payments))
            @else
                @foreach($payments as $payment)
                    <tr>
                        <td colspan="4">{{ $payment->getBank->name }} - {{ $payment->getBank->address }} </td>    
                        <td colspan="5">Validated Deposit Slip on {{ date('m/d/Y', strtotime($payment->date_received)) }}</td>
                        <td colspan="4">₱&nbsp;{{ empty($payment->amount_received) ? "0.00" : number_format($payment->amount_received, 2) }}</td>
                    </tr>
                @endforeach
            @endif
            <tr>
                <td colspan="9" style="text-align: left;"><b>TOTAL</b></td>
                <td class="" colspan="4">₱&nbsp;{{ empty($total) ? "0.00" : number_format($total, 2) }}</td>
            </tr>
            <tr>
                <th colspan="13" style="text-align: left;"><b>C. ACCOUNTABILITY FOR ACCOUNTABLE FORMS</b></th>
            </tr>
            <tr>
                <td rowspan="3" style="vertical-align: middle;">Name of Form and No.</td>
                <td colspan="3">Beginning Balance</td>
                <td colspan="3">Receipt</td>
                <td colspan="3">Issued</td>
                <td colspan="3">Ending Balance</td>
            </tr>
            <tr>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
            </tr>
            <tr>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
            </tr>
            <tr>
                <td style="text-align: left;"><b>With Money Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Cash Tickets</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left;"><b>Without Money Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Official Receipts</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['first']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['lastdvs']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity'] - $ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':($ORPadsData['lastdvs'] == 0?$ORPad['start']:$ORPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']) }}</td>
            </tr>
            <tr>
                <td>Checks</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['first']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['lastdvs']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity'] - $DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':($DVPadsData['lastdvs'] == 0?$DVPad['start']:$DVPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']) }}</td>
            </tr>
        </thead>
    </table>
    <div class="page-break"></div>
    <table class="table table-striped table-bordered text-center">
        <thead>
            <tr>
                <td colspan="13" style="text-align: left;"><b>D. CERTIFICATION:</b>
                    <div class="row">
                        <p style="margin: 2rem 1rem;"><i>I hereby certify that this Report of Collections and Deposits; and Accountable Forms including supporting documents are tru and correct.</i></p>
                    </div>
                    <table width="100%">
                        <tr>
                            <td width="50%" style="border: none;"><p style="margin-left: 9rem; font-weight: 600;">MICHELLE V. MENIANO</p><p class="bt-po" style="width: 70%; margin-left: 5rem; margin-top: -1rem;"></p><p style="margin-left: 11rem; margin-top: -1px;">Barangay Treasurer</p></td>
                            <td width="50%" style="border: none;"><p style="margin-left: 13rem;">{{ (!empty($year)?$month.'-'.$year:'N/A')}}</p><p class="bt-po" style="margin-left: 10rem; width: 50%; margin-top: -1rem;"></p><p style="margin-left: 16rem; margin-top: -1px;">Date</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="13" style="text-align: left;"><b>E. ACCOUNTING ENTRIES</b></th>
            </tr>
            <tr>
                <td colspan="4">Account Title</td>
                <td colspan="3">Account Code</td>
                <td colspan="3">Debit</td>
                <td colspan="3">Credit</td>
            </tr>
            <tr>
                <td colspan="4"></td>
                <td colspan="3"></td>
                <td colspan="3"></td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td colspan="13">
                    <table width="100%">
                        <tr>
                            <td width="45%" style="border: none; text-align: left;">Prepared by:</td>
                            <td width="50%" style="border: none; text-align: left;">Approved by:</td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="50%" style="border: none;"><p style="margin-left: 2.5rem;">Sample Signatory</p><p class="bt-po" style="width: 70%; margin-left: 5rem; margin-top: -1rem;"></p><p style="margin-left: 3rem; margin-top: -1px;">Barangay Bookkeeper</p></td>
                            <td width="50%" style="border: none;"><p style="margin-left: 2.5rem;">Sample Signatory</p><p class="bt-po" style="margin-left: 5rem; width: 70%; margin-top: -1rem;"></p><p style="margin-left: 3rem; margin-top: -1px;">City/Municipal Accountant</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
    </table>