<p></p>
<table class="table table-striped table-bordered table-hover text-center table-mediascreen" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%; margin-bottom: 1rem;">
    <thead class="thead-dark">
        <tr align="center">
            <th></th>
            <th style="vertical-align: middle;">Original Budget</th>
            <th style="vertical-align: middle;">Final Budget</th>
            <th style="vertical-align: middle;">Difference Original and Final Budget</th>
            <th style="vertical-align: middle;">Actual Amounts</th>
            <th style="vertical-align: middle;">Difference Final and Actual Amounts</th>
        </tr>
      
    </thead>
    <tbody class="nodatatr" name="nodatatr">
        <tr align="center" style="border-style:hidden;">
            <td colspan="15">No data available in table</td>
        </tr>
    </tbody>
    <tbody class="report state-app" name="report">
    </tbody>
</table>
<table width="100%" style="margin-top: 1em">
    <tr>
        <td width="50%" style="border: none;">Prepared by:</td>
        <td width="50%" style="border: none;">Noted by:</td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width="50%" style="border: none;" align="center"><p style="font-weight: 600;">JOVY D. RONIA</p><p class="bt-po" style="width: 70%; margin-top: -1rem;"></p><p style="margin-top: 0;">Barangay Treasurer</p></td>
        <td width="50%" style="border: none;" align="center"><p style="font-weight: 600;">CESAR R. DELA FUENTE, JR.</p><p class="bt-po" style="width: 70%; margin-top: -1rem;"></p><p style="margin-top: 0;">Punong Barangay</p></td>
    </tr>
</table>