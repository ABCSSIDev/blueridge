
<br>
<br>
<br>    

<table class="table table-striped table-bordered table-hover text-center" table style="width:100%; height:100%; border: 1px solid black;">
    <thead class="thead-dark">
        <tr>
            <th class="border-head-custom" colspan="2">CODE</th>
            <th class="border-head-custom" colspan="2">ADDRESS</th> 
            <th class="border-head-custom" colspan="4">NAME</th> 
            <th class="border-head-custom" colspan="3">DATE OF BIRTH</th> 
            <th class="border-head-custom" colspan="9">PERSONAL DETAILS</th> 
        </tr> 
        <tr> 
            <th class="border-head-custom">Cluster No.</th>
            <th class="border-head-custom">Household No.</th>
            <th class="border-head-custom">No.</th>
            <th class="border-head-custom">Street</th>
            <th class="border-head-custom">Last Name</th>
            <th class="border-head-custom">First Name</th>
            <th class="border-head-custom">Middle Name</th>
            <th class="border-head-custom">Quantifier</th>
            <th class="border-head-custom">Month</th>
            <th class="border-head-custom">Day</th>
            <th class="border-head-custom">Year</th>
            <th class="border-head-custom">Place of Birth</th>
            <th class="border-head-custom">Sex</th>
            <th class="border-head-custom">Civil Status</th>
            <th class="border-head-custom">Occupation</th>
            <th class="border-head-custom">Citizenship</th>
            <th class="border-head-custom">Religion</th>
            <th class="border-head-custom">Health Status</th>
            <th class="border-head-custom">Educational Attainment</th>
            <th class="border-head-custom">Contact No.</th>
        </tr>
    </thead>
    <tbody>
        {!! $tbody !!}
    </tbody>
</table>