<p></p>
<table class="table table-striped table-bordered responsive text-center" style="width: 100%; margin-bottom: 1rem;">
    <thead>
        <tr>
            <th class="border-head-custom" rowspan="2" style="vertical-align: middle;">Code</th> 
            <th class="border-head-custom" rowspan="2" style="vertical-align: middle;">Function/Program/Project</th> 
            <th class="border-head-custom" colspan="3" style="vertical-align: middle;">Appropriations</th>
            <th class="border-head-custom" rowspan="2" style="vertical-align: middle;">Obligations</th> 
            <th class="border-head-custom" rowspan="2" style="vertical-align: middle;">Balances</th>
        </tr> 
        <tr>
            <th class="border-head-custom">Original</th>
            <th class="border-head-custom">Supplemental</th>
            <th class="border-head-custom">Final</th>
        </tr>
        <tbody class="nodatatr" name="nodatatr">
            <tr align="center" style="border-style:hidden;">
                <td colspan="15">No Data Available</td>
            </tr>
        </tbody>
        <tbody class="report " name="report">
        </tbody>

        <tr>
            <td class="soa-certified" colspan="7">
                <table width="100%" style="margin-top: -5px;">
                    <tr>
                        <td width="50%" style="border: none;" ><p style="text-align: left;">Certified Correct:</p></td>
                        <td width="50%" style="border: none;"><p style="text-align: left;">Noted by:</p></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td width="50%" style="border: none;" align="center"><p>Anna Francesca L. Maristela</p><p class="bt-po" style="width: 80%; margin-top: -1rem;"></p><p style="margin-top:0px">&nbsp; Chairman, Committee on Appropriations</p></td>
                        <td width="50%" style="border: none;" align="center"><p style="font-weight: 600;">ESPERANZA CASTRO-LEE</p><p class="bt-po" style="width: 80%; margin-top: -1rem;"></p><p style="margin-top:0px">Punong Barangay</p></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td width="50%" style="border: none;margin-right:20px;" align="center"><input readonly style="border: none;background-color: transparent;text-align: center" class="date" id="date" align="center"><br><br><p class="bt-po" style="width: 40%; margin-top: -1rem;"></p><p style="margin-top:0px">Date</p></td>
                        <td width="50%" style="border: none;" align="center"><input readonly style="border: none;background-color: transparent;text-align: center" class="date" id="date"><br><br><p class="bt-po" style="width: 40%; margin-top: -1rem;"></p><p style="margin-top:0px">Date</p></td>
                    </tr>
                </table>
            </td>
        </tr>
    </thead>
</table>