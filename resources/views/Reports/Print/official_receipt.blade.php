@include('layouts.header')
<table width="100%">
    <tr>
        <td width="60%"><p>Name of Barangay Treasurer: MICHELLE V. MENIANO </p></td>
        <td width="40%"><p>Date: {{ (empty($year)?'N/A':date('M Y',strtotime($year.'-01-'.$month)))}}</p></td>
    </tr>
</table>
<table class="table table-striped table-bordered text-center" style="margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>A. Collection</b></th>
            </tr>
            <tr>
                <td class="" colspan="4">Official Receipt/RCR</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Payor</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Nature of Collection</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Amount</td>
            </tr>
            <tr>
                <td colspan="2">Date</td>
                <td colspan="2">Number</td>
            </tr>
            @if(empty($invoices))
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
            @else
                @forelse($invoices as $invoice)
                    <tr>
                        <td colspan="2">{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                        <td colspan="2">{{ $invoice->or_number }}</td>
                        <td colspan="4">{{ $invoice->getPayee->name }}</td>
                        <td colspan="4">{{ $invoice->collection_nature }}</td>
                        <td colspan="4">₱&nbsp;{{ number_format($invoice->getInvoice->unit_price, 2) }}</td>
                    </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>B. DEPOSITS</b></th>
            </tr>
            <tr>
                <td class="" colspan="4">Bank/Branch</td>
                <td class="" colspan="5">Reference</td>
                <td class="" colspan="4">Amount</td>
            </tr>
            @if(empty($payments))
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @else
                @forelse($payments as $payment)
                    <tr>
                        <td colspan="4">{{ $payment->getBank->name }} - {{ $payment->getBank->address }} </td>    
                        <td colspan="5">Validated Deposit Slip on {{ date('m/d/Y', strtotime($payment->date_received)) }}</td>
                        <td colspan="4">₱&nbsp;{{ number_format($payment->amount_received, 2) }}</td>
                    </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <td class="" colspan="9" style="text-align: left;"><b>TOTAL</b></td>
                <td class="" colspan="4">₱&nbsp;{{ empty($total) ? "" : number_format($total, 2) }}</td>
            </tr>
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>C. ACCOUNTABILITY FOR ACCOUNTABLE FORMS</b></th>
            </tr>
            <tr>
                <td class="" rowspan="3" style="vertical-align: middle;">Name&nbsp;of&nbsp;Form&nbsp;and&nbsp;No.</td>
                <td class="" colspan="3">Beginning Balance</td>
                <td class="" colspan="3">Receipt</td>
                <td class="" colspan="3">Issued</td>
                <td class="" colspan="3">Ending Balance</td>
            </tr>
            <tr>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
            </tr>
            <tr>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
            </tr>
            
            <tr>
                <td style="text-align: left;"><b>With&nbsp;Money&nbsp;Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Cash Tickets</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left;"><b>Without&nbsp;Money&nbsp;Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Official Receipts</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']-1) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['first']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['lastdvs']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity'] - $ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':($ORPadsData['lastdvs'] == 0?$ORPad['start']:$ORPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']-1) }}</td>
            </tr>
            <tr>
                <td>Checks</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']-1) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['first']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['lastdvs']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity'] - $DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':($DVPadsData['lastdvs'] == 0?$DVPad['start']:$DVPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']-1) }}</td>
            </tr>
            <tr>
                <td colspan="13">
                    <p style="text-align: left; font-weight: 600;">D. CERTIFICATION:</p>
                    <p><i>I hereby certify that this Report of Collections and Deposits; and Accountable Forms including supporting documents are true and correct</i></p>
                    <table width="100%">
                        <tr>
                            <td width="50%" style="border: none;"><p class="bt-po" style="width: 80%; margin-left: 3rem;">MICHELLE V. MENIANO</p><p style="margin-top: -1px;">Barangay Treasurer</p></td>
                            <td width="50%" style="border: none;"><p class="bt-po" style="margin-left: 7rem; width: 60%;">{{ (empty($year)?'N/A':date('M Y',strtotime(date('Y/m/d H:i:s'))))}}</p><p style="margin-left: 3rem; margin-top: -1px;">Date</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="13"></td>
            </tr>
            <tr>
                <th colspan="13" style="text-align: left;">ACCOUNTING ENTRIES</th>
            </tr>
            <tr>
                <td colspan="5" >Account Title</td>
                <td colspan="3" >Account Code</td>
                <td colspan="3" >Debit</td>
                <td colspan="3" >Credit</td>
            </tr>
            <tr>
                <td colspan="5" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
            </tr>
            <tr>
            <td class="" colspan="13">
                <table width="100%">
                    <tr>
                        <td width="50%" style="border: none;"><p style="text-align: left;">Prepared by:</p></td>
                        <td width="50%" style="border: none;"><p style="text-align: left;">Approved by:</p></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td width="50%" style="border: none;"><p class="bt-po" style="width: 80%; margin-left: 3rem;"></p><p style="margin-top: -1px;">Barangay Bookkeeper</p></td>
                        <td width="50%" style="border: none;"><p class="bt-po" style="margin-left: 7rem; width: 60%;"></p><p style="margin-left: 3rem; margin-top: -1px;">City/Municipal Accountant</p></td>
                    </tr>
                </table>
            </td>
        </tr>
        </thead>
        <tbody></tbody>
    </table>