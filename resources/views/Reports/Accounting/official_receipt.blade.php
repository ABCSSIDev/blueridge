@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Reports of Collections and Deposits</h4>
    <div class="" style="margin-top: 50px;">
        <div align="right" style="margin: 0px 26px 20px 0px;"><a href="{{route('reports.index')}}"><button class="btn btn-secondary mr-n26"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
        <form method ="POST" action="{{ route('search.receipt') }}" id="search_receipt" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="POST" >
            <div class="form-row">
                <div class="col-md-3">
                    <label for="year">Year</span>
                    <input type="text" class="form-control yearPicker" value={{ empty($year) ? "Year" : $year }} placeholder="Year" name="year" id="year">
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <label for="month" class="mb-0">Month</label>
                    <div class="form-group" align="right">                    
                        <select class="form-control" name="month" id="month">
                            <option {{ empty($month) ? "selected" : "" }}>Month</option>
                            <option value="01" {{ !empty($month) ? $month == "01" ? "selected" : "" : "" }}>January</option>
                            <option value="02" {{ !empty($month) ? $month == "02" ? "selected" : "" : "" }}>February</option>
                            <option value="03" {{ !empty($month) ? $month == "03" ? "selected" : "" : "" }}>March</option>
                            <option value="04" {{ !empty($month) ? $month == "04" ? "selected" : "" : "" }}>April</option>
                            <option value="05" {{ !empty($month) ? $month == "05" ? "selected" : "" : "" }}>May</option>
                            <option value="06" {{ !empty($month) ? $month == "06" ? "selected" : "" : "" }}>June</option>
                            <option value="07" {{ !empty($month) ? $month == "07" ? "selected" : "" : "" }}>July</option>
                            <option value="08" {{ !empty($month) ? $month == "08" ? "selected" : "" : "" }}>August</option>
                            <option value="09" {{ !empty($month) ? $month == "09" ? "selected" : "" : "" }}>September</option>
                            <option value="10" {{ !empty($month) ? $month == "10" ? "selected" : "" : "" }}>October</option>
                            <option value="11" {{ !empty($month) ? $month == "11" ? "selected" : "" : "" }}>November</option>
                            <option value="12" {{ !empty($month) ? $month == "12" ? "selected" : "" : "" }}>December</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <div class="form-group" align="left">
                        <button type="submit" class="btn btn-primary mt-4"><span class="fa fa-search"></span> SEARCH</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">

    <table class="table table-striped table-bordered text-center" style="margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>A. Collection</b></th>
            </tr>
            <tr>
                <td class="" colspan="4">Official Receipt/RCR</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Payor</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Nature of Collection</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Amount</td>
            </tr>
            <tr>
                <td colspan="2">Date</td>
                <td colspan="2">Number</td>
            </tr>
            @if(empty($invoices))
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
            @else
                @forelse($invoices as $invoice)
                    <tr>
                        <td colspan="2">{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                        <td colspan="2">{{ $invoice->or_number }}</td>
                        <td colspan="4">{{ $invoice->getPayee->name }}</td>
                        <td colspan="4">{{ $invoice->collection_nature }}</td>
                        <td colspan="4">₱&nbsp;{{ number_format($invoice->getInvoice->unit_price, 2) }}</td>
                    </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>B. DEPOSITS</b></th>
            </tr>
            <tr>
                <td class="" colspan="4">Bank/Branch</td>
                <td class="" colspan="5">Reference</td>
                <td class="" colspan="4">Amount</td>
            </tr>
            @if(empty($payments))
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @else
                @forelse($payments as $payment)
                    <tr>
                        <td colspan="4">{{ $payment->getBank->name }} - {{ $payment->getBank->address }} </td>    
                        <td colspan="5">Validated Deposit Slip on {{ date('m/d/Y', strtotime($payment->date_received)) }}</td>
                        <td colspan="4">₱&nbsp;{{ number_format($payment->amount_received, 2) }}</td>
                    </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <td class="" colspan="9" style="text-align: left;"><b>TOTAL</b></td>
                <td class="" colspan="4">₱&nbsp;{{ empty($total) ? "0.00" : number_format($total, 2) }}</td>
            </tr>
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>C. ACCOUNTABILITY FOR ACCOUNTABLE FORMS</b></th>
            </tr>
            <tr>
                <td class="" rowspan="3" style="vertical-align: middle;">Name&nbsp;of&nbsp;Form&nbsp;and&nbsp;No.</td>
                <td class="" colspan="3">Beginning Balance</td>
                <td class="" colspan="3">Receipt</td>
                <td class="" colspan="3">Issued</td>
                <td class="" colspan="3">Ending Balance</td>
            </tr>
            <tr>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
            </tr>
            <tr>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
            </tr>
            
            <tr>
                <td style="text-align: left;"><b>With&nbsp;Money&nbsp;Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Cash Tickets</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left;"><b>Without&nbsp;Money&nbsp;Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Official Receipts</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']-1) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['first']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['lastdvs']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity'] - $ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':($ORPadsData['lastdvs'] == 0?$ORPad['start']:$ORPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']-1) }}</td>
            </tr>
            <tr>
                <td>Checks</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']-1) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['first']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['lastdvs']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity'] - $DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':($DVPadsData['lastdvs'] == 0?$DVPad['start']:$DVPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']-1) }}</td>
            </tr>
            <tr>
                <td colspan="13">
                    <p style="text-align: left; font-weight: 600;">D. CERTIFICATION:</p>
                    <p><i>I hereby certify that this Report of Collections and Deposits; and Accountable Forms including supporting documents are true and correct</i></p>
                    <div classs="form-row" style="display: -webkit-box; display: flex; flex-wrap: wrap; margin-right: -5px; margin-left: -5px;">
                        <div class="col-md-6">
                            <p style="margin: 0;">MICHELLE V. MENIANO</p>
                            <p class="bt-or" style="width: 65%; left: 6rem;"></p>
                            <p>Barangay Treasurer</p>
                        </div>
                        <div class="col-md-6">
                            <p style="margin: 0;">{{ (empty($year)?'N/A':date('M Y',strtotime(date('Y/m/d H:i:s'))))}}</p>
                            <p class="bt-or" style="width: 65%; left: 6rem;"></p>
                            <p>Date</p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="13"></td>
            </tr>
            <tr>
                <th colspan="13" style="text-align: left;">ACCOUNTING ENTRIES</th>
            </tr>
            <tr>
                <td colspan="5" >Account Title</td>
                <td colspan="3" >Account Code</td>
                <td colspan="3" >Debit</td>
                <td colspan="3" >Credit</td>
            </tr>
            <tr>
                <td colspan="5" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
                <td colspan="3" >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="13">
                    <div classs="form-row" style="display: -webkit-box; display: flex; flex-wrap: wrap; margin-right: -5px; margin-left: -5px;">
                        <div class="col-md-6"><p style="text-align: left;">Prepared by:</p></div>
                        <div class="col-md-6"><p style="text-align: left;">Approved by:</p></div>
                    </div>
                    <div classs="form-row" style="display: -webkit-box; display: flex; flex-wrap: wrap; margin-right: -5px; margin-left: -5px;">
                        <div class="col-md-6">
                            <p style="margin: 0;">&nbsp;</p>
                            <p class="bt-or" style="width: 65%; left: 6rem;"></p>
                            <p>Barangay Bookkeeper</p>
                        </div>
                        <div class="col-md-6">
                            <p style="margin: 0;">&nbsp;</p>
                            <p class="bt-or" style="width: 65%; left: 6rem;"></p>
                            <p>City/Municipal Accountant</p>
                        </div>
                    </div>
                </td>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <div class="form-group" align="right">
        <button class="btn btn-primary print-form-report" data-form="print_receipt"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>
<div id="print_receipt" hidden>
    @include('Reports.Print.official_receipt')
    <style>
        .bt-or {
            border-bottom: 2px solid; 
            position: inherit; 
            bottom: 0; 
            margin-bottom: 0;
        }
        .nbt {
            border-bottom: 2px solid; 
            position: inherit; 
            bottom: 5rem; 
            margin-bottom: 0;
        }
    </style>
</div>

@endsection

<style>
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
</style>
@push('scripts')
<script>
     let rules = {
        year : {
            required: true
        },
        month : {
            required: true
        },
        
    };
    $('#search_receipt').registerFieldReport(rules);  
    $('.yearPicker').datetimepicker({
        format: 'Y'
    });
    $('.monthPicker').datetimepicker({
        format: 'MM' 
    });
</script>
@endpush
