@extends('layouts.app')

@section('content') 

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Accountability Forms Report</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 50px 0px;"><a href="{{ route('reports.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <form method ="POST" action="{{ route('search.functionAccountability') }}" id="search_accountability" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="POST" >
                <div class="form-row">
                    <div class="col-md-3">
                    <input type="text" class="form-control yearPicker" value={{ empty($year) ? "Year" : $year }} placeholder="Year" name="year" id="year">
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group" align="right">
                            <select class="form-control" name="month" id="month">
                                <option {{ empty($month) ? "selected" : "" }}  value="">Month</option>
                                <option value="01" {{ !empty($month) ? $month == "01" ? "selected" : "" : "" }}>January</option>
                                <option value="02" {{ !empty($month) ? $month == "02" ? "selected" : "" : "" }}>February</option>
                                <option value="03" {{ !empty($month) ? $month == "03" ? "selected" : "" : "" }}>March</option>
                                <option value="04" {{ !empty($month) ? $month == "04" ? "selected" : "" : "" }}>April</option>
                                <option value="05" {{ !empty($month) ? $month == "05" ? "selected" : "" : "" }}>May</option>
                                <option value="06" {{ !empty($month) ? $month == "06" ? "selected" : "" : "" }}>June</option>
                                <option value="07" {{ !empty($month) ? $month == "07" ? "selected" : "" : "" }}>July</option>
                                <option value="08" {{ !empty($month) ? $month == "08" ? "selected" : "" : "" }}>August</option>
                                <option value="09" {{ !empty($month) ? $month == "09" ? "selected" : "" : "" }}>September</option>
                                <option value="10" {{ !empty($month) ? $month == "10" ? "selected" : "" : "" }}>October</option>
                                <option value="11" {{ !empty($month) ? $month == "11" ? "selected" : "" : "" }}>November</option>
                                <option value="12" {{ !empty($month) ? $month == "12" ? "selected" : "" : "" }}>December</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="form-group" align="left">
                            <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span> SEARCH</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div style="overflow: auto; margin-bottom: 1rem;">
        <table class="table table-striped table-bordered table-hover text-center">
            <thead class="thead-dark">
                <tr>
                    <th rowspan="3" style="vertical-align: middle; width: 20%">Name&nbsp;of&nbsp;Form&nbsp;and&nbsp;No.</th>
                    <th colspan="3" style="width: 20%">Beginning Balance</th>
                    <th colspan="3" style="width: 20%">Receipt</th>
                    <th colspan="3" style="width: 20%">Issued</th>
                    <th colspan="3" style="width: 20%">Ending Balance</th>
                </tr>
                <tr>
                    <th rowspan="2" style="vertical-align: middle;">Qty</th>    
                    <th colspan="2">Inclusive Serial No.</th>
                    <th rowspan="2" style="vertical-align: middle;">Qty</th>    
                    <th colspan="2">Inclusive Serial No.</th>
                    <th rowspan="2" style="vertical-align: middle;">Qty</th>    
                    <th colspan="2">Inclusive Serial No.</th>
                    <th rowspan="2" style="vertical-align: middle;">Qty</th>    
                    <th colspan="2">Inclusive Serial No.</th>
                </tr>
                <tr>
                    <th>From</th>
                    <th>To</th>
                    <th>From</th>
                    <th>To</th>
                    <th>From</th>
                    <th>To</th>
                    <th>From</th>
                    <th>To</th>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>With&nbsp;Money&nbsp;Value:</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cash&nbsp;Tickets</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Without&nbsp;Money&nbsp;Value:</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                
                    <td>Official Receipts</td>
                    <td>{{ (empty($ORPad)?'':$ORPad['quantity']) }}</td>
                    <td>{{ (empty($ORPad)?'':$ORPad['start']) }}</td>
                    <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']) }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ (empty($ORPadsData)?'':$ORPadsData['quantity']) }}</td>
                    <td>{{ (empty($ORPadsData)?'':$ORPadsData['first']) }}</td>
                    <td>{{ (empty($ORPadsData)?'':$ORPadsData['lastdvs']) }}</td>
                    <td>{{ (empty($ORPad)?'':$ORPad['quantity'] - $ORPadsData['quantity']) }}</td>
                    <td>{{ (empty($ORPad)?'':($ORPadsData['lastdvs'] == 0?$ORPad['start']:$ORPadsData['lastdvs'] + 1)) }}</td>
                    <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']) }}</td>
                </tr>
                <tr>
                    <td>Checks</td>
                    <td>{{ (empty($DVPad)?'':$DVPad['quantity']) }}</td>
                    <td>{{ (empty($DVPad)?'':$DVPad['start']) }}</td>
                    <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']) }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ (empty($DVPadsData)?'':$DVPadsData['quantity']) }}</td>
                    <td>{{ (empty($DVPadsData)?'':$DVPadsData['first']) }}</td>
                    <td>{{ (empty($DVPadsData)?'':$DVPadsData['lastdvs']) }}</td>
                    <td>{{ (empty($DVPad)?'':$DVPad['quantity'] - $DVPadsData['quantity']) }}</td>
                    <td>{{ (empty($DVPad)?'':($DVPadsData['lastdvs'] == 0?$DVPad['start']:$DVPadsData['lastdvs'] + 1)) }}</td>
                    <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']) }}</td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="form-group" align="right" >
        <button class="btn btn-primary print-form-report" data-form="print_accountable"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>
<div id="print_accountable" hidden>
    @include('layouts.header')
    @include('Reports.Print.accountability_form')
    <style>
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
    </style>
</div>
@endsection
@push('scripts')

<script>
    let rules = {
        year : {
            required: true
        },
        month : {
            required: true
        },
        
    };
    $('#search_accountability').registerFieldReport(rules);  
    $('#year').datetimepicker({
        format: 'Y'
    });
    $('.yearPicker').datetimepicker({
        format: 'Y'
    });
    $('.monthPicker').datetimepicker({
        format: 'MM' 
    });
</script>

@endpush