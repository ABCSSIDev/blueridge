@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Residents Report</h4>
    <div class="" style="margin-top: 50px;">
        <div align="right" style="margin: 0px 26px 20px 0px;"><a href="{{route('reports.index')}}"><button class="btn btn-secondary mr-n26"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
        <form method ="POST" action="{{ route('search.transmittal') }}" id="search_transmittal" enctype="multipart/form-data">
        <div style="width: 100%;overflow:auto"> 
            <table class="table table-striped table-bordered table-hover text-center" style="margin-bottom: 1rem;">
                <thead class="thead-dark">
                    <tr>
                        <th class="border-head-custom" colspan="2">CODE</th>
                        <th class="border-head-custom" colspan="2">ADDRESS</th> 
                        <th class="border-head-custom" colspan="4">NAME</th> 
                        <th class="border-head-custom" colspan="3">DATE OF BIRTH</th> 
                        <th class="border-head-custom" colspan="9">PERSONAL DETAILS</th> 
                    </tr> 
                    <tr> 
                        <th class="border-head-custom">Cluster No.</th>
                        <th class="border-head-custom">Household No.</th>
                        <th class="border-head-custom">No.</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Street</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Last Name</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">First Name</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Middle Name</th>
                        <th class="border-head-custom">Quantifier</th>
                        <th class="border-head-custom">Month</th>
                        <th class="border-head-custom">Day</th>
                        <th class="border-head-custom">Year</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Place of Birth</th>
                        <th class="border-head-custom">Sex</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Civil Status</th>
                        <th class="border-head-custom">Occupation</th>
                        <th class="border-head-custom">Citizenship</th>
                        <th class="border-head-custom">Religion</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Health Status</th>
                        <th class="border-head-custom" style="padding-right: 50;padding-left: 50;">Educational Attainment</th>
                        <th class="border-head-custom">Contact No.</th>
                    </tr>
                </thead>
                <tbody>
                    {!! $tbody !!}
                </tbody>
            </table>
        </div>
        </form>
    </div>
</div>


<div id="print_rbi" hidden>
    @include('layouts.header')
    @include('Reports.Print.rbi')
</div>
<div class="col-md-12">
    <div align="right" >
        <button class="btn btn-primary print-form-css" data-form="print_rbi"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>
@endsection
<style>
    body, html{ width:100%; height:100%; margin:0; padding:0;}
</style>
<style>
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
    .break-page{
        page-break-after: always;
    }
    .footer {
        page-break-after: always;
    }
</style>
@push('scripts')
<script>
let rules = {}
    $('.yearPicker').datetimepicker({
        format: 'Y'
    });
    $('.monthPicker').datetimepicker({
        format: 'MM' 
    });

    </script>
@endpush
