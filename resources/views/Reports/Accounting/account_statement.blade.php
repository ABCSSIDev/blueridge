@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item" >Reports</li>
                <li class="breadcrumb-item active" aria-current="page">Account Statement</li>
            </ol>
        </nav> -->
        <h4 class="form-head" style="margin-top:40px">Account Statement {{ ($bank?'for ':'').($bank?$bank->name:'') }}</h4>
        <form method ="POST" action="{{ route('search.account_statement') }}" id="search_account_statement" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="POST" >
            <div class="row justify-content-center">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="">Bank</label>
                        <select class="form-control" name="bank" id="bank"> 
                            <option value="0">Select Bank</option>
                            @foreach($BankOption as $val)
                                <option value="{{ $val->id }}"{{ ($bank?$val->id == $bank->id ?'selected':'':'') }}>{{ $val->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="">From</label>
                        <input type="text" class="form-control year"  id="from" name="from" autocomplete="false" placeholder="From" value="{{ $from }}">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="">To</label>
                        <input type="text" class="form-control year"  id="to" name="to" autocomplete="false" placeholder="To" value="{{ $to }}">
                    </div>
                </div>
                <div class="col-sm-3" style="margin-top: 6px;">
                    <div class="form-group">
                        <button class="btn btn-primary mt-4"><span class="fa fa-search"></span> SEARCH</button>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <table class="table  table-bordered table-hover table-sm table-mediascreen" >
                    <thead class="thead-dark" align="center">
                    <tr>
                        <th scope="col">Type</th>
                        <th scope="col">Date</th>
                        <th scope="col">Description</th>
                        <th scope="col" width="10%">Spent</th>
                        <th scope="col" width="10%">Received</th>
                        <th scope="col" width="10%">Ending Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                        {!! $tbody !!}
                        <!-- <tr align="center">
                            <td colspan="15">No Data Available</td>
                        </tr> -->
                    </tbody>
                </table>

            </div>
        </form>
    </div>
@endsection
@push('scripts')

<script>
    let rules = {
        year : {
            required: true
        },
        from : {
            required: true
        },
        to : {
            required: true
        },
        
    };
    $('#search_account_statement').registerFieldReport(rules);  
    $('.year').datetimepicker({
        format: 'YYYY-MM-DD'
    });
</script>

@endpush