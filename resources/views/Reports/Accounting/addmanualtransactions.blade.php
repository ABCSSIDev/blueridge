

<div class="container">
    <form method="POST" id="add_manual_transactions_form" onSubmit="registerFields(this)" class="form-horizontal" action="{{ route('manual_transactions.create') }}" enctype="multipart/form-data">
        <div class="errors"></div>
            {{csrf_field()}}
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Transactions Type</label>
                            <select class="form-control " name="type" id="type" onchange="changeType(this)">
                                <option value="">Select Transactions</option>
                                <option value="0">Expenses</option>
                                <option value="1">Income</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Amount</label>
                            <input type="number" class="form-control price" id="amount" name="amount" placeholder="Amount" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Ledgers</label>
                            <select class="form-control" id="ledgers" name="ledgers">
                                <option value="">Select Ledgers</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Payment Methods</label>
                            <select class="form-control" id="payment_methods" name="payment_methods">
                                <option value="">Select Methods</option>
                                @foreach($payment_methods as $method)
                                    <option value="{{ $method->id }}">{{ $method->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Description</label>
                            <textarea  type="text" class="form-control" rows="4" cols="50" id="description" name="description" placeholder="Description" autocomplete="off"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" align="right" >
                <button type="close" class="btn btn-primary" id="closemodal" data-dismiss="modal" aria-label="Close"><span class=""></span> CLOSE</button>
                <button type="submit" class="btn btn-primary"><span class="fa fa-floppy-o"></span> SUBMIT</button>
            </div> 
    </form>
</div>
<script>
let rules = {
            amount: {
                required: true
            },
            type : {
                required: true
            },
            ledgers : {
                required: true
            },
            payment_methods : {
                required: true
            },
        };
        $('#add_manual_transactions_form').registerFields(rules); 
  
(function($) {
    //general function for validation and submit of forms
    $.fn.registerFields = function (rules, resets=true) {
        let $form_id = $(this);
        $($form_id).validate({
            rules: rules,
            errorPlacement: function(error,element){//custom function for special cases of the error messages
                let placement = $(element).data('error');
                console.log(error[0])
                let html = '<label class="error">Highlighted fields are required.</label>'
                if(placement){
                    $(placement).html(html)
                }else{
                    error.insertAfter(element);
                }
            },
            highlight: function (element) {
                $(element).parent().addClass('validate-has-error');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('validate-has-error');
            },
            submitHandler: function (event) {
                let $formData = new FormData($form_id[0]);
                $.ajax({
                    type: 'POST',
                    url: $form_id.attr('action'),
                    data: $formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        //add pre load animation
                    },
                    error: function (data) {
                        //error
                    },
                    success: function (data) {
                        if (data.status === "success"){
                            if (data.reload == true) {
                                window.location.reload();
                            }
                            if (data.href == true){
                                window.location.href = data.route;
                            }
                            if(resets){
                                $form_id.each(function(){
                                    this.reset(); //resets the form values
                                    // empties elements not included as form fields
                                    var revert_selector = "[revert]";
                                    $(revert_selector).each(function(){$(this).text("").val("")})
                                    if($form_id.attr('id') === "budget_form"){
                                        $('.compute-total').each(function(){ $(this).attr('remove', false).text("Compute Total"); });
                                        $(revert_selector).each(function(){$(this).attr('remove', false)});
                                    }
                                    $("#supplier_id").prop('disabled', true);
                                    $(".selectpicker").val('default');
                                    $(".selectpicker").selectpicker("refresh");
                                });
                            }
                            toastMessage('Success',data.desc,data.status);
                        } else{
                            toastMessage('Ooops!',data.desc,data.status);
                        }
                    },
                });
                return false;
            },
            invalidHandler: function(event, validator){
                toastMessage('Oopps!','It seems like you missed some required fields',"error");
            }
        });
    };

}(jQuery));
</script>