
@extends('layouts.app')

@section('content')
<div class="container pb-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Record of Estimated and Actual Income</h4>
    <div class="row justify-content-center">
        <div class="col-md-12"> 
        <div align="right" style="margin: 0px -26px 50px 0px"><a href="{{route('reports.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
               
                <div class="tab-content">
                
                        <div class="form-row">
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="year" name="year" autocomplete="false" placeholder="Year">
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" name="btn_search" id="btn_search"><i class="fa fa-search" aria-hidden="true"></i> SEARCH</button>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
       
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover table-mediascreen" style="margin-top: 50px">
                <thead class="thead-dark">
                    <tr align="center">
                    <th rowspan="2" width="15%">Date</th>
                    <th rowspan="2" width="10%">Particulars</th>
                    <th rowspan="2" width="10%">Reference No.</th>
                    <th colspan="6" width="60%">Income Accounts</th>
                    <th rowspan="2" width="10%">Total</th>
                    </tr>
                    <tr align="center">
                    <th width="10%">RPT</th>
                    <th width="10%">Business Tax</th>
                    <th width="10%">Community Tax</th>
                    <th width="10%">Share from IRA</th>
                    <th width="10%">Other Service Income</th>
                    <th width="10%">Grants and Donations in Cash</th>
                    </tr>
                </thead>
              
                <tbody class="report state-app" name="report">
                    <tr align="center" style="border-style:hidden;">
                        <td colspan="15">No data available in table</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div align="right" >
                <button class="btn btn-primary print-form-css mr-n4" data-form="print_estimated_actual_income"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
            </div>
        </div>
    </div>

    <div id="print_estimated_actual_income" hidden>
        @include('Reports.Print.estimated_actual_income')
    </div>
</div>


@endsection

@push('scripts')
    <script>
    let rules = {
        year : {
            required: true
        },
    };
    $('#search_income').registerFieldReport(rules);  
        $('#year').datetimepicker({
            format: 'Y'
        });
        $('#btn_search').click(function () {
            if($('#year').val() != ''){
                $.ajax({
                    url: "{{route('search.income')}}",
                    data: {
                        year : $('#year').val(),
                        month : $('#month').val()
                    },
                    method: "GET",
                    success: function(data){
                        if(data != ''){
                            $('.nodatatr').hide(); 
                            $('.report').empty(); 
                            $(".report").html(data); 
                        }else{ 
                            $('.report').empty(); 
                            $('.nodatatr').show(); 
                        }
                    }
                    
                    
                });
            }else{
                    toastMessage('Warning!',"It seems like you missed some required fields","warning");
            }
    });
    </script>

@endpush

<style>
    th,td{
        vertical-align: middle !important;
        
    }
</style>