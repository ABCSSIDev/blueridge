@extends('layouts.app')

@section('content')

<div class="container pb-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Cashbook Report</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 50px 0px"><a href="{{route('reports.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="form-row">
                    <div class="col-md-3">
                        <input type="text" class="form-control"  id="year" name="year" autocomplete="false" placeholder="Year">
                    </div>
                    <div class="col-md-3">
                        <div name="quarterly" id="quarterly">
                            <select class="form-control selectpicker" name="quarterlys[]" id="quarterlys" title="Select Quarterly" data-size="10" data-live-search="true" multiple> 
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <select class="form-control" name="bank" id="bank" title="Select Bank"> 
                            <option value="">Select Bank</option>
                            @foreach($Bank as $val)
                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endforeach

                        </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="btn_search" id="btn_search"><i class="fa fa-search" aria-hidden="true"></i> SEARCH</button>
                        </div>
                    </div>
               
                </div>
            </div>
        </div>
        <div style="overflow: auto">
            <div class="col-md-12">
                <table class="table table-striped table-bordered table-hover text-center table-responsive" style="margin-top: 50px">
                    <thead class="thead-dark">
                        <tr align="center">
                            <th rowspan="2">Date</th>
                            <th rowspan="2">Particulars</th>
                            <th rowspan="2">Reference</th>
                            <th colspan="3">Cash in Local Treasury</th>
                            <th colspan="3">Cash in Bank</th>
                            <th colspan="3">Cash Advances</th>
                            <th colspan="3">Pettycash</th>
                        </tr>
                        <tr>
                            <th >Collection</th>
                            <th >Deposit</th>
                            <th >Balance</th>
                            <th >Deposit</th>
                            <th >Check Issued</th>
                            <th >Balance</th>
                            <th >Receipt</th>
                            <th >Disbursements</th>
                            <th >Balance</th>
                            <th >Receipts/Replenishment</th>
                            <th >Payments</th>
                            <th >Balance</th>
                        </tr>
                    </thead>
                    <tbody class="nodatatr" name="nodatatr">
                        <tr align="center" style="border-style:hidden;">
                            <td colspan="15">No Data Available</td>
                        </tr>
                    </tbody>
                    <tbody class="report" name="report">
                    </tbody>
                </table>
       
    </div> 
   
    <div id="print_cashbook" hidden>
        @include('layouts.header')
        @include('Reports.Print.cashbook')
    </div>
</div>
<div class="col-md-12">
    <div align="right" >
        <button class="btn btn-primary print-form-css" data-form="print_cashbook"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>
@endsection
<style>
    td{
        vertical-align: middle !important;
    }
</style>


@push('scripts')
    <script>
          $('#year').datetimepicker({
            format: 'Y'
        });
        $('#quarterlys').change(function(){
            let fruits = ['January', 'February','March', 'April','May', 'June','July', 'August', 'September','October', 'November', 'December']
            let val = fruits[$(this).val() - 1] +' '+$('#year').val();
            $('#dateprint').val(val);
        });
        $('#btn_search').click(function () {
            if($('#year').val() != '' && $('#bank').val() != '' && $('#quarterlys').val() != ''){
                $.ajax({
                    url: "{{route('cashbook.search')}}",
                    data: {
                        year : $('#year').val(),
                        bank : $('#bank').val(),
                        quarterly : $('#quarterlys').val()
                    },
                    method: "GET",
                    success: function(data){
                        if(data != ''){
                            $('.nodatatr').hide(); 
                            $('.report').empty(); 
                            $(".report").append(data);
                        }else{ 
                            $('.report').empty(); 
                            $('.nodatatr').show(); 
                        }
                    }
                    
                    
                });
            }else{
                    toastMessage('Warning!',"It seems like you missed some required fields","warning");
            }
        });
        $('#quarterlys').change(function(){
            let fruits = ['January', 'February','March', 'April','May', 'June','July', 'August', 'September','October', 'November', 'December']
            let val = fruits[$(this).val() - 1] +' '+$('#year').val();
            console.log(val);
            $('.mon').val(val);
        });
    </script>
@endpush
