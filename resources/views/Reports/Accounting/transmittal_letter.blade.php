@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Transmittal Letter</h4>
    <div class="" style="margin-top: 50px;">
        <div align="right" style="margin: 0px 26px 20px 0px;"><a href="{{route('reports.index')}}"><button class="btn btn-secondary mr-n26"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
        <form method ="POST" action="{{ route('search.transmittal') }}" id="search_transmittal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="POST" >
            <div class="form-row">
                <div class="col-md-3">
                    <input type="text" class="form-control yearPicker" value={{ empty($year) ? "Year" : $year }} placeholder="Year" name="year" id="year">
                </div>
                <div class="col-md-3">
                    <select class="form-control" name="collection_nature" id="collection_nature">
                        <option value="">Select Nature of Collection</option>
                        @foreach($collection_nature as $data)
                            <option value="{{ $data->collection_nature }}" {{ $data->collection_nature  == $collectionval ? "selected" : '' }}>{{ $data->collection_nature }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group" align="right">                    
                        <select class="form-control" name="month" id="month">
                            <option {{ empty($month) ? "selected" : "" }} value="">Month</option>
                            <option value="01" {{ !empty($month) ? $month == "01" ? "selected" : "" : "" }}>January</option>
                            <option value="02" {{ !empty($month) ? $month == "02" ? "selected" : "" : "" }}>February</option>
                            <option value="03" {{ !empty($month) ? $month == "03" ? "selected" : "" : "" }}>March</option>
                            <option value="04" {{ !empty($month) ? $month == "04" ? "selected" : "" : "" }}>April</option>
                            <option value="05" {{ !empty($month) ? $month == "05" ? "selected" : "" : "" }}>May</option>
                            <option value="06" {{ !empty($month) ? $month == "06" ? "selected" : "" : "" }}>June</option>
                            <option value="07" {{ !empty($month) ? $month == "07" ? "selected" : "" : "" }}>July</option>
                            <option value="08" {{ !empty($month) ? $month == "08" ? "selected" : "" : "" }}>August</option>
                            <option value="09" {{ !empty($month) ? $month == "09" ? "selected" : "" : "" }}>September</option>
                            <option value="10" {{ !empty($month) ? $month == "10" ? "selected" : "" : "" }}>October</option>
                            <option value="11" {{ !empty($month) ? $month == "11" ? "selected" : "" : "" }}>November</option>
                            <option value="12" {{ !empty($month) ? $month == "12" ? "selected" : "" : "" }}>December</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group" align="left">
                        <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span> SEARCH</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="container">
    <table class="table table-striped table-bordered text-center" style="margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th colspan="2" style="text-align: left;">A. DV/Payroll</th>
                <th colspan="2">Check</th>
                <th rowspan="2" style="vertical-align: middle;">Payee</th>
                <th rowspan="2" style="vertical-align: middle;">Amount</th>
                <th colspan="2">PB Certification</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>No.</th>
                <th>Date</th>
                <th>No.</th>
                <th>Date</th>
                <th>No.</th>
            </tr>
            @if(empty($DisbursementVoucher))
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
            @else
                @forelse($DisbursementVoucher as $data)
                        <tr>
                            <td>{{ date('m/d/Y', strtotime($data->created_at)) }}</td>
                            <td>{{ App\Common::getPRNumberFormat('roas', $data->getROA) }}</td>
                            <td>{{ date('m/d/Y', strtotime($data->created_at)) }}</td>
                            <td>{{ $data->check_number }}</td>
                            <td>{{ ($data->or_id == null?'N/A':$data->getInvoice->getPayee->name) }}</td>
                            <td>₱&nbsp;{{ $data->getROA->total_amount }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <th colspan="8" style="text-align: left;">B. RCDs Supporting Documents (RCRs, Ors and VDS)</th>
            </tr>
            <tr>
                <td colspan="3">Date</td>
                <td colspan="3">No.</td>
                <td colspan="3">Amount</td>
            </tr>
            @if(!empty($RCDSuppDoc))
                <tr>
                    <td colspan="3">{{ $RCDSuppDoc['Date'] }}</td>
                    <td colspan="3">{{ $RCDSuppDoc['rangepad'] }}</td>
                    <td colspan="3">₱&nbsp;{{ $RCDSuppDoc['total'] }}</td>
                </tr>
            @else
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @endif
            <tr>
                <th colspan="8" style="text-align: left;">C. Other Reports</th>
            </tr>
            <tr>
                <td colspan="3">Date</td>
                <td colspan="5">Type of Report</td>
            </tr>
            @if(!empty($OtherReports))
                <tr>
                    <td colspan="3">{{ date('M Y',strtotime($year.'-'.$month))}}</td>
                    <td colspan="5">Report of Collection and Deposits</td>
                </tr>
                <tr>
                    <td colspan="3">{{ date('M Y',strtotime($year.'-'.$month))}}</td>
                    <td colspan="5">Report of Accountability for Accountable Forms</td>
                </tr>
                <tr>
                    <td colspan="3">{{ date('M Y',strtotime($year.'-'.$month))}}</td>
                    <td colspan="5">Deposits Slip</td>
                </tr>
            @else
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @endif
        </thead>
    </table>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <!-- <div class="form-group">
                <p>Name of Barangay Treasurer: </p>
            </div> -->
        </div>
        <div class="col-md-6">
            <!-- <div class="form-group">
                <p>Date: </p>
            </div> -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- <div class="form-group">
                <p>Barangay: </p>
            </div> -->
        </div>
        <div class="col-md-6">
            <!-- <div class="form-group">
                <p>RCD No.: </p>
            </div> -->
        </div>
    </div>
    <table class="table table-striped table-bordered text-center" style="margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>A. Collection</b></th>
            </tr>
            <tr>
                <td class="" colspan="4">Official Receipt/RCR</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Payor</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Nature of Collection</td>
                <td class="" colspan="4" rowspan="2" style="vertical-align: middle;">Amount</td>
            </tr>
            <tr>
                <td colspan="2">Date</td>
                <td colspan="2">Number</td>
            </tr>
            @if(empty($invoices))
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
            @else
                @forelse($invoices as $invoice)
                    <tr>
                        <td colspan="2">{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                        <td colspan="2">{{ $invoice->or_number }}</td>
                        <td colspan="4">{{ $invoice->getPayee->name }}</td>
                        <td colspan="4">{{ $invoice->collection_nature }}</td>
                        <td colspan="4">₱&nbsp;{{ number_format($invoice->getInvoice->unit_price, 2) }}</td>
                    </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>B. DEPOSITS</b></th>
            </tr>
            <tr>
                <td class="" colspan="4">Bank/Branch</td>
                <td class="" colspan="5">Reference</td>
                <td class="" colspan="4">Amount</td>
            </tr>
            @if(empty($payments))
                <tr align="center">
                    <td colspan="15">No data available in table</td>
                </tr>
            @else
                @forelse($payments as $payment)
                    <tr>
                        <td colspan="4">{{ $payment->getBank->name }} - {{ $payment->getBank->address }} </td>    
                        <td colspan="5">Validated Deposit Slip on {{ date('m/d/Y', strtotime($payment->date_received)) }}</td>
                        <td colspan="4">₱&nbsp;{{ empty($payment->amount_received) ? "0.00" : number_format($payment->amount_received, 2) }}</td>
                    </tr>
                @empty
                    <tr align="center">
                        <td colspan="15">No data available in table</td>
                    </tr>
                @endforelse
            @endif
            <tr>
                <td class="" colspan="9" style="text-align: left;"><b>TOTAL</b></td>
                <td class="" colspan="4">₱&nbsp;{{ empty($total) ? "0.00" : number_format($total, 2) }}</td>
            </tr>
            <tr>
                <th class="" colspan="13" style="text-align: left;"><b>C. ACCOUNTABILITY FOR ACCOUNTABLE FORMS</b></th>
            </tr>
            <tr>
                <td class="" rowspan="3" style="vertical-align: middle;">Name&nbsp;of&nbsp;Form&nbsp;and&nbsp;No.</td>
                <td class="" colspan="3">Beginning Balance</td>
                <td class="" colspan="3">Receipt</td>
                <td class="" colspan="3">Issued</td>
                <td class="" colspan="3">Ending Balance</td>
            </tr>
            <tr>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
                <td rowspan="2" style="vertical-align: middle;">Qty</td>    
                <td colspan="2">Inclusive Serial No.</td>
            </tr>
            <tr>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
                <td>From</td>
                <td>To</td>
            </tr>
            
            <tr>
                <td style="text-align: left;"><b>With&nbsp;Money&nbsp;Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Cash Tickets</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left;"><b>Without&nbsp;Money&nbsp;Value:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Official Receipts</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']-1) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['first']) }}</td>
                <td>{{ (empty($ORPadsData)?'':$ORPadsData['lastdvs']) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['quantity'] - $ORPadsData['quantity']) }}</td>
                <td>{{ (empty($ORPad)?'':($ORPadsData['lastdvs'] == 0?$ORPad['start']:$ORPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($ORPad)?'':$ORPad['start']+$ORPad['quantity']-1) }}</td>
            </tr>
            <tr>
                <td>Checks</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']-1) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['first']) }}</td>
                <td>{{ (empty($DVPadsData)?'':$DVPadsData['lastdvs']) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['quantity'] - $DVPadsData['quantity']) }}</td>
                <td>{{ (empty($DVPad)?'':($DVPadsData['lastdvs'] == 0?$DVPad['start']:$DVPadsData['lastdvs'] + 1)) }}</td>
                <td>{{ (empty($DVPad)?'':$DVPad['start']+$DVPad['quantity']-1) }}</td>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <div class="form-group" align="right">
        <button class="btn btn-primary print-form-report" data-form="print_transmittal"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>
<div id="print_transmittal" hidden>
    @include('Reports.Print.transmittal_letter')
    <style>
        .bt-po {
            border-bottom: 2px solid; 
            position: inherit; 
            bottom: 28px; 
            margin-bottom: 0;
        }
    </style>
</div>

@endsection

<style>
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
    .break-page{
        page-break-after: always;
    }
    .footer {
        page-break-after: always;
    }
</style>
@push('scripts')
<script>
    let rules = {
        year : {
            required: true
        },
        month : {
            required: true
        },
    };
    $('#search_transmittal').registerFieldReport(rules);  
    $('.yearPicker').datetimepicker({
        format: 'Y'
    });
    $('.monthPicker').datetimepicker({
        format: 'MM' 
    });
    
    </script>
@endpush
