@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Statement of Comparison of Budget and Actual Amounts Report</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 50px 0px;"><a href="{{ route('reports.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
          
                <div class="form-row">
                    <div class="col-md-3">
                        <input type="text" class="form-control"  id="year" name="year" autocomplete="false" placeholder="Year">
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group" align="right">
                            <select class="form-control" name="month" id="month">
                                <option value="">Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="form-group" align="left">
                            <button type="submit" class="btn btn-primary" name="btn_search" id="btn_search"><span class="fa fa-search"></span> SEARCH</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <table class="table table-striped table-bordered table-hover text-center table-mediascreen" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%; margin-bottom: 1rem;">
        <thead class="thead-dark">
            <tr>
                <th></th>
                <th class="v-align">Original Budget</th>
                <th class="v-align">Final Budget</th>
                <th class="v-align">Difference Original and Final Budget</th>
                <th class="v-align">Actual Amounts</th>
                <th class="v-align">Difference Final and Actual Amounts</th>
            </tr>
        </thead>
        <tbody class="nodatatr" name="nodatatr">
            <tr align="center" style="border-style:hidden;">
                <td colspan="15">No data available in table</td>
            </tr>
        </tbody>
        <tbody class="report state-app" name="report">
        </tbody>
    </table>
    <div class="form-group" align="right" >
        <button class="btn btn-primary print-form-report" data-form="print_income"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>

<div id="print_income" hidden>
    @include('layouts.header')
    @include('Reports.Print.comparison_statement')
</div>
@endsection

<style>
    .revenue-indent {
        text-align: left;
        text-indent: 1rem;
    }
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
</style>
@push('scripts')

<script>
   
    $('#year').datetimepicker({
        format: 'Y'
    });
    $('#btn_search').click(function () {
        if($('#year').val() != null && $('#month').val()){
            $.ajax({
                url: "{{route('comparison_statement.search')}}",
                data: {
                    year : $('#year').val(),
                    month : $('#month').val()
                },
                method: "GET",
                success: function(data){
                    if(data != ''){
                        $('.nodatatr').hide(); 
                        $('.report').empty(); 
                        $(".report").html(data); 
                    }else{ 
                        $('.report').empty(); 
                        $('.nodatatr').show(); 
                    }
                }
                
                
            });
        }else{
                toastMessage('Warning!',"It seems like you missed some required fields","warning");
        }
    });
</script>

@endpush