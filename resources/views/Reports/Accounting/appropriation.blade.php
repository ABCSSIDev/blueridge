@extends('layouts.app')

@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Statement of Appropriations Obligations & Balances Report</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 50px 0px;"><a href="{{route('reports.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            
                <div class="form-row">
                    <div class="col-md-3">
                        <input type="text" class="form-control"  id="year" name="year" autocomplete="false" placeholder="Year">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" align="right">
                            <button type="submit" class="btn btn-primary" name="btn_search" id="btn_search"><span class="fa fa-search"></span> SEARCH</button>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table-sm responsive text-center table-striped table-bordered table-black-border" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%; margin-bottom: 1rem;">
                <thead class="thead-dark">
                    <tr>
                        <th class="border-head-custom" rowspan="2">Code</th> 
                        <th class="border-head-custom" rowspan="2">Function/Program/Project</th> 
                        <th class="border-head-custom" colspan="3">Appropriations</th>
                        <th class="border-head-custom" rowspan="2">Obligations</th> 
                        <th class="border-head-custom" rowspan="2">Balances</th>
                    </tr> 
                    <tr>
                        <th class="border-head-custom">Original</th>
                        <th class="border-head-custom">Supplemental</th>
                        <th class="border-head-custom">Final</th>
                    </tr>
              
                </thead>
                <tbody class="nodatatr" name="nodatatr">
                    <tr align="center" style="border-style:hidden;">
                        <td colspan="15">No data available in table</td>
                    </tr>
                </tbody>
                <tbody class="report state-app" name="report">
                </tbody>
            </table>
        </div>
    </div>
    <div class="form-group" align="right" >
        <button class="btn btn-primary print-form-report" data-form="print_appropriation"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    </div>
</div>

<div id="print_appropriation" hidden>
    @include('layouts.header')
    @include('Reports.Print.appropriation')
</div>

@endsection

<style>
    .table-black-border {
        border: 3px solid #0f0f10! important;
    }
    .border-head-custom {
        border-left-width: initial! important;
        border-color: black! important;
        border-right-width: initial! important;
        background-color: #343a40;
        color: white;
    }
    .align-left {
        text-align: left;
    }
    .soa-p {
        left: 45px;
        position: relative;
        margin-top: 1rem;
    }
    .bbc-soa {
        border-bottom-color: black!important;
        border-left: hidden!important;
    }
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
    
</style>
@push('scripts')

<script>
  
    $('#year').datetimepicker({
        format: 'Y'
    });
    $('#btn_search').click(function () {
        if($('#year').val() != ''){
            $.ajax({
                url: "{{route('obligations_balances.search')}}",
                data: {
                    year : $('#year').val()
                },
                method: "GET",
                success: function(data){
                    if(data != ''){
                        $('.nodatatr').hide(); 
                        $('.report').empty(); 
                        $(".report").append(data); 
                        $('.date').val($('#year').val());
                    }else{ 
                        $('.report').empty(); 
                        $('.nodatatr').show(); 
                    }
                }
                
                
            });
        }else{
                toastMessage('Warning!',"It seems like you missed some required fields","warning");
        }
    });
</script>

@endpush