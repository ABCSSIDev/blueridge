@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Accounting</li>
        </ol>
    </nav>
    <h4 class="form-head">Registration of Obligation Personal Services,<br>MOOE and Capital Outlay Expenses Classification Report</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 50px 0px;"><a href="{{ route('reports.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="form-row">
            
                <div class="col-md-3">
                    <input type="text" class="form-control"  id="year" name="year" autocomplete="false" placeholder="Year">
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group" align="right">
                        <select class="form-control" name="month" id="month">
                            <option value="">Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <div class="form-group" align="left">
                        <button type="submit" class="btn btn-primary" name="btn_search" id="btn_search"><span class="fa fa-search"></span> SEARCH</button>
                    </div>
                </div>
                <div class="container report state-app" name="report">
                    <table class=" table table-striped table-bordered table-hover text-center table-mediascreen" cellpadding="0" width="100%" cellspacing="0" align="center" role="grid" style="width: 100%; margin-bottom: 1rem;" name="nodatatr">
                        <thead class="thead-dark">
                            <tr>
                                <th class="v-align">Date</th>
                                <th class="v-align">DOC No.</th>
                                <th class="v-align">ROA No.</th>
                                <th class="v-align">PAYEE</th>
                                <th class="v-align">PARTICULARS</th>
                                <th class="v-align">APPROPRIATION</th>
                                <th class="v-align">OBLIGATION</th>
                                <th class="v-align">BALANCE</th>
                            </tr>
                        </thead>
                        <tbody class="nodatatr" name="nodatatr">
                            <tr align="center" style="border-style:hidden;">
                                <td colspan="15">No data available in table</td>
                            </tr>
                        </tbody>
                        <tbody class="report state-app" name="report">
                        </tbody>
                    </table>           
                </div>    
            </div>
            <div class="form-group" align="right" >
                <button class="btn btn-primary print-form-report" data-form="print_registration_obligation"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
            </div>
        </div>
    </div>
</div>

<div id="print_registration_obligation" hidden>
    @include('layouts.header')
    <div class="mt-1-5rm">
        @include('Reports.Print.registration_obligation')
    </div>
    <style>
        .mt-1-5rm{
            margin-top: 1.5rem;
        }
    </style>
</div>
@endsection

<style>
    .bt-po {
        border-bottom: 2px solid; 
        position: inherit; 
        bottom: 28px; 
        margin-bottom: 0;
    }
    .border-n {
        border: none;
    }
</style>
@push('scripts')

<script>
    $('#year').datetimepicker({
        format: 'Y'
    });
    $('#btn_search').click(function () {
        if($('#year').val() != null && $('#month').val()){
            $.ajax({
                url: "{{route('registration_obligation.search')}}",
                data: {
                    year : $('#year').val(),
                    month : $('#month').val()
                },
                method: "GET",
                success: function(data){
                    if(data != ''){
                        $('.nodatatr').hide(); 
                        $('.report').empty(); 
                        $(".report").html(data); 
                    }else{ 
                        $('.report').empty(); 
                        $('.nodatatr').show(); 
                    }
                }
                
                
            });
        }else{
            if($('#year').val() != null || $('#month').val()){
                toastMessage('Warning!',"It seems like you missed some required fields","warning");
            }
        }
        
    });
</script>

@endpush    