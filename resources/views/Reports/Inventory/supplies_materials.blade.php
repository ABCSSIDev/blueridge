@extends('layouts.app')

@section('content')
<div class="container pb-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Reports</li>
            <li class="breadcrumb-item active" aria-current="page">Inventory</li>
        </ol>
    </nav>
    <h4 class="form-head">Supplies and Materials Report</h4>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align="right" style="margin: 0px 0px 50px 0px"><a href="{{route('reports.index')}}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="tab-content">
                <div class="form-row">
                    <div class="col-md-3">
                        <input type="text" class="form-control"  id="year" autocomplete="false" placeholder="Year">
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <select class="form-control" id="month" id="month">
                            <option value="">Select Month</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" name="btn_search" id="btn_search"><i class="fa fa-search" aria-hidden="true"></i> SEARCH</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover text-center table-mediascreen" style="margin-top: 50px">
                <thead class="thead-dark">
                    <tr align="center">
                        <th rowspan="2" width="15%">Article</th>
                        <th rowspan="2" width="15%">Description</th>
                        <th rowspan="2" width="10%">Stock Number</th>
                        <th rowspan="2" width="10%">Unit of Measure</th>
                        <th rowspan="2" width="10%">Unit Value</th>
                        <th rowspan="2" width="10%">Balance per Card (Quantity)</th>
                        <th rowspan="2" width="10%">On hand per Count (Quantity)</th>
                        <th colspan="2" width="10%">Shortage/Overage</th>
                        <th rowspan="2" width="10%">Remarks</th>
                    </tr>
                    <tr>
                        <th >Quantity</th>
                        <th >Value</th>
                    </tr>
                </thead>
                <tbody class="report" name="report">
                </tbody>
                <tbody class="nodatatr" name="nodatatr">
                    <tr align="center" style="border-style:hidden;">
                        <td colspan="10">No Data Available</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div align="right" >
                <button class="btn btn-primary print-form-css" data-form="print_supplies_materials"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
            </div>
        </div>
    </div>

    <div id="print_supplies_materials" hidden>
        @include('layouts.header')
        @include('Reports.Print.supplies-materials')
       <style>
           .mt-table{
               margin-top: 1.5rem!important;
           }
           .bbm-supply {
                border-bottom: 2px solid;
                position: inherit;
                margin-bottom: -20px;
                margin-top: -1rem;
                width: 80%;
            }
        </style>
    </div>
</div>
@endsection

<style>
    .table thead th, .table td {
        vertical-align: middle !important;
    }
    .bbm-supply {
        border-bottom: 2px solid;
        position: inherit;
        margin-bottom: -20px;
        margin-top: -1rem;
    }
</style>


@push('scripts')
    <script>
    $('#year').datetimepicker({
        format: 'Y'
    });
    
    $('#btn_search').click(function () {
        if($('#year').val() != '' || $('#month').val() != ''){
            $.ajax({
                url: "{{route('supplies_materials.search')}}",
                data: {
                    month : $('#month').val(),
                    year : $('#year').val()
                },
                method: "GET",
                success: function(data){
                    if(data != ''){
                        $('.nodatatr').hide(); 
                        $('.report').empty(); 
                        $(".report").append(data); 
                    }else{ 
                        $('.report').empty(); 
                        $('.nodatatr').show(); 
                    }
                
                }
            });
        }else{
            toastMessage('Warning!',"It seems like you missed some required fields","warning");
        }
    });  
    $('#month').change(function(){
        let fruits = ['January', 'February','March', 'April','May', 'June','July', 'August', 'September','October', 'November', 'December']
        let val = fruits[$(this).val() - 1] +' '+$('#year').val();
        $('.date-selected').val(val);
    });
    </script>

@endpush