@extends('layouts.app')


@section('content')       
<div class="container">
    <div class="row justify-content-center">
        @include('charts.accounting')

        @include('charts.procurement')
        
        @include('charts.inventory')
        
        @include('charts.assign-inventory')
        <input type="hidden" class="release_notes" value="{!! $version->release_notes !!}">
    </div>
</div>
@endsection
@push('scripts')
    <script>  
    $(document).ready(function(){
        $show = "{!! $show_release !!}";
        if($show == 1){
            var title = "Release Notes {{ $version->version }}";
            $('.modal-title').html(title);
            $('#myModal').modal({backdrop:'static'});
            $('#myModal').modal('show');
            $('div.modal-body').html($('.release_notes').val());
        }
    });
    function changeType(el){
        $.ajax({
            url: "{{route('ledgersmanual_transactions')}}",
            data: {
                type : $(el).val(),
            },
            method: "GET",
            success: function(data){
                $("#ledgers").html(data); 
            }
            
        });
    }   

        
        let MONTHS = ['January', 'February', 'March', 'April', 'May', 'June'];
        let color = Chart.helpers.color;


        window.onload = function () {
            $(".clickable-rows").click(function() {
                window.open($(this).data("href"));
            });
        };

        var jsonData = $.ajax({
            url: '{{ route("budget.chart") }}',
            dataType: 'json'
        }).done(function(results){

            $('.receivable').html(results.financial_overview.receivables.toFixed(2));
            $('.payable').html(results.financial_overview.payables.toFixed(2));
            $('.or-count').html(results.financial_overview.or.toFixed(2));
            $('.dv-count').html(results.financial_overview.dv.toFixed(2));
            if (results.bank_statement.end_balance < 0){
                let balance = Math.abs(results.bank_statement.end_balance);
                $('.bank-balance').html(balance);
                $('.bank-balance').parent().append(')');
                $('.bank-balance').parent().prepend('(');
            }

            $('.num').numScroll();

            let labels = [], planned = [],spent = [];
            let data = results.budget_alloc;
            data.labels.forEach(function(value,key){
                labels.push(value);
                planned.push(data.allocated[key])
                spent.push(data.expended[key])
            });
            let barChartData = {
                labels: labels,
                datasets:  [{
                    label: 'Planned Cost',
					backgroundColor: color('green').alpha(0.5).rgbString(),
					borderColor: 'green',
					data: planned,
					fill: false,
                },
                {
                    label: 'Actual Cost',
					fill: false,
					backgroundColor: color('red').alpha(0.5).rgbString(),
					borderColor: 'red',
					data: spent,
                    fill: false
                }]
            };

            var ctx = document.getElementById('my-chart').getContext('2d');
			window.myHorizontalBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: false,
						text: 'Chart.js Bar Chart'
					}
				}
            });

			//Total Purchase Order Price
            $('.num_po').html(results.procurement.purchase_order);

            //Purchase by Category
            let chart = document.getElementById('purchase-by-category').getContext('2d');
            let pr_by_category_label = results.procurement.purchase_request_category['label'];
            window.myPie = new Chart(chart,{
                type: 'pie',
                data: {
                    datasets:[{
                        data: results.procurement.purchase_request_category['data'],
                        label: results.procurement.purchase_request_category['label'],
                        backgroundColor: listRandomColors(pr_by_category_label.length),
                    }],
                    labels: results.procurement.purchase_request_category['label'],
                },
                options: {
                    responsive: true
                }
            });


            //Total Count vs. Current Year's of Inventory per category
			let d = new Date();
			let line_chart_label = 'Inventory by Categories of ' + d.getFullYear();
            let categoryChartData = {
                labels: results.inventory.inventory_per_category.category_label,
                datasets: [{
                    
                    type: 'line',
                    label: line_chart_label,
                    backgroundColor: 'blue',
                    borderColor: 'blue',
                    borderWidth: 2,
                    fill: false,
                    data: results.inventory.inventory_per_category.current_categories_count
                },
                    {
                        type: 'bar',
                        label: "Total Inventory by Categories",
                        backgroundColor: color('red').alpha(0.5).rgbString(),
                        fill: false,
                        data: results.inventory.inventory_per_category.total_category_count,
                        borderColor: 'white',
                        borderWidth: 2
                    }]

            };

            let categoryChart =  document.getElementById('category-chart').getContext('2d');
            window.myMixedChart  = new Chart(categoryChart,{
                type: 'bar',
                data: categoryChartData,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'right'
                    },
                    title: {
                        display: true,
                        text: 'Inventory by category versus received per month'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true
                    }
                }
            });

            //Recent Assigned Supplies to Users
            document.querySelector('#assigned_users_tbody').innerHTML = results.inventory.recently_assigned;
            let xxx = document.getElementById('cash_bank').getContext('2d');
            window.myLiness = new Chart(xxx,{
                type: 'line',
                data: {
                    labels: results.bank_statement.labels,
                    datasets: [{
                        label: 'My First dataset',
                        backgroundColor: 'green',
                        borderColor: color('green').alpha(0.5).rgbString(),
                        data: results.bank_statement.balance,
                        fill: false,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Landbank of the Philippines'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                    }

                }
            });
            // window.myLine = new Chart(xxx,config_bank_recon);

        });
        $('#bank').change(function(){
            $.ajax({
                url: "{{route('home.functionBank')}}",
                data: {
                    id : $(this).val()
                },
                dataType: "json",
                success: function(data){
                    $('.banktable').html('<span class="num bank-balance">'+data.initial_deposit+'</span>');
                    let xxx = document.getElementById('cash_bank').getContext('2d');
                    window.myLiness = new Chart(xxx,{
                        type: 'line',
                        data: {
                            labels: data.bank_statement.labels,
                            datasets: [{
                                label: 'My First dataset',
                                backgroundColor: 'green',
                                borderColor: color('green').alpha(0.5).rgbString(),
                                data: data.bank_statement.balance,
                                fill: false,
                            }]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: data.bank_statement.name
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Month'
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Value'
                                    }
                                }]
                            }

                        }
                    });
                }
            });
        });
        $('#budget_year').change(function(){
            var url = '{{ route("budget.year", ':id') }}';
            url = url.replace(':id', $(this).val());
            var jsonData = $.ajax({
                url: url,
                dataType: 'json'
            }).done(function(results){
                let labels = [], planned = [],spent = [];
                let data = results.budget_alloc;
                data.labels.forEach(function(value,key){
                    labels.push(value);
                    planned.push(data.allocated[key])
                    spent.push(data.expended[key])
                });
                let barChartData = {
                    labels: labels,
                    datasets:  [{
                        label: 'Planned Cost',
                        backgroundColor: color('green').alpha(0.5).rgbString(),
                        borderColor: 'green',
                        data: planned,
                        fill: false,
                    },
                        {
                            label: 'Actual Cost',
                            fill: false,
                            backgroundColor: color('red').alpha(0.5).rgbString(),
                            borderColor: 'red',
                            data: spent,
                            fill: false
                        }]
                };
                var ctx = document.getElementById('my-chart').getContext('2d');
                window.myHorizontalBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: false,
                            text: 'Chart.js Bar Chart'
                        }
                    }
                });
            });
        });

    </script>
@endpush

<style>
    .margin-top{
        margin-top:50px;
    }
    hr {
        border: 1px solid #969292;
    }
    ul {
        list-style: circle;
        font-size: 15px;
    }
    ol {
        list-style: square;
        font-size: 15px;
    }
</style>
