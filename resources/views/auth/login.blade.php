<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Blueridge</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="bg-login">
    <div id="app">
        <main>
        <header><div id="login-header"></div></header>
            <template id="login-template">
                <div class="cube">
                <div class="shadow"></div>
                <div class="sides">
                    <div class="back"></div>
                    <div class="top"></div>
                    <div class="left"></div>
                    <div class="front"></div>
                    <div class="right"></div>
                    <div class="bottom"></div>
                </div>
                </div>
            </template>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 margin-card" align="center">
                <div class="row justify-content-center brb-headlogin pb-md-4 pl-4">
                    <div class="col-md-3 mt-n3" >
                        <image src="images/BRB_logo.png" class="brb-logo">
                    </div>
                    <div class="col-md-9 text-left pl-5 pt-sm-2">
                        <h1 class="fsize-2rm fcolor-primary"><b>BLUE RIDGE B</b></h1>
                        <h5 class="line-h0 fsize-standard fcolor-primary"><b>ACCOUNTING AND INVENTORY SYSTEM</b></h5>
                        <span class="lbl-dark fsize-10"><b> version 1.37.152-beta</b></span>
                    </div>
                </div>
                    <div class="card card-w1 mt-sm-3 b-shadow1">
                        <!-- <div class="card-header">{{ __('Login') }}</div> -->

                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <center><span class="lbl-dark fsize-16"><b><span style="color: #00007c">Login</span> your BRB account</b></span></center>
                                <div align="center" class="pt-5">
                                    <div class="input-container">
                                        <input type="email" id="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                                        <label>Enter Email</label>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror   		
                                    </div>
                                    <div class="input-container">		
                                        <input type="password" id="password" class=" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"/>
                                        <label>Enter Password</label>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                    <div align="center" class="pt-2rm">
                                        <button type="submit" class="btn btn-primary btn-blueridge p-2">
                                            <b>{{ __('LOGIN') }}</b>
                                        </button>
                                    </div>
                                <div class="form-group row">
                                    <div class="ml-6 pt-md-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div align="center" class="mt-3">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                    <hr class="mt-0 hr1">
                    <p align="center" class="lbl-dark"><b>Powered by:</b></p>
                    <img src="images/ABC_SysSol.png" class="AbcsysSol-img">
                    <p class="lbl-dark fsize-16"><b>ABC System Solutions Inc. Dev Team 2020</b></p>
                </div>
            </div>
        </div>
            
        </main>
    </div>
</body>
</html>

<style>
    .bg-login{
        background-image: url("images/Login_Background.png");
        background-size: cover;
        background-repeat: no-repeat, no-repeat;
        background-position: Left bottom, right bottom;
        background-attachment: fixed;
        overflow-y: hidden;
    }
    .margin-card{
        margin:10%;
    }
    .brb-logo{
        width: 7rem;
        position: absolute;
    }
    .brb-headlogin{
        margin-top: -5rem;
    }
    .input-container{
        width: 80%;
        position:relative;
        margin-bottom:25px;
}
    .input-container label{
        position:absolute;
        top:0px;
        left:0px;
        font-size:16px;
        color:#7e7e7e;;	
        /* pointer-event:none; */
        transition: all 0.5s ease-in-out;
    }
    .input-container input{ 
        border:0;
        border-bottom:2px solid #d8d3d3;  
        background:transparent;
        width:100%;
        padding:8px 0 5px 0;
        font-size:16px;
        color:#000;
    }
    .input-container input:focus{ 
        border:none;	
        outline:none;
        border-bottom:2px solid #00007c;	
    }
    .input-container input:focus ~ label,
    .input-container input:valid ~ label{
        top:-12px;
        font-size:12px;
        color: #00007c;
        font-weight: bold;
    }
    .lbl-dark{
        color: #7e7e7e;
    }
    .btn-primary.btn-blueridge{
        background-color: #00007c;
        width: 80%;
    }
    .line-h0{
        line-height: 0rem;
    }
    .ml-4f{
        margin-left: 4.5rem;
    }
    .btn-link{
        color: #00007c;
        font-weight: bold;
    }
    .card-w1{
        width: 75%;
    }
    .ml-6{
        margin-left: 3.5rem;
    }
    .pt-2rm{
        padding-top: 2rem;
    }
    .fcolor-primary{
        color: #00007c;
    }
    .fsize-standard{
        font-size: 13px;
    }
    .fsize-16{
        font-size: 16px;
    }
    .fsize-2rm{
        font-size: 2rem;
    }
    .hr1{
        width: 75%;
    }
    .b-shadow1{
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px #fff;
    }
    .AbcsysSol-img{
        margin-top: -1.5rem;
        width: 15rem;
    }
</style>

<script>
let Strut = {
  random: function (e, t) {
    return Math.random() * (t - e) + e;
  },
  arrayRandom: function (e) {
    return e[Math.floor(Math.random() * e.length)];
  },
  interpolate: function (e, t, n) {
    return e * (1 - n) + t * n;
  },
  rangePosition: function (e, t, n) {
    return (n - e) / (t - e);
  },
  clamp: function (e, t, n) {
    return Math.max(Math.min(e, n), t);
  },
  queryArray: function (e, t) {
    return t || (t = document.body), Array.prototype.slice.call(t.querySelectorAll(e));
  },
  ready: function (e) {
    document.readyState == 'complete' ? e() : document.addEventListener('DOMContentLoaded', e);
  }
};
const reduceMotion = matchMedia("(prefers-reduced-motion)").matches;

{
  // =======
  // helpers
  // =======

  const setState = (state, speed) =>
    directions.forEach(axis => {
      state[axis] += speed[axis];
      if (Math.abs(state[axis]) < 360) return;
      const max = Math.max(state[axis], 360);
      const min = max == 360 ? Math.abs(state[axis]) : 360;
      state[axis] = max - min;
    });

  const cubeIsHidden = left => left > parentWidth + 30;


  // =================
  // shared references
  // =================

  let headerIsHidden = false;

  const template = document.getElementById("login-template");

  const parent = document.getElementById("login-header");
  const getParentWidth = () => parent.getBoundingClientRect().width;
  let parentWidth = getParentWidth();
  window.addEventListener("resize", () => parentWidth = getParentWidth());

  const directions = ["x", "y"];

  const palette = {
    white: {
      color: [255, 255, 255],
      shading: [160, 190, 218]
    },
    orange: {
      color: [255, 250, 230],
      shading: [255, 120, 50]
    },
    green: {
      color: [205, 255, 204],
      shading: [0, 211, 136]
    }
  };


  // ==============
  // cube instances
  // ==============

  const setCubeStyles = ({cube, size, left, top}) => {
    Object.assign(cube.style, {
      width: `${size}px`,
      height: `${size}px`,
      left: `${left}px`,
      top: `${top}px`
    });

    Object.assign(cube.querySelector(".shadow").style, {
      filter: `blur(${Math.round(size * .6)}px)`,
      opacity: Math.min(size / 120, .4)
    });
  };

  const createCube = size => {
    const fragment = document.importNode(template.content, true);
    const cube = fragment.querySelector(".cube");

    const state = {
      x: 0,
      y: 0
    };

    const speed = directions.reduce((object, axis) => {
      const max = size > sizes.m ? .3 : .6;
      object[axis] = Strut.random(-max, max);
      return object;
    }, {});

    const sides = Strut.queryArray(".sides div", cube).reduce((object, side) => {
      object[side.className] = {
        side,
        hidden: false,
        rotate: {
          x: 0,
          y: 0
        }
      };
      return object;
    }, {});

    sides.top.rotate.x = 90;
    sides.bottom.rotate.x = -90;
    sides.left.rotate.y = -90;
    sides.right.rotate.y = 90;
    sides.back.rotate.y = -180

    return {fragment, cube, state, speed, sides: Object.values(sides)};
  };

  const sizes = {
    xs: 15,
    s: 25,
    m: 40,
    l: 100,
    xl: 120
  };

  const cubes = [
    {
      tint: palette.green,
      size: sizes.xs,
      left: 35,
      top: 315
    },{
      tint: palette.white,
      size: sizes.s,
      left: 45,
      top: 250
    },{
      tint: palette.white,
      size: sizes.xl,
      left: 100,
      top: 315
    },{
      tint: palette.white,
      size: sizes.m,
      left: 370,
      top: 90
    },{
      tint: palette.green,
      size: sizes.xs,
      left: 400,
      top: 230
    },{
      tint: palette.orange,
      size: sizes.s,
      left: 420,
      top: 178
    },{
      tint: palette.white,
      size: sizes.l,
      left: 450,
      top: 610
    },{
      tint: palette.green,
      size: sizes.s,
      left: 1080,
      top: 600
    },{
      tint: palette.white,
      size: sizes.xl,
      left: 1200,
      top: 12
    },{
      tint: palette.orange,
      size: sizes.l,
      left: 1100,
      top: 310
    },{
      tint: palette.green,
      size: sizes.m,
      left: 1030,
      top: 200
    }
  ].map(object => Object.assign(createCube(object.size), object));

  cubes.forEach(setCubeStyles);


  // =======================
  // cube rotating animation
  // =======================

  const getDistance = (state, rotate) =>
    directions.reduce((object, axis) => {
      object[axis] = Math.abs(state[axis] + rotate[axis]);
      return object;
    }, {});

  const getRotation = (state, size, rotate) => {
    const axis = rotate.x ? "Z" : "Y";
    const direction = rotate.x > 0 ? -1 : 1;

    return `
      rotateX(${state.x + rotate.x}deg)
      rotate${axis}(${direction * (state.y + rotate.y)}deg)
      translateZ(${size / 2}px)
    `;
  };

  const getShading = (tint, rotate, distance) => {
    const darken = directions.reduce((object, axis) => {
      const delta = distance[axis];
      const ratio = delta / 180;
      object[axis] = delta > 180 ? Math.abs(2 - ratio) : ratio;
      return object;
    }, {});

    if (rotate.x)
      darken.y = 0;
    else {
      const {x} = distance;
      if (x > 90 && x < 270)
        directions.forEach(axis => darken[axis] = 1 - darken[axis]);
    }

    const alpha = (darken.x + darken.y) / 2;
    const blend = (value, index) => Math.round(Strut.interpolate(value, tint.shading[index], alpha));
    const [r, g, b] = tint.color.map(blend);

    return `rgb(${r}, ${g}, ${b})`;
  };

  const shouldHide = (rotateX, x, y) => {
    if (rotateX)
      return x > 90 && x < 270;
    if (x < 90)
      return y > 90 && y < 270;
    if (x < 270)
      return y < 90;
    return y > 90 && y < 270;
  };

  const updateSides = ({state, speed, size, tint, sides, left}) => {
    if (headerIsHidden || cubeIsHidden(left)) return;

    const animate = object => {
      const {side, rotate, hidden} = object;
      const distance = getDistance(state, rotate);

      // don't animate hidden sides
      if (shouldHide(rotate.x, distance.x, distance.y)) {
        if (!hidden) {
          side.hidden = true;
          object.hidden = true;
        }
        return;
      }

      if (hidden) {
        side.hidden = false;
        object.hidden = false;
      }

      side.style.transform = getRotation(state, size, rotate);
      side.style.backgroundColor = getShading(tint, rotate, distance);
    };

    setState(state, speed);
    sides.forEach(animate);
  };

  const tick = () => {
    cubes.forEach(updateSides);
    if (reduceMotion) return;
    requestAnimationFrame(tick);
  };


  // ===============
  // parallax scroll
  // ===============

  // give it some extra space to account for the parallax and the shadows of the cubes
  const parallaxLimit = document.querySelector("main > header").getBoundingClientRect().height + 80;

  window.addEventListener("scroll", () => {
    const scroll = window.scrollY;
    if (scroll < parallaxLimit) {
      headerIsHidden = false;
      cubes.forEach(({cube, speed}) =>
        cube.style.transform = `translateY(${Math.abs(speed.x * .5) * scroll}px)`);
      return;
    }
    headerIsHidden = true;
  });


  // ==========
  // initialize
  // ==========

  const container = document.createElement("div");
  container.className = "login-cubes";
  cubes.forEach(({fragment}) => container.appendChild(fragment));

  const start = () => {
    tick();
    parent.appendChild(container);
  };

  'requestIdleCallback' in window ? requestIdleCallback(start) : start();
}
</script>

