<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Blue Ridge B</title>
    <link rel="shortcut icon" href="{{ asset('/images/BRB_logo.png') }}" type="image/png">

    <!-- Styles -->
    
    <link href="{{ asset('css/app.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" media="print" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" media="print" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet"  type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css'> 

    <!-- Scripts -->

    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/vendor.js') }}" ></script>
    <script src="{{ asset('js/manifest.js') }}" ></script>
    <script src="{{ asset('js/jquery-ui.js') }}" ></script>
    <script src="{{ asset('js/blueridge.js') }}"></script>
    <script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/jquery-ui-autocomplete-with-clear-button.js') }}"></script>
    
    <!-- <script src="print.js"></script> -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    
</head>
<body class="white-bg">
    <div id="app">
        <main>
            <div class="container-fluid p-0">
                <div class="row" id="body-row">
                    @include('layouts.sidebar')
                    <div class="col">
                        @yield('content')
                    </div>
                        @include('layouts.modal')
                </div>
            </div>
            
        </main>
    </div>
</body>
</html>
@stack('scripts')
<!-- autocomplete -->
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat');

/*-------------------------------- END ----*/
    
    #body-row {
        margin-left: 0;
        margin-right: 0;
    }
    .white-bg{
        background-color:white;
    }

    #sidebar-container {
        min-height: 100vh;
        background-color: #dee2e6;
        padding: 0;
        /* flex: unset; */
    }
    
    .sidebar-expanded {
        width: 250px;
    }
    
    .sidebar-collapsed {
        width: 60px;
        /* width: 100px; */
    }

    /* ----------| Submenu item*/    
    /* #sidebar-container .list-group li.list-group-item {
        background-color: #dee2e6;
    } */
    
    #sidebar-container .list-group .sidebar-submenu a {
        height: auto;
        padding-left: 30px;
    }
    
    .sidebar-submenu {
        font-size: 0.9rem;
    }

    /* ----------| Separators */    
    .sidebar-separator-title {
        background-color: #dee2e6;
        height: 35px;
    }
    
    .logo-separator {
        background-color: #dee2e6;
        height: 60px;
    }
    
   
    .header-sidebar{
        text-transform: uppercase;
        margin-bottom: 0;
        font-weight: 600;
    }
    .dropdown-sidebar{
        color: #7e7e7ebd;
    }
    .modal-header{
        background-color: #343a40;
        color: #fff;
    }
    .sidebar-none {
        display:none;
    }
    label{
        font-weight: bold;
    }
</style>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script>
// Hide submenus
$('#body-row .collapse').collapse('hide');

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});


function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('sidebar-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container, #content-sidebar').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Treating d-flex/d-none on separators with title
    var SeparatorTitle = $('.sidebar-separator-title');
    if ( SeparatorTitle.hasClass('d-flex') ) {
        SeparatorTitle.removeClass('d-flex');
    } else {
        SeparatorTitle.addClass('d-flex');
    }
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}



$(document).on('click','.popup_form',function(e){
    e.preventDefault();
    var title = $(this).attr('title');
    var url = $(this).attr('data-url');
    if (url == null){
        url = $(this).attr('href');
    }
    $('.modal-title').html(title);
    $.ajax({
        method: "GET",
        url: url,
        success: function(data){
            $('#myModal').modal({backdrop:'static'});
            $('#myModal').modal('show');
            $('div.modal-body').html(data);
        }
    });
});

</script>
