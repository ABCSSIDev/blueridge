<div class='popover-container'>
    <div class='clearfix'>
        <div class='section'>
            <h4>Statements</h4>
            <ul class='hoverEffect'>
                <li><a href='{{ route('account_statement.report', 1) }}'>Account Statements</a></li>
                <li><a class='popup_form' data-toggle='modal' title='Add Manual Transaction' href='{{ route('manual_transactions.report') }}'>Add Manual Transactions</a></li>
            </ul>
        </div>
        <div class='section'>
            <h4>Reconcile</h4>
            <ul class='hoverEffect'>
                <li><a href='#'></a></li>
            </ul>
        </div>
    </div>
</div>

