<div class="card sidebar1 pb-3">
    <div align="center" class="pt-4 h32 top-fixed">
        <a href="{{ url('/') }}">
            <img src="{{ asset('images/BRB_logo.png') }}" style="width: 3rem">
        </a>
    <!-- <a href="{{ route('account.show',Auth::User()->id) }}">
            <img src="/images/Fields.png" class="img-profile">
        </a> -->

    </div>
    <div align="center" class="bottom-fixed">
        <a href="{{ route('account.show',Auth::User()->id) }}">
            <img src="{{ Auth::User()->image == null ? asset('images/default.png') : route('account.image',Auth::User()->id) }}" class="img-profile">
        </a>
        <br>
        <i class="fa fa-bell fx-sidebaricon"></i>
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <i class="fa fa-sign-out text-white fsize-1-2rm"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </div>
</div>
<!-- Sidebar -->
<div id="sidebar-container" class="sidebar-expanded d-none d-md-block"><!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
        <!-- Bootstrap List Group -->
        <ul id="content-sidebar" class="list-group pt-3 position-fixed sb-contentexpanded scroll-bar" style="font-weight: bold">
            <!-- Separator with title -->
            
            <li class="sidebar-separator-title text-sidebar d-flex align-items-center menu-collapsed mt-sm-2 pl-2 mb-lg-3">
            
                <div class="row pl-3">
                    <!-- <img src="{{ asset('images/BRB_logo.png') }}" style="width:3.5rem"> -->
                    <h5 class="header-sidebar pl-2 pt-sm-3 txt-dblue">BLUE RIDGE B</h5>
                </div>
            
            </li>

            <!-- /END Separator -->

            <!-- Menu with submenu -->
        
            <!-- Submenu content -->
            
            <!-- Submenu content -->
                       
            <!-- Submenu content -->
                     
            <!-- Separator with title -->

            <!-- /END Separator -->
            
            <!-- Submenu content -->
            
            <!-- Submenu content -->
                     
            <!-- Separator without title -->
                      
        </li>
        {!! $globalModules->getModules() !!}
        <!-- <a href="#" data-toggle="collapse" aria-expanded="false" class="sidebar-background sidebar-list-group list-group-item-action flex-column align-items-start sidebar-highlight print-form-css" data-form="print-all-documents">
            <div class="d-flex w-100 justify-content-start align-items-center text-sidebar">
                <span class="fa fa-book mr-3" style="display: inline;padding-left: 8px;"></span> 
                <span class="menu-collapsed">Documents</span>
            </div>
        </a> -->
        <li class="sidebar-list-group sidebar-separator menu-collapsed"></li>
        <a href="#" data-toggle="sidebar-colapse" class="sidebar-background sidebar-list-group list-group-item-action d-flex align-items-center">
            <div class="d-flex w-100 justify-content-start align-items-center text-sidebar">
                <span id="collapse-icon" class="fa fa-2x mr-3 arrow-animation mt-n1 ml-7px"></span>
                <span id="collapse-text" class="menu-collapsed">Close</span>
            </div>
        </a>
    </ul><!-- List Group END-->
</div><!-- sidebar-container END -->


<!-- DOCUMENTS -->

<style>
    #body-row {
        margin-left:0;
        margin-right:0;
    }
    #sidebar-container {
        min-height: 100vh;
        background-color: #333;
        padding: 0;
    }

    .sidebar-expanded {
        width: 230px;
    }
    .sidebar-collapsed {
        width: 60px!important;
    }

    .sb-contentexpanded{
        width: 250px;
    }

    /* Menu item*/
    #sidebar-container .list-group a {
        height: 50px;
        color: white;
    }

    /* Submenu item*/
    #sidebar-container .list-group .sidebar-submenu a {
        height: 45px;
        padding-left: 30px;
    }
    .sidebar-submenu {
        font-size: 0.9rem;
    }

    .sidebar-separator-title {
        background-color: #333;
        height: 35px;
    }
    .sidebar-separator {
        background-color: #dee2e6;
        height: 25px;
    }
    .logo-separator {
        background-color: #333;
        height: 60px;
    }
    .sidebar1{
        width:4rem; 
        background: #00007c; 
        border-radius: 0rem;
        margin-bottom: 0px!important;
    }
    .fsize-1-2rm{
        font-size: 1.2rem;
    }
    .h32{
        height: 32rem;
    }
    .img-profile{
        border-radius: 50%;
        width: 2.5rem;
    }
    .fx-sidebaricon{
        color: white;
        padding-top: 1.5rem;
        font-size: 1.2rem;
    }
    .primary-bgcolor{
        background: #00007c;
    }
    .secondary-bgcolor{
        background: #40506d;
    }
    .txt-dblue{
        color: #00007c;
    }
    .text-sidebar{
        color:#6c757d !important;
    }
    .text-sidebar:hover{
        color: #00007c!important;
    }
    .dropdown-sidebar:hover{
        color: #00007c!important
    }
    .sidebar-background {
        background: #dee2e6;
    }
    .sidebar-background:hover {
        background: #cdd1d5;
    }
    .sidebar-list-group {
        position: relative;
        display: block;
        padding: 0.75rem 1.25rem;
    }
    .sidebar-list-group:hover, .sidebar-list-group:focus {
        background-color: #cdd1d5;
    }
    #sidebar-container .list-group .sidebar-list-group[aria-expanded="false"] .submenu-icon::after {
        content: " \f0d7";
        font-family: FontAwesome;
        display: inline;
        text-align: right;
        padding-left: 10px;
    }
    /* Opened submenu icon */
        #sidebar-container .list-group .sidebar-list-group[aria-expanded="true"] .submenu-icon::after {
        content: " \f0da";
        font-family: FontAwesome;
        display: inline;
        text-align: right;
    }
    .sidebar-highlight:hover .sidebar-none {
        display: inline!important;
    }
    #sidebar-container .sidebar-highlight:hover {
        width: 250px !important;
    }
    .bottom-fixed{
        position: fixed; 
        bottom: 1rem; 
        padding-bottom: 2rem; 
        padding-left: 5px;
    }
    .top-fixed{
        position: fixed; 
        top: 0; 
        padding-left: 5px;
    }
    .ml-7px{
        margin-left: 7px;
    }

    /* animation */
    .arrow-animation {
        animation: animated-arrow 1.8s linear infinite;
        }

        @keyframes animated-arrow {
        80% {
            transform: translate(-12px, 0);
        }
        }
    .scroll-bar{
        max-height: calc(100% - 20px);
        height: calc(100% - 20px);
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>