
<div class="col-12">
    <div class="neumorphic-card">
        <h3 class="h3-dashboard">&nbsp;Assign Inventory</h3>
        <hr>
        <table class="table table-bordered text-center" style="margin-top: 10px">
            <thead class="thead-assign-inventory">
                <tr>
                    <th>Assigned To</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody id="assigned_users_tbody">
            </tbody>
        </table>
    </div>
</div>
