<div class="col-md-12">
    <div class="neumorphic-card mb-5">
        <h3 class="h3-dashboard">&nbsp;Inventory per Category</h3>
        <hr>
        <div class="row">   
            <div class="col-sm-12">
                <canvas id="category-chart"></canvas>
            </div>
        </div>
    </div>
</div>