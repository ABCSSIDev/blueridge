<div class="col-12 mb-5">
    <div class="row">
        <div class="col-sm-6">
            <div class="neumorphic-card">
                <h3 class="h3-dashboard">&nbsp;PO Summary</h3>
                <hr>
                <div class="tile-stats tile-PO">
                    <div class="num_po" data-num="">0</div>
                    <h3>Cost of PO</h3>
                    <p>Total Cost of Purchase Order</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="neumorphic-card">
                <h3 class="h3-dashboard">&nbsp;Purchase Request per Category</h3>
                <hr>
                <canvas id="purchase-by-category"></canvas>
            </div>
        </div>
    </div>
</div>
