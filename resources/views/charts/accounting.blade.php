<div class="col-12" style="margin-top: 3em">
    <div class="neumorphic-card mb-5">
        <h3 class="h3-dashboard">&nbsp;Financial Overview</h3>
        <hr>
        <div class="row">
            <div class="col-sm-3">
                <div class="tile-stats tile-red">
                    <div class="icon">
                        <i class="fas fa-hand-holding-usd"></i>
                    </div>
                    <div class="num receivable" data-num="">0</div>
                    <h3>Accounts Receivable</h3>
                    <p>Total Accounts Receivable</p>
                </div>
            </div>
            <div class="col-sm-3">   
                <div class=" tile-stats tile-green">
                        <div class="icon">
                        <i class="fas fa-donate"></i>
                    </div>
                    <div class="num payable" data-num="">0</div>
                    <h3>Accounts Payable</h3>
                    <p>Total Accounts Payable</p>
                </div>
            </div>
            <div class="col-sm-3">   
                <div class=" tile-stats tile-aqua">
                        <div class="icon">
                        <i class="fas fa-receipt"></i>
                    </div>
                    <div class="num or-count" data-num="">0</div>
                    <h3>OR Count</h3>
                    <p>Total Official Receipts</p>
                </div>
            </div>
            <div class="col-sm-3">   
                <div class=" tile-stats tile-blue">
                    <div class="icon">
                        <i class="fas fa-money-check"></i>
                    </div>
                    <div class="num dv-count" data-num="">0</div>
                    <h3>DV Count</h3>
                    <p>Total Disbursement Vouchers</p>
                </div>
            </div>
        </div>
    </div>
    <div class="neumorphic-card mb-5">
        <h3 class="h3-dashboard">&nbsp;Budget Allocation Report</h3>
        <hr>
        <div class="col-sm-2">
            <select class="form-control" id="budget_year">
                @foreach($budget_year as $year)
                    <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <canvas id="my-chart"></canvas>
            </div>
        </div>
    </div>

    <div class="neumorphic-card mb-5">
        <div class="row">
            <div class="col-sm-7">
                <h3 class="h3-dashboard"> Cash in Bank</h3>
            </div>
            <div class="col-sm-5">
                <select class="form-control" name="bank" id="bank">
                    <option value="none">Select Bank</option>
                    @foreach($bank as $data)  
                        <option value="{{$data->id}}">{{$data->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
      
        <div class="row">
            <div class="col-sm-7">
                <canvas height="80" width="200" id="cash_bank"></canvas>
            </div>
            <div class="col-sm-5">
                <div class="details mt-5">
                    <div class="bank-money">
                        <p>Balance in Bank</p>
                        <div class="money-value banktable" >
                            <span class="num bank-balance"></span>
                        </div>
                    </div>
                    <button type="button" class="btn btn-outline-success manage-accounts" data-toggle="popover" title="Popover Header" data-html="true" data-placement="bottom" data-content=" @include('layouts.popover') ">Manage Account</button>
                    <!-- <button type="button" class="btn btn-outline-success manage-accounts" data-toggle="popover" title="Popover Header" data-html="true"  data-placement="bottom" data-content=" @include('layouts.popover') ">Manage Accounts</button>    -->
                    <!-- <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown button
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class='dropdown-item popup_form' data-toggle='modal'  data-url='{{route("manual_transactions.report")}} ' title='Add Manual Transaction' >Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="neumorphic-card mb-5">
        <h3 class="h3-dashboard"> Overdues</h3>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="">
                    <div class="table-responsive mb-0" data-toggle="list">
                        <table class="table table-sm table-nowrap card-table text-center table-bordered table-striped">
                            <thead class="tab_header">
                                <tr>
                                    <th>Type</th>
                                    <th>Action</th>
                                    <th>Due Date</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @if(!empty($overdues))
                                    @foreach($overdues['due_date'] as $key => $value)
                                        @foreach($value as $key_module => $module)
                                            @foreach($module as $id)
                                                <?php
                                                        $route = '#';
                                                    switch ($key_module){
                                                        case 'invoice':
                                                            $reference = \App\Invoice::findOrFail($id);
                                                            $total = $reference->getInvoice->unit_price;
                                                            $message = 'Receive payment from OR# '.$reference->or_number;
                                                            $route = route('payment.create',['type' => 'invoice','id' => $id]);
                                                            break;
                                                        case 'cedula':
                                                            $reference = \App\Cedula::findOrFail($id);
                                                            $total = $reference->item_1 = $reference->item_2 + $reference->item_3 + $reference->item_4 + $reference->item_5;
                                                            $message = 'Receive payment from cedula#'.$reference->id;
                                                            $route = route('payment.create',['type' => 'cedula','id' => $id]);
                                                            break;
                                                        case 'disbursement':
                                                            $reference = \App\DisbursementVoucher::findOrFail($id);
                                                            $total = $reference->getRoa->total_amount;
                                                            $message = 'Make payment to ROA#: '.$reference->getRoa->obligation_number;
                                                            $route = route('payment.create',['type' => 'disbursement-voucher','id' => $id]);
                                                            break;
                                                        case 'payroll':
                                                            $reference = \App\Payroll::findOrFail($id);
                                                            $total = $reference->getPayrollItems->sum('salary') + $reference->getPayrollItems->sum('other_benefits') - $reference->getPayrollItems->sum('bir');
                                                            $message = '';
                                                            $route = route('payment.create',['type' => 'payroll','id' => $id]);
                                                            break;
                                                        default:
                                                            $total = 0;
                                                            $message = '';
                                                            break;
                                                    }
                                                ?>
                                                <tr class="clickable-rows" data-href="{{ $route }}">
                                                    <td>{{ ucfirst($key_module) }}</td>
                                                    <td>{{ $message }}</td>
                                                    <td>{{ \App\Common::formatDate('m/d/Y',$key) }}</td>
                                                    <td>{{ number_format($total,2) }}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No overdue payments</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
