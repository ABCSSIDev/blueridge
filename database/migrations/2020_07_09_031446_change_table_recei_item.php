<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableReceiItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receiving_items', function(Blueprint $table)
        {
            $table->dropColumn('quantity_received');
        });
    
        Schema::table('receiving_items', function(Blueprint $table)
        {
            $table->decimal('quantity_received',14,3)->after('canvass_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
