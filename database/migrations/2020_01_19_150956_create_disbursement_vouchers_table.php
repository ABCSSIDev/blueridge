<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisbursementVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disbursement_vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('roa_id');
            $table->unsignedBigInteger('claimant_id');
            $table->string('check_number');
            $table->string('claimant_address');
            $table->date('cheque_date');
            $table->date('due_date')->nullable();
            $table->string('tin')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('roa_id')->references('id')->on('roas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('claimant_id')->references('id')->on('general_users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disbursement_vouchers');
    }
}
