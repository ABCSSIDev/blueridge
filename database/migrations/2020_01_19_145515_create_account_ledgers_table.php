<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_ledgers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('ledger_id')->nullable();
            $table->string('account_code')->nullable();
            $table->tinyInteger('account_type');
            $table->string('ledger_name');
            $table->string('description')->nullable();
            $table->unsignedBigInteger('reference_group')->nullable();
            $table->unsignedBigInteger('reference_ledger')->nullable();
            $table->decimal('budget_percentage')->nullable();
            $table->string('attachments')->nullable();
            $table->string('attachments_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('group_id')->references('id')->on('account_groups')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ledger_id')->references('id')->on('account_ledgers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reference_group')->references('id')->on('account_groups')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reference_ledger')->references('id')->on('account_ledgers')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_ledgers');
    }
}
