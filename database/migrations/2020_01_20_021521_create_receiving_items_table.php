<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiving_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('receiving_id');
            $table->unsignedBigInteger('canvass_item_id');
            $table->decimal('quantity_received',14,2);
            $table->string('remarks');
            $table->enum('inventory_type',['capex','opex']);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('receiving_id')->references('id')->on('receivings')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('canvass_item_id')->references('id')->on('canvass_items')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiving_items');
    }
}
