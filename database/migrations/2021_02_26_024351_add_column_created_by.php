<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddColumnCreatedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dbName = env('DB_DATABASE');
        $tables = DB::select('SHOW TABLES');
        foreach($tables as $table)
        {
            Schema::table($table->{'Tables_in_' . $dbName}, function($table) {
                // Add the column
                $table->unsignedBigInteger('created_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dbName = env('DB_DATABASE');
        $tables = DB::select('SHOW TABLES');
        foreach($tables as $table)
        {
            Schema::table($table->{'Tables_in_' . $dbName}, function($table) {
                // Add the column
                $table->dropColumn('created_by')->nullable();
            });
        }
    }
}
