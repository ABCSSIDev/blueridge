<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignedSupplyItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_supply_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('assigned_supply_id');
            $table->unsignedBigInteger('inventory_id')->nullable();
            $table->unsignedBigInteger('asset_id')->nullable();
            $table->decimal('quantity',14,3);
            $table->timestamps();
            $table->foreign('assigned_supply_id')->references('id')->on('assigned_supplies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inventory_id')->references('id')->on('inventories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('asset_id')->references('id')->on('assets')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_supply_items');
    }
}
