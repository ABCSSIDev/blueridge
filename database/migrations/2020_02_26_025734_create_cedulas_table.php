<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCedulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cedulas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ledger_id');
            $table->unsignedBigInteger('cedula_id');
            $table->date('issued_date');
            $table->decimal('item_1',14,2)->nullable();
            $table->decimal('item_2',14,2)->nullable();
            $table->decimal('item_3',14,2)->nullable();
            $table->decimal('item_4',14,2)->nullable();
            $table->decimal('item_5',14,2)->nullable();
            $table->decimal('item_6',14,2)->nullable();
            $table->decimal('item_7',14,2)->nullable();
            $table->decimal('item_8',14,2)->nullable();
            $table->decimal('item_9',14,2)->nullable();
            $table->decimal('item_10',14,2)->nullable();
            $table->date('due_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ledger_id')->references('id')->on('account_ledgers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cedula_id')->references('id')->on('general_users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cedulas');
    }
}
