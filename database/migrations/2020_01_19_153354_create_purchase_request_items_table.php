<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_request_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pr_id');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('inventory_name_id');
            $table->decimal('quantity',14,3);
            $table->decimal('unit_cost',14,2);  
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('inventory_name_id')->references('id')->on('inventory_names')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_request_items');
    }
}
