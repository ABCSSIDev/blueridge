<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barangay');
            $table->string('treasurer');
            $table->string('city_municipality');
            $table->string('province');
            $table->string('payroll_number');
            $table->unsignedBigInteger('ledger_id');
            $table->date('date_from');
            $table->date('date_to');
            $table->date('due_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ledger_id')->references('id')->on('account_ledgers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
