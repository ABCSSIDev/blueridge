<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateROASTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ledger_id');
            $table->string('obligation_number')->nullable();
            $table->date('resolution_date');
            $table->string('resolution_number');
            $table->string('bids_resolution_number')->nullable();
            $table->string('bids_mode')->nullable();
            $table->date('approval_date');
            $table->string('signatory')->nullable();
            $table->decimal('total_amount', 14, 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ledger_id')
                ->references('id')
                ->on('account_ledgers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_o_a_s');
    }
}
