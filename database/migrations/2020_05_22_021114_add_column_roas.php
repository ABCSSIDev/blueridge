<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRoas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roas', function (Blueprint $table) {
            $table->string('cash_advance_voucher_no')->after('total_amount')->nullable();
            $table->date('cash_advance_date')->after('cash_advance_voucher_no')->nullable();
            $table->decimal('cash_advance_amount',14,2)->after('cash_advance_date')->nullable();
            $table->date('amount_refund_date')->after('cash_advance_amount')->nullable();
            $table->string('amount_refund_or_no')->after('amount_refund_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
