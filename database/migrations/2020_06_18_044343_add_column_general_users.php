<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnGeneralUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('general_users', function (Blueprint $table) {
            $table->unsignedBigInteger('cluster')->after('tin')->nullable();
            $table->string('household')->after('cluster')->nullable();
            $table->string('quantifier')->after('middle_initial')->nullable();
            $table->string('religion')->after('household')->nullable();
            $table->string('health_status')->after('religion')->nullable();
            $table->string('highest_education')->after('health_status')->nullable();
            $table->string('occupation')->after('highest_education')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
