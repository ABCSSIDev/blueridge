<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pr_id');
            $table->unsignedBigInteger('canvass_id');
            $table->date('po_date');
            $table->string('procurement_mode');
            $table->string('delivery_term');
            $table->string('place_of_delivery')->nullable();
            $table->string('payment_term');
            $table->date('delivery_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('canvass_id')->references('id')->on('canvasses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
