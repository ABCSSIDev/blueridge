<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCanvassItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canvass_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('canvass_id');
            $table->unsignedBigInteger('pr_item_id');
            $table->decimal('available_quantity',14,3);
            $table->decimal('unit_price',14,2);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('canvass_id')->references('id')->on('canvasses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pr_item_id')->references('id')->on('purchase_request_items')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canvass_items');
    }
}
