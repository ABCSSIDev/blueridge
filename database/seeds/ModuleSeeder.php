<?php

use Illuminate\Database\Seeder;
use App\Module;
use Illuminate\Support\Facades\URL;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents("database/data/modules.json");
        $data = json_decode($json);
        foreach ($data as $object){
            Module::create([
                "id" => $object->id,
                "module_id" => $object->module_id,
                "name" => $object->name,
                "is_enabled" => $object->is_enabled,
                "icon" => $object->icon,
                "route" => $this->checkRoute($object->route)
            ]);
        }
    }

    public function checkRoute($string){
        if (substr($string,0, 1) == '#'){
            return $string;
        }else{
            $route = str_replace('"\"','',$string);

            return $route;
        }
    }
}
