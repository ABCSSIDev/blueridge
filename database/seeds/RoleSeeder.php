<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Claimant'
        ]);
        Role::create([
            'name' => 'Payee'
        ]);
        Role::create([
            'name' => 'Assignee'
        ]);
        Role::create([
            'name' => 'Cedula'
        ]);
    }
}
