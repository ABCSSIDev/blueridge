<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents("database/data/users.json");
        $data = json_decode($json);
        foreach ($data as $object){
            User::create([
                "name" => $object->name,
                "email" => $object->email,
                "password" => $object->password,
                "position" => $object->position,
                "address" => $object->address,
                "contact_number" => $object->contact_number
            ]);
        }
    }
}
