<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents("database/data/categories.json");
        $data = json_decode($json);
        foreach ($data as $object){
            Category::create([
                "category_id" => $object->category_id,
                "category_name" => $object->category_name,
                "is_stored" => $object->is_stored
            ]);
        }
    }
}
