<?php

use Illuminate\Database\Seeder;
use App\AccountLedger;
class AccountLedgerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $json = file_get_contents("database/data/account_ledgers.json");
        $data = json_decode($json);
        foreach ($data as $object){
            AccountLedger::create([
                "id" => $object->id,
                "group_id" => $object->group_id,
                "ledger_id" => $object->ledger_id,
                "account_code" => $object->account_code,
                "account_type" => $object->account_type,
                "ledger_name" => $object->ledger_name,
                "reference_group" => $object->reference_group,
                "reference_ledger" => $object->reference_ledger,
                "budget_percentage" => $object->budget_percentage,
                "attachments" => $object->attachments
            ]);
        }
    }
}
