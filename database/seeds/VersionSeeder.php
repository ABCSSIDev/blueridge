<?php

use Illuminate\Database\Seeder;
use App\Version;

class VersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents("database/data/versions.json");
        $data = json_decode($json);
        foreach ($data as $object){
            Version::create([
                "version" => $object->version,
                "version_title" => $object->version_title,
                "release_notes" => $object->release_notes,
                "is_view" => $object->is_view,
                "created_at" => $object->created_at
            ]);
        }
    }
}
