<?php

use Illuminate\Database\Seeder;
use App\AccountGroup;
class AccountGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $json = file_get_contents("database/data/account_groups.json");
        $data = json_decode($json);
        foreach ($data as $object){
            AccountGroup::create([
                "id" => $object->id,
                "group_id" => $object->group_id,
                "group_name" => $object->group_name,
                "description" => $object->description
            ]);
        }
    }
}
