<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            'unit_name' => 'kilograms',
            'unit_symbol' => 'kg'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'pieces',
            'unit_symbol' => 'pc'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'meter',
            'unit_symbol' => 'm'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'inch',
            'unit_symbol' => 'inch'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'grams',
            'unit_symbol' => 'g'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'millimeters',
            'unit_symbol' => 'mm'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'Gallon',
            'unit_symbol' => 'gall'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'Bags',
            'unit_symbol' => 'bags'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'Roll',
            'unit_symbol' => 'roll'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'Pairs',
            'unit_symbol' => 'pairs'
        ]);
        
        DB::table('units')->insert([
            'unit_name' => 'Ream',
            'unit_symbol' => 'rm'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'elf',
            'unit_symbol' => 'elf'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'Can',
            'unit_symbol' => 'Can'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'pack',
            'unit_symbol' => 'pack'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'package',
            'unit_symbol' => 'package'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'tray',
            'unit_symbol' => 'tray'
        ]);
        
        DB::table('units')->insert([
            'unit_name' => 'box',
            'unit_symbol' => 'box'
        ]);

        DB::table('units')->insert([
            'unit_name' => 'capsule',
            'unit_symbol' => 'capsule'
        ]);
    }
}
