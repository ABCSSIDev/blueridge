<?php

use Illuminate\Database\Seeder;
use App\Document;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents("database/data/documents.json");
        $data = json_decode($json);
        foreach ($data as $object){
            Document::create([
                "id" => $object->id,
                "name" => $object->name,
                "signatory_count" => $object->signatory_count,
                "url_content" => $object->url_content
            ]);
        }
    }
}
