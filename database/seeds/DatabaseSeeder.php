<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            UnitsSeeder::class,
            AccountGroupSeeder::class,
            AccountLedgerSeeder::class,
            ModuleSeeder::class,
            DocumentSeeder::class,
            InventoryStatusSeeder::class,
            CategorySeeder::class,
            VersionSeeder::class
        ]);
    }
}
