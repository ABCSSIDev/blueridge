<?php

use Illuminate\Database\Seeder;
use App\PaymentMethod;
class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PaymentMethod::create([
            'name' => 'Bank Transfer'
        ]);
        PaymentMethod::create([
            'name' => 'Cash'
        ]);
        PaymentMethod::create([
            'name' => 'Check'
        ]);
        PaymentMethod::create([
            'name' => 'Credit Card'
        ]);
    }
}
